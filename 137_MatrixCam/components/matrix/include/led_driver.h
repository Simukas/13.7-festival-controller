#include <stdint.h>

typedef enum eLed_Enum_t{
    eLed_First = 0,
    eLed_A = eLed_First,
    eLed_B,
    eGpioDriver_Last
} eLed_Enum_t;

typedef struct sLed_Color_t
{
    uint8_t red;
    uint8_t green;
    uint8_t blue;
} sLed_Color_t;

typedef struct sLed_Pixel_t
{
    sLed_Color_t color;
    uint32_t index;
} sLed_Pixel_t;


