#include <stdio.h>
#include "driver/gpio.h"
#include "esp_log.h"
#include "sdkconfig.h"
#include "led_driver.h"
#include "FastLED.h"
#include "FX.h"

#define A_LED_COUNT 1260
#define A_GPIO 4
#define B_LED_COUNT 1260
#define B_GPIO 13

#define LED_BRIGHTNESS 0
#define LED_TYPE WS2815
#define COLOR_ORDER RGB

#define N_COLORS 17
// static const char *TAG = "LED";
esp_timer_handle_t timer_h;
CRGB a_led[A_LED_COUNT] = {0};
CRGB b_led[B_LED_COUNT] = {0};

extern CRGBPalette16 myRedWhiteBluePalette;
extern const TProgmemPalette16 IRAM_ATTR myRedWhiteBluePalette_p;
CRGBPalette16 currentPalette;
TBlendType currentBlending;
#include "palettes.h"

static const CRGB colors[N_COLORS] = {
    CRGB::Red,
    CRGB::Green,
    CRGB::Blue,
    CRGB::White,
    CRGB::AliceBlue,
    CRGB::ForestGreen,
    CRGB::Lavender,
    CRGB::MistyRose,
    CRGB::DarkOrchid,
    CRGB::DarkOrange,
    CRGB::Black,
    CRGB::Teal,
    CRGB::Violet,
    CRGB::Lime,
    CRGB::Chartreuse,
    CRGB::BlueViolet,
    CRGB::Aqua};

typedef struct sLed_StaticConfiguration_t
{

    uint8_t gpio;
    uint8_t brightness;
    uint32_t led_count;

} sLed_StaticConfiguration_t;

extern "C" void LED_driver_Init();
extern "C" void fastfade();
extern "C" void LED_Set_A(sLed_Pixel_t);
extern "C" void LED_Set_B(sLed_Pixel_t);

void LED_Set_A(sLed_Pixel_t pixel)
{
    a_led[pixel.index] = CRGB(pixel.color.red, pixel.color.green, pixel.color.blue);
}
void LED_Set_B(sLed_Pixel_t pixel)
{
    b_led[pixel.index] = CRGB(pixel.color.red, pixel.color.green, pixel.color.blue);
}

static void _fastfade_cb(void *param)
{
    FastLED.show();
};

void fastfade()
{
    esp_timer_start_once(timer_h, 0);
}

void LED_driver_Init()
{
    printf(" entering app main, call add leds\n");
    // the WS2811 family uses the RMT driver
    FastLED.addLeds<LED_TYPE, A_GPIO>(a_led, A_LED_COUNT);
    FastLED.addLeds<LED_TYPE, B_GPIO>(b_led, B_LED_COUNT);
    esp_timer_create_args_t timer_create_args = {
        .callback = _fastfade_cb,
        .arg = NULL,
        .dispatch_method = ESP_TIMER_TASK,
        .name = "fastfade_timer",
    };

    esp_timer_create(&timer_create_args, &timer_h);

    printf(" set max power\n");
    // I have a 2A power supply, although it's 12v
    FastLED.setMaxPowerInVoltsAndMilliamps(12, 83000);
}