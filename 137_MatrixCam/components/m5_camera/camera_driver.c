#include <esp_log.h>
#include <esp_system.h>
#include <nvs_flash.h>
#include <sys/param.h>
#include <string.h>
#include "esp_camera.h"
#include "camera_driver.h"
#include "bm8563.h"
#include "cam_cmd.h"
#include "nvs_flash.h"

#include "timer_cam_config.h"

#define portTICK_RATE_MS portTICK_PERIOD_MS

#define CAM_PIN_RESET 15 // software reset will be performed
#define CAM_PIN_PWDN -1
#define CAM_PIN_XCLK 27
#define CAM_PIN_VSYNC 22
#define CAM_PIN_PCLK 21
#define CAM_PIN_HREF 26

#define CAM_PIN_SIOC 23
#define CAM_PIN_SIOD 25

#define CAM_PIN_D7 19
#define CAM_PIN_D6 36
#define CAM_PIN_D5 18
#define CAM_PIN_D4 39
#define CAM_PIN_D3 5
#define CAM_PIN_D2 34
#define CAM_PIN_D1 35
#define CAM_PIN_D0 32

#define CAM_XCLK_FREQ 20000000

#define CAMERA_LED_GPIO 2

#define Ext_PIN_1 4
#define Ext_PIN_2 13

static const char *TAG = "take_picture";

static camera_config_t camera_config = {
    .pin_pwdn = CAM_PIN_PWDN,
    .pin_reset = CAM_PIN_RESET,
    .pin_xclk = CAM_PIN_XCLK,
    .pin_sscb_sda = CAM_PIN_SIOD,
    .pin_sscb_scl = CAM_PIN_SIOC,

    .pin_d7 = CAM_PIN_D7,
    .pin_d6 = CAM_PIN_D6,
    .pin_d5 = CAM_PIN_D5,
    .pin_d4 = CAM_PIN_D4,
    .pin_d3 = CAM_PIN_D3,
    .pin_d2 = CAM_PIN_D2,
    .pin_d1 = CAM_PIN_D1,
    .pin_d0 = CAM_PIN_D0,
    .pin_vsync = CAM_PIN_VSYNC,
    .pin_href = CAM_PIN_HREF,
    .pin_pclk = CAM_PIN_PCLK,

    // XCLK 20MHz or 10MHz for OV2640 double FPS (Experimental)
    .xclk_freq_hz = 20000000,
    .ledc_timer = LEDC_TIMER_0,
    .ledc_channel = LEDC_CHANNEL_0,

    .pixel_format = PIXFORMAT_GRAYSCALE, // YUV422,GRAYSCALE,RGB565,JPEG
    
    .frame_size = FRAMESIZE_96X96,       // QQVGA-UXGA Do not use sizes above QVGA when not JPEG

    .jpeg_quality = 15, // 0-63 lower number means higher quality
    .fb_count = 5,      // if more than one, i2s runs in continuous mode. Use only with JPEG
    // .grab_mode = CAMERA_GRAB_WHEN_EMPTY,
};

esp_err_t Camera_Init()
{
    // bm8563_init();

    // Initialize NVS
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND)
    {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }

    // initialize the camera
    esp_err_t err = esp_camera_init(&camera_config);
    if (err != ESP_OK)
    {
        ESP_LOGE(TAG, "Camera Init Failed");
        return err;
    }
        // sensor_t *s = esp_camera_sensor_get();
    // s->set_vflip(s, 1);
    // s->set_hmirror(s, 1);
    InitCamFun();
    return ESP_OK;
}