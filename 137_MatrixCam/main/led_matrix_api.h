// #include "camera_api.h"
#include "led_driver.h"



typedef struct sLed_Matrix_API_Frame_t
{
    uint8_t row_count;
    uint8_t column_count;
    sLed_Color_t matrix[60][41];
} sLed_Matrix_API_Frame_t;


void Led_Matrix_API_init();

void Led_Matrix_API_Set_Frame(sLed_Matrix_API_Frame_t *frame);
