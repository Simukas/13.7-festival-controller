#include <esp_log.h>
#include <esp_system.h>
#include <sys/param.h>
#include "esp_camera.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "camera_api.h"
#include "camera_driver.h"
#define MATRIX_ROW_COUNT 60
#define MATRIX_COLUMN_COUNT 41

sCamera_API_Frame_t *frame = NULL;
uint32_t *pixel_average_temp = NULL;

#define RESOLUTION 96
#define CROP_LEFT 28
#define CROP_RIGHT 27
#define CROP_TOP_BOT 18

static const char *TAG = "Camera_API";
camera_fb_t *pic = NULL;

void Camera_api_Init(void)
{
    Camera_Init();
    frame = pvPortMalloc(1 * sizeof(sCamera_API_Frame_t));
    pixel_average_temp = pvPortMalloc(1 * sizeof(uint32_t));
    frame->column_count = MATRIX_COLUMN_COUNT;
    frame->row_count = MATRIX_ROW_COUNT;
}

void Camera_API_Crop_frame()
{
    uint32_t index = CROP_TOP_BOT * RESOLUTION - 1;
    uint32_t pixel_max = 0;
    uint32_t pixel_min = 255;
    for (uint8_t i = 0; i < frame->row_count; i++)
    {
        index += CROP_LEFT;
        for (uint8_t j = 0; j < frame->column_count; j++)
        {

            index++;

            frame->matrix[i][j] = pic->buf[index];

            if (pixel_max < pic->buf[index])
            {
                pixel_max = pic->buf[index];
            }
            if (pixel_min > pic->buf[index])
            {
                pixel_min = pic->buf[index];
            }
        }
        index += CROP_RIGHT;
    }
    frame->pixel_max = pixel_max;
    frame->pixel_min = pixel_min;
}
void Camera_API_Return_Buffer()
{
    esp_camera_fb_return(pic);
}
sCamera_API_Frame_t *Camera_API_Get_frame()
{
    pic = esp_camera_fb_get();
    Camera_API_Crop_frame();
    Camera_API_Return_Buffer();

    return frame;
}