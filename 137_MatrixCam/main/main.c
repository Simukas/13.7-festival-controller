#include <esp_log.h>
#include <esp_system.h>
#include <sys/param.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_camera.h"
#include "frame_api.h"
#include "stream.h"

#define THRESHOLD_PROCENTAGE 40

sCamera_API_Frame_t *matrix_from_camera = NULL;
sLed_Matrix_API_Frame_t *matrix_to_leds = NULL;

static const char *TAGG = "main program";

static void Camera_Matrix()
{

    static uint64_t last_frame = 0;
    if (!last_frame)
    {
        last_frame = esp_timer_get_time();
    }
    while (true)
    {
        // Get cropped camera frame
        matrix_from_camera = Camera_API_Get_frame();
        // Add animation to matrix

        // Add camera frame to matrix frame
        matrix_to_leds = Frame_api_Transform_Threshold_Matrix(matrix_from_camera, THRESHOLD_PROCENTAGE);
        // Send matrix frame to leds
        Led_Matrix_API_Set_Frame(matrix_to_leds);
        uint64_t fr_end = esp_timer_get_time();
        uint64_t frame_time = fr_end - last_frame;
        last_frame = fr_end;
        frame_time /= 1000;
        ESP_LOGE(TAGG, "(%.1ffps)", 1000.0 / (uint32_t)frame_time);
    }
    last_frame = 0;
}

void app_main()
{

    Led_Matrix_API_init();
    Frame_api_Init();
    Camera_api_Init();
    Camera_Matrix();
}
