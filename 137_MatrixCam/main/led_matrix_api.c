
#include <stdbool.h>
#include <esp_system.h>
#include <sys/param.h>
#include "led_matrix_api.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#define FIRST_BLOCK_COLUMN_COUNT 20

// sLed_Matrix_API_Frame_t *matrix;

void LED_driver_Init();
void fastfade();
void LED_Set_A();
void LED_Set_B();
// sLed_Pixel_t *pixel = NULL;

void Led_Matrix_API_init()
{
    // pixel = pvPortMalloc(1 * sizeof(sLed_Pixel_t));
    LED_driver_Init();
    // matrix = pvPortMalloc(1 * sizeof(sLed_Matrix_API_Frame_t));
}

void Led_Matrix_API_Set_Frame(sLed_Matrix_API_Frame_t *frame)
{

    uint8_t matrix_row = frame->row_count - 1;
    for (uint8_t i = 0; i < frame->row_count; i++)
    {

        for (uint8_t j = 1; j < frame->column_count; j += 2)
        {
            if (j < 20)
            {
                sLed_Pixel_t pixel = {
                    .color = frame->matrix[i][j],
                    .index = j * frame->row_count + matrix_row,
                };

                LED_Set_A(pixel);
            }
            else
            {
                sLed_Pixel_t pixel = {
                    .color = frame->matrix[i][j],
                    .index = (j - FIRST_BLOCK_COLUMN_COUNT) * frame->row_count + matrix_row,
                };
                LED_Set_B(pixel);
            }
        }
        matrix_row--;

        for (uint8_t j = 0; j < frame->column_count; j += 2)
        {
            if (j < 20)
            {
                sLed_Pixel_t pixel = {
                    .color = frame->matrix[i][j],
                    .index = j * frame->row_count + i,
                };
                LED_Set_A(pixel);
            }
            else
            {
                sLed_Pixel_t pixel = {
                    .color = frame->matrix[i][j],
                    .index = (j - FIRST_BLOCK_COLUMN_COUNT) * frame->row_count + i,
                };
                LED_Set_B(pixel);
            }
        }
    }
    fastfade();
}
