#include "stdint.h"

typedef struct sCamera_API_Frame_t
{
    uint8_t row_count;
    uint8_t column_count;
    uint8_t pixel_max;
    uint8_t pixel_min;
    uint8_t matrix[60][41];

} sCamera_API_Frame_t;

void Camera_api_Init(void);
void Camera_API_Return_Buffer();
sCamera_API_Frame_t *Camera_API_Get_frame();
void Camera_API_Crop_frame();
