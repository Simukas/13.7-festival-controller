#include "led_matrix_api.h"


#include "camera_api.h"

void Frame_api_Init(void);
sLed_Matrix_API_Frame_t *Frame_api_Transform_Threshold_Matrix(sCamera_API_Frame_t *frame_from, uint8_t treshold_procentage);