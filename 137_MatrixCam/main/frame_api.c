#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "frame_api.h"
#include "stdint.h"

sLed_Matrix_API_Frame_t *frame_api_matrix = NULL;

void Frame_api_Init(void)
{
    frame_api_matrix = pvPortMalloc(1 * sizeof(sLed_Matrix_API_Frame_t));
    frame_api_matrix->column_count = 41;
    frame_api_matrix->row_count = 60;
}

sLed_Matrix_API_Frame_t *Frame_api_Transform_Threshold_Matrix(sCamera_API_Frame_t *frame_from, uint8_t treshold_procentage)
{

    uint8_t treshold = 255 * treshold_procentage / 100;
    for (uint8_t i = 0; i < frame_api_matrix->row_count; i++)
    {
        for (uint8_t j = 0; j < frame_api_matrix->column_count; j++)
        {
            if (frame_from->matrix[i][j] > frame_from->pixel_max - treshold)
            {
                sLed_Color_t color = {
                    .red = 0,
                    .green = 0,
                    .blue = frame_from->matrix[i][j],
                };
                frame_api_matrix->matrix[i][j] = color;
            } else{
                sLed_Color_t color = {
                    .red = 100,
                    .green = 100,
                    .blue = 100,
                };
                frame_api_matrix->matrix[i][j] = color;
            }
        }
    }
    return frame_api_matrix;
}

void Frame_api_Background_Animation(sCamera_API_Frame_t *frame_from)
{

    for (uint8_t i = 0; i < frame_api_matrix->row_count; i++)
    {
        for (uint8_t j = 0; j < frame_api_matrix->column_count; j++)
        {
        }
    }
}