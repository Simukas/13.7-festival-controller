#ifndef __VL53l3_API__H__
#define __VL53l3_API__H__

/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include <stdbool.h>
#include <stdint.h>
/**********************************************************************************************************************
 * Exported definitions and macros
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported types
 *********************************************************************************************************************/
typedef enum eVL53L3Api_Enum_t {
    eVL53L3Api_First = 0,
    eVL53L3Api_Primary = eVL53L3Api_First,
    eVL53L3Api_Secondary,
    eVL53L3Api_Last
} eVL53L3Api_Enum_t;
/**********************************************************************************************************************
 * Exported variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of exported functions
 *********************************************************************************************************************/
bool VL53L3_API_MeasureDistance (eVL53L3Api_Enum_t sensor);
bool VL53L3_API_GetDistance     (eVL53L3Api_Enum_t sensor, uint16_t *distance);
bool VL53L3_API_Calibrate       (eVL53L3Api_Enum_t sensor);
bool VL53L3_API_Enable          (eVL53L3Api_Enum_t sensor);
bool VL53L3_API_Disable         (eVL53L3Api_Enum_t sensor);
#endif /* __VL53l3CX_API__H__ */
