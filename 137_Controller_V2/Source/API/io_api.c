/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "io_api.h"
#include "gpio_driver.h"
#include "debug_api.h"
#include "stm32f4xx_ll_gpio.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
DEBUG_MODULE(IO_API)
/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/
typedef struct sIoApi_Dynamic_t {
    bool enabled;
    eIoApi_Function_Enum_t function;
} sIoApi_Dynamic_t;

typedef struct sIoApi_Static_t {
    eGpioDriver_Pin_Enum_t pin;
} sIoApi_Static_t;
/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
sIoApi_Static_t g_io_api_static_lut[eIoApi_Port_Last] = {
    [eIoApi_Port_1] = {
        .pin = eGpioDriver_Pin_GPIO_1
    },
    [eIoApi_Port_2] = {
        .pin = eGpioDriver_Pin_GPIO_2
    },
    [eIoApi_Port_3] = {
        .pin = eGpioDriver_Pin_GPIO_3
    },
    [eIoApi_Port_4] = {
        .pin = eGpioDriver_Pin_GPIO_4
    },
    [eIoApi_Port_5] = {
        .pin = eGpioDriver_Pin_GPIO_5
    },
    [eIoApi_Port_6] = {
        .pin = eGpioDriver_Pin_GPIO_6
    },
};
/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/
sIoApi_Dynamic_t g_io_api_dynamic_lut[eIoApi_Port_Last] = {
    [eIoApi_Port_1] = {
        .enabled = false,
        .function = eIoApi_Function_Input,
    },
    [eIoApi_Port_2] = {
        .enabled = false,
        .function = eIoApi_Function_Input,
    },
    [eIoApi_Port_3] = {
        .enabled = false,
        .function = eIoApi_Function_Input,
    },
    [eIoApi_Port_4] = {
        .enabled = false,
        .function = eIoApi_Function_Input,
    },
    [eIoApi_Port_5] = {
        .enabled = false,
        .function = eIoApi_Function_Input,
    },
    [eIoApi_Port_6] = {
        .enabled = false,
        .function = eIoApi_Function_Input,
    },
};
/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/
bool IO_API_Init (eIoApi_Port_Enum_t port, eIoApi_Function_Enum_t function);
/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/
bool IO_API_Init (eIoApi_Port_Enum_t port, eIoApi_Function_Enum_t function) {
    if (port >= eIoApi_Port_Last) {
        error("Wrong port [%u]!\r", port);
        return false;
    }
    sGpioDriver_Static_t gpio_info = {0};
    if (GPIO_Driver_GetPinInfo(g_io_api_static_lut[port].pin, &gpio_info) == false) {
        error("Failed to get pin info [%u]!\r", g_io_api_static_lut[port].pin);
        return false;
    }
    switch (function) {
        case eIoApi_Function_Input: {
            gpio_info.mode = LL_GPIO_MODE_INPUT;
            break;
        }
        case eIoApi_Function_Output: {
            gpio_info.mode = LL_GPIO_MODE_OUTPUT;
            break;
        }
        default: {
            break;
        }
    }
    if (GPIO_Driver_InitPin(g_io_api_static_lut[port].pin, &gpio_info) == false) {
        error("Failed to update pin info [%u]!\r", g_io_api_static_lut[port].pin);
        return false;
    }

    return true;
}
/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool IO_API_Enable (eIoApi_Port_Enum_t port, eIoApi_Function_Enum_t function) {
    if (port >= eIoApi_Port_Last) {
        error("Wrong port [%u]!\r", port);
        return false;
    }
    if (function >= eIoApi_Function_Last) {
        error("Wrong function [%u]!\r", function);
        return false;
    }
    if (g_io_api_dynamic_lut[port].enabled == true) {
        return true;
    }
    if (IO_API_Init(port, function) == false) {
        error("Failed IO API Init [%u]!\r", port);
        return false;
    }
    g_io_api_dynamic_lut[port].function = function;
    g_io_api_dynamic_lut[port].enabled = true;
    return true;
}

bool IO_API_Disable (eIoApi_Port_Enum_t port) {
    if (port >= eIoApi_Port_Last) {
        error("Wrong port [%u]!\r", port);
        return false;
    }
    if (g_io_api_dynamic_lut[port].enabled == false) {
        return true;
    }
    g_io_api_dynamic_lut[port].enabled = false;
    return true;
}

bool IO_API_SetPort (eIoApi_Port_Enum_t port, bool state) {
    if (port >= eIoApi_Port_Last) {
        error("Wrong port [%u]!\r", port);
        return false;
    }
    if (g_io_api_dynamic_lut[port].enabled == false) {
        error("Disabled port [%u]!\r", port);
        return false;
    }
    if (g_io_api_dynamic_lut[port].function == eIoApi_Function_Input) {
        error("Can't set input port [%u]!\r", port);
        return false;
    }
    if (state == true) {
        if (GPIO_Driver_SetPin(g_io_api_static_lut[port].pin) == false) {
            error("Failed to set pin [%u]!\r", g_io_api_static_lut[port].pin);
            return false;
        }
    } else {
        if (GPIO_Driver_ResetPin(g_io_api_static_lut[port].pin) == false) {
            error("Failed to reset pin [%u]!\r", g_io_api_static_lut[port].pin);
            return false;
        }
    }
    return true;
}

bool IO_API_ReadPort (eIoApi_Port_Enum_t port, bool *state) {
    if (port >= eIoApi_Port_Last) {
        error("Wrong port [%u]!\r", port);
        return false;
    }
    if (g_io_api_dynamic_lut[port].enabled == false) {
        error("Disabled port [%u]!\r", port);
        return false;
    }
    if (state == NULL) {
        error("NULL pointer!\r", port);
        return false;
    }
    if (g_io_api_dynamic_lut[port].function == eIoApi_Function_Output) {
        error("Can't read output port [%u]!\r", port);
        return false;
    }
    if (GPIO_Driver_ReadInputPin(g_io_api_static_lut[port].pin, state) == false) {
        error("Failed to read pin [%u]!\r", g_io_api_static_lut[port].pin);
        return false;
    }
    return true;
}
