/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "ws2815_api.h"
#include "stdint.h"
#include "stdbool.h"
#include "dma_driver.h"
#include "timer_driver.h"
#include "gpio_driver.h"
#include "color_utils.h"
#include "ws2815_lut.h"
#include "cmsis_os2.h"
#include "rtc_driver.h"
#include "heap_api.h"
#include "ws2815_lut.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
DEBUG_MODULE(WS2815_API)

#define WS2815_API_DATA_PER_PIXEL 24
#define WS2815_API_DATA_ELEMENTS 10
#define WS2815_API_DATA_ELEMENT_SIZE 2
#define WS2815_API_PULSE_DATA_BUFFER_SIZE (WS2815_API_DATA_PER_PIXEL * WS2815_API_DATA_ELEMENTS)

#define WS2815_API_LEDS_PER_INTERRUPT (WS2815_API_DATA_ELEMENTS / 2)

#define WS2815_API_HALF_PULSE_DATA_BUFFER_SIZE (WS2815_API_DATA_PER_PIXEL * WS2815_API_LEDS_PER_INTERRUPT)

#define WS2815_API_RESET_CYCLES 230 //224
#define WS2815_API_HIGH_PULSE_VALUE 48
#define WS2815_API_LOW_PULSE_VALUE 15

#define WS2815_API_DATA_OFFSET (WS2815_API_DATA_PER_PIXEL * WS2815_API_LEDS_PER_INTERRUPT)
#define WS2815_API_LED_OFFSET WS2815_API_LEDS_PER_INTERRUPT
#define WS2815_API_PULSE_DATA_OFFSET (WS2815_API_DATA_PER_PIXEL - 1)
#define WS2815_API_0_OFFSET 0

#define WS2815_API_TIMER eTimerDriver_Timer_1
/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/
typedef enum eWs2815Api_StripState_Enum_t {
    eWs2815Api_StripState_First = 0,
        eWs2815Api_StripState_Off = eWs2815Api_StripState_First,
        eWs2815Api_StripState_Color,
        eWs2815Api_StripState_Blank,
        eWs2815Api_StripState_Last,
} eWs2815Api_StripState_Enum_t;

typedef enum eWs2815Api_State_Enum_t {
    eWs2815Api_State_First = 0,
        eWs2815Api_State_Idle = eWs2815Api_State_First,
        eWs2815Api_State_Update,
        eWs2815Api_State_Stop,
        eWs2815Api_State_Last,
} eWs2815Api_State_Enum_t;

typedef struct sWs2815Api_Static_t {
    eTimerDriver_OC_Enum_t timer_channel;
    eGpioDriver_Pin_Enum_t pin;
    eDmaDriver_Stream_Enum_t dma_stream;
} sWs2815Api_Static_t;

typedef struct sWs2815Api_Dynamic_t {
    uint16_t pixel_count;
    uint16_t pulse_data[WS2815_API_PULSE_DATA_BUFFER_SIZE];
    sRgbColor_t *color_data;
    bool enabled;
    eWs2815Api_StripState_Enum_t state;
    bool updated;
} sWs2815Api_Dynamic_t;
/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/
static void WS2815_API_LoadPulseData (eWs2815Api_Port_Enum_t port, bool offset_data);
static void WS2815_API_DMA_IT_Callback (eDmaDriver_Stream_Enum_t stream, eDmaDriver_IT_Enum_t IT);
static bool WS2815_API_Init (sWs2815Api_Init_t *init_struct);
/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
static const sWs2815Api_Static_t g_static_ws2815_api_lut[eWs2815Api_Port_Last] = {
    [eWs2815Api_Port_1] = {.timer_channel = eTimerDriver_OC_1, .pin = eGpioDriver_Pin_ARGB_1, .dma_stream = eDmaDriver_Stream_ARGB1},
    [eWs2815Api_Port_2] = {.timer_channel = eTimerDriver_OC_2, .pin = eGpioDriver_Pin_ARGB_2, .dma_stream = eDmaDriver_Stream_ARGB2},
    [eWs2815Api_Port_3] = {.timer_channel = eTimerDriver_OC_3, .pin = eGpioDriver_Pin_ARGB_3, .dma_stream = eDmaDriver_Stream_ARGB3},
};
/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/
//typedef struct eWs2815Api_Animation_t {
//    void (*animation_function) (void*);
//} eWs2815Api_Animation_t;
//
//const eWs2815Api_Animation_t g_animation_function_lut[eArgbAnimation_Last] = {
//
//};
//
//static eWs2815Api_Animation_Data_t *g_ws2815_api_animation_queue[ANIMATION_QUEUE_SIZE] = {NULL};
static sWs2815Api_Dynamic_t g_dynamic_ws2815_api_lut[] = {
    [eWs2815Api_Port_1] = {.pixel_count = 0, .state = eWs2815Api_StripState_Off},
    [eWs2815Api_Port_2] = {.pixel_count = 0, .state = eWs2815Api_StripState_Off},
    [eWs2815Api_Port_3] = {.pixel_count = 0, .state = eWs2815Api_StripState_Off},
};

static uint16_t g_ws2815_api_leds_loaded = 0;
static uint16_t g_ws2815_api_most_pixels = 0;

static eWs2815Api_State_Enum_t g_ws2815_api_state = eWs2815Api_State_Idle;
/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/
static void WS2815_API_LoadPulseData (eWs2815Api_Port_Enum_t port, bool offset_data) {
    uint16_t half_buffer_offset = (offset_data == false) ? WS2815_API_0_OFFSET : WS2815_API_DATA_OFFSET;
    sRgbColor_t color = {0};
    uint16_t led_offset = 0;
    uint16_t pixel_offset = (offset_data == false) ? (WS2815_API_0_OFFSET) : (WS2815_API_LEDS_PER_INTERRUPT);
    for (uint8_t led = 0; led < WS2815_API_LEDS_PER_INTERRUPT; led++) {
        color = g_dynamic_ws2815_api_lut[port].color_data[pixel_offset + led];
        led_offset = led * WS2815_API_DATA_PER_PIXEL;
        memcpy(g_dynamic_ws2815_api_lut[port].pulse_data + 0 + half_buffer_offset + led_offset, &g_static_ws2815_value_lut[color.r * 8], 16);
        memcpy(g_dynamic_ws2815_api_lut[port].pulse_data + 8 + half_buffer_offset + led_offset, &g_static_ws2815_value_lut[color.g * 8], 16);
        memcpy(g_dynamic_ws2815_api_lut[port].pulse_data + 16 + half_buffer_offset + led_offset, &g_static_ws2815_value_lut[color.b * 8], 16);
    }
}

static void WS2815_API_DMA_IT_Callback (eDmaDriver_Stream_Enum_t stream, eDmaDriver_IT_Enum_t IT) {
    switch (g_ws2815_api_state) {
        case eWs2815Api_State_Update: {
            for (eWs2815Api_Port_Enum_t port = eWs2815Api_Port_First; port < eWs2815Api_Port_Last; port++) {
                if (g_dynamic_ws2815_api_lut[port].enabled == false) {
                    continue;
                }
                switch (g_dynamic_ws2815_api_lut[port].state) {
                    case eWs2815Api_StripState_Color: {
                        uint16_t half_buffer_offset = (IT == eDmaDriver_IT_TC ? WS2815_API_DATA_OFFSET : WS2815_API_0_OFFSET);
                        sRgbColor_t color = {0};
                        uint16_t led_offset = 0;
                        for (uint8_t led = 0; led < WS2815_API_LEDS_PER_INTERRUPT; led++) {
                            led_offset = led * WS2815_API_DATA_PER_PIXEL;
                            //if (g_ws2815_api_leds_loaded + led < g_dynamic_ws2815_api_lut[port].pixel_count) {
                            color = g_dynamic_ws2815_api_lut[port].color_data[g_ws2815_api_leds_loaded + led];
                            memcpy(g_dynamic_ws2815_api_lut[port].pulse_data + 0 + half_buffer_offset + led_offset, &g_static_ws2815_value_lut[color.r * 8], 16);
                            memcpy(g_dynamic_ws2815_api_lut[port].pulse_data + 8 + half_buffer_offset + led_offset, &g_static_ws2815_value_lut[color.g * 8], 16);
                            memcpy(g_dynamic_ws2815_api_lut[port].pulse_data + 16 + half_buffer_offset + led_offset, &g_static_ws2815_value_lut[color.b * 8], 16);
//                            } else {
//                                memset(g_dynamic_ws2815_api_lut[port].pulse_data + half_buffer_offset + led_offset, 0, 48);
//                            }
                        }
                        if (g_ws2815_api_leds_loaded + WS2815_API_LEDS_PER_INTERRUPT >= g_dynamic_ws2815_api_lut[port].pixel_count) {
                            g_dynamic_ws2815_api_lut[port].state = eWs2815Api_StripState_Blank;
                        }
                        break;
                    }
                    case eWs2815Api_StripState_Blank: {
                        uint16_t offset = ((IT == eDmaDriver_IT_TC) ? WS2815_API_HALF_PULSE_DATA_BUFFER_SIZE : WS2815_API_0_OFFSET);
                        memset(g_dynamic_ws2815_api_lut[port].pulse_data + offset, 0, WS2815_API_PULSE_DATA_BUFFER_SIZE);
                        break;
                    }
                    default: {
                        continue;
                    }
                }

            }
            g_ws2815_api_leds_loaded += WS2815_API_LEDS_PER_INTERRUPT;
            if (g_ws2815_api_leds_loaded >= g_ws2815_api_most_pixels + WS2815_API_LEDS_PER_INTERRUPT) {
//                switch (IT) {
//                    case eDmaDriver_IT_HT: {
//                        DMA_Driver_DisableEventInterrupt(g_static_ws2815_api_lut[eWs2815Api_Port_1].dma_stream, eDmaDriver_IT_HT);
//                        break;
//                    }
//                    case eDmaDriver_IT_TC: {
//                        DMA_Driver_DisableEventInterrupt(g_static_ws2815_api_lut[eWs2815Api_Port_1].dma_stream, eDmaDriver_IT_TC);
//                        break;
//                    }
//                    default: {
//                        break;
//                    }
//                }
                g_ws2815_api_state = eWs2815Api_State_Stop;
            }
            return;
        }
        case eWs2815Api_State_Stop: {
            Timer_Driver_Stop(WS2815_API_TIMER);
            //Timer_Driver_SetCounter(WS2815_API_TIMER, 0);
            //DMA_Driver_DisableStreamInterrupt(eDmaDriver_Stream_ARGB1);
//            switch (IT) {
//                case eDmaDriver_IT_HT: {
//                    DMA_Driver_DisableEventInterrupt(g_static_ws2815_api_lut[eWs2815Api_Port_1].dma_stream, eDmaDriver_IT_HT);
//                    break;
//                }
//                case eDmaDriver_IT_TC: {
//                    DMA_Driver_DisableEventInterrupt(g_static_ws2815_api_lut[eWs2815Api_Port_1].dma_stream, eDmaDriver_IT_TC);
//                    break;
//                }
//                default: {
//                    break;
//                }
//            }

            for (eWs2815Api_Port_Enum_t port = eWs2815Api_Port_First; port < eWs2815Api_Port_Last; port++) {
                if (g_dynamic_ws2815_api_lut[port].enabled == false) {
                    continue;
                }

                //Timer_Driver_SetDutyCycle(WS2815_API_TIMER, g_static_ws2815_api_lut[port].timer_channel, 0);
                //Timer_Driver_DisableChannel(WS2815_API_TIMER, g_static_ws2815_api_lut[port].timer_channel);
                DMA_Driver_DisableStream(g_static_ws2815_api_lut[port].dma_stream);
                g_dynamic_ws2815_api_lut[port].updated = true;
                g_dynamic_ws2815_api_lut[port].state = eWs2815Api_StripState_Color;
            }
            g_ws2815_api_state = eWs2815Api_State_Idle;
//            osEventFlagsSet(g_ws2815_api_event_flag, g_ws2815_api_event_flag_mask);
            return;
        }
        default: {
//            for (eWs2815Api_Port_Enum_t port = eWs2815Api_Port_First; port < eWs2815Api_Port_Last; port++) {
//                GPIO_Driver_ResetPin(g_static_ws2815_api_lut[port].pin);
//            }
            return;
        }
    }

}

bool WS2815_API_Init (sWs2815Api_Init_t *init_struct) {
    if (init_struct->port >= eWs2815Api_Port_Last) {
        error("Wrong port provided!\r");
        return false;
    }
    if (init_struct->pixel_count == 0) {
        error("Wrong number of pixels!\r");
        return false;
    }
    if (init_struct->color_data_buffer == NULL) {
        error("NULL pointer provided!\r");
        return false;
    }

    g_dynamic_ws2815_api_lut[init_struct->port].color_data = init_struct->color_data_buffer;

    if (Timer_Driver_Init(WS2815_API_TIMER) == false) {
        return false;
    }

    sTimerDriver_Static_t timer_info = {0};
    if (Timer_Driver_GetTimerInfo(WS2815_API_TIMER, &timer_info) == false) {
        return false;
    }

    void *ccr_address = NULL;
    switch (g_static_ws2815_api_lut[init_struct->port].timer_channel) {
        case eTimerDriver_OC_1:
            ccr_address = (void*) &timer_info.timer->CCR1;
            break;
        case eTimerDriver_OC_2:
            ccr_address = (void*) &timer_info.timer->CCR2;
            break;
        case eTimerDriver_OC_3:
            ccr_address = (void*) &timer_info.timer->CCR3;
            break;
        case eTimerDriver_OC_4:
            ccr_address = (void*) &timer_info.timer->CCR4;
            break;
        default:
            error("Wrong value!\r");
            break;
    }

    DMA_Driver_Init_t dma_init_struct = {
        .IT_callback = &WS2815_API_DMA_IT_Callback,
        .data_amount = WS2815_API_PULSE_DATA_BUFFER_SIZE,
        .destination_address = g_dynamic_ws2815_api_lut[init_struct->port].pulse_data,
        .peripheral_or_source_address = ccr_address,
        .stream = g_static_ws2815_api_lut[init_struct->port].dma_stream,
    };

    if (DMA_Driver_Init(&dma_init_struct) == false) {
        error("Failed to initialize DMA!\r");
        return false;
    }

    g_dynamic_ws2815_api_lut[init_struct->port].pixel_count = init_struct->pixel_count;

    if (g_ws2815_api_most_pixels < init_struct->pixel_count) {
        g_ws2815_api_most_pixels = init_struct->pixel_count;
    }

    g_dynamic_ws2815_api_lut[init_struct->port].state = eWs2815Api_StripState_Color;

    return true;
}
/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool WS2815_API_UpdatePixels (void) {
    if (g_ws2815_api_state != eWs2815Api_State_Idle) {
        //debug("LED update is still running!\r");
        return false;
    }
    bool one_port_enabled_and_not_updated = false;
    for (eWs2815Api_Port_Enum_t port = eWs2815Api_Port_First; port < eWs2815Api_Port_Last; port++) {
        if (g_dynamic_ws2815_api_lut[port].enabled == true && g_dynamic_ws2815_api_lut[port].updated == false) {
            one_port_enabled_and_not_updated = true;
            break;
        }
    }
    if (one_port_enabled_and_not_updated == false) {
        return true;
    }

    g_ws2815_api_state = eWs2815Api_State_Update;
    g_ws2815_api_leds_loaded = 0;
    for (eWs2815Api_Port_Enum_t port = eWs2815Api_Port_First; port < eWs2815Api_Port_Last; port++) {

        if (g_dynamic_ws2815_api_lut[port].enabled == false) {
            continue;
        }
        if (g_dynamic_ws2815_api_lut[port].updated == true) {
            continue;
        }
        WS2815_API_LoadPulseData(port, false);
        WS2815_API_LoadPulseData(port, true);
        DMA_Driver_ClearFlag(g_static_ws2815_api_lut[port].dma_stream, eDmaDriver_IT_HT);
        DMA_Driver_ClearFlag(g_static_ws2815_api_lut[port].dma_stream, eDmaDriver_IT_TC);
        DMA_Driver_ClearFlag(g_static_ws2815_api_lut[port].dma_stream, eDmaDriver_IT_TE);
        DMA_Driver_EnableStream(g_static_ws2815_api_lut[port].dma_stream);
        Timer_Driver_SetDutyCycle(WS2815_API_TIMER, g_static_ws2815_api_lut[port].timer_channel, 0);
        //Timer_Driver_EnableChannel(WS2815_API_TIMER, g_static_ws2815_api_lut[port].timer_channel);
    }

//    DMA_Driver_EnableEventInterrupt(g_static_ws2815_api_lut[eWs2815Api_Port_1].dma_stream, eDmaDriver_IT_TC);
//    DMA_Driver_EnableEventInterrupt(g_static_ws2815_api_lut[eWs2815Api_Port_1].dma_stream, eDmaDriver_IT_HT);

    g_ws2815_api_leds_loaded = WS2815_API_DATA_ELEMENTS;
    //DMA_Driver_EnableStreamInterrupt(eDmaDriver_Stream_ARGB1);
    Timer_Driver_Start(WS2815_API_TIMER);
    return true;
}

bool WS2815_API_Enable (eWs2815Api_Port_Enum_t port, uint16_t pixel_count) {
    if (port >= eWs2815Api_Port_Last) {
        error("Wrong port [%u]!\r", port);
        return false;
    }
    if (pixel_count == 0) {
        error("Wrong pixel count [%u]!\r", pixel_count);
        return false;
    }
    if (g_dynamic_ws2815_api_lut[port].enabled == true) {
        return true;
    }
    sRgbColor_t *color_data_buffer = Heap_API_Calloc(pixel_count, sizeof(sRgbColor_t));
    if (color_data_buffer == NULL) {
        return false;
    }
    sWs2815Api_Init_t init_struct = {
        .color_data_buffer = color_data_buffer,
        .pixel_count = pixel_count,
        .port = port,
    };
    if (WS2815_API_Init(&init_struct) == false) {
        return false;
    }
    g_dynamic_ws2815_api_lut[port].enabled = true;
    g_dynamic_ws2815_api_lut[port].updated = false;
    return true;
}

bool WS2815_API_Disable (eWs2815Api_Port_Enum_t port) {
    if (port >= eWs2815Api_Port_Last) {
        error("Wrong port [%u]!\r", port);
        return false;
    }
    if (g_dynamic_ws2815_api_lut[port].enabled == false) {
        return true;
    }

    Heap_API_Free(g_dynamic_ws2815_api_lut[port].color_data, sizeof(sRgbColor_t) * g_dynamic_ws2815_api_lut[port].pixel_count);

    g_dynamic_ws2815_api_lut[port].enabled = false;
    g_dynamic_ws2815_api_lut[port].updated = false;
    return true;
}

bool WS2815_API_SetPixelRgb (eWs2815Api_Port_Enum_t port, uint16_t pixel_index, sRgbColor_t rgb_color) {
    if (port >= eWs2815Api_Port_Last) {
        error("Wrong port [%u]!\r", port);
        return false;
    }
    if (pixel_index >= g_dynamic_ws2815_api_lut[port].pixel_count) {
        error("Wrong index! %d\r", pixel_index);
        return false;
    }
    g_dynamic_ws2815_api_lut[port].color_data[pixel_index] = rgb_color;
    g_dynamic_ws2815_api_lut[port].updated = false;
    return true;
}

bool WS2815_API_FillPixelsRgb (eWs2815Api_Port_Enum_t port, sRgbColor_t rgb_color) {
    if (port >= eWs2815Api_Port_Last) {
        error("Wrong port [%u]!\r", port);
        return false;
    }
    for (uint16_t pixel_index = 0; pixel_index < g_dynamic_ws2815_api_lut[port].pixel_count; pixel_index++) {
        g_dynamic_ws2815_api_lut[port].color_data[pixel_index] = rgb_color;
    }
    g_dynamic_ws2815_api_lut[port].updated = false;
    return true;
}

bool WS2815_API_FillSegmentRgb (eWs2815Api_Port_Enum_t port, uint16_t start_index, uint16_t end_index, sRgbColor_t rgb_color) {
    if (port >= eWs2815Api_Port_Last) {
        error("Wrong port [%u]!\r", port);
        return false;
    }
    if (g_dynamic_ws2815_api_lut[port].enabled == false) {
        error("Port not enabled [%u]!\r", port);
        return false;
    }
    if (start_index >= g_dynamic_ws2815_api_lut[port].pixel_count) {
        error("Wrong start index [%u]!\r", start_index);
        return false;
    }
    if (end_index >= g_dynamic_ws2815_api_lut[port].pixel_count) {
        error("Wrong end index [%u]!\r", end_index);
        return false;
    }

    bool invert = false;
    if (start_index > end_index) {
        invert = true;
    }
    if (invert == true) {
        for (uint16_t pixel_index = end_index; pixel_index <= start_index; pixel_index++) {
            g_dynamic_ws2815_api_lut[port].color_data[pixel_index] = rgb_color;
        }

    } else {
        for (uint16_t pixel_index = start_index; pixel_index <= end_index; pixel_index++) {
            g_dynamic_ws2815_api_lut[port].color_data[pixel_index] = rgb_color;
        }
    }
    g_dynamic_ws2815_api_lut[port].updated = false;
    return true;
}

bool WS2815_API_SetPixelHsv (eWs2815Api_Port_Enum_t port, uint16_t pixel_index, sHsvColor_t hsv_color) {
    if (port >= eWs2815Api_Port_Last) {
        error("Wrong port [%u]!\r", port);
        return false;
    }
    if (g_dynamic_ws2815_api_lut[port].enabled == false) {
        error("Port not enabled [%u]!\r", port);
        return false;
    }
    if (pixel_index >= g_dynamic_ws2815_api_lut[port].pixel_count) {
        error("Wrong index! %d\r", pixel_index);
        return false;
    }
    g_dynamic_ws2815_api_lut[port].color_data[pixel_index] = HsvToRgb(hsv_color);
    return true;
}

bool WS2815_API_FillPixelsHsv (eWs2815Api_Port_Enum_t port, sHsvColor_t hsv_color) {
    if (port >= eWs2815Api_Port_Last) {
        error("Wrong port!\r");
        return false;
    }
    if (g_dynamic_ws2815_api_lut[port].enabled == false) {
        error("Port not enabled [%u]!\r", port);
        return false;
    }
    sRgbColor_t rgb_color = HsvToRgb(hsv_color);
    for (uint16_t pixel_index = 0; pixel_index < g_dynamic_ws2815_api_lut[port].pixel_count; pixel_index++) {
        g_dynamic_ws2815_api_lut[port].color_data[pixel_index] = rgb_color;
    }
    g_dynamic_ws2815_api_lut[port].updated = false;
    return true;
}

bool WS2815_API_FillSegmentHsv (eWs2815Api_Port_Enum_t port, uint16_t start_index, uint16_t end_index, sHsvColor_t hsv_color) {
    if (port >= eWs2815Api_Port_Last) {
        error("Wrong port [%u]!\r", port);
        return false;
    }
    if (g_dynamic_ws2815_api_lut[port].enabled == false) {
        error("Port not enabled [%u]!\r", port);
        return false;
    }
    if (start_index >= g_dynamic_ws2815_api_lut[port].pixel_count) {
        error("Wrong start index [%u]!\r", start_index);
        return false;
    }
    if (end_index >= g_dynamic_ws2815_api_lut[port].pixel_count) {
        error("Wrong end index [%u]!\r", end_index);
        return false;
    }
    sRgbColor_t rgb_color = HsvToRgb(hsv_color);
    for (uint16_t pixel_index = start_index; pixel_index <= end_index; pixel_index++) {
        g_dynamic_ws2815_api_lut[port].color_data[pixel_index] = rgb_color;
    }
    g_dynamic_ws2815_api_lut[port].updated = false;
    return true;
}

bool WS2815_API_DimSegmentRgb (eWs2815Api_Port_Enum_t port, uint16_t start_index, uint16_t end_index, uint8_t amount) {
    if (port >= eWs2815Api_Port_Last) {
        error("Wrong port [%u]!\r", port);
        return false;
    }
    if (g_dynamic_ws2815_api_lut[port].enabled == false) {
        error("Port not enabled [%u]!\r", port);
        return false;
    }
    if (start_index >= g_dynamic_ws2815_api_lut[port].pixel_count) {
        error("Wrong start index [%u]!\r", start_index);
        return false;
    }
    if (end_index >= g_dynamic_ws2815_api_lut[port].pixel_count) {
        error("Wrong end index [%u]!\r", end_index);
        return false;
    }
    if (amount == 0) {
        return true;
    }
    sRgbColor_t color = {0};
    bool invert = false;
    if (start_index > end_index) {
        invert = true;
    }
    if (invert == true) {
        for (uint16_t pixel_index = end_index; pixel_index <= start_index; pixel_index++) {
            color = g_dynamic_ws2815_api_lut[port].color_data[pixel_index];
            g_dynamic_ws2815_api_lut[port].color_data[pixel_index].r = (color.r - amount > amount) ? (color.r - amount) : (0);
            g_dynamic_ws2815_api_lut[port].color_data[pixel_index].g = (color.g - amount > amount) ? (color.g - amount) : (0);
            g_dynamic_ws2815_api_lut[port].color_data[pixel_index].b = (color.b - amount > amount) ? (color.b - amount) : (0);
        }
    } else {
        for (uint16_t pixel_index = start_index; pixel_index <= end_index; pixel_index++) {
            color = g_dynamic_ws2815_api_lut[port].color_data[pixel_index];
            g_dynamic_ws2815_api_lut[port].color_data[pixel_index].r = (color.r - amount > amount) ? (color.r - amount) : (0);
            g_dynamic_ws2815_api_lut[port].color_data[pixel_index].g = (color.g - amount > amount) ? (color.g - amount) : (0);
            g_dynamic_ws2815_api_lut[port].color_data[pixel_index].b = (color.b - amount > amount) ? (color.b - amount) : (0);
        }
    }
    g_dynamic_ws2815_api_lut[port].updated = false;
    return true;
}

bool WS2815_API_GradientSegmentRgb (eWs2815Api_Port_Enum_t port, eWs2815Api_PowerLut_Enum_t power_lut_index, uint16_t start_index, uint16_t end_index, sRgbColor_t start_rgb_color, sRgbColor_t end_rgb_color) {
    if (port >= eWs2815Api_Port_Last) {
        error("Wrong port [%u]!\r", port);
        return false;
    }
    if (g_dynamic_ws2815_api_lut[port].enabled == false) {
        error("Port not enabled [%u]!\r", port);
        return false;
    }
    if (start_index >= g_dynamic_ws2815_api_lut[port].pixel_count) {
        error("Wrong start index [%u]!\r", start_index);
        return false;
    }
    if (end_index >= g_dynamic_ws2815_api_lut[port].pixel_count) {
        error("Wrong end index [%u]!\r", end_index);
        return false;
    }

    sRgbColor_t rgb_color = {0};
    double ratio = 0;

    bool invert = false;
    if (start_index > end_index) {
        invert = true;
    }
    uint16_t segment_length = 0;
    if (invert == true) {
        segment_length = start_index - end_index;
        for (uint16_t pixel_index = end_index; pixel_index <= start_index; pixel_index++) {
            ratio = (double) (pixel_index - end_index) / segment_length;
            rgb_color = GetGradient(end_rgb_color, start_rgb_color, ratio);
            rgb_color.r = g_static_ws2815_power_lut[power_lut_index][rgb_color.r];
            rgb_color.g = g_static_ws2815_power_lut[power_lut_index][rgb_color.g];
            rgb_color.b = g_static_ws2815_power_lut[power_lut_index][rgb_color.b];
            g_dynamic_ws2815_api_lut[port].color_data[pixel_index] = rgb_color;
        }
    } else {
        segment_length = end_index - start_index;
        for (uint16_t pixel_index = start_index; pixel_index <= end_index; pixel_index++) {
            ratio = (double) (pixel_index - start_index) / segment_length;
            rgb_color = GetGradient(start_rgb_color, end_rgb_color, ratio);
            rgb_color.r = g_static_ws2815_power_lut[power_lut_index][rgb_color.r];
            rgb_color.g = g_static_ws2815_power_lut[power_lut_index][rgb_color.g];
            rgb_color.b = g_static_ws2815_power_lut[power_lut_index][rgb_color.b];
            g_dynamic_ws2815_api_lut[port].color_data[pixel_index] = rgb_color;
        }
    }
    g_dynamic_ws2815_api_lut[port].updated = false;
    return true;
}
