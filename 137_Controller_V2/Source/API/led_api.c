/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include <math_utils.h>
#include "led_api.h"
#include "debug_api.h"
#include "timer_driver.h"
#include "stdbool.h"
#include "color_utils.h"
#include "gpio_driver.h"
#include "cmsis_os2.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
DEBUG_MODULE(LED_API)

#define MAX_DELAY_BETWEEN_BLINKS 1500
#define MIN_DELAY_BETWEEN_BLINKS 500
#define MAX_BLINKS 3
#define MIN_BLINKS 1
#define MAX_ON_TIME 100
#define MIN_ON_TIME 20

/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/
typedef struct sLedApi_Static_t {
    eTimerDriver_Timer_Enum_t timer;
    eTimerDriver_OC_Enum_t r_channel;
    eTimerDriver_OC_Enum_t g_channel;
    eTimerDriver_OC_Enum_t b_channel;
    eTimerDriver_OC_Enum_t w_channel;
} sLedApi_Static_t;

typedef struct sLedApi_Dynamic_t {
    bool enabled;
    uint8_t r;
    uint8_t g;
    uint8_t b;
    uint8_t w;
    bool updated;
} sLedApi_Dynamic_t;
/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
static const sLedApi_Static_t g_static_led_api_lut[eLedApi_Port_Last] = {
    [eLedApi_Port_1] = {
        .timer = eTimerDriver_Timer_3,
        .r_channel = eTimerDriver_OC_1,
        .g_channel = eTimerDriver_OC_2,
        .b_channel = eTimerDriver_OC_4,
        .w_channel = eTimerDriver_OC_3,
    },
    [eLedApi_Port_2] = {
        .timer = eTimerDriver_Timer_4,
        .r_channel = eTimerDriver_OC_2,
        .g_channel = eTimerDriver_OC_1,
        .b_channel = eTimerDriver_OC_3,
        .w_channel = eTimerDriver_OC_4,
    }
};
/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/
static bool g_led_api_initialized = false;

static sLedApi_Dynamic_t g_dynamic_led_api_lut[eLedApi_Port_Last] = {
    [eLedApi_Port_1] = {
        .enabled = false,
        .r = 0,
        .g = 0,
        .b = 0,
        .w = 0,
        .updated = false,
    },
    [eLedApi_Port_2] = {
        .enabled = false,
        .r = 0,
        .g = 0,
        .b = 0,
        .w = 0,
        .updated = false,
    },
};
/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/
bool LED_API_Init (eLedApi_Port_Enum_t port);
/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/
bool LED_API_Init (eLedApi_Port_Enum_t port) {
    if (port >= eLedApi_Port_Last) {
        error("Wrong port provided [%u]!\r", port);
        return false;
    }
    if (g_led_api_initialized == true) {
        return true;
    }
    Timer_Driver_Init(g_static_led_api_lut[port].timer);
    Timer_Driver_Start(g_static_led_api_lut[port].timer);

    return true;
}
/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool LED_API_Enable (eLedApi_Port_Enum_t port) {
    if (port >= eLedApi_Port_Last) {
        error("Wrong port provided [%u]!\r", port);
        return false;
    }
    if (g_dynamic_led_api_lut[port].enabled == true) {
        return true;
    }

    if (LED_API_Init(port) == false) {
        return false;
    }

    eTimerDriver_Timer_Enum_t timer = g_static_led_api_lut[port].timer;
    Timer_Driver_EnableChannel(timer, g_static_led_api_lut[port].r_channel);
    Timer_Driver_EnableChannel(timer, g_static_led_api_lut[port].g_channel);
    Timer_Driver_EnableChannel(timer, g_static_led_api_lut[port].b_channel);
    Timer_Driver_EnableChannel(timer, g_static_led_api_lut[port].w_channel);

    g_dynamic_led_api_lut[port].enabled = true;
    g_dynamic_led_api_lut[port].updated = false;
    return true;
}

bool LED_API_Disable (eLedApi_Port_Enum_t port) {
    if (port >= eLedApi_Port_Last) {
        error("Wrong port provided [%u]!\r", port);
        return false;
    }
    if (g_dynamic_led_api_lut[port].enabled == false) {
        return true;
    }

    eTimerDriver_Timer_Enum_t timer = g_static_led_api_lut[port].timer;
    Timer_Driver_DisableChannel(timer, g_static_led_api_lut[port].r_channel);
    Timer_Driver_DisableChannel(timer, g_static_led_api_lut[port].g_channel);
    Timer_Driver_DisableChannel(timer, g_static_led_api_lut[port].b_channel);
    Timer_Driver_DisableChannel(timer, g_static_led_api_lut[port].w_channel);

    g_dynamic_led_api_lut[port].enabled = false;
    g_dynamic_led_api_lut[port].updated = false;
    return true;
}

bool LED_API_Update (void) {
    for (eLedApi_Port_Enum_t port = eLedApi_Port_First; port < eLedApi_Port_Last; port++) {
        if (g_dynamic_led_api_lut[port].enabled == false) {
            continue;
        }
        if (g_dynamic_led_api_lut[port].updated == true) {
            continue;
        }

        eTimerDriver_Timer_Enum_t timer = g_static_led_api_lut[port].timer;
        Timer_Driver_SetDutyCycle(timer, g_static_led_api_lut[port].r_channel, g_dynamic_led_api_lut[port].r);
        Timer_Driver_SetDutyCycle(timer, g_static_led_api_lut[port].g_channel, g_dynamic_led_api_lut[port].g);
        Timer_Driver_SetDutyCycle(timer, g_static_led_api_lut[port].b_channel, g_dynamic_led_api_lut[port].b);
        Timer_Driver_SetDutyCycle(timer, g_static_led_api_lut[port].w_channel, g_dynamic_led_api_lut[port].w);
        g_dynamic_led_api_lut[port].updated = true;
    }

    return true;
}

bool LED_API_SetHsvColor (eLedApi_Port_Enum_t port, sHsvColor_t color) {
    if (port >= eLedApi_Port_Last) {
        error("Wrong port provided [%u]!\r", port);
        return false;
    }
    if (g_dynamic_led_api_lut[port].enabled == false) {
        return false;
    }
    sRgbwColor_t rgbw_color = HsvToRgbw(color);
    LED_API_SetRgbwColor(port, rgbw_color);
    g_dynamic_led_api_lut[port].updated = false;
    return true;
}

bool LED_API_SetRgbwColor (eLedApi_Port_Enum_t port, sRgbwColor_t rgbw_color) {
    if (port >= eLedApi_Port_Last) {
        error("Wrong port provided [%u]!\r", port);
        return false;
    }
    if (g_dynamic_led_api_lut[port].enabled == false) {
        return false;
    }

    g_dynamic_led_api_lut[port].r = rgbw_color.r;
    g_dynamic_led_api_lut[port].g = rgbw_color.g;
    g_dynamic_led_api_lut[port].b = rgbw_color.b;
    g_dynamic_led_api_lut[port].w = rgbw_color.w;
    g_dynamic_led_api_lut[port].updated = false;
    return true;
}
