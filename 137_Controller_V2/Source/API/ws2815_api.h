#ifndef __WS2815_API__H__
#define __WS2815_API__H__
/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "stdbool.h"
#include "stdint.h"
#include "color_utils.h"
#include "cmsis_os.h"
#include "time_utils.h"
#include "ws2815_lut.h"
/**********************************************************************************************************************
 * Exported definitions and macros
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported types
 *********************************************************************************************************************/
typedef enum eWs2815Api_Port_Enum_t {
    eWs2815Api_Port_First = 0,
    eWs2815Api_Port_1 = eWs2815Api_Port_First,
    eWs2815Api_Port_2,
    eWs2815Api_Port_3,
    eWs2815Api_Port_Last,
} eWs2815Api_Port_Enum_t;

typedef struct sWs2815Api_Init_t {
    eWs2815Api_Port_Enum_t port;
    uint16_t pixel_count;
    sRgbColor_t *color_data_buffer;
} sWs2815Api_Init_t;

typedef enum eWs2815Api_Animation_Enum_t {
    eArgbAnimation_First = 0,
    eArgbAnimation_StaticColor = eArgbAnimation_First,
    eArgbAnimation_StaticGradient,
    eArgbAnimation_StaticRainbow,
    eArgbAnimation_StaticRandomColor,
    eArgbAnimation_ChangingColor,
    eArgbAnimation_RandomColorDots,
    eArgbAnimation_BlinkingRandomColorDots,
    eArgbAnimation_ColorDots,
    eArgbAnimation_BlinkingColorDots,
    eArgbAnimation_Last
} eWs2815Api_Animation_Enum_t;

typedef struct eWs2815Api_Animation_Data_t {
    eWs2815Api_Port_Enum_t port;
    eWs2815Api_Animation_Enum_t animation;
    uint16_t start_pixel;
    uint16_t end_pixel;
    sRgbColor_t start_color;
    sRgbColor_t end_color;
    timestamp_t start_time;
    uint32_t run_time_ms;
    void *runtime_data;
} eWs2815Api_Animation_Data_t;
/**********************************************************************************************************************
 * Exported variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of exported functions
 *********************************************************************************************************************/
bool WS2815_API_UpdatePixels (void);
bool WS2815_API_Enable (eWs2815Api_Port_Enum_t port, uint16_t pixel_amount);
bool WS2815_API_Disable (eWs2815Api_Port_Enum_t port);
bool WS2815_API_SetPixelRgb (eWs2815Api_Port_Enum_t port, uint16_t pixel_index, sRgbColor_t rgb_color);
bool WS2815_API_FillPixelsRgb (eWs2815Api_Port_Enum_t port, sRgbColor_t rgb_color);
bool WS2815_API_FillSegmentRgb (eWs2815Api_Port_Enum_t port, uint16_t start_index, uint16_t end_index, sRgbColor_t rgb_color);
bool WS2815_API_SetPixelHsv (eWs2815Api_Port_Enum_t port, uint16_t pixel_index, sHsvColor_t hsv_color);
bool WS2815_API_FillPixelsHsv (eWs2815Api_Port_Enum_t port, sHsvColor_t hsv_color);
bool WS2815_API_FillSegmentHsv (eWs2815Api_Port_Enum_t port, uint16_t start_index, uint16_t end_index, sHsvColor_t hsv_color);
bool WS2815_API_DimSegmentRgb (eWs2815Api_Port_Enum_t port, uint16_t start_index, uint16_t end_index, uint8_t amount);
bool WS2815_API_GradientSegmentRgb (eWs2815Api_Port_Enum_t port, eWs2815Api_PowerLut_Enum_t power_lut_index, uint16_t start_index, uint16_t end_index, sRgbColor_t start_rgb_color, sRgbColor_t end_rgb_color);
#endif /* __WS2815_API__H__ */
