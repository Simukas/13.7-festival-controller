/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "debug_api.h"
#include "usb_api.h"
#include "cmsis_os2.h"
#include "time_utils.h"
#include "rtc_driver.h"
#include "baudrate_utils.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
#define DEBUG_MESSAGE_MAX_SIZE 150

#define DEBUG_BAUDRATE eBaudrate_115200

#define DEBUG_MESSAGE_TIMEOUT 10
#define ERROR_MESSAGE_TIMEOUT 100
/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
const osMutexAttr_t g_debug_api_mutex_attr = {
    "debug_api_mutex",
    osMutexRecursive | osMutexPrioInherit,
    NULL,
    0U
};

uint32_t g_debug_api_timeout_lut[eDebugApi_PrintType_Last] = {
    [eDebugApi_PrintType_Debug] = DEBUG_MESSAGE_TIMEOUT,
    [eDebugApi_PrintType_Error] = ERROR_MESSAGE_TIMEOUT,
};
/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/
static osMutexId_t g_debug_api_mutex_id = NULL;
static char g_debug_api_print_buffer[DEBUG_MESSAGE_MAX_SIZE] = {0};
/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool Debug_API_Init (void) {
    if (USB_API_Init(DEBUG_BAUDRATE) == false) {
        return false;
    }
    g_debug_api_mutex_id = osMutexNew(&g_debug_api_mutex_attr);
    if (g_debug_api_mutex_id == NULL) {
        return false;
    }

    return true;
}

void Debug_API_Log (const char *debug_module_name, eDebugApi_PrintType_Enum_t print_type, char *file, uint16_t line, char *format, ...) {
    if (debug_module_name == NULL) {
        return;
    }
    if (print_type >= eDebugApi_PrintType_Last) {
        return;
    }
    if (format == NULL) {
        return;
    }
    uint32_t timeout = g_debug_api_timeout_lut[print_type];
    if (osMutexAcquire(g_debug_api_mutex_id, timeout) != osOK) {
        return;
    }
    uint16_t offset = 0;
    sDateTime_t date_time = {0};
    RTC_Driver_GetDateTime(&date_time);

    switch (print_type) {
        case eDebugApi_PrintType_Debug: {
            offset += snprintf(&g_debug_api_print_buffer[offset], DEBUG_MESSAGE_MAX_SIZE, "[%s]\t[%s]\tDEBUG:\t", formatDateTime(&date_time), debug_module_name);
            break;
        }
        case eDebugApi_PrintType_Error: {
            offset += snprintf(&g_debug_api_print_buffer[offset], DEBUG_MESSAGE_MAX_SIZE, "[%s]\t[%s]\tERROR: (%s (%d))\t", formatDateTime(&date_time), debug_module_name, file, line);
            break;
        }
        default: {
            break;
        }
    }

    if (format != NULL) {
        va_list args;
        va_start(args, format);
        offset += vsnprintf(&g_debug_api_print_buffer[offset], DEBUG_MESSAGE_MAX_SIZE - offset, format, args);
        va_end(args);
    }
    USB_API_SendString(g_debug_api_print_buffer, offset, timeout);
    osMutexRelease(g_debug_api_mutex_id);
}

void Debug_API_Print (char *string, uint32_t max_length) {
    if (string == NULL) {
        return;
    }
    if (max_length == 0) {
        return;
    }
    USB_API_SendString(string, max_length, DEBUG_MESSAGE_TIMEOUT);
}

void Debug_API_PrintF (char *format, ...) {
    if (format == NULL) {
        return;
    }
    va_list args;
    va_start(args, format);
    uint32_t size = vsnprintf(g_debug_api_print_buffer, DEBUG_MESSAGE_MAX_SIZE, format, args);
    va_end(args);
    USB_API_SendString(g_debug_api_print_buffer, size, DEBUG_MESSAGE_TIMEOUT);
}

bool Debug_API_Lock (void) {
    if (osMutexAcquire(g_debug_api_mutex_id, DEBUG_MESSAGE_TIMEOUT) != osOK) {
        return false;
    }
    return true;
}

bool Debug_API_Unlock (void) {
    if (osMutexRelease(g_debug_api_mutex_id) != osOK) {
        return false;
    }
    return true;
}
