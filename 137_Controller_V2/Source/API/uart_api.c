/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "uart_api.h"
#include "uart_driver.h"
#include "cmsis_os.h"
#include "debug_api.h"
#include "thread_info.h"
#include <stdbool.h>
#include "heap_api.h"
#include "binary_protocol.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
#define UART_API_BUFFER_SIZE UART_DRIVER_RX_MESSAGE_BUFFER_SIZE
#define UART_API_MESSAGE_QUEUE_COUNT 5

#define UART_API_MAX_MESSAGE_LENGTH 20

#define UART_API_IDLE_FLAG 0x00000001U

DEBUG_MODULE(UART_API)
/**********************************************************************************************************************
 * Private types
 *********************************************************************************************************************/
// @formatter:off
typedef struct sUartApi_Dynamic_t {
    osMessageQueueId_t msg_queue;
    osSemaphoreId_t semaphore_id;
    osMutexId_t tx_mutex_id;
    eBaudrate_Enum_t baudrate;
    bool initialised;
    osEventFlagsId_t event_flag;
//    uint8_t buffer[UART_API_BUFFER_SIZE];
} sUartApi_Dynamic_t;

typedef struct sUartApi_Static_t {
        eUartDriver_Enum_t uart;
} sUartApi_Static_t;

typedef enum eUartApi_State_Enum_t {
    eUartApi_State_First = 0,
        eUartApi_State_Setup = eUartApi_State_First,
        eUartApi_State_GetAddress,
        eUartApi_State_GetCmdID,
        eUartApi_State_GetData,
        eUartApi_State_Flush,
        eUartApi_State_Last
} eUartApi_State_Enum_t;

// @formatter:on
/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
const static osThreadAttr_t g_uart_api_task_attributes = {
    .name = "uart_api_task",
    .stack_size = UART_API_TASK_STACK_SIZE,
    .priority = (osPriority_t) UART_API_TASK_PRIORITY,
};

const static sUartApi_Static_t g_static_uart_api_lut[eUartApi_Last] = {
    [eUartApi_RS485] = {
        .uart = eUartDriver_Usart6,
    }
};

const static osMutexAttr_t uart_api_mutex_attr = {
    "uart_api_mutex",
    osMutexRecursive | osMutexPrioInherit,
    NULL,
    0U
};

//const static eUartDriver_Enum_t g_static_uart_api_translation_lut[eUartDriver_Last] = {
//    [eUartDriver_Usart6] = eUartApi_RS485,
//};

/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/
//static osThreadId_t uart_api_task_handle = NULL;
//static osEventFlagsId_t IT_flag = NULL;
static sUartApi_Dynamic_t g_dynamic_uart_api_lut[eUartApi_Last] = {
    [eUartApi_RS485] = {
        .initialised = false,
        .baudrate = 0,
        .msg_queue = NULL,
        .tx_mutex_id = NULL,
    }
};

static osEventFlagsId_t g_uart_api_event_flag_id = NULL;
//static eUartApi_State_Enum_t g_uart_api_fsm_state = eUartApi_State_Setup;
//static uint32_t g_uart_api_collection_index = 0;
//static uint8_t *g_uart_api_collection_buffer = NULL;
static osThreadId_t g_uart_api_task_handle = NULL;
//static uint8_t g_uart_api_cmd_data_size = 0;
/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/
//static void UART_API_Task (void *argument);
void UART_API_UART_ITCallback (eUartDriver_Enum_t uart, eUartDriver_Event_Enum_t IT);
/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/

void UART_API_UART_ITCallback (eUartDriver_Enum_t uart, eUartDriver_Event_Enum_t IT) {
    switch (IT) {
        case eUartDriver_Event_IDLE: {
            osEventFlagsSet(g_uart_api_event_flag_id, UART_API_IDLE_FLAG);
            break;
        }
//        case eUartDriver_Event_RX_HT: {
//            sMessage_t message = {0};
//            if (UART_Driver_GetMessage(uart, &message) == true) {
//                if (osMessageQueuePut(g_dynamic_uart_api_lut[uart].msg_queue, &message, 0, 0U) != osOK) {
//
//                }
//            }
//            break;
//        }
//        case eUartDriver_Event_RX_TC: {
//            sMessage_t message = {0};
//            if (UART_Driver_GetMessage(uart, &message) == true) {
//                if (osMessageQueuePut(g_dynamic_uart_api_lut[uart].msg_queue, &message, 0, 0U) != osOK) {
//
//                }
//            }
//            break;
//        }
        case eUartDriver_Event_TX_TC: {
            if (osSemaphoreRelease(g_dynamic_uart_api_lut[uart].semaphore_id) != osOK) {
                //error("Failed to release semaphore!\r");
            }
            break;
        }
        default: {
            break;
        }
    }
}

static void UART_API_Task (void *argument) {
    sMessage_t message = {0};
    while (true) {
        uint32_t received_flag = osEventFlagsWait(g_uart_api_event_flag_id, UART_API_IDLE_FLAG, osFlagsWaitAny, 100);
//        if (received_flag != UART_API_IDLE_FLAG) {
//            error("Failed to get new data flag!\r");
//            continue;
//        }
        for (eUartApi_Enum_t uart = eUartApi_First; uart < eUartApi_Last; uart++) {
            while (UART_Driver_GetMessage(uart, &message) == true) {
                if (osMessageQueuePut(g_dynamic_uart_api_lut[uart].msg_queue, &message, 0, 0U) != osOK) {

                }
                osThreadYield();
            }

//            bool repeat = true;
//            while (repeat == true) {
//                repeat = false;
//                switch (g_uart_api_fsm_state) {
//                    case eUartApi_State_Setup: {
//                        g_uart_api_collection_index = 0;
//                        g_uart_api_collection_buffer = Heap_API_Calloc(UART_API_MAX_MESSAGE_LENGTH, sizeof(uint8_t));
//                        if (g_uart_api_collection_buffer == NULL) {
//                            error("Failed to allocate FSM buffer!\r");
//                            continue;
//                        }
//                        g_uart_api_fsm_state = eUartApi_State_GetAddress;
//                    }
//                    case eUartApi_State_GetAddress: {
//                        uint8_t byte = 0;
//                        if (UART_Driver_GetByte(g_static_uart_api_lut[uart].uart, &byte) == true) {
//                            g_uart_api_collection_buffer[g_uart_api_collection_index] = byte;
//                            g_uart_api_collection_index++;
//                            g_uart_api_fsm_state = eUartApi_State_GetCmdID;
//                        }
//                        if (g_uart_api_fsm_state != eUartApi_State_GetCmdID) {
//                            break;
//                        }
//                    }
//                    case eUartApi_State_GetCmdID: {
//                        uint8_t byte = 0;
//                        if (UART_Driver_GetByte(g_static_uart_api_lut[uart].uart, &byte) == true) {
//                            g_uart_api_collection_buffer[g_uart_api_collection_index] = byte;
//                            Binary_Protocol_GetCommandSize(byte, &g_uart_api_cmd_data_size);
//                            g_uart_api_collection_index++;
//                            if (g_uart_api_cmd_data_size == 0) {
//                                g_uart_api_fsm_state = eUartApi_State_Flush;
//                                repeat = true;
//                                break;
//                            }
//                            g_uart_api_fsm_state = eUartApi_State_GetData;
//                        }
//                        if (g_uart_api_fsm_state != eUartApi_State_GetData) {
//                            break;
//                        }
//                    }
//                    case eUartApi_State_GetData: {
//                        uint8_t byte = 0;
//                        while (UART_Driver_GetByte(g_static_uart_api_lut[uart].uart, &byte) == true) {
//                            g_uart_api_collection_buffer[g_uart_api_collection_index] = byte;
//                            if (g_uart_api_collection_index - 2 == g_uart_api_cmd_data_size) {
//                                g_uart_api_fsm_state = eUartApi_State_Flush;
//                            } else {
//                                g_uart_api_collection_index++;
//                            }
//                        }
//                        if (g_uart_api_fsm_state != eUartApi_State_Flush) {
//                            break;
//                        }
//                    }
//                    case eUartApi_State_Flush: {
//                        sUartApi_Message_t message = {
//                            .message_buffer = g_uart_api_collection_buffer,
//                            .message_length = g_uart_api_collection_index
//                        };
//                        if (osMessageQueuePut(g_dynamic_uart_api_lut[uart].msg_queue, &message, 0, osWaitForever) != osOK) {
//                            error("Failed to put message into queue!\r");
//                            continue;
//                        }
//                        g_uart_api_fsm_state = eUartApi_State_Setup;
//                        break;
//                    }
//                    default: {
//                        break;
//                    }
//                }
//            }
        }

    }
}
/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool UART_API_Init (eUartApi_Enum_t uart, eBaudrate_Enum_t baudrate) {
    if (uart >= eUartApi_Last) {
        error("Wrong UART provided!\r");
        return false;
    }
    if (baudrate >= eBaudrate_Last) {
        error("Wrong baudrate provided!\r");
        return false;
    }

    if (UART_Driver_SetITCallback(g_static_uart_api_lut[uart].uart, &UART_API_UART_ITCallback) == false) {
        error("Failed to set UART IT callback!\r");
        return false;
    }

    if (UART_Driver_Init(g_static_uart_api_lut[uart].uart, baudrate) == false) {
        error("Failed to initialize UART driver!\r");
        return false;
    }

    g_dynamic_uart_api_lut[uart].tx_mutex_id = osMutexNew(&uart_api_mutex_attr);
    if (g_dynamic_uart_api_lut[uart].tx_mutex_id == NULL) {
        error("Failed to create mutex!\r");
        return false;
    }

    g_uart_api_event_flag_id = osEventFlagsNew(NULL);
    if (g_uart_api_event_flag_id == NULL) {
        return false;
    }

    g_dynamic_uart_api_lut[uart].semaphore_id = osSemaphoreNew(1, 1, NULL);
    if (g_dynamic_uart_api_lut[uart].semaphore_id == NULL) {
        return false;
    }

    g_dynamic_uart_api_lut[uart].msg_queue = osMessageQueueNew(UART_API_MESSAGE_QUEUE_COUNT, sizeof(sMessage_t), NULL);
    if (g_dynamic_uart_api_lut[uart].msg_queue == NULL) {
        error("Failed to create message queue!\r");
        return false;
    }
    g_uart_api_task_handle = osThreadNew(UART_API_Task, NULL, &g_uart_api_task_attributes);
    if (g_uart_api_task_handle == NULL) {
        return false;
    }
    return true;
}

bool UART_API_SendData (eUartApi_Enum_t uart, uint8_t *data, uint16_t data_size, uint32_t timeout) {
    if (data == NULL) {
        error("NULL pointer provided!\r");
        return false;
    }
    if (uart >= eUartApi_Last) {
        error("Wrong UART provided!\r");
        return false;
    }
    if (osSemaphoreAcquire(g_dynamic_uart_api_lut[uart].semaphore_id, timeout) != osOK) {
        error("Failed to acquire semaphore!\r");
        return false;
    }
//    if (osMutexAcquire(g_dynamic_uart_api_lut[uart].tx_mutex_id, timeout) != osOK) {
//        error("Failed to acquire mutex!\r");
//        return false;
//    }
    if (UART_Driver_SendBytes(uart, data, data_size) == false) {
        return false;
    }
//    if (osMutexRelease(g_dynamic_uart_api_lut[uart].tx_mutex_id) != osOK) {
//        error("Failed to release mutex!\r");
//        return false;
//    }
    return true;
}

bool UART_API_SendString (eUartApi_Enum_t uart, const char *string, uint32_t timeout) {
    if (string == NULL) {
        error("NULL pointer provided!\r");
        return false;
    }
    if (uart >= eUartApi_Last) {
        error("Wrong UART provided!\r");
        return false;
    }
    if (osSemaphoreAcquire(g_dynamic_uart_api_lut[uart].semaphore_id, timeout) != osOK) {
        error("Failed to acquire semaphore!\r");
        return false;
    }
    if (osMutexAcquire(g_dynamic_uart_api_lut[uart].tx_mutex_id, timeout) != osOK) {
        error("Failed to acquire mutex!\r");
        return false;
    }
    if (UART_Driver_SendBytes(uart, (uint8_t*) string, strlen(string)) == false) {
        return false;
    }
    if (osMutexRelease(g_dynamic_uart_api_lut[uart].tx_mutex_id) != osOK) {
        error("Failed to release mutex!\r");
        return false;
    }
    return true;
}

bool UART_API_GetMessage (eUartApi_Enum_t uart, sMessage_t *message, uint32_t timeout) {
    if (message == NULL) {
        error("NULL pointer provided!\r");
        return false;
    }
    if (uart >= eUartApi_Last) {
        error("Wrong UART provided!\r");
        return false;
    }
    if (osMessageQueueGet(g_dynamic_uart_api_lut[uart].msg_queue, message, NULL, timeout) != osOK) {
        return false;
    }
    return true;
}

bool UART_API_SetBaudrate (eUartApi_Enum_t uart, eBaudrate_Enum_t baudrate) {
    if (uart >= eUartApi_Last) {
        error("Wrong UART provided!\r");
        return false;
    }
    if (baudrate >= eBaudrate_Last) {
        error("Wrong baudrate provided!\r");
        return false;
    }
    if (osMutexAcquire(g_dynamic_uart_api_lut[uart].tx_mutex_id, osWaitForever) != osOK) {
        error("Failed to acquire mutex!\r");
        return false;
    }
    if (UART_Driver_SetBaudrate(g_static_uart_api_lut[uart].uart, baudrate) == false) {
        error("Failed to set baudrate!\r");
        return false;
    }
    if (osMutexRelease(g_dynamic_uart_api_lut[uart].tx_mutex_id) != osOK) {
        error("Failed to release mutex!\r");
        return false;
    }
    return true;
}

bool UART_API_Lock (eUartApi_Enum_t uart, uint32_t timeout) {
    if (uart >= eUartApi_Last) {
        error("Wrong UART provided!\r");
        return false;
    }
    if (osMutexAcquire(g_dynamic_uart_api_lut[uart].tx_mutex_id, timeout) != osOK) {
        error("Failed to acquire mutex!\r");
        return false;
    }
    return true;
}

bool UART_API_Unlock (eUartApi_Enum_t uart) {
    if (uart >= eUartApi_Last) {
        error("Wrong UART provided!\r");
        return false;
    }
    if (osMutexRelease(g_dynamic_uart_api_lut[uart].tx_mutex_id) != osOK) {
        error("Failed to release mutex!\r");
        return false;
    }
    return true;
}
