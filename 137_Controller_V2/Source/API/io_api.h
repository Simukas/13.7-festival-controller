#ifndef __IO_API__H__
#define __IO_API__H__
/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "stdbool.h"
/**********************************************************************************************************************
 * Exported definitions and macros
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported types
 *********************************************************************************************************************/
typedef enum eIoApi_Port_Enum_t {
    eIoApi_Port_First = 0,
    eIoApi_Port_1 = eIoApi_Port_First,
    eIoApi_Port_2,
    eIoApi_Port_3,
    eIoApi_Port_4,
    eIoApi_Port_5,
    eIoApi_Port_6,
    eIoApi_Port_Last,
} eIoApi_Port_Enum_t;

typedef enum eIoApi_Function_Enum_t {
    eIoApi_Function_First = 0,
    eIoApi_Function_Input = eIoApi_Function_First,
    eIoApi_Function_Output,
    eIoApi_Function_Last
} eIoApi_Function_Enum_t;
/**********************************************************************************************************************
 * Exported variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of exported functions
 **********************************
 *********************************************************************************************************************/
bool IO_API_Enable      (eIoApi_Port_Enum_t port, eIoApi_Function_Enum_t function);
bool IO_API_Disable     (eIoApi_Port_Enum_t port);
bool IO_API_SetPort     (eIoApi_Port_Enum_t port, bool state);
bool IO_API_ReadPort    (eIoApi_Port_Enum_t port, bool *state);
#endif /* __IO_API__H__ */
