/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "heap_api.h"
#include "cmsis_os2.h"
#include "stdbool.h"
#include "stdlib.h"
#include "debug_api.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
//DEBUG_MODULE(HEAP_API)
#define HEAP_API_CALLOC_TIMEOUT 100
#define HEAP_API_FREE_TIMEOUT 1000
/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
static const osMutexAttr_t g_heap_api_mutex_attributes = {
    .name = "heap_api_mutex",
    .attr_bits = osMutexRecursive | osMutexPrioInherit,
    .cb_mem = NULL,
    .cb_size = 0U
};

/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/
static osMutexId_t g_heap_api_mutex_id = NULL;
static uint32_t g_heap_api_unfreed_memory_blocks = 0;
static uint32_t g_heap_api_unfreed_memory_bytes = 0;
/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool Heap_API_Init (void) {
    g_heap_api_mutex_id = osMutexNew(&g_heap_api_mutex_attributes);
    if (g_heap_api_mutex_id == NULL) {
        return false;
    }
    return true;
}

void* Heap_API_Calloc (size_t amount, size_t size) {
    if (amount == 0) {
        return false;
    }
    if (size == 0) {
        return false;
    }
    if (osMutexAcquire(g_heap_api_mutex_id, HEAP_API_CALLOC_TIMEOUT) != osOK) {
        return NULL;
    }
    void *pointer = calloc(amount, size);
    g_heap_api_unfreed_memory_blocks++;
    g_heap_api_unfreed_memory_bytes += amount * size;
    if (osMutexRelease(g_heap_api_mutex_id) != osOK) {
        free(pointer);
        return NULL;
    }
    return pointer;
}

bool Heap_API_Free (void *pointer, size_t size) {
    if (pointer == NULL) {
        return false;
    }
    if (osMutexAcquire(g_heap_api_mutex_id, HEAP_API_FREE_TIMEOUT) != osOK) {
        return false;
    }
    free(pointer);
    g_heap_api_unfreed_memory_blocks--;
    g_heap_api_unfreed_memory_bytes -= size;
    if (osMutexRelease(g_heap_api_mutex_id) != osOK) {
        return false;
    }
    return true;
}

void* Heap_API_CallocISR (size_t amount, size_t size) {
    if (amount == 0) {
        return false;
    }
    if (size == 0) {
        return false;
    }
    void *pointer = calloc(amount, size);
    g_heap_api_unfreed_memory_blocks++;
    return pointer;
}

bool Heap_API_FreeISR (void *pointer) {
    if (pointer == NULL) {
        return false;
    }
    free(pointer);
    g_heap_api_unfreed_memory_blocks--;
    return true;
}

bool Heap_API_GetUnfreedBlocks (uint32_t *blocks) {
    if (blocks == NULL) {
        return false;
    }
    *blocks = g_heap_api_unfreed_memory_blocks;
    return true;
}

bool Heap_API_GetUnfreedBytes (uint32_t *bytes) {
    if (bytes == NULL) {
        return false;
    }
    *bytes = g_heap_api_unfreed_memory_bytes;
    return true;
}
