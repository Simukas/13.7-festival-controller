/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "hx711_api.h"
#include "stdint.h"
#include "gpio_driver.h"
#include "cmsis_os.h"
#include "time_utils.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
#define HX711_API_DATA_BITS 24
/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/
typedef struct sHx711Api_Static_t {
    eGpioDriver_Pin_Enum_t clock_pin;
    eGpioDriver_Pin_Enum_t data_pin;
} sHx711Api_Static_t;

typedef struct sHx711Api_Dynamic_t {
    uint32_t callibration_offset;
    bool enabled;
    uint32_t measusred_weight;
} sHx711Api_Dynamic_t;
/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
static const sHx711Api_Static_t g_static_hx711_api_lut[eHx711Api_Last] = {
    [eHx711Api_Primary] = {
        .clock_pin = eGpioDriver_Pin_I2C_SCL,
        .data_pin = eGpioDriver_Pin_I2C_SDA,
    },
    [eHx711Api_Secondary] = {
        .clock_pin = eGpioDriver_Pin_SPI_SCK,
        .data_pin = eGpioDriver_Pin_SPI_MISO,
    },
};
/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/
static sHx711Api_Dynamic_t g_dynamic_hx711_api_lut[eHx711Api_Last] = {
    [eHx711Api_Primary] = {
        .callibration_offset = 0,
        .enabled = false,
        .measusred_weight = 0,
    },
    [eHx711Api_Secondary] = {
        .callibration_offset = 0,
        .enabled = false,
        .measusred_weight = 0,
    },
};
/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/
static bool HX711_API_ReadValue (eHx711Api_Enum_t port, uint32_t *measured_value);
bool HX711_API_Init (eHx711Api_Enum_t port);
/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/
bool HX711_API_Init (eHx711Api_Enum_t port) {
    if (port >= eHx711Api_Last) {
        return false;
    }

    sGpioDriver_Static_t pin_struct = {0};
    if (GPIO_Driver_GetPinInfo(g_static_hx711_api_lut[port].clock_pin, &pin_struct) == false) {
        return false;
    }

    pin_struct.mode = LL_GPIO_MODE_OUTPUT;
    pin_struct.speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
    pin_struct.output = LL_GPIO_OUTPUT_PUSHPULL;
    pin_struct.pull = LL_GPIO_PULL_NO;
    pin_struct.alternate = LL_GPIO_AF_0;

    if (GPIO_Driver_InitPin(g_static_hx711_api_lut[port].clock_pin, &pin_struct) == false) {
        return false;
    }

    if (GPIO_Driver_GetPinInfo(g_static_hx711_api_lut[port].data_pin, &pin_struct) == false) {
        return false;
    }

    pin_struct.mode = LL_GPIO_MODE_INPUT;
    pin_struct.speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
    pin_struct.output = LL_GPIO_OUTPUT_PUSHPULL;
    pin_struct.pull = LL_GPIO_PULL_UP;
    pin_struct.alternate = LL_GPIO_AF_0;

    if (GPIO_Driver_InitPin(g_static_hx711_api_lut[port].data_pin, &pin_struct) == false) {
        return false;
    }

    GPIO_Driver_SetPin(g_static_hx711_api_lut[port].clock_pin);
    osDelay(10);
    GPIO_Driver_ResetPin(g_static_hx711_api_lut[port].clock_pin);
    osDelay(10);

    uint32_t measured_value = 0;
    HX711_API_ReadValue(port, &measured_value);
    HX711_API_ReadValue(port, &measured_value);

    return true;
}

static bool HX711_API_ReadValue (eHx711Api_Enum_t port, uint32_t *measured_value) {
    if (port >= eHx711Api_Last) {
        return false;
    }

    bool pin_state = false;
    if (GPIO_Driver_ReadInputPin(g_static_hx711_api_lut[port].data_pin, &pin_state) == false) {
        return false;
    }
    if (pin_state == true) {
        return false;
    }

    uint32_t value = 0;
    for (uint8_t i = 0; i < HX711_API_DATA_BITS; i++) {
        GPIO_Driver_SetPin(g_static_hx711_api_lut[port].clock_pin);
        delay(1);
        GPIO_Driver_ResetPin(g_static_hx711_api_lut[port].clock_pin);
        delay(1);
        value = value << 1;
        bool pin_state = false;
        GPIO_Driver_ReadInputPin(g_static_hx711_api_lut[port].data_pin, &pin_state);
        if (pin_state == true) {
            value += 1;
        }
    }
    *measured_value = value ^ 0x800000;
    GPIO_Driver_SetPin(g_static_hx711_api_lut[port].clock_pin);
    delay(1);
    GPIO_Driver_ResetPin(g_static_hx711_api_lut[port].clock_pin);
    delay(1);
    return true;
}
/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool HX711_API_MeasureWeight (eHx711Api_Enum_t port) {
    if (port >= eHx711Api_Last) {
        return false;
    }
    if (g_dynamic_hx711_api_lut[port].enabled == false) {
        return false;
    }
    GPIO_Driver_ResetPin(g_static_hx711_api_lut[port].clock_pin);
    uint32_t value = 0;
    if (HX711_API_ReadValue(port, &value) == false) {
        return false;
    }
    if (value >= g_dynamic_hx711_api_lut[port].callibration_offset) {
        g_dynamic_hx711_api_lut[port].measusred_weight = value - g_dynamic_hx711_api_lut[port].callibration_offset;
    } else {
        g_dynamic_hx711_api_lut[port].measusred_weight = 0;
    }
    return true;
}

bool HX711_API_GetWeight (eHx711Api_Enum_t port, uint32_t *weight) {
    if (port >= eHx711Api_Last) {
        return false;
    }
    if (g_dynamic_hx711_api_lut[port].enabled == false) {
        return false;
    }
    *weight = g_dynamic_hx711_api_lut[port].measusred_weight;
    return true;
}

bool HX711_API_Callibrate (eHx711Api_Enum_t port) {
    if (port >= eHx711Api_Last) {
        return false;
    }
    if (g_dynamic_hx711_api_lut[port].enabled == false) {
        return false;
    }
    uint32_t value = 0;
    if (HX711_API_ReadValue(port, &value) == false) {
        return false;
    }
    g_dynamic_hx711_api_lut[port].callibration_offset = value;

    return true;
}

bool HX711_API_Enable (eHx711Api_Enum_t port) {
    if (port >= eHx711Api_Last) {
        return false;
    }
    if (g_dynamic_hx711_api_lut[port].enabled == true) {
        return true;
    }
    if (HX711_API_Init(port) == false) {
        return false;
    }
    g_dynamic_hx711_api_lut[port].enabled = true;
    return true;
}

bool HX711_API_Disable (eHx711Api_Enum_t port) {
    if (port >= eHx711Api_Last) {
        return false;
    }
    if (g_dynamic_hx711_api_lut[port].enabled == false) {
        return true;
    }
    g_dynamic_hx711_api_lut[port].enabled = false;
    return true;
}
