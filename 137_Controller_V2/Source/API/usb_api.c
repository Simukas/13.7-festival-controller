/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include <usb_api.h>
#include "usbd_def.h"
#include "usbd_desc.h"
#include "cmsis_os2.h"
#include "debug_api.h"
#include "heap_api.h"
#include "baudrate_utils.h"
#include "gpio_driver.h"
#include "thread_info.h"
#include "data_ring_buffer.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
DEBUG_MODULE(USB_API)
#define USB_API_MESSAGE_QUEUE_COUNT 3
#define USB_API_MAX_MESSAGE_LENGTH 100

#define USB_API_MESSAGE_DELIMITERS "\n\r"

#define USB_API_EVENT_NEW_DATA 0b0001

#define USB_API_RX_BUFFER_SIZE      1000
#define USB_API_RING_BUFFER_SIZE    1000

#define USB_API_LOCK_TIMEOUT 0
/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/
typedef enum eUsbApi_State_Enum_t {
    eUsbApi_State_First = 0,
        eUsbApi_State_Setup = eUsbApi_State_First,
        eUsbApi_State_Collect,
        eUsbApi_State_Flush,
        eUsbApi_State_Last
} eUsbApi_State_Enum_t;
/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/
void OTG_FS_IRQHandler (void);
static int8_t USB_API_InitFS (void);
static int8_t USB_API_DeinitFS (void);
static int8_t USB_API_ControlFS (uint8_t command, uint8_t *data, uint16_t data_length);
static int8_t USB_API_ReceiveFS (uint8_t *data, uint32_t *length);
static int8_t USB_API_TransmitCompleteFS (uint8_t *data, uint32_t *length, uint8_t epnum);
static bool USB_API_TransmitFS (uint8_t *data, uint16_t length, uint32_t timeout);
static bool USB_API_GetByte (uint8_t *byte);
static void USB_API_Task (void *argument);
/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
static const osMutexAttr_t g_usb_api_mutex_attributes = {
    "debug_api_mutex",
    osMutexRecursive | osMutexPrioInherit,
    NULL,
    0U
};

static const osThreadAttr_t g_usb_api_task_attributes = {
    .name = "usb_api_task",
    .stack_size = USB_API_TASK_STACK_SIZE,
    .priority = (osPriority_t) USB_API_TASK_PRIORITY,
};
/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/
static USBD_CDC_LineCodingTypeDef g_usb_api_line_coding = {
    .bitrate = 115200, /* baud rate */
    .format = 0x00, /* stop bits - 1 */
    .paritytype = 0x00, /* parity - none */
    .datatype = 0x08 /* number of bits - 8 */
};
extern PCD_HandleTypeDef hpcd_USB_OTG_FS;
static USBD_HandleTypeDef g_usb_api_usb_device = {0};

USBD_CDC_ItfTypeDef g_usb_api_usb_interface = {
    .Init = USB_API_InitFS,
    .DeInit = USB_API_DeinitFS,
    .Control = USB_API_ControlFS,
    .Receive = USB_API_ReceiveFS,
    .TransmitCplt = USB_API_TransmitCompleteFS
};

static uint8_t g_usb_api_rx_buffer[USB_API_RX_BUFFER_SIZE] = {0};
static uint8_t *g_usb_api_tx_buffer_pointer = NULL;
static data_rb_handle_t g_usb_api_rb_handle = NULL;

static osMutexId_t g_usb_api_mutex_id = NULL;
static osMessageQueueId_t g_usb_api_message_queue_id = NULL;
static osThreadId_t g_usb_api_task_handle = NULL;
static osEventFlagsId_t g_usb_api_event_flag_id = NULL;
static osSemaphoreId_t g_usb_api_semaphore_id = NULL;

static eUsbApi_State_Enum_t g_usb_api_fsm_state = eUsbApi_State_Setup;
static uint32_t g_usb_api_collection_index = 0;
static uint8_t *g_usb_api_collection_buffer = NULL;

static volatile bool g_usb_api_connected = false;
/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/
static int8_t USB_API_InitFS (void) {
    USBD_CDC_SetRxBuffer(&g_usb_api_usb_device, g_usb_api_rx_buffer);
    if (osSemaphoreRelease(g_usb_api_semaphore_id) != osOK) {
        Error_Handler();
    }
    g_usb_api_connected = true;
    return (USBD_OK);
}

static int8_t USB_API_DeinitFS (void) {
    if (osSemaphoreAcquire(g_usb_api_semaphore_id, 0U) != osOK) {
        Error_Handler();
    }
    g_usb_api_connected = false;
    return (USBD_OK);
}

static int8_t USB_API_ControlFS (uint8_t command, uint8_t *data, uint16_t data_length) {
    switch (command) {
        case CDC_SEND_ENCAPSULATED_COMMAND:

            break;

        case CDC_GET_ENCAPSULATED_RESPONSE:

            break;

        case CDC_SET_COMM_FEATURE:

            break;

        case CDC_GET_COMM_FEATURE:

            break;

        case CDC_CLEAR_COMM_FEATURE:

            break;

        case CDC_SET_LINE_CODING: {
            g_usb_api_line_coding.bitrate = (uint32_t) (data[0] | (data[1] << 8) | (data[2] << 16) | (data[3] << 24));
            g_usb_api_line_coding.format = data[4];
            g_usb_api_line_coding.paritytype = data[5];
            g_usb_api_line_coding.datatype = data[6];
            break;
        }

        case CDC_GET_LINE_CODING: {
            data[0] = (uint8_t) (g_usb_api_line_coding.bitrate);
            data[1] = (uint8_t) (g_usb_api_line_coding.bitrate >> 8);
            data[2] = (uint8_t) (g_usb_api_line_coding.bitrate >> 16);
            data[3] = (uint8_t) (g_usb_api_line_coding.bitrate >> 24);
            data[4] = g_usb_api_line_coding.format;
            data[5] = g_usb_api_line_coding.paritytype;
            data[6] = g_usb_api_line_coding.datatype;
            break;
        }

        case CDC_SET_CONTROL_LINE_STATE:

            break;

        case CDC_SEND_BREAK:

            break;

        default:
            break;
    }

    return (USBD_OK);
}

static int8_t USB_API_ReceiveFS (uint8_t *data, uint32_t *length) {
    //USBD_CDC_SetRxBuffer(&g_usb_api_usb_device, &data[0]);
    for (uint32_t i = 0; i < *length; i++) {
        Data_RB_Push(g_usb_api_rb_handle, data[i]);
    }
    uint32_t set_flags = osEventFlagsSet(g_usb_api_event_flag_id, USB_API_EVENT_NEW_DATA);
    if (set_flags != USB_API_EVENT_NEW_DATA) {
        error("Failed to set new data flag!\r");
    }
    USBD_CDC_ReceivePacket(&g_usb_api_usb_device);
    return (USBD_OK);
}

static bool USB_API_TransmitFS (uint8_t *data, uint16_t length, uint32_t timeout) {
    if (data == NULL) {
        return false;
    }
    if (length == 0) {
        return false;
    }
    if (osSemaphoreAcquire(g_usb_api_semaphore_id, timeout) != osOK) {
        return false;
    }
    USBD_CDC_HandleTypeDef *cdc_handle = (USBD_CDC_HandleTypeDef*) g_usb_api_usb_device.pClassData;
    if (cdc_handle->TxState != 0) {
        return false;
    }

    g_usb_api_tx_buffer_pointer = Heap_API_Calloc(length, sizeof(uint8_t));
    if (g_usb_api_tx_buffer_pointer == NULL) {
        return false;
    }

    memcpy(g_usb_api_tx_buffer_pointer, data, length);

    USBD_CDC_SetTxBuffer(&g_usb_api_usb_device, g_usb_api_tx_buffer_pointer, length);
    if (USBD_CDC_TransmitPacket(&g_usb_api_usb_device) != USBD_OK) {
        return false;
    }
    return true;
}

static int8_t USB_API_TransmitCompleteFS (uint8_t *data, uint32_t *length, uint8_t epnum) {
    if (Heap_API_FreeISR(g_usb_api_tx_buffer_pointer) == false) {
        Error_Handler();
    }
    if (osSemaphoreRelease(g_usb_api_semaphore_id) != osOK) {
        Error_Handler();
    }
    UNUSED(data);
    UNUSED(length);
    UNUSED(epnum);
    return USBD_OK;
}

static bool USB_API_GetByte (uint8_t *byte) {
    if (byte == NULL) {
        return false;
    }
    if (Data_RB_Pop(g_usb_api_rb_handle, byte) == false) {
        return false;
    }
    return true;
}

static void USB_API_Task (void *argument) {
    if (GPIO_Driver_InitPin(eGpioDriver_Pin_USB_DM, NULL) == false) {
        Error_Handler();
    }
    if (GPIO_Driver_InitPin(eGpioDriver_Pin_USB_DP, NULL) == false) {
        Error_Handler();
    }
    if (USBD_Init(&g_usb_api_usb_device, &FS_Desc, DEVICE_FS) != USBD_OK) {
        Error_Handler();
    }
    if (USBD_RegisterClass(&g_usb_api_usb_device, &USBD_CDC) != USBD_OK) {
        Error_Handler();
    }
    if (USBD_CDC_RegisterInterface(&g_usb_api_usb_device, &g_usb_api_usb_interface) != USBD_OK) {
        Error_Handler();
    }
    if (USBD_Start(&g_usb_api_usb_device) != USBD_OK) {
        Error_Handler();
    }
    while (true) {
        uint32_t received_flag = osEventFlagsWait(g_usb_api_event_flag_id, USB_API_EVENT_NEW_DATA, osFlagsWaitAny, osWaitForever);
        if (received_flag != USB_API_EVENT_NEW_DATA) {
            error("Failed to get new data flag!\r");
            continue;
        }
        switch (g_usb_api_fsm_state) {
            case eUsbApi_State_Setup: {
                g_usb_api_collection_index = 0;
                g_usb_api_collection_buffer = Heap_API_Calloc(USB_API_MAX_MESSAGE_LENGTH, sizeof(uint8_t));
                if (g_usb_api_collection_buffer == NULL) {
                    error("Failed to allocate FSM buffer!\r");
                    continue;
                }
                g_usb_api_fsm_state = eUsbApi_State_Collect;
            }
            case eUsbApi_State_Collect: {
                uint8_t byte = 0;
                while (USB_API_GetByte(&byte) == true) {
                    if (strchr(USB_API_MESSAGE_DELIMITERS, (char) byte) != NULL) {
                        g_usb_api_fsm_state = eUsbApi_State_Flush;
                    } else {
                        g_usb_api_collection_buffer[g_usb_api_collection_index] = byte;
                        g_usb_api_collection_index++;
                        if (g_usb_api_collection_index == USB_API_MAX_MESSAGE_LENGTH) {
                            if (Heap_API_Free(g_usb_api_collection_buffer, USB_API_MAX_MESSAGE_LENGTH) == false) {
                                error("Failed to free FSM buffer!\r");
                            }
                            g_usb_api_fsm_state = eUsbApi_State_Setup;
                            error("FSM buffer overflow!\r");
                            continue;
                        }
                    }
                }
                if (g_usb_api_fsm_state != eUsbApi_State_Flush) {
                    break;
                }
            }
            case eUsbApi_State_Flush: {
                sMessage_t message = {
                    .buffer = g_usb_api_collection_buffer,
                    .buffer_size = g_usb_api_collection_index
                };
                if (osMessageQueuePut(g_usb_api_message_queue_id, &message, 0, osWaitForever) != osOK) {
                    error("Failed to put message into queue!\r");
                    continue;
                }
                g_usb_api_fsm_state = eUsbApi_State_Setup;
                break;
            }
            default: {
                break;
            }
        }
    }
}
/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool USB_API_Init (eBaudrate_Enum_t baudrate) {
    if (baudrate >= eBaudrate_Last) {
        return false;
    }
    g_usb_api_line_coding.bitrate = getBaudrate(baudrate);
    g_usb_api_rb_handle = Data_RB_Init(USB_API_RING_BUFFER_SIZE);
    if (g_usb_api_rb_handle == NULL) {
        return false;
    }
    g_usb_api_mutex_id = osMutexNew(&g_usb_api_mutex_attributes);
    if (g_usb_api_mutex_id == NULL) {
        return false;
    }
    g_usb_api_message_queue_id = osMessageQueueNew(USB_API_MESSAGE_QUEUE_COUNT, sizeof(sUsbApi_Message_t), NULL);
    if (g_usb_api_message_queue_id == NULL) {
        return false;
    }
    g_usb_api_event_flag_id = osEventFlagsNew(NULL);
    if (g_usb_api_event_flag_id == NULL) {
        return false;
    }
    g_usb_api_semaphore_id = osSemaphoreNew(1, 1, NULL);
    if (g_usb_api_semaphore_id == NULL) {
        return false;
    }
    if (osSemaphoreAcquire(g_usb_api_semaphore_id, 0U) != osOK) {
        return false;
    }
    g_usb_api_task_handle = osThreadNew(USB_API_Task, NULL, &g_usb_api_task_attributes);
    if (g_usb_api_task_handle == NULL) {
        return false;
    }
    return true;
}

bool USB_API_SendData (uint8_t *data, uint16_t length, uint32_t timeout) {
    if (g_usb_api_connected == false) {
        return false;
    }
    if (data == NULL) {
        return false;
    }
    if (length == 0) {
        return false;
    }
    if (osMutexAcquire(g_usb_api_mutex_id, timeout) != osOK) {
        return false;
    }
    if (USB_API_TransmitFS(data, length, timeout) == false) {
        return false;
    }
    if (osMutexRelease(g_usb_api_mutex_id) != osOK) {
        return false;
    }
    return true;
}

bool USB_API_SendString (char *string, uint16_t max_length, uint32_t timeout) {
    if (g_usb_api_connected == false) {
        return false;
    }
    if (string == NULL) {
        return false;
    }
    if (osMutexAcquire(g_usb_api_mutex_id, timeout) != osOK) {
        return false;
    }
    USB_API_TransmitFS((uint8_t*) string, strnlen(string, max_length), timeout);
    if (osMutexRelease(g_usb_api_mutex_id) != osOK) {
        return false;
    }
    return true;
}

bool USB_API_Lock (uint32_t timeout) {
    if (g_usb_api_connected == false) {
        return false;
    }
    if (osMutexAcquire(g_usb_api_mutex_id, timeout) != osOK) {
        return false;
    }
    return true;
}

bool USB_API_Unlock (void) {
    if (g_usb_api_connected == false) {
        return false;
    }
    if (osMutexRelease(g_usb_api_mutex_id) != osOK) {
        return false;
    }
    return true;
}

bool USB_API_GetMessage (sUsbApi_Message_t *message) {
    if (message == NULL) {
        return false;
    }
    if (osMessageQueueGet(g_usb_api_message_queue_id, message, 0U, osWaitForever) != osOK) {
        return false;
    }
    return true;
}

void OTG_FS_IRQHandler (void) {
    HAL_PCD_IRQHandler(&hpcd_USB_OTG_FS);
}
