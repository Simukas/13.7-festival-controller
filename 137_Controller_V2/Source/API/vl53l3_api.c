/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "vl53l3_api.h"

#include <stdbool.h>

#include "vl53lx_api.h"
#include "vl53lx_core.h"
#include "debug_api.h"
#include "i2c_driver.h"
#include "gpio_driver.h"
#include "cmsis_os.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
DEBUG_MODULE(VL53L3_API)
#define MEASUREMENT_TIMING_BUDGET_US 100000
#define K_COEFFICIENT                (1.1317) // 1/0.8836
#define VL53L3_I2C_DEFAULT_ADDRESS   0x52
#define FIRST_MEASUREMENT_THRESHOLD  1000
#define FSM_BOOT_PERIOD              2
/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/
typedef struct sVL53L3_Static_t {
    eGpioDriver_Pin_Enum_t xshut_pin;
    sGpioDriver_Static_t pin_data;
    uint8_t address;
    eI2cDriver_Enum_t i2c;
} sVL53L3_Static_t;

typedef struct sVL53L3_Dynamic_t {
    VL53LX_Dev_t sensor_config_lut;
    bool enabled;
    uint16_t static_error;
    uint16_t measured_distance;
} sVL53L3_Dynamic_t;
/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
static const sVL53L3_Static_t g_vl53l3_api_static_lut[eVL53L3Api_Last] = {
    [eVL53L3Api_Primary] = {
        .xshut_pin = eGpioDriver_Pin_GPIO_1,
        .address = 0x54,
        .i2c = eI2cDriver_2
    },
    [eVL53L3Api_Secondary] = {
        .xshut_pin = eGpioDriver_Pin_GPIO_2,
        .address = 0x56,
        .i2c = eI2cDriver_2
    }
};
/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/
static sVL53L3_Dynamic_t g_vl53l3_api_dynamic_lut[eVL53L3Api_Last] = {
    [eVL53L3Api_Primary] = {.enabled = false},
    [eVL53L3Api_Secondary] = {.enabled = false}
};
/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/
static bool VL53L3_API_Init (eVL53L3Api_Enum_t sensor);
/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/
static bool VL53L3_API_Init (eVL53L3Api_Enum_t sensor) {
    if (sensor >= eVL53L3Api_Last) {
        return false;
    }

    if (GPIO_Driver_InitPin(g_vl53l3_api_static_lut[sensor].xshut_pin, NULL) == false) {
        error("Failed to init xshut!\r");
        return false;
    }

    if (GPIO_Driver_ResetPin(g_vl53l3_api_static_lut[sensor].xshut_pin) == false) {
        error("Failed to set xshut!\r");
        return false;
    }

    osDelay(FSM_BOOT_PERIOD);

    if (GPIO_Driver_SetPin(g_vl53l3_api_static_lut[sensor].xshut_pin) == false) {
        error("Failed to set xshut!\r");
        return false;
    }

    osDelay(FSM_BOOT_PERIOD);

    g_vl53l3_api_dynamic_lut[sensor].sensor_config_lut.i2c_slave_address = VL53L3_I2C_DEFAULT_ADDRESS;

    if (VL53LX_WaitDeviceBooted(&g_vl53l3_api_dynamic_lut[sensor].sensor_config_lut) != VL53LX_ERROR_NONE) {
        error("Failed to boot device!\r");
        return false;
    }

    if (VL53LX_DataInit(&g_vl53l3_api_dynamic_lut[sensor].sensor_config_lut) != VL53LX_ERROR_NONE) {
        error("Failed to initialize sensor data!\r");
        return false;
    }

    if (VL53LX_SetDeviceAddress(&g_vl53l3_api_dynamic_lut[sensor].sensor_config_lut, g_vl53l3_api_static_lut[sensor].address) != VL53LX_ERROR_NONE) {
        error("Failed to change sensor address!\r");
        return false;
    }

    g_vl53l3_api_dynamic_lut[sensor].sensor_config_lut.i2c_slave_address = g_vl53l3_api_static_lut[sensor].address;

    if (VL53LX_SetDistanceMode(&g_vl53l3_api_dynamic_lut[sensor].sensor_config_lut, VL53LX_DISTANCEMODE_SHORT) != VL53LX_ERROR_NONE) {
        error("Failed to set distance mode!\r");
        return false;
    }

    if (VL53LX_SetMeasurementTimingBudgetMicroSeconds(&g_vl53l3_api_dynamic_lut[sensor].sensor_config_lut, MEASUREMENT_TIMING_BUDGET_US) != VL53LX_ERROR_NONE) {
        error("Failed to set timing budget!\r");
        return false;
    }

    if (VL53LX_StartMeasurement(&g_vl53l3_api_dynamic_lut[sensor].sensor_config_lut) != VL53LX_ERROR_NONE) {
        error("Error starting measurement!\r");
        return false;
    }

    return true;
}
/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool VL53L3_API_MeasureDistance (eVL53L3Api_Enum_t sensor) {
    if (sensor >= eVL53L3Api_Last) {
        return false;
    }
    if (g_vl53l3_api_dynamic_lut[sensor].enabled == false) {
        return false;
    }

    VL53LX_MultiRangingData_t ranging_data = {0};

    if (VL53LX_WaitMeasurementDataReady(&g_vl53l3_api_dynamic_lut[sensor].sensor_config_lut) != VL53LX_ERROR_NONE) {
        return false;
    }

    if (VL53LX_GetMultiRangingData(&g_vl53l3_api_dynamic_lut[sensor].sensor_config_lut, &ranging_data) != VL53LX_ERROR_NONE) {
        return false;
    }

    if (VL53LX_ClearInterruptAndStartMeasurement(&g_vl53l3_api_dynamic_lut[sensor].sensor_config_lut) != VL53LX_ERROR_NONE) {
        return false;
    }

    g_vl53l3_api_dynamic_lut[sensor].measured_distance = ((ranging_data.RangeData[0].RangeMilliMeter - g_vl53l3_api_dynamic_lut[sensor].static_error) * K_COEFFICIENT); // - STATIC_ERROR_MM;

    return true;
}

bool VL53L3_API_GetDistance (eVL53L3Api_Enum_t sensor, uint16_t *distance) {
    if (sensor >= eVL53L3Api_Last) {
        return false;
    }
    if (g_vl53l3_api_dynamic_lut[sensor].enabled == false) {
        return false;
    }
    if (distance == NULL) {
        return false;
    }

    *distance = g_vl53l3_api_dynamic_lut[sensor].measured_distance;
    return true;
}

bool VL53L3_API_Enable (eVL53L3Api_Enum_t sensor) {
    if (sensor >= eVL53L3Api_Last) {
        return false;
    }
    if (g_vl53l3_api_dynamic_lut[sensor].enabled == true) {
        return true;
    }

    if (I2C_Driver_Init(eI2cDriver_2) == false) {
        error("Failed to initialize i2c driver!\r");
        return false;
    }

    if (VL53L3_API_Init(sensor) == false) {
        error("Failed to initialize the sensor!\r");
        return false;
    }

    osDelay(FSM_BOOT_PERIOD); //Need to wait for device's internal fsm to boot

    if (VL53LX_StartMeasurement(&g_vl53l3_api_dynamic_lut[sensor].sensor_config_lut) != VL53LX_ERROR_NONE) {
        return false;
    }

    g_vl53l3_api_dynamic_lut[sensor].enabled = true;

    return true;
}

bool VL53L3_API_Disable (eVL53L3Api_Enum_t sensor) {
    if (sensor >= eVL53L3Api_Last) {
        return false;
    }
    if (g_vl53l3_api_dynamic_lut[sensor].enabled == false) {
        return false;
    }

    if (GPIO_Driver_ResetPin(g_vl53l3_api_static_lut[sensor].xshut_pin) == false) {
        error("Failed to reset xshut!\r");
        return false;
    }

    g_vl53l3_api_dynamic_lut[sensor].enabled = false;

    return true;
}

bool VL53L3_API_Calibrate (eVL53L3Api_Enum_t sensor) {
    if (sensor >= eVL53L3Api_Last) {
        return false;
    }
    if (g_vl53l3_api_dynamic_lut[sensor].enabled == false) {
        return false;
    }

    if (VL53L3_API_MeasureDistance(sensor) == false) {
        error("Failed to get calibration offset!\r", sensor);
        return false;
    }

    uint16_t distance = 0;
    if (VL53L3_API_GetDistance(sensor, &distance) == false) {
        error("Failed to calibrate %d sensor!\r", sensor);
        return false;
    }

    g_vl53l3_api_dynamic_lut[sensor].static_error = distance;

    return true;
}
