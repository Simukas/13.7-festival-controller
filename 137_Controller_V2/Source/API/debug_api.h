#ifndef __DEBUG_API__H__
#define __DEBUG_API__H__

/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "stdio.h"
#include "string.h"
#include <stdarg.h>
#include <stdint.h>
#include <stdbool.h>
#include "uart_api.h"
#include "uart_driver.h"
/**********************************************************************************************************************
 * Exported definitions and macros
 *********************************************************************************************************************/
#define DISABLE_DEBUG
#ifndef DISABLE_DEBUG
#define DEBUG_MODULE(module_name)   static const char *debug_module_name = #module_name;
#define debug(...) Debug_API_Log(debug_module_name, eDebugApi_PrintType_Debug, NULL, 0, __VA_ARGS__)
#define error(...) Debug_API_Log(debug_module_name, eDebugApi_PrintType_Error, __FILE__, __LINE__, __VA_ARGS__)
#define print(string) Debug_API_Print((char* )string, sizeof(string))
#define printF(...) Debug_API_PrintF(__VA_ARGS__)
#else
#define DEBUG_MODULE(void)
#define debug(...) ((void)__VA_ARGS__)
#define error(...) ((void)__VA_ARGS__)
#define print(void)
#define printF(...) ((void)__VA_ARGS__)
#endif
/**********************************************************************************************************************
 * Exported types
 *********************************************************************************************************************/
typedef enum eDebugApi_PrintType_Enum_t {
    eDebugApi_PrintType_First = 0,
    eDebugApi_PrintType_Debug = eDebugApi_PrintType_First,
    eDebugApi_PrintType_Error,
    eDebugApi_PrintType_Last
} eDebugApi_PrintType_Enum_t;
/**********************************************************************************************************************
 * Exported variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of exported functions
 *********************************************************************************************************************/
bool Debug_API_Init (void);
void Debug_API_Log (const char *debug_module_name, eDebugApi_PrintType_Enum_t print_type, char *file, uint16_t line, char *format, ...);
void Debug_API_Print (char *string, uint32_t max_length);
void Debug_API_PrintF (char *format, ...);
bool Debug_API_Lock (void);
bool Debug_API_Unlock (void);
#endif /* __DEBUG_API__H__ */
