/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "servo_api.h"
#include "motor_api.h"
#include "adc_driver.h"
#include "stdbool.h"
#include "cmsis_os2.h"
#include "math_utils.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
DEBUG_MODULE(SERVO_API)

#define SERVO_API_UPDATE_PERIOD 100
#define PWM_SERVO_DUTY_CYCLE_OFFSET 63 // = MAX_DUTY_CYCLE * 0.025
/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/
typedef enum eServoApi_Type_Enum_t {
    eServoApi_Type_First = 0,
        eServoApi_Type_Pwm = eServoApi_Type_First,
        eServoApi_Type_Motor,
        eServoApi_Type_Last
} eServoApi_Type_Enum_t;

typedef struct sServoApi_Static_t {
    eServoApi_Type_Enum_t type;
    eTimerDriver_Timer_Enum_t timer;
    eTimerDriver_OC_Enum_t output;
    eMotorApi_Motor_Enum_t motor;
    eAdcDriver_Channel_Enum_t feedback_input;
    uint8_t target_min_position;
    uint8_t target_max_position;
} sServoApi_Static_t;

typedef struct sServoApi_Dynamic_t {
    bool enabled;
    uint8_t target_position;
    uint8_t last_position;
    uint8_t speed;
} sServoApi_Dynamic_t;
/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
static const sServoApi_Static_t g_static_servo_api_lut[eServoApi_Last] = {
    [eServoApi_Pwm] = {
        .type = eServoApi_Type_Pwm,
        .output = eTimerDriver_OC_1,
        .timer = eTimerDriver_Timer_2,
        .target_min_position = 0,
        .target_max_position = 255,
    },
    [eServoApi_Primary] = {
        .type = eServoApi_Type_Motor,
        .motor = eMotorApi_Motor_Primary,
        .timer = eTimerDriver_Timer_5,
        .feedback_input = eAdcDriver_Channel_2,
        .target_min_position = 0,
        .target_max_position = 255,
    },
    [eServoApi_Secondary] = {
        .type = eServoApi_Type_Motor,
        .motor = eMotorApi_Motor_Secondary,
        .timer = eTimerDriver_Timer_5,
        .feedback_input = eAdcDriver_Channel_1,
        .target_min_position = 0,
        .target_max_position = 255,
    },
};
/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/
static osTimerId_t g_servo_api_timer = NULL;
/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/
static sServoApi_Dynamic_t g_dynamic_servo_api_lut[eServoApi_Last] = {
    [eServoApi_Pwm] = {0},
    [eServoApi_Primary] = {0},
    [eServoApi_Secondary] = {0},
};
/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/
bool Servo_API_Init (eServoApi_Enum_t servo);
static void Servo_API_Timer_Callback (void *argument);
/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/
static void Servo_API_Timer_Callback (void *argument) {
    ADC_Driver_ReadChannels(eAdcDriver_Adc_1);
    uint16_t adc_value = 0;
    uint8_t current_position = 0;
    uint8_t target_position = 0;
    uint8_t speed = 0;

    for (eServoApi_Enum_t servo = eServoApi_First; servo < eServoApi_Last; servo++) {
        if (g_static_servo_api_lut[servo].type == eServoApi_Type_Motor) {
            if (g_dynamic_servo_api_lut[servo].enabled == true) {
                ADC_Driver_GetChannelValue(g_static_servo_api_lut[servo].feedback_input, &adc_value);
                current_position = map(adc_value, 0, 4095, 0, 255);
                target_position = g_dynamic_servo_api_lut[servo].target_position;

                if (current_position > target_position) {
                    Motor_API_SetDirection(g_static_servo_api_lut[servo].motor, eMotorApi_Direction_Left);
                    speed = current_position - target_position;
                }
                if (current_position < target_position) {
                    Motor_API_SetDirection(g_static_servo_api_lut[servo].motor, eMotorApi_Direction_Right);
                    speed = target_position - current_position;
                }

                Motor_API_SetSpeed(g_static_servo_api_lut[servo].motor, speed);

                g_dynamic_servo_api_lut[servo].last_position = current_position;
            }
        }
    }
}

bool Servo_API_Init (eServoApi_Enum_t servo) {
    if (servo >= eServoApi_Last) {
        error("Wrong servo provided!\r");
        return false;
    }

    switch (g_static_servo_api_lut[servo].type) {
        case eServoApi_Type_Pwm: {
            if (Timer_Driver_Init(g_static_servo_api_lut[servo].timer) == false) {
                error("Failed to initialize Timer Driver!\r");
                return false;
            }
            if (Timer_Driver_Start(g_static_servo_api_lut[servo].timer) == false) {
                error("Failed to start Timer!\r");
                return false;
            }
            break;
        }
        case eServoApi_Type_Motor: {
            if (g_servo_api_timer == NULL) {
                g_servo_api_timer = osTimerNew(Servo_API_Timer_Callback, osTimerPeriodic, NULL, NULL);
                if (g_servo_api_timer == NULL) {
                    error("Failed to create Servo API timer!\r");
                    return false;
                }
                if (osTimerStart(g_servo_api_timer, SERVO_API_UPDATE_PERIOD) != osOK) {
                    error("Failed to start Servo API timer!\r");
                    return false;
                }
            }
            break;
        }
        default: {
            return false;
        }
    }

    return true;
}
/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool Servo_API_Enable (eServoApi_Enum_t servo) {
    if (servo >= eServoApi_Last) {
        error("Wrong servo provided!\r");
        return false;
    }
    if (g_dynamic_servo_api_lut[servo].enabled == true) {
        return true;
    }
    if (Servo_API_Init(servo) == false) {
        return false;
    }
    switch (g_static_servo_api_lut[servo].type) {
        case eServoApi_Type_Pwm: {
            Timer_Driver_EnableChannel(g_static_servo_api_lut[servo].timer, g_static_servo_api_lut[servo].output);
            break;
        }
        case eServoApi_Type_Motor: {
            Motor_API_Enable(g_static_servo_api_lut[servo].motor);
            break;
        }
        default: {
            return false;
        }
    }
    g_dynamic_servo_api_lut[servo].enabled = true;
    return true;
}

bool Servo_API_Disable (eServoApi_Enum_t servo) {
    if (servo >= eServoApi_Last) {
        error("Wrong servo provided!\r");
        return false;
    }
    if (g_dynamic_servo_api_lut[servo].enabled == false) {
        return true;
    }
    g_dynamic_servo_api_lut[servo].enabled = false;
    switch (g_static_servo_api_lut[servo].type) {
        case eServoApi_Type_Pwm: {
            Timer_Driver_DisableChannel(g_static_servo_api_lut[servo].timer, g_static_servo_api_lut[servo].output);
            break;
        }
        case eServoApi_Type_Motor: {
            Motor_API_Disable(g_static_servo_api_lut[servo].motor);
            break;
        }
        default: {
            break;
        }
    }
    return true;
}

bool Servo_API_SetPosition (eServoApi_Enum_t servo, uint8_t position) {
    if (servo >= eServoApi_Last) {
        error("Wrong servo provided!\r");
        return false;
    }
    if ((position < g_static_servo_api_lut[servo].target_min_position) || (position > g_static_servo_api_lut[servo].target_max_position)) {
        error("Requested position out of bounds!\r");
        return false;
    }

    switch (g_static_servo_api_lut[servo].type) {
        case eServoApi_Type_Pwm: {
            Timer_Driver_SetDutyCycle(g_static_servo_api_lut[servo].timer, g_static_servo_api_lut[servo].output, position + PWM_SERVO_DUTY_CYCLE_OFFSET);
            break;
        }
        case eServoApi_Type_Motor: {
            g_dynamic_servo_api_lut[servo].target_position = position;
            break;
        }
        default: {
            break;
        }
    }
    return true;
}

