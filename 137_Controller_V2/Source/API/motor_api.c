/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "motor_api.h"
#include "debug_api.h"
#include "timer_driver.h"
#include "gpio_driver.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
DEBUG_MODULE(MOTOR_API)

#define MOTOR_API_MAX_SPEED_VALUE 255
#define MOTOR_API_MIN_SPEED_VALUE 1
/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/
typedef struct sMotorApi_Static_t {
    eTimerDriver_OC_Enum_t a1_channel;
    eTimerDriver_OC_Enum_t a2_channel;
} sMotorApi_Static_t;

typedef struct sMotorApi_Dynamic_t {
    uint8_t speed;
    eMotorApi_Direction_Enum_t direction;
    bool enabled;
} sMotorApi_Dynamic_t;
/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
const static sMotorApi_Static_t g_static_motor_api_lut[] = {
    [eMotorApi_Motor_Primary] = {.a1_channel = eTimerDriver_OC_3, .a2_channel = eTimerDriver_OC_4},
    [eMotorApi_Motor_Secondary] = {.a1_channel = eTimerDriver_OC_1, .a2_channel = eTimerDriver_OC_2},
};
/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/
static sMotorApi_Dynamic_t g_dynamic_motor_api_lut[] = {
    [eMotorApi_Motor_Primary] = {.direction = eMotorApi_Direction_Left, .speed = 0},
    [eMotorApi_Motor_Secondary] = {.direction = eMotorApi_Direction_Left, .speed = 0},
};
/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/
bool Motor_API_Init (void);
bool Motor_API_Update (eMotorApi_Motor_Enum_t motor);
/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/
bool Motor_API_Init (void) {
    if (Timer_Driver_Init(eTimerDriver_Timer_5) == false) {
        error("Failed to initialize Timer Driver!\r");
        return false;
    }
    if (Timer_Driver_Start(eTimerDriver_Timer_5) == false) {
        error("Failed to start Timer!\r");
        return false;
    }

    return true;
}

bool Motor_API_Update (eMotorApi_Motor_Enum_t motor) {
    if (g_dynamic_motor_api_lut[motor].direction == eMotorApi_Direction_Left) {
        Timer_Driver_SetDutyCycle(eTimerDriver_Timer_5, g_static_motor_api_lut[motor].a1_channel, g_dynamic_motor_api_lut[motor].speed);
        Timer_Driver_SetDutyCycle(eTimerDriver_Timer_5, g_static_motor_api_lut[motor].a2_channel, 0);
//        Timer_Driver_EnableChannel(eTimerDriver_Timer_5, g_static_motor_api_lut[motor].a1_channel);
//        Timer_Driver_DisableChannel(eTimerDriver_Timer_5, g_static_motor_api_lut[motor].a2_channel);
    } else {
        Timer_Driver_SetDutyCycle(eTimerDriver_Timer_5, g_static_motor_api_lut[motor].a2_channel, g_dynamic_motor_api_lut[motor].speed);
        Timer_Driver_SetDutyCycle(eTimerDriver_Timer_5, g_static_motor_api_lut[motor].a1_channel, 0);
//        Timer_Driver_EnableChannel(eTimerDriver_Timer_5, g_static_motor_api_lut[motor].a2_channel);
//        Timer_Driver_DisableChannel(eTimerDriver_Timer_5, g_static_motor_api_lut[motor].a1_channel);
    }
    return true;
}
/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool Motor_API_SetSpeed (eMotorApi_Motor_Enum_t motor, uint8_t speed) {
    if (motor >= eMotorApi_Motor_Last) {
        error("Invalid motor ID!\r");
        return false;
    }
    if (g_dynamic_motor_api_lut[motor].enabled == false) {
        error("Motor is not enabled!\r");
        return false;
    }
    if (speed > MOTOR_API_MAX_SPEED_VALUE) {
        error("Invalid motor speed!\r");
        return false;
    }

    g_dynamic_motor_api_lut[motor].speed = speed;

    if (Motor_API_Update(motor) == false) {
        error("Failed to update motor!\r");
        return false;
    }

    return true;
}

bool Motor_API_SetDirection (eMotorApi_Motor_Enum_t motor, eMotorApi_Direction_Enum_t direction) {
    if (motor >= eMotorApi_Motor_Last) {
        error("Invalid motor ID!\r");
        return false;
    }
    if (g_dynamic_motor_api_lut[motor].enabled == false) {
        error("Motor is not enabled!\r");
        return false;
    }
    if (direction >= eMotorApi_Direction_Last) {
        error("Invalid motor direction!\r");
    }

    g_dynamic_motor_api_lut[motor].direction = direction;

    if (Motor_API_Update(motor) == false) {
        error("Failed to update motor!\r");
        return false;
    }

    return true;
}

bool Motor_API_Enable (eMotorApi_Motor_Enum_t motor) {
    if (motor >= eMotorApi_Motor_Last) {
        error("Invalid motor provided\n");
        return false;
    }
    if (g_dynamic_motor_api_lut[motor].enabled == true) {
        return true;
    }

    if (Motor_API_Init() == false) {
        error("Failed to initialize Motor API!\r");
        return false;
    }

    if (Timer_Driver_EnableChannel(eTimerDriver_Timer_5, g_static_motor_api_lut[motor].a1_channel) == false) {
        error("Failed to enable A1 channel!\r");
        return false;
    }
    if (Timer_Driver_EnableChannel(eTimerDriver_Timer_5, g_static_motor_api_lut[motor].a2_channel) == false) {
        error("Failed to enable A2 channel!\r");
        return false;
    }

    g_dynamic_motor_api_lut[motor].enabled = true;

    return true;
}

bool Motor_API_Disable (eMotorApi_Motor_Enum_t motor) {
    if (motor >= eMotorApi_Motor_Last) {
        error("Invalid motor provided\n");
        return false;
    }
    if (g_dynamic_motor_api_lut[motor].enabled == false) {
        return true;
    }

    if (Timer_Driver_DisableChannel(eTimerDriver_Timer_5, g_static_motor_api_lut[motor].a1_channel) == false) {
        error("Failed to disable A1 channel!\r");
        return false;
    }
    if (Timer_Driver_DisableChannel(eTimerDriver_Timer_5, g_static_motor_api_lut[motor].a2_channel) == false) {
        error("Failed to disable A2 channel!\r");
        return false;
    }

    g_dynamic_motor_api_lut[motor].speed = 0;
    g_dynamic_motor_api_lut[motor].direction = 0;

    if (Motor_API_Update(motor) == false) {
        error("Failed to update motor!\r");
        return false;
    }

    g_dynamic_motor_api_lut[motor].enabled = false;

    return true;
}
