#ifndef __UART_API__H__
#define __UART_API__H__

/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include "uart_driver.h"
#include "message_ring_buffer.h"
/**********************************************************************************************************************
 * Exported definitions and macros
 *********************************************************************************************************************/
// @formatter:off
typedef enum eUartApi_Enum_t {
    eUartApi_First,
    eUartApi_RS485 = eUartApi_First,
    eUartApi_Last,
} eUartApi_Enum_t;

typedef enum eUartApi_Protocol_Enum_t {
    eUartApi_Protocol_First = 0,
    eUartApi_Protocol_Binary = eUartApi_Protocol_First,
    eUartApi_Protocol_String,
    eUartApi_Protocol_Last
} eUartApi_Protocol_Enum_t;

/**********************************************************************************************************************
 * Exported types
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported variables
 *********************************************************************************************************************/
// @formatter:on
/**********************************************************************************************************************
 * Prototypes of exported functions
 *********************************************************************************************************************/
bool UART_API_Init (eUartApi_Enum_t uart, eBaudrate_Enum_t baudrate);
bool UART_API_SendString (eUartApi_Enum_t uart, const char *string, uint32_t timeout);
bool UART_API_SendData (eUartApi_Enum_t uart, uint8_t *data, uint16_t data_size, uint32_t timeout);
bool UART_API_GetMessage (eUartApi_Enum_t uart, sMessage_t *message, uint32_t timeout);
bool UART_API_SetBaudrate (eUartApi_Enum_t uart, eBaudrate_Enum_t baudrate);
bool UART_API_Lock (eUartApi_Enum_t uart, uint32_t timeout);
bool UART_API_Unlock (eUartApi_Enum_t uart);
#endif /* __UART_API__H__ */
