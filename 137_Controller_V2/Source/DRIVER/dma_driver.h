#ifndef __DMA_DRIVER__H__
#define __DMA_DRIVER__H__
/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "stdint.h"
#include "stdbool.h"
/**********************************************************************************************************************
 * Exported definitions and macros
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported types
 *********************************************************************************************************************/
// @formatter:off
//typedef enum eDmaDriver_DMA_Enum_t {
//    eDmaDriver_DMA_First = 0,
//    eDmaDriver_DMA1 = eDmaDriver_DMA_First,
//    eDmaDriver_DMA2,
//    eDmaDriver_DMA_Last,
//} eDmaDriver_DMA_Enum_t;

typedef enum eDmaDriver_Stream_Enum_t {
    eDmaDriver_Stream_First = 0,
    eDmaDriver_Stream_USART6_RX = eDmaDriver_Stream_First,
    eDmaDriver_Stream_USART6_TX,
    eDmaDriver_Stream_I2C2_RX,
    eDmaDriver_Stream_I2C2_TX,
    eDmaDriver_Stream_ARGB1,
    eDmaDriver_Stream_ARGB2,
    eDmaDriver_Stream_ARGB3,
    eDmaDriver_Stream_ADC,
    eDmaDriver_Stream_Last,
} eDmaDriver_Stream_Enum_t;

typedef enum eDmaDriver_IT_Enum_t {
    eDmaDriver_IT_First = 0,
    eDmaDriver_IT_TC = eDmaDriver_IT_First,
    eDmaDriver_IT_HT,
    eDmaDriver_IT_TE,
    eDmaDriver_IT_Last,
} eDmaDriver_IT_Enum_t;

typedef struct DMA_Driver_Init_t {
    eDmaDriver_Stream_Enum_t stream;
    void *peripheral_or_source_address;
    void *destination_address;
    uint16_t data_amount;
    void (*IT_callback) (eDmaDriver_Stream_Enum_t, eDmaDriver_IT_Enum_t);
} DMA_Driver_Init_t;
// @formatter:on
/**********************************************************************************************************************
 * Exported variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of exported functions
 *********************************************************************************************************************/
bool DMA_Driver_Init (DMA_Driver_Init_t *init_data);
bool DMA_Driver_EnableStream (eDmaDriver_Stream_Enum_t stream);
bool DMA_Driver_DisableStream (eDmaDriver_Stream_Enum_t stream);
bool DMA_Driver_ClearFlag (eDmaDriver_Stream_Enum_t stream, eDmaDriver_IT_Enum_t flag);
bool DMA_Driver_EnableEventInterrupt (eDmaDriver_Stream_Enum_t stream, eDmaDriver_IT_Enum_t flag);
bool DMA_Driver_DisableEventInterrupt (eDmaDriver_Stream_Enum_t stream, eDmaDriver_IT_Enum_t flag);
bool DMA_Driver_EnableStreamInterrupt (eDmaDriver_Stream_Enum_t stream);
bool DMA_Driver_DisableStreamInterrupt (eDmaDriver_Stream_Enum_t stream);
bool DMA_Driver_IsInterruptEnabled (eDmaDriver_Stream_Enum_t stream, eDmaDriver_IT_Enum_t flag, bool *return_value);
bool DMA_Driver_GetRemainingCount (eDmaDriver_Stream_Enum_t stream, uint16_t *remaining_data);
bool DMA_Driver_GetCompletedCount (eDmaDriver_Stream_Enum_t stream, uint16_t *remaining_data);
bool DMA_Driver_SetDataCount (eDmaDriver_Stream_Enum_t stream, uint16_t data_count);
bool DMA_Driver_SetDataPointer (eDmaDriver_Stream_Enum_t stream, uint8_t *data);
#endif /* __DMA_DRIVER__H__ */
