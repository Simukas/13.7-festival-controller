/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "rtc_driver.h"
#include "stdbool.h"
#include "stm32f4xx_ll_rtc.h"
#include "stm32f4xx_ll_rcc.h"
#include "stm32f4xx_ll_bus.h"
#include "time_utils.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
#define START_YEAR 2000
#define MAX_DAYS_IN_MONTH 31
#define MS_INVERSION_CONSTANT 999UL
/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/
typedef struct sRtcDriver_Static_t {
    void (*clock_function) (uint32_t);
    uint32_t clock;
    uint32_t hour_format;
    uint32_t rtc_format;
    uint32_t async_prescaler;
    uint32_t sync_prescaler;
    uint32_t clock_source;
} sRtcDriver_Static_t;
/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
static const sRtcDriver_Static_t g_static_rtc_driver_lut = {
    .clock_function = LL_APB1_GRP1_EnableClock,
    .clock = LL_APB1_GRP1_PERIPH_PWR,
    .clock_source = LL_RCC_RTC_CLKSOURCE_LSI,
    .hour_format = LL_RTC_HOURFORMAT_24HOUR,
    .rtc_format = LL_RTC_FORMAT_BCD,
    .async_prescaler = 31,
    .sync_prescaler = 999,
};
/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool RTC_Driver_Init (void) {
    g_static_rtc_driver_lut.clock_function(g_static_rtc_driver_lut.clock);
    HAL_PWR_EnableBkUpAccess();

    LL_RCC_LSI_Enable();
    while (LL_RCC_LSI_IsReady() != true) {
    }

    LL_RCC_ForceBackupDomainReset();
    LL_RCC_ReleaseBackupDomainReset();
    LL_RCC_SetRTCClockSource(g_static_rtc_driver_lut.clock_source);

    LL_RCC_EnableRTC();

    if (LL_RTC_DeInit(RTC) != SUCCESS) {
        return false;
    }

    LL_RTC_InitTypeDef rtc_init_struct = {0};
    rtc_init_struct.HourFormat = g_static_rtc_driver_lut.hour_format;
    rtc_init_struct.AsynchPrescaler = g_static_rtc_driver_lut.async_prescaler;
    rtc_init_struct.SynchPrescaler = g_static_rtc_driver_lut.sync_prescaler;
    if (LL_RTC_Init(RTC, &rtc_init_struct) != SUCCESS) {
        return false;
    }

    return true;
}

bool RTC_Driver_SetDateTime (sDateTime_t *date_time) {
    if (date_time == NULL) {
        return false;
    }

    LL_RTC_TimeTypeDef rtc_time_init_struct = {0};
    rtc_time_init_struct.TimeFormat = LL_RTC_TIME_FORMAT_AM_OR_24;
    rtc_time_init_struct.Hours = __LL_RTC_CONVERT_BIN2BCD(date_time->hours);
    rtc_time_init_struct.Minutes = __LL_RTC_CONVERT_BIN2BCD(date_time->minutes);
    rtc_time_init_struct.Seconds = __LL_RTC_CONVERT_BIN2BCD(date_time->seconds);
    if (LL_RTC_TIME_Init(RTC, g_static_rtc_driver_lut.rtc_format, &rtc_time_init_struct) != SUCCESS) {
        return false;
    }

    LL_RTC_DateTypeDef rtc_date_initstruct = {0};
    rtc_date_initstruct.WeekDay = __LL_RTC_CONVERT_BIN2BCD(date_time->weekday);
    rtc_date_initstruct.Day = __LL_RTC_CONVERT_BIN2BCD(date_time->day);
    rtc_date_initstruct.Month = __LL_RTC_CONVERT_BIN2BCD(date_time->month);
    rtc_date_initstruct.Year = __LL_RTC_CONVERT_BIN2BCD(date_time->year - START_YEAR);

    if (LL_RTC_DATE_Init(RTC, g_static_rtc_driver_lut.rtc_format, &rtc_date_initstruct) != SUCCESS) {
        return false;
    }

    return true;
}

bool RTC_Driver_SetTimestamp (timestamp_t *timestamp) {
    if (timestamp == NULL) {
        return false;
    }

    sDateTime_t date_time = {0};
    timestampToTimeStruct(timestamp, &date_time);
    if (RTC_Driver_SetDateTime(&date_time) == false) {
        return false;
    }

    return true;
}

bool RTC_Driver_GetDateTime (sDateTime_t *date_time) {
    if (date_time == NULL) {
        return false;
    }

    //Milliseconds
    date_time->milliseconds = (MS_INVERSION_CONSTANT - LL_RTC_TIME_GetSubSecond(RTC));
    //Year
    date_time->year = START_YEAR + __LL_RTC_CONVERT_BCD2BIN(LL_RTC_DATE_GetYear(RTC));
    //Month of year
    date_time->month = __LL_RTC_CONVERT_BCD2BIN(LL_RTC_DATE_GetMonth(RTC));
    //Day of month
    date_time->day = __LL_RTC_CONVERT_BCD2BIN(LL_RTC_DATE_GetDay(RTC));
    //Day of week
    date_time->weekday = __LL_RTC_CONVERT_BCD2BIN(LL_RTC_DATE_GetWeekDay(RTC));
    //Hours
    date_time->hours = __LL_RTC_CONVERT_BCD2BIN(LL_RTC_TIME_GetHour(RTC));
    //Minutes
    date_time->minutes = __LL_RTC_CONVERT_BCD2BIN(LL_RTC_TIME_GetMinute(RTC));
    //Seconds
    date_time->seconds = __LL_RTC_CONVERT_BCD2BIN(LL_RTC_TIME_GetSecond(RTC));

    return true;
}

bool RTC_Driver_GetTimestamp (timestamp_t *timestamp) {
    if (timestamp == NULL) {
        return false;
    }

    sDateTime_t date_time = {0};
    if (RTC_Driver_GetDateTime(&date_time) == false) {
        return false;
    }

    timeStructToTimestamp(timestamp, &date_time);

    return true;
}

//bool RTC_Driver_SetAlarm (eRtcDriver_Alarm_Enum_t alarm) {
//    LL_RTC_AlarmTypeDef rtc_alarm_initstruct = {0};
//    rtc_alarm_initstruct.AlarmTime.TimeFormat = LL_RTC_ALMA_TIME_FORMAT_AM;
//    rtc_alarm_initstruct.AlarmTime.Hours = 0x12;
//    rtc_alarm_initstruct.AlarmTime.Minutes = 0x00;
//    rtc_alarm_initstruct.AlarmTime.Seconds = 0x25;
//
//    /* RTC Alarm Generation: Alarm on Hours, Minutes and Seconds (ignore date/weekday)*/
//    rtc_alarm_initstruct.AlarmMask = LL_RTC_ALMA_MASK_DATEWEEKDAY;
//    rtc_alarm_initstruct.AlarmDateWeekDaySel = LL_RTC_ALMA_DATEWEEKDAYSEL_DATE;
//    rtc_alarm_initstruct.AlarmDateWeekDay = 0x01;
//
//    /* Initialize ALARM A according to parameters defined in initialization structure. */
//    if (LL_RTC_ALMA_Init(RTC, LL_RTC_FORMAT_BCD, &rtc_alarm_initstruct) != SUCCESS) {
//        return false;
//    }
//    return true;
//}
//
//bool RTC_Driver_EnableAlarm (eRtcDriver_Alarm_Enum_t alarm) {
//    LL_RTC_DisableWriteProtection(RTC);
//    LL_RTC_ALMA_Enable(RTC);
//    LL_RTC_ClearFlag_ALRA(RTC);
//    LL_RTC_EnableIT_ALRA(RTC);
//    LL_RTC_EnableWriteProtection(RTC);
//    return true;
//}
//
//bool RTC_Driver_DisableAlarm (eRtcDriver_Alarm_Enum_t alarm) {
//    LL_RTC_DisableWriteProtection(RTC);
//    LL_RTC_ALMA_Disable(RTC);
//    LL_RTC_ClearFlag_ALRA(RTC);
//    LL_RTC_DisableIT_ALRA(RTC);
//    LL_RTC_EnableWriteProtection(RTC);
//    return true;
//}
