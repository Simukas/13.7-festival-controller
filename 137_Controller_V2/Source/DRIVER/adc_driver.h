#ifndef __ADC_DRIVER__H__
#define __ADC_DRIVER__H__
/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "stdbool.h"
#include "stdint.h"
/**********************************************************************************************************************
 * Exported definitions and macros
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported types
 *********************************************************************************************************************/
// @formatter:off
typedef enum eAdcDriver_Adc_Enum_t {
    eAdcDriver_Adc_First = 0,
    eAdcDriver_Adc_1 = eAdcDriver_Adc_First,
    eAdcDriver_Adc_Last,
} eAdcDriver_Adc_Enum_t;

typedef enum eAdcDriver_Channel_Enum_t {
    eAdcDriver_Channel_First = 0,
    eAdcDriver_Channel_1 = eAdcDriver_Channel_First,
    eAdcDriver_Channel_2,
    eAdcDriver_Channel_3,
    eAdcDriver_Channel_PinLast,
    eAdcDriver_Channel_InputVoltage = eAdcDriver_Channel_PinLast,
    eAdcDriver_Channel_Temperature,
    eAdcDriver_Channel_Last,
} eAdcDriver_Channel_Enum_t;
// @formatter:on
/**********************************************************************************************************************
 * Exported variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of exported functions
 *********************************************************************************************************************/
bool ADC_Driver_Init (eAdcDriver_Adc_Enum_t adc);
bool ADC_Driver_ReadChannels (eAdcDriver_Adc_Enum_t adc);
bool ADC_Driver_GetChannelValue (eAdcDriver_Channel_Enum_t channel, uint16_t *value);
bool ADC_Driver_GetTemperature (int8_t *temperature);
bool ADC_Driver_GetSupplyVoltage (uint16_t *voltage_mV);
#endif /* __ADC_DRIVER__H__ */
