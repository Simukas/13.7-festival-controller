/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "gpio_driver.h"
#include "stm32f4xx_ll_bus.h"
#include "stm32f4xx_ll_gpio.h"
#include "stm32f4xx_ll_exti.h"
#include "stm32f4xx_ll_system.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
//@formatterOff
static const sGpioDriver_Static_t g_static_gpio_driver_lut[eGpioDriver_Pin_Last] = {
    [eGpioDriver_Pin_RS485_TX] = {.port = GPIOC, .pin = LL_GPIO_PIN_6, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_AHB1_GRP1_PERIPH_GPIOC, .alternate = LL_GPIO_AF_8, },
    [eGpioDriver_Pin_RS485_RX] = {.port = GPIOC, .pin = LL_GPIO_PIN_7, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_AHB1_GRP1_PERIPH_GPIOC, .alternate = LL_GPIO_AF_8, },
    [eGpioDriver_Pin_RS485_DE] = {.port = GPIOB, .pin = LL_GPIO_PIN_14, .mode = LL_GPIO_MODE_OUTPUT, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_AHB1_GRP1_PERIPH_GPIOB, .alternate = LL_GPIO_AF_0, },

    [eGpioDriver_Pin_AddressBit_0] = {.port = GPIOA, .pin = LL_GPIO_PIN_5, .mode = LL_GPIO_MODE_INPUT, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_UP, .clock = LL_AHB1_GRP1_PERIPH_GPIOA, .alternate = LL_GPIO_AF_0, },
    [eGpioDriver_Pin_AddressBit_1] = {.port = GPIOA, .pin = LL_GPIO_PIN_6, .mode = LL_GPIO_MODE_INPUT, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_UP, .clock = LL_AHB1_GRP1_PERIPH_GPIOA, .alternate = LL_GPIO_AF_0, },
    [eGpioDriver_Pin_AddressBit_2] = {.port = GPIOA, .pin = LL_GPIO_PIN_7, .mode = LL_GPIO_MODE_INPUT, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_UP, .clock = LL_AHB1_GRP1_PERIPH_GPIOA, .alternate = LL_GPIO_AF_0, },
    [eGpioDriver_Pin_AddressBit_3] = {.port = GPIOC, .pin = LL_GPIO_PIN_4, .mode = LL_GPIO_MODE_INPUT, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_UP, .clock = LL_AHB1_GRP1_PERIPH_GPIOC, .alternate = LL_GPIO_AF_0, },
    [eGpioDriver_Pin_AddressBit_4] = {.port = GPIOC, .pin = LL_GPIO_PIN_5, .mode = LL_GPIO_MODE_INPUT, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_UP, .clock = LL_AHB1_GRP1_PERIPH_GPIOC, .alternate = LL_GPIO_AF_0, },
    [eGpioDriver_Pin_AddressBit_5] = {.port = GPIOB, .pin = LL_GPIO_PIN_0, .mode = LL_GPIO_MODE_INPUT, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_UP, .clock = LL_AHB1_GRP1_PERIPH_GPIOB, .alternate = LL_GPIO_AF_0, },
    [eGpioDriver_Pin_AddressBit_6] = {.port = GPIOB, .pin = LL_GPIO_PIN_1, .mode = LL_GPIO_MODE_INPUT, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_UP, .clock = LL_AHB1_GRP1_PERIPH_GPIOB, .alternate = LL_GPIO_AF_0, },
    [eGpioDriver_Pin_AddressBit_7] = {.port = GPIOB, .pin = LL_GPIO_PIN_2, .mode = LL_GPIO_MODE_INPUT, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_UP, .clock = LL_AHB1_GRP1_PERIPH_GPIOB, .alternate = LL_GPIO_AF_0, },

    [eGpioDriver_Pin_Motor1_A] = {.port = GPIOA, .pin = LL_GPIO_PIN_0, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_AHB1_GRP1_PERIPH_GPIOA, .alternate = LL_GPIO_AF_2, },
    [eGpioDriver_Pin_Motor1_B] = {.port = GPIOA, .pin = LL_GPIO_PIN_1, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_AHB1_GRP1_PERIPH_GPIOA, .alternate = LL_GPIO_AF_2, },
    [eGpioDriver_Pin_Motor2_A] = {.port = GPIOA, .pin = LL_GPIO_PIN_2, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_AHB1_GRP1_PERIPH_GPIOA, .alternate = LL_GPIO_AF_2, },
    [eGpioDriver_Pin_Motor2_B] = {.port = GPIOA, .pin = LL_GPIO_PIN_3, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_AHB1_GRP1_PERIPH_GPIOA, .alternate = LL_GPIO_AF_2, },

    [eGpioDriver_Pin_RGB1_R] = {.port = GPIOB, .pin = LL_GPIO_PIN_4, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_AHB1_GRP1_PERIPH_GPIOB, .alternate = LL_GPIO_AF_2, },
    [eGpioDriver_Pin_RGB1_G] = {.port = GPIOB, .pin = LL_GPIO_PIN_5, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_AHB1_GRP1_PERIPH_GPIOB, .alternate = LL_GPIO_AF_2, },
    [eGpioDriver_Pin_RGB1_B] = {.port = GPIOC, .pin = LL_GPIO_PIN_9, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_AHB1_GRP1_PERIPH_GPIOC, .alternate = LL_GPIO_AF_2, },
    [eGpioDriver_Pin_RGB1_W] = {.port = GPIOC, .pin = LL_GPIO_PIN_8, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_AHB1_GRP1_PERIPH_GPIOC, .alternate = LL_GPIO_AF_2, },

    [eGpioDriver_Pin_RGB2_R] = {.port = GPIOB, .pin = LL_GPIO_PIN_7, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_AHB1_GRP1_PERIPH_GPIOB, .alternate = LL_GPIO_AF_2, },
    [eGpioDriver_Pin_RGB2_G] = {.port = GPIOB, .pin = LL_GPIO_PIN_6, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_AHB1_GRP1_PERIPH_GPIOB, .alternate = LL_GPIO_AF_2, },
    [eGpioDriver_Pin_RGB2_B] = {.port = GPIOB, .pin = LL_GPIO_PIN_8, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_AHB1_GRP1_PERIPH_GPIOB, .alternate = LL_GPIO_AF_2, },
    [eGpioDriver_Pin_RGB2_W] = {.port = GPIOB, .pin = LL_GPIO_PIN_9, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_AHB1_GRP1_PERIPH_GPIOB, .alternate = LL_GPIO_AF_2, },

    [eGpioDriver_Pin_ARGB_1] = {.port = GPIOA, .pin = LL_GPIO_PIN_8, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_AHB1_GRP1_PERIPH_GPIOA, .alternate = LL_GPIO_AF_1, },
    [eGpioDriver_Pin_ARGB_2] = {.port = GPIOA, .pin = LL_GPIO_PIN_9, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_AHB1_GRP1_PERIPH_GPIOA, .alternate = LL_GPIO_AF_1, },
    [eGpioDriver_Pin_ARGB_3] = {.port = GPIOA, .pin = LL_GPIO_PIN_10, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_AHB1_GRP1_PERIPH_GPIOA, .alternate = LL_GPIO_AF_1, },

    [eGpioDriver_Pin_Servo] = {.port = GPIOA, .pin = LL_GPIO_PIN_15, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_AHB1_GRP1_PERIPH_GPIOA, .alternate = LL_GPIO_AF_1, },

    [eGpioDriver_Pin_AnalogIn_1] = {.port = GPIOC, .pin = LL_GPIO_PIN_1, .mode = LL_GPIO_MODE_ANALOG, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_AHB1_GRP1_PERIPH_GPIOC, .alternate = LL_GPIO_AF_0, },
    [eGpioDriver_Pin_AnalogIn_2] = {.port = GPIOC, .pin = LL_GPIO_PIN_2, .mode = LL_GPIO_MODE_ANALOG, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_AHB1_GRP1_PERIPH_GPIOC, .alternate = LL_GPIO_AF_0, },
    [eGpioDriver_Pin_AnalogIn_3] = {.port = GPIOC, .pin = LL_GPIO_PIN_3, .mode = LL_GPIO_MODE_ANALOG, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_AHB1_GRP1_PERIPH_GPIOC, .alternate = LL_GPIO_AF_0, },

    [eGpioDriver_Pin_InputVoltage] = {.port = GPIOC, .pin = LL_GPIO_PIN_0, .mode = LL_GPIO_MODE_ANALOG, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_AHB1_GRP1_PERIPH_GPIOC, .alternate = LL_GPIO_AF_0, },

    [eGpioDriver_Pin_I2C_SCL] = {.port = GPIOB, .pin = LL_GPIO_PIN_10, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_OPENDRAIN, .pull = LL_GPIO_PULL_UP, .clock = LL_AHB1_GRP1_PERIPH_GPIOB, .alternate = LL_GPIO_AF_4, },
    [eGpioDriver_Pin_I2C_SDA] = {.port = GPIOB, .pin = LL_GPIO_PIN_3, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_OPENDRAIN, .pull = LL_GPIO_PULL_UP, .clock = LL_AHB1_GRP1_PERIPH_GPIOB, .alternate = LL_GPIO_AF_9, },

    [eGpioDriver_Pin_SPI_NSS] = {.port = GPIOA, .pin = LL_GPIO_PIN_4, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_AHB1_GRP1_PERIPH_GPIOA, .alternate = LL_GPIO_AF_6, },
    [eGpioDriver_Pin_SPI_SCK] = {.port = GPIOC, .pin = LL_GPIO_PIN_10, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_AHB1_GRP1_PERIPH_GPIOC, .alternate = LL_GPIO_AF_6, },
    [eGpioDriver_Pin_SPI_MISO] = {.port = GPIOC, .pin = LL_GPIO_PIN_11, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_AHB1_GRP1_PERIPH_GPIOC, .alternate = LL_GPIO_AF_6, },
    [eGpioDriver_Pin_SPI_MOSI] = {.port = GPIOC, .pin = LL_GPIO_PIN_12, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_AHB1_GRP1_PERIPH_GPIOC, .alternate = LL_GPIO_AF_6, },

    [eGpioDriver_Pin_GPIO_1] = {.port = GPIOC, .pin = LL_GPIO_PIN_13, .mode = LL_GPIO_MODE_OUTPUT, .speed = LL_GPIO_SPEED_FREQ_LOW, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_AHB1_GRP1_PERIPH_GPIOC, .alternate = LL_GPIO_AF_0, .init_state = true},
    [eGpioDriver_Pin_GPIO_2] = {.port = GPIOC, .pin = LL_GPIO_PIN_14, .mode = LL_GPIO_MODE_OUTPUT, .speed = LL_GPIO_SPEED_FREQ_LOW, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_AHB1_GRP1_PERIPH_GPIOC, .alternate = LL_GPIO_AF_0, .init_state = true},
    [eGpioDriver_Pin_GPIO_3] = {.port = GPIOC, .pin = LL_GPIO_PIN_15, .mode = LL_GPIO_MODE_INPUT, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_AHB1_GRP1_PERIPH_GPIOC, .alternate = LL_GPIO_AF_0, },
    [eGpioDriver_Pin_GPIO_4] = {.port = GPIOB, .pin = LL_GPIO_PIN_13, .mode = LL_GPIO_MODE_INPUT, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_AHB1_GRP1_PERIPH_GPIOB, .alternate = LL_GPIO_AF_0, },
    [eGpioDriver_Pin_GPIO_5] = {.port = GPIOB, .pin = LL_GPIO_PIN_12, .mode = LL_GPIO_MODE_INPUT, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_AHB1_GRP1_PERIPH_GPIOB, .alternate = LL_GPIO_AF_0, },
    [eGpioDriver_Pin_GPIO_6] = {.port = GPIOD, .pin = LL_GPIO_PIN_2, .mode = LL_GPIO_MODE_INPUT, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_AHB1_GRP1_PERIPH_GPIOD, .alternate = LL_GPIO_AF_0, },

    [eGpioDriver_Pin_USB_DP] = {.port = GPIOA, .pin = GPIO_PIN_12, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_AHB1_GRP1_PERIPH_GPIOA, .alternate = LL_GPIO_AF_10, },
    [eGpioDriver_Pin_USB_DM] = {.port = GPIOA, .pin = GPIO_PIN_11, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_AHB1_GRP1_PERIPH_GPIOA, .alternate = LL_GPIO_AF_10, },

};
//@formatterOn
/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool GPIO_Driver_InitPin (eGpioDriver_Pin_Enum_t pin, const sGpioDriver_Static_t *pin_parameters) {
    if (pin >= eGpioDriver_Pin_Last) {
        return false;
    }
    sGpioDriver_Static_t pin_init_parameters = {0};
    if (pin_parameters == NULL) {
        pin_init_parameters = g_static_gpio_driver_lut[pin];
    } else {
        pin_init_parameters = *pin_parameters;
    }
    LL_AHB1_GRP1_EnableClock(pin_init_parameters.clock);

    if (pin_init_parameters.init_state == true) {
        LL_GPIO_SetOutputPin(pin_init_parameters.port, pin_init_parameters.pin);
    } else {
        LL_GPIO_ResetOutputPin(pin_init_parameters.port, pin_init_parameters.pin);
    }

    LL_GPIO_InitTypeDef GPIO_InitStruct = {0};
    GPIO_InitStruct.Mode = pin_init_parameters.mode;
    GPIO_InitStruct.Speed = pin_init_parameters.speed;
    GPIO_InitStruct.OutputType = pin_init_parameters.output;
    GPIO_InitStruct.Pull = pin_init_parameters.pull;
    GPIO_InitStruct.Alternate = pin_init_parameters.alternate;
    GPIO_InitStruct.Pin = pin_init_parameters.pin;

    if (LL_GPIO_Init(pin_init_parameters.port, &GPIO_InitStruct) != SUCCESS) {
        return false;
    }
    return true;
}

bool GPIO_Driver_SetPin (eGpioDriver_Pin_Enum_t gpio) {
    if (gpio >= eGpioDriver_Pin_Last) {
        return false;
    }
    LL_GPIO_SetOutputPin(g_static_gpio_driver_lut[gpio].port, g_static_gpio_driver_lut[gpio].pin);
    return true;
}

bool GPIO_Driver_ResetPin (eGpioDriver_Pin_Enum_t gpio) {
    if (gpio >= eGpioDriver_Pin_Last) {
        return false;
    }
    LL_GPIO_ResetOutputPin(g_static_gpio_driver_lut[gpio].port, g_static_gpio_driver_lut[gpio].pin);
    return true;
}

bool GPIO_Driver_TogglePin (eGpioDriver_Pin_Enum_t gpio) {
    if (gpio >= eGpioDriver_Pin_Last) {
        return false;
    }
    LL_GPIO_TogglePin(g_static_gpio_driver_lut[gpio].port, g_static_gpio_driver_lut[gpio].pin);
    return true;
}

bool GPIO_Driver_ReadInputPin (eGpioDriver_Pin_Enum_t gpio, bool *pin_state) {
    if (gpio >= eGpioDriver_Pin_Last || pin_state == NULL) {
        return false;
    }
    if (LL_GPIO_IsInputPinSet(g_static_gpio_driver_lut[gpio].port, g_static_gpio_driver_lut[gpio].pin) == 1UL) {
        *pin_state = true;
    } else {
        *pin_state = false;
    }
    return true;
}

bool GPIO_Driver_ReadOutputPin (eGpioDriver_Pin_Enum_t gpio, bool *pin_state) {
    if (gpio >= eGpioDriver_Pin_Last || pin_state == NULL) {
        return false;
    }
    if (LL_GPIO_IsOutputPinSet(g_static_gpio_driver_lut[gpio].port, g_static_gpio_driver_lut[gpio].pin) == 1UL) {
        *pin_state = true;
    } else {
        *pin_state = false;
    }
    return true;
}

bool GPIO_Driver_GetPinInfo (eGpioDriver_Pin_Enum_t gpio, sGpioDriver_Static_t *pin_struct) {
    if (gpio >= eGpioDriver_Pin_Last || pin_struct == NULL) {
        return false;
    }
    *pin_struct = g_static_gpio_driver_lut[gpio];
    return true;
}
