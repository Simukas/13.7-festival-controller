#ifndef __GPIO_DRIVER__H__
#define __GPIO_DRIVER__H__
/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "stdbool.h"
#include "stdint.h"
#include "stm32f4xx_ll_gpio.h"
/**********************************************************************************************************************
 * Exported definitions and macros
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported types
 *********************************************************************************************************************/
//@formatterOff
typedef struct sGpioDriverStatic_t {
    GPIO_TypeDef *port;
    uint32_t pin;
    uint32_t mode;
    uint32_t speed;
    uint32_t output;
    uint32_t pull;
    uint32_t clock;
    uint32_t alternate;
    bool init_state;
} sGpioDriver_Static_t;

typedef enum eGpioDriver_Pin_Enum_t {
    eGpioDriver_Pin_First = 0,

    // For RS485 communication
    eGpioDriver_Pin_RS485_TX = eGpioDriver_Pin_First,
    eGpioDriver_Pin_RS485_RX,
    eGpioDriver_Pin_RS485_DE,

    // For setting address of the controller
    eGpioDriver_Pin_AddressBit_0,
    eGpioDriver_Pin_AddressBit_1,
    eGpioDriver_Pin_AddressBit_2,
    eGpioDriver_Pin_AddressBit_3,
    eGpioDriver_Pin_AddressBit_4,
    eGpioDriver_Pin_AddressBit_5,
    eGpioDriver_Pin_AddressBit_6,
    eGpioDriver_Pin_AddressBit_7,

    // For controlling motors with H-bridges
    eGpioDriver_Pin_Motor1_A,
    eGpioDriver_Pin_Motor1_B,
    eGpioDriver_Pin_Motor2_A,
    eGpioDriver_Pin_Motor2_B,

    // For controlling the RGBW led strips with N-Channel mosfets
    eGpioDriver_Pin_RGB1_R,
    eGpioDriver_Pin_RGB1_G,
    eGpioDriver_Pin_RGB1_B,
    eGpioDriver_Pin_RGB1_W,
    eGpioDriver_Pin_RGB2_R,
    eGpioDriver_Pin_RGB2_G,
    eGpioDriver_Pin_RGB2_B,
    eGpioDriver_Pin_RGB2_W,

    // For controlling addressable LED's
    eGpioDriver_Pin_ARGB_1,
    eGpioDriver_Pin_ARGB_2,
    eGpioDriver_Pin_ARGB_3,

    // For controlling a servo motor
    eGpioDriver_Pin_Servo,

    // For analog input
    eGpioDriver_Pin_AnalogIn_1,
    eGpioDriver_Pin_AnalogIn_2,
    eGpioDriver_Pin_AnalogIn_3,

    // For input voltage measurement
    eGpioDriver_Pin_InputVoltage,

    // For connecting I2C devices
    eGpioDriver_Pin_I2C_SCL,
    eGpioDriver_Pin_I2C_SDA,

    // For connecting SPI devices
    eGpioDriver_Pin_SPI_NSS,
    eGpioDriver_Pin_SPI_SCK,
    eGpioDriver_Pin_SPI_MISO,
    eGpioDriver_Pin_SPI_MOSI,

    // For connecting buttons, switches and relays
    eGpioDriver_Pin_GPIO_1,
    eGpioDriver_Pin_GPIO_2,
    eGpioDriver_Pin_GPIO_3,
    eGpioDriver_Pin_GPIO_4,
    eGpioDriver_Pin_GPIO_5,
    eGpioDriver_Pin_GPIO_6,

    // For USB
    eGpioDriver_Pin_USB_DP,
    eGpioDriver_Pin_USB_DM,

    eGpioDriver_Pin_Last,
} eGpioDriver_Pin_Enum_t;
//@formatterOn
/**********************************************************************************************************************
 * Exported variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of exported functions
 *********************************************************************************************************************/
bool GPIO_Driver_InitPin (eGpioDriver_Pin_Enum_t pin, const sGpioDriver_Static_t *pin_parameters);
bool GPIO_Driver_SetPin (eGpioDriver_Pin_Enum_t gpio);
bool GPIO_Driver_ResetPin (eGpioDriver_Pin_Enum_t gpio);
bool GPIO_Driver_TogglePin (eGpioDriver_Pin_Enum_t gpio);
bool GPIO_Driver_ReadInputPin (eGpioDriver_Pin_Enum_t gpio, bool *pin_state);
bool GPIO_Driver_ReadOutputPin (eGpioDriver_Pin_Enum_t gpio, bool *pin_state);
bool GPIO_Driver_GetPinInfo (eGpioDriver_Pin_Enum_t gpio, sGpioDriver_Static_t *info);
#endif /* __GPIO_DRIVER__H__ */
