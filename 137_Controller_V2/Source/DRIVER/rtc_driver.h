#ifndef __RTC_DRIVER__H__
#define __RTC_DRIVER__H__
/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "stdbool.h"
#include "stdint.h"
#include "time_utils.h"
/**********************************************************************************************************************
 * Exported definitions and macros
 *********************************************************************************************************************/
#define TIMESTAMP_TO_HOURS(timestamp) ((uint8_t)(timestamp/3600000UL)%24UL)
#define TIMESTAMP_TO_MINUTES(timestamp) ((uint8_t)(timestamp/60000UL)%60UL)
#define TIMESTAMP_TO_SECONDS(timestamp) ((timestamp/1000UL)%60UL)
#define TIME_TO_TIMESTAMP(time) (time*1000UL)
#define TIMESTAMP_TO_TIME(timestamp) (timestamp/1000UL)
/**********************************************************************************************************************
 * Exported types
 *********************************************************************************************************************/
//@formatterOff
typedef enum eRtcDriver_Alarm_Enum_t {
    eRtcDriver_Alarm_First = 0,
    eRtcDriver_Alarm_A = eRtcDriver_Alarm_First,
    eRtcDriver_Alarm_B,
    eRtcDriver_Alarm_Last
} eRtcDriver_Alarm_Enum_t;
//@formatterOn
/**********************************************************************************************************************
 * Exported variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of exported functions
 *********************************************************************************************************************/
bool RTC_Driver_Init (void);
bool RTC_Driver_SetDateTime (sDateTime_t *date_time);
bool RTC_Driver_SetTimestamp (timestamp_t *timestamp);
bool RTC_Driver_GetDateTime (sDateTime_t *date_time);
bool RTC_Driver_GetTimestamp (timestamp_t *timestamp);
#endif /* __RTC_DRIVER__H__ */
