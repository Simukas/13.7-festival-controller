/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include <message_ring_buffer.h>
#include "uart_driver.h"
#include "stdbool.h"
#include "stm32f4xx_ll_usart.h"
#include "stm32f4xx_ll_bus.h"
#include "stm32f4xx_it.h"
#include "gpio_driver.h"
#include "string.h"
#include "dma_driver.h"
#include "heap_api.h"
#include "time_utils.h"
#include "baudrate_utils.h"
#include "rs485_app.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
#define UART_DRIVER_RX_BUFFER_SIZE 255
#define UART_DRIVER_RING_BUFFER_MESSAGE_AMOUNT 50
/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/
typedef struct sUartDriver_Static_t {
    // UART parameters
    USART_TypeDef *uart;
    uint32_t clock;
    void (*clock_function) (uint32_t);
    uint32_t baud;
    uint32_t width;
    uint32_t bits;
    uint32_t parity;
    uint32_t direction;
    uint32_t flow;
    uint32_t oversampling;
    uint32_t IRQn;

    // IT parameters
    bool enable_IRQn;
    bool enable_IT_IDLE;
    bool enable_IT_RXNE;
    bool enable_IT_TXE;
    bool de_enabled;
    bool fifo_enabled;
    uint32_t prescaler;
    bool enable_ring_buffer;
    eGpioDriver_Pin_Enum_t de_pin;
    eGpioDriver_Pin_Enum_t rx_pin;
    eGpioDriver_Pin_Enum_t tx_pin;
    bool enable_DMA_RX;
    eDmaDriver_Stream_Enum_t DMA_RX_stream;
    bool enable_DMA_TX;
    eDmaDriver_Stream_Enum_t DMA_TX_stream;
} sUartDriver_Static_t;

typedef struct sUartDriver_Dynamic_t {
    uint8_t rx_ring_buffer[UART_DRIVER_RX_BUFFER_SIZE];
    uint8_t *tx_message_buffer;
    uint8_t rx_message_buffer[UART_DRIVER_RX_MESSAGE_BUFFER_SIZE];
    uint32_t rx_buffer_head;
    uint32_t rx_buffer_tail;
    uint32_t rx_message_buffer_index;
    message_rb_handle_t ring_buffer;
    void (*IT_callback) (eUartDriver_Enum_t, eUartDriver_Event_Enum_t);
} sUartDriver_Dynamic_t;
/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
// @formatter:off
static const sUartDriver_Static_t g_static_uart_driver_lut[eUartDriver_Last] = {
	[eUartDriver_Usart6] = {
	    .uart = USART6,
	    .clock = LL_APB2_GRP1_PERIPH_USART6,
	    .clock_function = &LL_APB2_GRP1_EnableClock,
	    .width = LL_USART_DATAWIDTH_8B,
	    .bits = LL_USART_STOPBITS_1,
	    .parity = LL_USART_PARITY_NONE,
	    .direction = LL_USART_DIRECTION_TX_RX,
	    .flow = LL_USART_HWCONTROL_NONE,
	    .oversampling = LL_USART_OVERSAMPLING_16,
	    .IRQn = USART6_IRQn,
	    .de_enabled = true,
	    .de_pin = eGpioDriver_Pin_RS485_DE,
	    .rx_pin = eGpioDriver_Pin_RS485_RX,
	    .tx_pin = eGpioDriver_Pin_RS485_TX,
	    .enable_IRQn = false,
	    .enable_IT_IDLE = true,
	    .enable_IT_RXNE = false,
	    .enable_IT_TXE = false,
	    .enable_DMA_RX = true,
	    .DMA_RX_stream = eDmaDriver_Stream_USART6_RX,
	    .enable_DMA_TX = true,
	    .DMA_TX_stream = eDmaDriver_Stream_USART6_TX,
    },
};

static const eUartDriver_Enum_t g_uart_driver_dma_stream_lut[eDmaDriver_Stream_Last] = {
    [eDmaDriver_Stream_USART6_RX] = eUartDriver_Usart6,
    [eDmaDriver_Stream_USART6_TX] = eUartDriver_Usart6,
};
// @formatter:on
/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/
static sUartDriver_Dynamic_t g_dynamic_uart_driver_lut[eUartDriver_Last] = {
    [eUartDriver_Usart6] = {
        .IT_callback = NULL,
        .rx_buffer_head = 0,
        .rx_buffer_tail = 0,
        .rx_message_buffer_index = 0,
    }
};
/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/
static bool UART_Driver_CaptureData (eUartDriver_Enum_t uart, eUartDriver_Event_Enum_t event);
static void UART_Driver_DMA_ITCallback_RX (eDmaDriver_Stream_Enum_t stream, eDmaDriver_IT_Enum_t IT);
static void UART_Driver_DMA_ITCallback_TX (eDmaDriver_Stream_Enum_t stream, eDmaDriver_IT_Enum_t IT);
/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/
static bool UART_Driver_CaptureData (eUartDriver_Enum_t uart, eUartDriver_Event_Enum_t event) {
//    if (uart >= eUartDriver_Last) {
//        return;
//    }
//    if (event >= eUartDriver_Event_Last) {
//        return;
//    }

    uint16_t head = 0;
    uint16_t tail = g_dynamic_uart_driver_lut[uart].rx_buffer_tail;

    DMA_Driver_GetCompletedCount(g_static_uart_driver_lut[uart].DMA_RX_stream, &head);

    if (RS485_CheckAddress(g_dynamic_uart_driver_lut[uart].rx_ring_buffer[tail]) == false) {
        g_dynamic_uart_driver_lut[uart].rx_buffer_tail = head;
        return false;
    }

    if (head != tail) {
        uint16_t data_count = 0;

        if (head > tail) {
            data_count = head - tail;
            sMessage_t message = {
                .buffer = Heap_API_CallocISR(data_count, sizeof(uint8_t)),
                .buffer_size = data_count
            };
            if (message.buffer == NULL) {
                return false;
            }
//            for (uint16_t i = tail; i < head; i++) {
//                RB_Push(g_dynamic_uart_driver_lut[uart].ring_buffer, g_dynamic_uart_driver_lut[uart].rx_ring_buffer[i]);
//            }
            memcpy(message.buffer, &g_dynamic_uart_driver_lut[uart].rx_ring_buffer[tail], data_count);
            Message_RB_Push(g_dynamic_uart_driver_lut[uart].ring_buffer, &message);
        } else {
            uint16_t delta = UART_DRIVER_RX_BUFFER_SIZE - tail;
            data_count = head + delta;
            sMessage_t message = {
                .buffer = Heap_API_CallocISR(data_count, sizeof(uint8_t)),
                .buffer_size = data_count
            };
            if (message.buffer == NULL) {
                return false;
            }
            //uint8_t *message_buffer = &g_dynamic_uart_driver_lut[uart].rx_message_buffer[g_dynamic_uart_driver_lut[uart].rx_message_buffer_index];
//            for (uint16_t i = tail; i < UART_DRIVER_RX_BUFFER_SIZE; i++) {
//                RB_Push(g_dynamic_uart_driver_lut[uart].ring_buffer, g_dynamic_uart_driver_lut[uart].rx_ring_buffer[i]);
//            }
//            for (uint16_t i = 0; i < head; i++) {
//                RB_Push(g_dynamic_uart_driver_lut[uart].ring_buffer, g_dynamic_uart_driver_lut[uart].rx_ring_buffer[i]);
//            }
            memcpy(message.buffer, &g_dynamic_uart_driver_lut[uart].rx_ring_buffer[tail], delta);
            memcpy(message.buffer + delta, &g_dynamic_uart_driver_lut[uart].rx_ring_buffer[0], head);
            Message_RB_Push(g_dynamic_uart_driver_lut[uart].ring_buffer, &message);
        }

//        g_dynamic_uart_driver_lut[uart].rx_message_buffer_index += data_count;
        g_dynamic_uart_driver_lut[uart].rx_buffer_tail = head;
        return true;
    }
    return false;
}

void UART_Driver_DMA_ITCallback_RX (eDmaDriver_Stream_Enum_t stream, eDmaDriver_IT_Enum_t IT) {
    switch (IT) {
        case eDmaDriver_IT_HT: {
            //UART_Driver_CaptureData(g_uart_driver_dma_stream_lut[stream], eUartDriver_Event_RX_HT);
            //g_dynamic_uart_driver_lut[g_uart_driver_dma_stream_lut[stream]].IT_callback(eUartDriver_Usart6, eUartDriver_Event_RX_HT);
            break;
        }
        case eDmaDriver_IT_TC: {
            //UART_Driver_CaptureData(g_uart_driver_dma_stream_lut[stream], eUartDriver_Event_RX_TC);
            //g_dynamic_uart_driver_lut[g_uart_driver_dma_stream_lut[stream]].IT_callback(eUartDriver_Usart6, eUartDriver_Event_RX_TC);
            break;
        }
        default: {
            break;
        }
    }
}

void UART_Driver_DMA_ITCallback_TX (eDmaDriver_Stream_Enum_t stream, eDmaDriver_IT_Enum_t IT) {
    switch (IT) {
        case eDmaDriver_IT_TC: {
            eUartDriver_Enum_t uart = g_uart_driver_dma_stream_lut[stream];

            DMA_Driver_DisableStream(g_static_uart_driver_lut[uart].DMA_TX_stream);
            Heap_API_FreeISR(g_dynamic_uart_driver_lut[uart].tx_message_buffer);
            g_dynamic_uart_driver_lut[uart].tx_message_buffer = NULL;

            if (g_static_uart_driver_lut[uart].de_enabled == true) {
                delay(200);
                GPIO_Driver_ResetPin(g_static_uart_driver_lut[uart].de_pin);
            }

            g_dynamic_uart_driver_lut[uart].IT_callback(uart, eUartDriver_Event_TX_TC);
            break;
        }
        default: {
            break;
        }
    }
}
/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool UART_Driver_Init (eUartDriver_Enum_t uart, eBaudrate_Enum_t baudrate) {
    if (uart >= eUartDriver_Last) {
        return false;
    }
    if (baudrate >= eBaudrate_Last) {
        return false;
    }

    if (GPIO_Driver_InitPin(g_static_uart_driver_lut[uart].rx_pin, NULL) == false) {
        return false;
    }
    if (GPIO_Driver_InitPin(g_static_uart_driver_lut[uart].tx_pin, NULL) == false) {
        return false;
    }
    if (g_static_uart_driver_lut[uart].de_enabled == true) {
        GPIO_Driver_InitPin(g_static_uart_driver_lut[uart].de_pin, NULL);
    }

    if (g_static_uart_driver_lut[uart].enable_DMA_RX == true) {
        DMA_Driver_Init_t dma_init_struct = {
            .stream = (uint32_t) g_static_uart_driver_lut[uart].DMA_RX_stream,
            .peripheral_or_source_address = (void*) LL_USART_DMA_GetRegAddr(g_static_uart_driver_lut[uart].uart),
            .destination_address = g_dynamic_uart_driver_lut[uart].rx_ring_buffer,
            .data_amount = UART_DRIVER_RX_BUFFER_SIZE,
            .IT_callback = &UART_Driver_DMA_ITCallback_RX
        };
        if (DMA_Driver_Init(&dma_init_struct) == false) {
            return false;
        }
    }
    if (g_static_uart_driver_lut[uart].enable_DMA_TX == true) {
        DMA_Driver_Init_t dma_init_struct = {
            .stream = g_static_uart_driver_lut[uart].DMA_TX_stream,
            .peripheral_or_source_address = (void*) LL_USART_DMA_GetRegAddr(g_static_uart_driver_lut[uart].uart),
            .destination_address = NULL,
            .data_amount = 0,
            .IT_callback = &UART_Driver_DMA_ITCallback_TX
        };
        if (DMA_Driver_Init(&dma_init_struct) == false) {
            return false;
        }
    }

    g_dynamic_uart_driver_lut[uart].ring_buffer = Message_RB_Init(UART_DRIVER_RING_BUFFER_MESSAGE_AMOUNT);
    if (g_dynamic_uart_driver_lut[uart].ring_buffer == NULL) {
        return false;
    }

    g_static_uart_driver_lut[uart].clock_function(g_static_uart_driver_lut[uart].clock);

    LL_USART_InitTypeDef ll_usart_init_struct = {
        .BaudRate = getBaudrate(baudrate),
        .DataWidth = g_static_uart_driver_lut[uart].width,
        .StopBits = g_static_uart_driver_lut[uart].bits,
        .Parity = g_static_uart_driver_lut[uart].parity,
        .TransferDirection = g_static_uart_driver_lut[uart].direction,
        .HardwareFlowControl = g_static_uart_driver_lut[uart].flow,
        .OverSampling = g_static_uart_driver_lut[uart].oversampling,
    };
    if (LL_USART_Init(g_static_uart_driver_lut[uart].uart, &ll_usart_init_struct) != SUCCESS) {
        return false;
    }

    LL_USART_ConfigAsyncMode(g_static_uart_driver_lut[uart].uart);

    NVIC_SetPriority(g_static_uart_driver_lut[uart].IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 5, 0));
    NVIC_EnableIRQ(g_static_uart_driver_lut[uart].IRQn);

    if (g_static_uart_driver_lut[uart].enable_IT_IDLE == true) {
        LL_USART_EnableIT_IDLE(g_static_uart_driver_lut[uart].uart);
    } else {
        LL_USART_DisableIT_IDLE(g_static_uart_driver_lut[uart].uart);
    }

    if (g_static_uart_driver_lut[uart].enable_IT_RXNE == true) {
        LL_USART_EnableIT_RXNE(g_static_uart_driver_lut[uart].uart);
    } else {
        LL_USART_DisableIT_RXNE(g_static_uart_driver_lut[uart].uart);
    }

    if (g_static_uart_driver_lut[uart].enable_IT_TXE == true) {
        LL_USART_EnableIT_TXE(g_static_uart_driver_lut[uart].uart);
    } else {
        LL_USART_DisableIT_TXE(g_static_uart_driver_lut[uart].uart);
    }

    LL_USART_Enable(g_static_uart_driver_lut[uart].uart);

    if (g_static_uart_driver_lut[uart].enable_DMA_RX == true) {
        LL_USART_EnableDMAReq_RX(g_static_uart_driver_lut[uart].uart);
        DMA_Driver_EnableStream(g_static_uart_driver_lut[uart].DMA_RX_stream);
    }
    if (g_static_uart_driver_lut[uart].enable_DMA_TX == true) {
        LL_USART_EnableDMAReq_TX(g_static_uart_driver_lut[uart].uart);
    }

    return true;
}

bool UART_Driver_Deinit (eUartDriver_Enum_t uart) {
    if (uart >= eUartDriver_Last) {
        return false;
    }

    if (g_static_uart_driver_lut[uart].enable_DMA_RX == true) {
        DMA_Driver_DisableStream(g_static_uart_driver_lut[uart].DMA_RX_stream);
        LL_USART_DisableDMAReq_RX(g_static_uart_driver_lut[uart].uart);
    }

    if (g_static_uart_driver_lut[uart].enable_DMA_TX == true) {
        LL_USART_DisableDMAReq_TX(g_static_uart_driver_lut[uart].uart);
    }

    LL_USART_Disable(g_static_uart_driver_lut[uart].uart);

    Message_RB_Free(g_dynamic_uart_driver_lut[uart].ring_buffer);

    if (g_static_uart_driver_lut[uart].enable_IT_IDLE == true) {
        LL_USART_DisableIT_IDLE(g_static_uart_driver_lut[uart].uart);
    }
    if (g_static_uart_driver_lut[uart].enable_IT_RXNE == true) {
        LL_USART_DisableIT_RXNE(g_static_uart_driver_lut[uart].uart);
    }
    if (g_static_uart_driver_lut[uart].enable_IT_TXE == true) {
        LL_USART_DisableIT_TXE(g_static_uart_driver_lut[uart].uart);
    }

    NVIC_DisableIRQ(g_static_uart_driver_lut[uart].IRQn);

    if (LL_USART_DeInit(g_static_uart_driver_lut[uart].uart) != SUCCESS) {
        return false;
    }

    return true;
}

bool UART_Driver_SendBytes (eUartDriver_Enum_t uart, uint8_t *buffer, uint16_t amount) {
//    if (uart >= eUartDriver_Last) {
//        return false;
//    }
//    if (buffer == NULL) {
//        return false;
//    }
//    if (amount == 0) {
//        return false;
//    }
//
//    while (g_dynamic_uart_driver_lut[uart].tx_message_buffer != NULL) {
//    }
    g_dynamic_uart_driver_lut[uart].tx_message_buffer = buffer;
//    if (g_dynamic_uart_driver_lut[uart].tx_message_buffer == NULL) {
//        return false;
//    }
//    memcpy(g_dynamic_uart_driver_lut[uart].tx_message_buffer, buffer, amount);
    if (DMA_Driver_SetDataCount(g_static_uart_driver_lut[uart].DMA_TX_stream, amount) == false) {
        free(g_dynamic_uart_driver_lut[uart].tx_message_buffer);
        return false;
    }
    if (DMA_Driver_SetDataPointer(g_static_uart_driver_lut[uart].DMA_TX_stream, g_dynamic_uart_driver_lut[uart].tx_message_buffer) == false) {
        free(g_dynamic_uart_driver_lut[uart].tx_message_buffer);
        return false;
    }
    if (g_static_uart_driver_lut[uart].de_enabled == true) {
        GPIO_Driver_SetPin(g_static_uart_driver_lut[uart].de_pin);
    }
    if (DMA_Driver_EnableStream(g_static_uart_driver_lut[uart].DMA_TX_stream) == false) {
        free(g_dynamic_uart_driver_lut[uart].tx_message_buffer);
        return false;
    }

    return true;
}

bool UART_Driver_GetMessage (eUartDriver_Enum_t uart, sMessage_t *message) {
    if (uart >= eUartDriver_Last) {
        return false;
    }
    if (message == NULL) {
        return false;
    }
    if (Message_RB_Pop(g_dynamic_uart_driver_lut[uart].ring_buffer, message) == false) {
        return false;
    }
    return true;
}

//bool UART_Driver_GetData (eUartDriver_Enum_t uart, uint8_t *data_buffer, uint32_t buffer_length) {
////    if (uart >= eUartDriver_Last) {
////        return false;
////    }
////    if (data_buffer == NULL) {
////        return false;
////    }
////    if (buffer_length == 0) {
////        return false;
////    }
//    uint8_t *local_buffer = g_dynamic_uart_driver_lut[uart].rx_message_buffer;
//    memcpy(data_buffer, local_buffer, buffer_length);
//    //memset(local_buffer, 0, buffer_length);
//    g_dynamic_uart_driver_lut[uart].rx_message_buffer_index = 0;
//    return true;
//}

//bool UART_Driver_GetDataCount (eUartDriver_Enum_t uart, uint32_t *count) {
////    if (uart >= eUartDriver_Last) {
////        return false;
////    }
////    if (count == NULL) {
////        return false;
////    }
//    *count = g_dynamic_uart_driver_lut[uart].rx_message_buffer_index;
//    return true;
//}

bool UART_Driver_SetBaudrate (eUartDriver_Enum_t uart, eBaudrate_Enum_t baudrate) {
    if (uart >= eUartDriver_Last) {
        return false;
    }
    if (baudrate >= eBaudrate_Last) {
        return false;
    }
    if (UART_Driver_Deinit(uart) == false) {
        return false;
    }
    if (UART_Driver_Init(uart, baudrate) == false) {
        return false;
    }

    return true;
}

bool UART_Driver_SetITCallback (eUartDriver_Enum_t uart, void (*IT_callback) (eUartDriver_Enum_t, eUartDriver_Event_Enum_t)) {
    if (uart >= eUartDriver_Last) {
        return false;
    }
    if (IT_callback == NULL) {
        return false;
    }
    g_dynamic_uart_driver_lut[uart].IT_callback = IT_callback;
    return true;
}

void USART6_IRQHandler (void) {
    if ((LL_USART_IsEnabledIT_IDLE(USART6) == true) && (LL_USART_IsActiveFlag_IDLE(USART6) == true)) {
        LL_USART_ClearFlag_IDLE(USART6);
        if (UART_Driver_CaptureData(eUartDriver_Usart6, eUartDriver_Event_IDLE) == true) {
            g_dynamic_uart_driver_lut[eUartDriver_Usart6].IT_callback(eUartDriver_Usart6, eUartDriver_Event_IDLE);
        }
    }
}
