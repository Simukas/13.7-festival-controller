#ifndef __UART_DRIVER__H__
#define __UART_DRIVER__H__

/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "stdbool.h"
#include "stdint.h"
#include "baudrate_utils.h"
#include "message_ring_buffer.h"
/**********************************************************************************************************************
 * Exported definitions and macros
 *********************************************************************************************************************/
#define UART_DRIVER_RX_MESSAGE_BUFFER_SIZE 64
/**********************************************************************************************************************
 * Exported types
 *********************************************************************************************************************/
// @formatter:off
typedef enum eUartDriver_Enum_t {
	eUartDriver_First = 0,
    eUartDriver_Usart6 = eUartDriver_First,
    eUartDriver_Last,
} eUartDriver_Enum_t;

typedef enum eUartDriver_IT_Enum_t {
    eUartDriver_IT_First = 0,
    eUartDriver_IT_IDLE = eUartDriver_IT_First,
    eUartDriver_IT_RXNE,
    eUartDriver_IT_TXE,
    eUartDriver_IT_Last,
} eUartDriver_IT_Enum_t;

typedef enum eUartDriver_Event_Enum_t {
    eUartDriver_Event_First = 0,
    eUartDriver_Event_RX_TC = eUartDriver_Event_First,
    eUartDriver_Event_RX_HT,
    eUartDriver_Event_TX_TC,
    eUartDriver_Event_TX_HT,
    eUartDriver_Event_IDLE,
    eUartDriver_Event_TXE,
    eUartDriver_Event_Last,
} eUartDriver_Event_Enum_t;

// @formatter:on
/**********************************************************************************************************************
 * Exported variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of exported functions
 *********************************************************************************************************************/
bool UART_Driver_Init (eUartDriver_Enum_t uart, eBaudrate_Enum_t baudrate);
bool UART_Driver_Deinit (eUartDriver_Enum_t uart);
bool UART_Driver_SendByte (eUartDriver_Enum_t uart, uint8_t byte);
bool UART_Driver_SendBytes (eUartDriver_Enum_t uart, uint8_t *buffer, uint16_t amount);
bool UART_Driver_SendString (eUartDriver_Enum_t uart, const char *string, uint32_t timeout);
bool UART_Driver_GetMessage (eUartDriver_Enum_t uart, sMessage_t *message);
//bool UART_Driver_GetDataCount (eUartDriver_Enum_t uart, uint32_t *count);
bool UART_Driver_SetBaudrate (eUartDriver_Enum_t uart, eBaudrate_Enum_t baudrate);
bool UART_Driver_SetITCallback (eUartDriver_Enum_t uart, void (*IT_callback) (eUartDriver_Enum_t, eUartDriver_Event_Enum_t));

#endif /*__UART_DRIVER__H__*/
