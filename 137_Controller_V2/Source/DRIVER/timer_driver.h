#ifndef __TIMER_DRIVER__H__
#define __TIMER_DRIVER__H__
/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "debug_api.h"
#include "stm32f4xx_ll_tim.h"
#include "gpio_driver.h"
/**********************************************************************************************************************
 * Exported definitions and macros
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported types
 *********************************************************************************************************************/
typedef enum eTimerDriver_Timer_Enum_t {
    eTimerDriver_Timer_First = 0,
        eTimerDriver_Timer_1 = eTimerDriver_Timer_First,
        eTimerDriver_Timer_2,
        eTimerDriver_Timer_3,
        eTimerDriver_Timer_4,
        eTimerDriver_Timer_5,
        eTimerDriver_Timer_9,
        eTimerDriver_Timer_10,
        eTimerDriver_Timer_Last,
} eTimerDriver_Timer_Enum_t;

typedef enum eTimerDriver_OC_Enum_t {
    eTimerDriver_OC_First = 0,
        eTimerDriver_OC_1 = eTimerDriver_OC_First,
        eTimerDriver_OC_2,
        eTimerDriver_OC_3,
        eTimerDriver_OC_4,
        eTimerDriver_OC_5,
        eTimerDriver_OC_6,
        eTimerDriver_OC_Last,
} eTimerDriver_OC_Enum_t;

typedef struct sTimerDriver_OC_Static_t {
    uint32_t channel;
    bool enable_preload;
    bool enable_fast;
    uint32_t oc_mode;
    uint32_t oc_state;
    uint32_t ocn_state;
    uint32_t compare_value;
    uint32_t oc_polarity;
    uint32_t ocn_polarity;
    uint32_t oc_idle_state;
    uint32_t ocn_idle_state;
    bool enable_dma_request;
    eGpioDriver_Pin_Enum_t pin;
} sTimerDriver_OC_Static_t;

typedef struct sTimerDriver_Static_t {
    //Timer parameters
    void (*clock_function) (uint32_t);
    uint32_t clock;
    uint16_t prescaler;
    uint32_t counter_mode;
    uint32_t autoreload;
    uint32_t repetition_counter;
    uint32_t clock_division;
    TIM_TypeDef *timer;
    bool enable_arr_preload;
    uint32_t clock_source;
    uint32_t trigger_output_1;
    bool enable_master_slave_mode;
    bool enable_dma_update;
    bool enable_dma_trigger;

    //OC parameters
    uint8_t oc_count;
    const sTimerDriver_OC_Static_t *oc_table;

    //BDTR parameters
    uint32_t ossr_state;
    uint32_t ossi_state;
    uint32_t lock_level;
    uint32_t dead_time;
    uint32_t break_state;
    uint32_t break_polarity;
    uint32_t automatic_output;
} sTimerDriver_Static_t;

typedef struct sTimerDriver_Dynamic_t {
    bool initialized;
} sTimerDriver_Dynamic_t;
/**********************************************************************************************************************
 * Exported variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of exported functions
 *********************************************************************************************************************/
bool Timer_Driver_Init (eTimerDriver_Timer_Enum_t timer);
bool Timer_Driver_SetDutyCycle (eTimerDriver_Timer_Enum_t timer, eTimerDriver_OC_Enum_t channel, uint16_t duty);
bool Timer_Driver_Start (eTimerDriver_Timer_Enum_t timer);
bool Timer_Driver_Stop (eTimerDriver_Timer_Enum_t timer);
bool Timer_Driver_GetTimerInfo (eTimerDriver_Timer_Enum_t timer, sTimerDriver_Static_t *info);
bool Timer_Driver_EnableChannel (eTimerDriver_Timer_Enum_t timer, eTimerDriver_OC_Enum_t channel);
bool Timer_Driver_DisableChannel (eTimerDriver_Timer_Enum_t timer, eTimerDriver_OC_Enum_t channel);
bool Timer_Driver_SetCounter (eTimerDriver_Timer_Enum_t timer, uint16_t value);
bool Timer_Driver_GetCounter (eTimerDriver_Timer_Enum_t timer, uint16_t *value);

bool Timer_Driver_InitWatchdog (void);
bool Timer_Driver_ResetWatchdog (void);

void configureTimerForRunTimeStats (void);
unsigned long getRunTimeCounterValue (void);
#endif /* __TIMER_DRIVER__H__ */
