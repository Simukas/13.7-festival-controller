/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "stm32f4xx_ll_tim.h"
#include "stm32f4xx_ll_bus.h"
#include "stm32f4xx_ll_iwdg.h"
#include "timer_driver.h"
#include "stdbool.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/
// @formatter:off
typedef enum eTimerDriver_Timer1_OC_Enum_t {
    eTimerDriver_Timer1_OC_First = 0,
    eTimerDriver_Timer1_OC_1 = eTimerDriver_Timer1_OC_First,
    eTimerDriver_Timer1_OC_2,
    eTimerDriver_Timer1_OC_3,
    eTimerDriver_Timer1_OC_Last,
} eTimerDriver_Timer1_OC_Enum_t;

typedef enum eTimerDriverTimer2_OCEnum_t {
    eTimerDriver_Timer2_OC_First = 0,
    eTimerDriver_Timer2_OC_1 = eTimerDriver_Timer2_OC_First,
    eTimerDriver_Timer2_OC_Last,
} eTimerDriver_Timer2_OC_Enum_t;

typedef enum eTimerDriver_Timer3_OC_Enum_t {
    eTimerDriver_Timer3_OC_First = 0,
    eTimerDriver_Timer3_OC_1 = eTimerDriver_Timer3_OC_First,
    eTimerDriver_Timer3_OC_2,
    eTimerDriver_Timer3_OC_3,
    eTimerDriver_Timer3_OC_4,
    eTimerDriver_Timer3_OC_Last,
} eTimerDriver_Timer3_OC_Enum_t;

typedef enum eTimerDriver_Timer4_OC_Enum_t {
    eTimerDriver_Timer4_OC_First = 0,
    eTimerDriver_Timer4_OC_1 = eTimerDriver_Timer4_OC_First,
    eTimerDriver_Timer4_OC_2,
    eTimerDriver_Timer4_OC_3,
    eTimerDriver_Timer4_OC_4,
    eTimerDriver_Timer4_OC_Last,
} eTimerDriver_Timer4_OC_Enum_t;

typedef enum eTimerDriver_Timer5_OC_Enum_t {
    eTimerDriver_Timer5_OC_First = 0,
    eTimerDriver_Timer5_OC_1 = eTimerDriver_Timer5_OC_First,
    eTimerDriver_Timer5_OC_2,
    eTimerDriver_Timer5_OC_3,
    eTimerDriver_Timer5_OC_4,
    eTimerDriver_Timer5_OC_Last,
} eTimerDriver_Timer5_OC_Enum_t;

// @formatter:on
/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
// @formatter:off
const static sTimerDriver_OC_Static_t g_static_timer_driver_timer1_oc_lut[] = {
    [eTimerDriver_Timer1_OC_1] = {.channel = LL_TIM_CHANNEL_CH1, .enable_preload = true, .oc_mode = LL_TIM_OCMODE_PWM1, .oc_state = LL_TIM_OCSTATE_ENABLE, .ocn_state = LL_TIM_OCSTATE_DISABLE, .compare_value = 0, .oc_polarity = LL_TIM_OCPOLARITY_HIGH, .oc_idle_state = LL_TIM_OCIDLESTATE_LOW, .ocn_idle_state = LL_TIM_OCIDLESTATE_LOW, .enable_fast = false, .enable_dma_request = true, .pin = eGpioDriver_Pin_ARGB_1},
    [eTimerDriver_Timer1_OC_2] = {.channel = LL_TIM_CHANNEL_CH2, .enable_preload = true, .oc_mode = LL_TIM_OCMODE_PWM1, .oc_state = LL_TIM_OCSTATE_ENABLE, .ocn_state = LL_TIM_OCSTATE_DISABLE, .compare_value = 0, .oc_polarity = LL_TIM_OCPOLARITY_HIGH, .oc_idle_state = LL_TIM_OCIDLESTATE_LOW, .ocn_idle_state = LL_TIM_OCIDLESTATE_LOW, .enable_fast = false, .enable_dma_request = true, .pin = eGpioDriver_Pin_ARGB_2},
    [eTimerDriver_Timer1_OC_3] = {.channel = LL_TIM_CHANNEL_CH3, .enable_preload = true, .oc_mode = LL_TIM_OCMODE_PWM1, .oc_state = LL_TIM_OCSTATE_ENABLE, .ocn_state = LL_TIM_OCSTATE_DISABLE, .compare_value = 0, .oc_polarity = LL_TIM_OCPOLARITY_HIGH, .oc_idle_state = LL_TIM_OCIDLESTATE_LOW, .ocn_idle_state = LL_TIM_OCIDLESTATE_LOW, .enable_fast = false, .enable_dma_request = true, .pin = eGpioDriver_Pin_ARGB_3},
};

const static sTimerDriver_OC_Static_t g_static_timer_driver_timer2_oc_lut[] = {
    [eTimerDriver_Timer2_OC_1] = {.channel = LL_TIM_CHANNEL_CH1, .enable_preload = true, .oc_mode = LL_TIM_OCMODE_PWM1, .oc_state = LL_TIM_OCSTATE_DISABLE, .ocn_state = LL_TIM_OCSTATE_DISABLE, .compare_value = 0, .oc_polarity = LL_TIM_OCPOLARITY_HIGH, .oc_idle_state = LL_TIM_OCIDLESTATE_LOW, .ocn_idle_state = LL_TIM_OCIDLESTATE_LOW, .enable_fast = false, .enable_dma_request = false, .pin = eGpioDriver_Pin_Servo},
};

const static sTimerDriver_OC_Static_t g_static_timer_driver_timer3_oc_lut[] = {
    [eTimerDriver_Timer3_OC_1] = {.channel = LL_TIM_CHANNEL_CH1, .enable_preload = true, .oc_mode = LL_TIM_OCMODE_PWM1, .oc_state = LL_TIM_OCSTATE_ENABLE, .ocn_state = LL_TIM_OCSTATE_DISABLE, .compare_value = 0, .oc_polarity = LL_TIM_OCPOLARITY_HIGH, .oc_idle_state = LL_TIM_OCIDLESTATE_LOW, .ocn_idle_state = LL_TIM_OCIDLESTATE_LOW, .enable_fast = false, .enable_dma_request = false, .pin = eGpioDriver_Pin_RGB1_R},
    [eTimerDriver_Timer3_OC_2] = {.channel = LL_TIM_CHANNEL_CH2, .enable_preload = true, .oc_mode = LL_TIM_OCMODE_PWM1, .oc_state = LL_TIM_OCSTATE_ENABLE, .ocn_state = LL_TIM_OCSTATE_DISABLE, .compare_value = 0, .oc_polarity = LL_TIM_OCPOLARITY_HIGH, .oc_idle_state = LL_TIM_OCIDLESTATE_LOW, .ocn_idle_state = LL_TIM_OCIDLESTATE_LOW, .enable_fast = false, .enable_dma_request = false, .pin = eGpioDriver_Pin_RGB1_G},
    [eTimerDriver_Timer3_OC_3] = {.channel = LL_TIM_CHANNEL_CH3, .enable_preload = true, .oc_mode = LL_TIM_OCMODE_PWM1, .oc_state = LL_TIM_OCSTATE_ENABLE, .ocn_state = LL_TIM_OCSTATE_DISABLE, .compare_value = 0, .oc_polarity = LL_TIM_OCPOLARITY_HIGH, .oc_idle_state = LL_TIM_OCIDLESTATE_LOW, .ocn_idle_state = LL_TIM_OCIDLESTATE_LOW, .enable_fast = false, .enable_dma_request = false, .pin = eGpioDriver_Pin_RGB1_W},
    [eTimerDriver_Timer3_OC_4] = {.channel = LL_TIM_CHANNEL_CH4, .enable_preload = true, .oc_mode = LL_TIM_OCMODE_PWM1, .oc_state = LL_TIM_OCSTATE_ENABLE, .ocn_state = LL_TIM_OCSTATE_DISABLE, .compare_value = 0, .oc_polarity = LL_TIM_OCPOLARITY_HIGH, .oc_idle_state = LL_TIM_OCIDLESTATE_LOW, .ocn_idle_state = LL_TIM_OCIDLESTATE_LOW, .enable_fast = false, .enable_dma_request = false, .pin = eGpioDriver_Pin_RGB1_B},
};

const static sTimerDriver_OC_Static_t g_static_timer_driver_timer4_oc_lut[] = {
    [eTimerDriver_Timer4_OC_1] = {.channel = LL_TIM_CHANNEL_CH1, .enable_preload = true, .oc_mode = LL_TIM_OCMODE_PWM1, .oc_state = LL_TIM_OCSTATE_ENABLE, .ocn_state = LL_TIM_OCSTATE_DISABLE, .compare_value = 0, .oc_polarity = LL_TIM_OCPOLARITY_HIGH, .oc_idle_state = LL_TIM_OCIDLESTATE_LOW, .ocn_idle_state = LL_TIM_OCIDLESTATE_LOW, .enable_fast = false, .enable_dma_request = false, .pin = eGpioDriver_Pin_RGB2_G},
    [eTimerDriver_Timer4_OC_2] = {.channel = LL_TIM_CHANNEL_CH2, .enable_preload = true, .oc_mode = LL_TIM_OCMODE_PWM1, .oc_state = LL_TIM_OCSTATE_ENABLE, .ocn_state = LL_TIM_OCSTATE_DISABLE, .compare_value = 0, .oc_polarity = LL_TIM_OCPOLARITY_HIGH, .oc_idle_state = LL_TIM_OCIDLESTATE_LOW, .ocn_idle_state = LL_TIM_OCIDLESTATE_LOW, .enable_fast = false, .enable_dma_request = false, .pin = eGpioDriver_Pin_RGB2_R},
    [eTimerDriver_Timer4_OC_3] = {.channel = LL_TIM_CHANNEL_CH3, .enable_preload = true, .oc_mode = LL_TIM_OCMODE_PWM1, .oc_state = LL_TIM_OCSTATE_ENABLE, .ocn_state = LL_TIM_OCSTATE_DISABLE, .compare_value = 0, .oc_polarity = LL_TIM_OCPOLARITY_HIGH, .oc_idle_state = LL_TIM_OCIDLESTATE_LOW, .ocn_idle_state = LL_TIM_OCIDLESTATE_LOW, .enable_fast = false, .enable_dma_request = false, .pin = eGpioDriver_Pin_RGB2_B},
    [eTimerDriver_Timer4_OC_4] = {.channel = LL_TIM_CHANNEL_CH4, .enable_preload = true, .oc_mode = LL_TIM_OCMODE_PWM1, .oc_state = LL_TIM_OCSTATE_ENABLE, .ocn_state = LL_TIM_OCSTATE_DISABLE, .compare_value = 0, .oc_polarity = LL_TIM_OCPOLARITY_HIGH, .oc_idle_state = LL_TIM_OCIDLESTATE_LOW, .ocn_idle_state = LL_TIM_OCIDLESTATE_LOW, .enable_fast = false, .enable_dma_request = false, .pin = eGpioDriver_Pin_RGB2_W},
};

const static sTimerDriver_OC_Static_t g_static_timer_driver_timer5_oc_lut[] = {
    [eTimerDriver_Timer5_OC_1] = {.channel = LL_TIM_CHANNEL_CH1, .enable_preload = true, .oc_mode = LL_TIM_OCMODE_PWM1, .oc_state = LL_TIM_OCSTATE_DISABLE, .ocn_state = LL_TIM_OCSTATE_DISABLE, .compare_value = 0, .oc_polarity = LL_TIM_OCPOLARITY_HIGH, .oc_idle_state = LL_TIM_OCIDLESTATE_LOW, .ocn_idle_state = LL_TIM_OCIDLESTATE_LOW, .enable_fast = false, .enable_dma_request = false, .pin = eGpioDriver_Pin_Motor1_A},
    [eTimerDriver_Timer5_OC_2] = {.channel = LL_TIM_CHANNEL_CH2, .enable_preload = true, .oc_mode = LL_TIM_OCMODE_PWM1, .oc_state = LL_TIM_OCSTATE_DISABLE, .ocn_state = LL_TIM_OCSTATE_DISABLE, .compare_value = 0, .oc_polarity = LL_TIM_OCPOLARITY_HIGH, .oc_idle_state = LL_TIM_OCIDLESTATE_LOW, .ocn_idle_state = LL_TIM_OCIDLESTATE_LOW, .enable_fast = false, .enable_dma_request = false, .pin = eGpioDriver_Pin_Motor1_B},
    [eTimerDriver_Timer5_OC_3] = {.channel = LL_TIM_CHANNEL_CH3, .enable_preload = true, .oc_mode = LL_TIM_OCMODE_PWM1, .oc_state = LL_TIM_OCSTATE_DISABLE, .ocn_state = LL_TIM_OCSTATE_DISABLE, .compare_value = 0, .oc_polarity = LL_TIM_OCPOLARITY_HIGH, .oc_idle_state = LL_TIM_OCIDLESTATE_LOW, .ocn_idle_state = LL_TIM_OCIDLESTATE_LOW, .enable_fast = false, .enable_dma_request = false, .pin = eGpioDriver_Pin_Motor2_A},
    [eTimerDriver_Timer5_OC_4] = {.channel = LL_TIM_CHANNEL_CH4, .enable_preload = true, .oc_mode = LL_TIM_OCMODE_PWM1, .oc_state = LL_TIM_OCSTATE_DISABLE, .ocn_state = LL_TIM_OCSTATE_DISABLE, .compare_value = 0, .oc_polarity = LL_TIM_OCPOLARITY_HIGH, .oc_idle_state = LL_TIM_OCIDLESTATE_LOW, .ocn_idle_state = LL_TIM_OCIDLESTATE_LOW, .enable_fast = false, .enable_dma_request = false, .pin = eGpioDriver_Pin_Motor2_B},
};

const static sTimerDriver_Static_t g_static_timer_driver_lut[eTimerDriver_Timer_Last] = {
    //Used for ARGB LEDs
    [eTimerDriver_Timer_1] = {
        //Timer parameters
        .clock_function = &LL_APB2_GRP1_EnableClock,
        .clock = LL_APB2_GRP1_PERIPH_TIM1,
        .prescaler = 0,
        .counter_mode = LL_TIM_COUNTERMODE_UP,
        .autoreload = 104,
        .repetition_counter = 0,
        .clock_division = LL_TIM_CLOCKDIVISION_DIV1,
        .timer = TIM1,
        .enable_arr_preload = false,
        .clock_source = LL_TIM_CLOCKSOURCE_INTERNAL,
        .trigger_output_1 = LL_TIM_TRGO_RESET,
        .enable_master_slave_mode = false,
        .enable_dma_update = false,
        .enable_dma_trigger = false,

        //OC parameters
        .oc_count = eTimerDriver_Timer1_OC_Last,
        .oc_table = g_static_timer_driver_timer1_oc_lut,

        //BDTR parameters
        .ossr_state = LL_TIM_OSSR_DISABLE,
        .ossi_state = LL_TIM_OSSI_DISABLE,
        .lock_level = LL_TIM_LOCKLEVEL_OFF,
        .dead_time = 0,
        .break_state = LL_TIM_BREAK_DISABLE,
        .break_polarity = LL_TIM_BREAK_POLARITY_HIGH,
        .automatic_output = LL_TIM_AUTOMATICOUTPUT_DISABLE,
    },

    //Used for PWM servo
    [eTimerDriver_Timer_2] = {
        //Timer parameters
        .clock_function = &LL_APB1_GRP1_EnableClock,
        .clock = LL_APB1_GRP1_PERIPH_TIM2,
        .prescaler = 470,
        .counter_mode = LL_TIM_COUNTERMODE_UP,
        .autoreload = 2549,
        .repetition_counter = 0,
        .clock_division = LL_TIM_CLOCKDIVISION_DIV1,
        .timer = TIM2,
        .enable_arr_preload = false,
        .clock_source = LL_TIM_CLOCKSOURCE_INTERNAL,
        .trigger_output_1 = LL_TIM_TRGO_RESET,
        .enable_master_slave_mode = false,

        //OC parameters
        .oc_count = eTimerDriver_Timer2_OC_Last,
        .oc_table = g_static_timer_driver_timer2_oc_lut,
    },

    //Used for RGBW1 LEDs
    [eTimerDriver_Timer_3] = {
        //Timer parameters
        .clock_function = &LL_APB1_GRP1_EnableClock,
        .clock = LL_APB1_GRP1_PERIPH_TIM3,
        .prescaler = 10,
        .counter_mode = LL_TIM_COUNTERMODE_UP,
        .autoreload = 255,
        .repetition_counter = 0,
        .clock_division = LL_TIM_CLOCKDIVISION_DIV1,
        .timer = TIM3,
        .enable_arr_preload = false,
        .enable_master_slave_mode = false,

        //OC parameters
        .oc_count = eTimerDriver_Timer3_OC_Last,
        .oc_table = g_static_timer_driver_timer3_oc_lut,
    },

    //Used for RGBW2 LEDs
    [eTimerDriver_Timer_4] = {
        //Timer parameters
        .clock_function = &LL_APB1_GRP1_EnableClock,
        .clock = LL_APB1_GRP1_PERIPH_TIM4,
        .prescaler = 10,
        .counter_mode = LL_TIM_COUNTERMODE_UP,
        .autoreload = 255,
        .repetition_counter = 0,
        .clock_division = LL_TIM_CLOCKDIVISION_DIV1,
        .timer = TIM4,
        .enable_arr_preload = false,
        .enable_master_slave_mode = false,

        //OC parameters
        .oc_count = eTimerDriver_Timer4_OC_Last,
        .oc_table = g_static_timer_driver_timer4_oc_lut,
    },

    //Used for motors
    [eTimerDriver_Timer_5] = {
        //Timer parameters
        .clock_function = &LL_APB1_GRP1_EnableClock,
        .clock = LL_APB1_GRP1_PERIPH_TIM5,
        .prescaler = 10,
        .counter_mode = LL_TIM_COUNTERMODE_UP,
        .autoreload = 255,
        .repetition_counter = 0,
        .clock_division = LL_TIM_CLOCKDIVISION_DIV1,
        .timer = TIM5,
        .enable_arr_preload = true,
        .clock_source = LL_TIM_CLOCKSOURCE_INTERNAL,
        .enable_master_slave_mode = false,

        //OC parameters
        .oc_count = eTimerDriver_Timer5_OC_Last,
        .oc_table = g_static_timer_driver_timer5_oc_lut,
    },

    //Unused
    [eTimerDriver_Timer_9] = {
        //Timer parameters
        .clock_function = &LL_APB2_GRP1_EnableClock,
        .clock = LL_APB2_GRP1_PERIPH_TIM9,
        .prescaler = 0,
        .counter_mode = LL_TIM_COUNTERMODE_UP,
        .autoreload = 65535,
        .repetition_counter = 0,
        .clock_division = LL_TIM_CLOCKDIVISION_DIV1,
        .timer = TIM9,
        .enable_arr_preload = false,
        .clock_source = LL_TIM_CLOCKSOURCE_INTERNAL,
    },

    //Used for microsecond delay
    [eTimerDriver_Timer_10] = {
        //Timer parameters
        .clock_function = &LL_APB2_GRP1_EnableClock,
        .clock = LL_APB2_GRP1_PERIPH_TIM10,
        .prescaler = 83,
        .counter_mode = LL_TIM_COUNTERMODE_UP,
        .autoreload = 65535,
        .repetition_counter = 0,
        .clock_division = LL_TIM_CLOCKDIVISION_DIV1,
        .timer = TIM10,
        .enable_arr_preload = false,
        .clock_source = LL_TIM_CLOCKSOURCE_INTERNAL,
    },
};

// @formatter:on
/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/
static sTimerDriver_Dynamic_t g_dynamic_timer_driver_lut[eTimerDriver_Timer_Last];

//volatile unsigned long debug_timer_ticks = 0;
/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/
bool Timer_Driver_EnableDmaRequest (eTimerDriver_Timer_Enum_t timer, eTimerDriver_OC_Enum_t channel);
/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/
bool Timer_Driver_EnableDmaRequest (eTimerDriver_Timer_Enum_t timer, eTimerDriver_OC_Enum_t channel) {
    if (timer >= eTimerDriver_Timer_Last) {
        return false;
    }
    if (channel >= g_static_timer_driver_lut[timer].oc_count) {
        return false;
    }
    switch (channel) {
        case eTimerDriver_OC_1:
            LL_TIM_EnableDMAReq_CC1(g_static_timer_driver_lut[timer].timer);
            break;
        case eTimerDriver_OC_2:
            LL_TIM_EnableDMAReq_CC2(g_static_timer_driver_lut[timer].timer);
            break;
        case eTimerDriver_OC_3:
            LL_TIM_EnableDMAReq_CC3(g_static_timer_driver_lut[timer].timer);
            break;
        case eTimerDriver_OC_4:
            LL_TIM_EnableDMAReq_CC4(g_static_timer_driver_lut[timer].timer);
            break;
        default:
            return false;
    }

    return true;
}
/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/

bool Timer_Driver_Init (eTimerDriver_Timer_Enum_t timer) {
    if (timer >= eTimerDriver_Timer_Last) {
        return false;
    }
    if (g_dynamic_timer_driver_lut[timer].initialized == true) {
        return true;
    }

    //Timer init
    g_static_timer_driver_lut[timer].clock_function(g_static_timer_driver_lut[timer].clock);
    LL_TIM_SetClockSource(g_static_timer_driver_lut[timer].timer, g_static_timer_driver_lut[timer].clock_source);
    LL_TIM_InitTypeDef timer_init_struct = {0};
    timer_init_struct.Prescaler = g_static_timer_driver_lut[timer].prescaler;
    timer_init_struct.CounterMode = g_static_timer_driver_lut[timer].counter_mode;
    timer_init_struct.Autoreload = g_static_timer_driver_lut[timer].autoreload;
    timer_init_struct.ClockDivision = g_static_timer_driver_lut[timer].clock_division;
    timer_init_struct.RepetitionCounter = g_static_timer_driver_lut[timer].repetition_counter;
    if (LL_TIM_Init(g_static_timer_driver_lut[timer].timer, &timer_init_struct) != SUCCESS) {
        return false;
    }
    if (g_static_timer_driver_lut[timer].enable_arr_preload == true) {
        LL_TIM_EnableARRPreload(g_static_timer_driver_lut[timer].timer);
    } else {
        LL_TIM_DisableARRPreload(g_static_timer_driver_lut[timer].timer);
    }

//    if (IS_TIM_MASTER_INSTANCE(g_static_timer_driver_lut[timer].timer)) {
//        LL_TIM_SetTriggerOutput(g_static_timer_driver_lut[timer].timer, g_static_timer_driver_lut[timer].trigger_output_1);
//    }

    if (IS_TIM_SLAVE_INSTANCE(g_static_timer_driver_lut[timer].timer) == true) {
        if (g_static_timer_driver_lut[timer].enable_master_slave_mode == true) {
            LL_TIM_EnableMasterSlaveMode(g_static_timer_driver_lut[timer].timer);
        } else {
            LL_TIM_DisableMasterSlaveMode(g_static_timer_driver_lut[timer].timer);
        }
    }

    if (g_static_timer_driver_lut[timer].enable_dma_update == true) {
        LL_TIM_EnableDMAReq_UPDATE(g_static_timer_driver_lut[timer].timer);
    } else {
        LL_TIM_DisableDMAReq_UPDATE(g_static_timer_driver_lut[timer].timer);
    }

    if (g_static_timer_driver_lut[timer].enable_dma_trigger == true) {
        LL_TIM_EnableDMAReq_TRIG(g_static_timer_driver_lut[timer].timer);
    } else {
        LL_TIM_DisableDMAReq_TRIG(g_static_timer_driver_lut[timer].timer);
    }

    //OC init
    for (eTimerDriver_OC_Enum_t channel = eTimerDriver_OC_First; channel < g_static_timer_driver_lut[timer].oc_count; channel++) {
        LL_TIM_OC_InitTypeDef timer_oc_init_struct = {0};
        timer_oc_init_struct.OCMode = g_static_timer_driver_lut[timer].oc_table[channel].oc_mode;
        timer_oc_init_struct.OCState = g_static_timer_driver_lut[timer].oc_table[channel].oc_state;
        timer_oc_init_struct.OCNState = g_static_timer_driver_lut[timer].oc_table[channel].ocn_state;
        timer_oc_init_struct.CompareValue = g_static_timer_driver_lut[timer].oc_table[channel].compare_value;
        timer_oc_init_struct.OCPolarity = g_static_timer_driver_lut[timer].oc_table[channel].oc_polarity;
        timer_oc_init_struct.OCNPolarity = g_static_timer_driver_lut[timer].oc_table[channel].ocn_polarity;
        timer_oc_init_struct.OCIdleState = g_static_timer_driver_lut[timer].oc_table[channel].oc_idle_state;
        timer_oc_init_struct.OCNIdleState = g_static_timer_driver_lut[timer].oc_table[channel].ocn_idle_state;
        if (LL_TIM_OC_Init(g_static_timer_driver_lut[timer].timer, g_static_timer_driver_lut[timer].oc_table[channel].channel, &timer_oc_init_struct) != SUCCESS) {
            return false;
        }
        if (GPIO_Driver_InitPin(g_static_timer_driver_lut[timer].oc_table[channel].pin, NULL) == false) {
            return false;
        }
        if (g_static_timer_driver_lut[timer].oc_table[channel].enable_preload) {
            LL_TIM_OC_EnablePreload(g_static_timer_driver_lut[timer].timer, g_static_timer_driver_lut[timer].oc_table[channel].channel);
        } else {
            LL_TIM_OC_DisablePreload(g_static_timer_driver_lut[timer].timer, g_static_timer_driver_lut[timer].oc_table[channel].channel);
        }
        if (g_static_timer_driver_lut[timer].oc_table[channel].enable_fast) {
            LL_TIM_OC_DisableFast(g_static_timer_driver_lut[timer].timer, g_static_timer_driver_lut[timer].oc_table[channel].channel);
        } else {
            LL_TIM_OC_DisableFast(g_static_timer_driver_lut[timer].timer, g_static_timer_driver_lut[timer].oc_table[channel].channel);
        }

        if (g_static_timer_driver_lut[timer].oc_table[channel].enable_dma_request) {
            Timer_Driver_EnableDmaRequest(timer, channel);
        }
    }

    //BDTR init
    if (IS_TIM_BREAK_INSTANCE(g_static_timer_driver_lut[timer].timer)) {
        LL_TIM_BDTR_InitTypeDef timer_bdtr_init_struct = {0};
        timer_bdtr_init_struct.OSSRState = g_static_timer_driver_lut[timer].ossr_state;
        timer_bdtr_init_struct.OSSIState = g_static_timer_driver_lut[timer].ossi_state;
        timer_bdtr_init_struct.LockLevel = g_static_timer_driver_lut[timer].lock_level;
        timer_bdtr_init_struct.DeadTime = g_static_timer_driver_lut[timer].dead_time;
        timer_bdtr_init_struct.BreakState = g_static_timer_driver_lut[timer].break_state;
        timer_bdtr_init_struct.BreakPolarity = g_static_timer_driver_lut[timer].break_polarity;
        timer_bdtr_init_struct.AutomaticOutput = g_static_timer_driver_lut[timer].automatic_output;
        if (LL_TIM_BDTR_Init(g_static_timer_driver_lut[timer].timer, &timer_bdtr_init_struct) != SUCCESS) {
            return false;
        }
        LL_TIM_EnableAllOutputs(g_static_timer_driver_lut[timer].timer);
    }

    g_dynamic_timer_driver_lut[timer].initialized = true;

    return true;
}

bool Timer_Driver_SetDutyCycle (eTimerDriver_Timer_Enum_t timer, eTimerDriver_OC_Enum_t channel, uint16_t duty) {
    if (timer >= eTimerDriver_Timer_Last) {
        return false;
    }
    if (channel >= g_static_timer_driver_lut[timer].oc_count) {
        return false;
    }
    switch (channel) {
        case eTimerDriver_OC_1:
            LL_TIM_OC_SetCompareCH1(g_static_timer_driver_lut[timer].timer, duty);
            break;
        case eTimerDriver_OC_2:
            LL_TIM_OC_SetCompareCH2(g_static_timer_driver_lut[timer].timer, duty);
            break;
        case eTimerDriver_OC_3:
            LL_TIM_OC_SetCompareCH3(g_static_timer_driver_lut[timer].timer, duty);
            break;
        case eTimerDriver_OC_4:
            LL_TIM_OC_SetCompareCH4(g_static_timer_driver_lut[timer].timer, duty);
            break;
        default:
            return false;
    }

    return true;
}

bool Timer_Driver_Start (eTimerDriver_Timer_Enum_t timer) {
    if (timer >= eTimerDriver_Timer_Last) {
        return false;
    }
    LL_TIM_EnableCounter(g_static_timer_driver_lut[timer].timer);
    return true;
}

bool Timer_Driver_Stop (eTimerDriver_Timer_Enum_t timer) {
    if (timer >= eTimerDriver_Timer_Last) {
        return false;
    }
    LL_TIM_DisableCounter(g_static_timer_driver_lut[timer].timer);
    return true;
}

bool Timer_Driver_EnableChannel (eTimerDriver_Timer_Enum_t timer, eTimerDriver_OC_Enum_t channel) {
    if (timer >= eTimerDriver_Timer_Last) {
        return false;
    }
    if (channel >= g_static_timer_driver_lut[timer].oc_count) {
        return false;
    }
    LL_TIM_CC_EnableChannel(g_static_timer_driver_lut[timer].timer, g_static_timer_driver_lut[timer].oc_table[channel].channel);
    return true;
}

bool Timer_Driver_DisableChannel (eTimerDriver_Timer_Enum_t timer, eTimerDriver_OC_Enum_t channel) {
    if (timer >= eTimerDriver_Timer_Last) {
        return false;
    }
    if (channel >= g_static_timer_driver_lut[timer].oc_count) {
        return false;
    }
    LL_TIM_CC_DisableChannel(g_static_timer_driver_lut[timer].timer, g_static_timer_driver_lut[timer].oc_table[channel].channel);
    return true;
}

bool Timer_Driver_GetTimerInfo (eTimerDriver_Timer_Enum_t timer, sTimerDriver_Static_t *info) {
    if (timer >= eTimerDriver_Timer_Last) {
        return false;
    }
    if (info == NULL) {
        return false;
    }
    *info = g_static_timer_driver_lut[timer];
    return true;
}

bool Timer_Driver_SetCounter (eTimerDriver_Timer_Enum_t timer, uint16_t value) {
    if (timer >= eTimerDriver_Timer_Last) {
        return false;
    }
    LL_TIM_SetCounter(g_static_timer_driver_lut[timer].timer, value);
    return true;
}

bool Timer_Driver_GetCounter (eTimerDriver_Timer_Enum_t timer, uint16_t *value) {
    if (timer >= eTimerDriver_Timer_Last) {
        return false;
    }
    *value = LL_TIM_GetCounter(g_static_timer_driver_lut[timer].timer);
    return true;
}

bool Timer_Driver_InitWatchdog (void) {
    LL_IWDG_Enable(IWDG);
    LL_IWDG_EnableWriteAccess(IWDG);
    LL_IWDG_SetPrescaler(IWDG, LL_IWDG_PRESCALER_32);
    LL_IWDG_SetReloadCounter(IWDG, 2624);
    while (LL_IWDG_IsReady(IWDG) != 1){}
    return true;
}

bool Timer_Driver_ResetWatchdog (void) {
    LL_IWDG_ReloadCounter(IWDG);
    return true;
}

extern TIM_HandleTypeDef htim11;

void TIM1_TRG_COM_TIM11_IRQHandler (void) {
    HAL_TIM_IRQHandler(&htim11);
}

//For debugging stack sizes

volatile unsigned long debug_timer_ticks = 0;

void configureTimerForRunTimeStats (void) {
    debug_timer_ticks = 0;
    LL_TIM_EnableIT_UPDATE(TIM10);
    LL_TIM_EnableCounter(TIM10);
}

unsigned long getRunTimeCounterValue (void) {
    return debug_timer_ticks;
}

void TIM10_IRQHandler (void) {
    debug_timer_ticks++;
    LL_TIM_ClearFlag_UPDATE(TIM10);
}
