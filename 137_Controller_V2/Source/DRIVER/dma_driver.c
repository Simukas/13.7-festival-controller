/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "dma_driver.h"
#include "stm32f4xx_ll_dma.h"
#include "stm32f4xx_ll_bus.h"
#include "stdint.h"
#include "stdbool.h"
#include "debug_api.h"
#include "gpio_driver.h"
#include "ws2815_api.h"
#include "adc_driver.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/
typedef struct sDmaDriverStatic_t {
    void (*clock_function) (uint32_t);
    uint32_t clock;
    DMA_TypeDef *dma;
    uint32_t stream;
    uint32_t channel;
    uint32_t direction;
    uint32_t mode;
    uint32_t source_incrementation_mode;
    uint32_t destination_incrementation_mode;
    uint32_t source_data_size;
    uint32_t destination_data_size;
    uint32_t peripheral_request;
    uint32_t priority;
    IRQn_Type stream_interrupt;
    bool enable_fifo;
    bool enable_stream_interrupt;
    bool enable_tc_interrupt;
    bool enable_ht_interrupt;
    bool enable_te_interrupt;
} sDmaDriverStatic_t;

typedef struct sDmaDriverDynamic_t {
    uint16_t data_buffer_size;
    void *peripheral_or_source_address;
    void *destination_address;
    void (*IT_callback) (eDmaDriver_Stream_Enum_t, eDmaDriver_IT_Enum_t);
    bool initialized;
} sDmaDriverDynamic_t;
/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
static const sDmaDriverStatic_t g_static_dma_driver_stream_lut[eDmaDriver_Stream_Last] = {
    [eDmaDriver_Stream_USART6_RX] = {
        .clock_function = &LL_AHB1_GRP1_EnableClock,
        .clock = LL_AHB1_GRP1_PERIPH_DMA2,
        .dma = DMA2,
        .stream = LL_DMA_STREAM_1,
        .channel = LL_DMA_CHANNEL_5,
        .direction = LL_DMA_DIRECTION_PERIPH_TO_MEMORY,
        .priority = LL_DMA_PRIORITY_VERYHIGH,
        .mode = LL_DMA_MODE_CIRCULAR,
        .destination_incrementation_mode = LL_DMA_PERIPH_NOINCREMENT,
        .source_incrementation_mode = LL_DMA_MEMORY_INCREMENT,
        .destination_data_size = LL_DMA_MDATAALIGN_BYTE,
        .source_data_size = LL_DMA_PDATAALIGN_BYTE,
        .enable_fifo = false,
        .stream_interrupt = DMA2_Stream1_IRQn,
        .enable_stream_interrupt = false,
        .enable_ht_interrupt = false,
        .enable_tc_interrupt = false,
        .enable_te_interrupt = false,
    },
    [eDmaDriver_Stream_USART6_TX] = {
        .clock_function = &LL_AHB1_GRP1_EnableClock,
        .clock = LL_AHB1_GRP1_PERIPH_DMA2,
        .dma = DMA2,
        .stream = LL_DMA_STREAM_7,
        .channel = LL_DMA_CHANNEL_5,
        .direction = LL_DMA_DIRECTION_MEMORY_TO_PERIPH,
        .priority = LL_DMA_PRIORITY_VERYHIGH,
        .mode = LL_DMA_MODE_NORMAL,
        .destination_incrementation_mode = LL_DMA_PERIPH_NOINCREMENT,
        .source_incrementation_mode = LL_DMA_MEMORY_INCREMENT,
        .destination_data_size = LL_DMA_MDATAALIGN_BYTE,
        .source_data_size = LL_DMA_PDATAALIGN_BYTE,
        .enable_fifo = false,
        .stream_interrupt = DMA2_Stream7_IRQn,
        .enable_stream_interrupt = true,
        .enable_ht_interrupt = false,
        .enable_tc_interrupt = true,
        .enable_te_interrupt = false,
    },
    [eDmaDriver_Stream_ARGB1] = {
        .clock_function = &LL_AHB1_GRP1_EnableClock,
        .clock = LL_AHB1_GRP1_PERIPH_DMA2,
        .dma = DMA2,
        .stream = LL_DMA_STREAM_3,
        .channel = LL_DMA_CHANNEL_6,
        .direction = LL_DMA_DIRECTION_MEMORY_TO_PERIPH,
        .priority = LL_DMA_PRIORITY_VERYHIGH,
        .mode = LL_DMA_MODE_CIRCULAR,
        .destination_incrementation_mode = LL_DMA_PERIPH_NOINCREMENT,
        .source_incrementation_mode = LL_DMA_MEMORY_INCREMENT,
        .destination_data_size = LL_DMA_MDATAALIGN_HALFWORD,
        .source_data_size = LL_DMA_PDATAALIGN_HALFWORD,
        .enable_fifo = false,
        .stream_interrupt = DMA2_Stream3_IRQn,
        .enable_stream_interrupt = true,
        .enable_ht_interrupt = true,
        .enable_tc_interrupt = true,
        .enable_te_interrupt = false,
    },
    [eDmaDriver_Stream_ARGB2] = {
        .clock_function = &LL_AHB1_GRP1_EnableClock,
        .clock = LL_AHB1_GRP1_PERIPH_DMA2,
        .dma = DMA2,
        .stream = LL_DMA_STREAM_2,
        .channel = LL_DMA_CHANNEL_6,
        .direction = LL_DMA_DIRECTION_MEMORY_TO_PERIPH,
        .priority = LL_DMA_PRIORITY_VERYHIGH,
        .mode = LL_DMA_MODE_CIRCULAR,
        .destination_incrementation_mode = LL_DMA_PERIPH_NOINCREMENT,
        .source_incrementation_mode = LL_DMA_MEMORY_INCREMENT,
        .destination_data_size = LL_DMA_MDATAALIGN_HALFWORD,
        .source_data_size = LL_DMA_PDATAALIGN_HALFWORD,
        .enable_fifo = false,
        .stream_interrupt = DMA2_Stream2_IRQn,
        .enable_stream_interrupt = false,
        .enable_ht_interrupt = false,
        .enable_tc_interrupt = false,
        .enable_te_interrupt = false,
    },
    [eDmaDriver_Stream_ARGB3] = {
        .clock_function = &LL_AHB1_GRP1_EnableClock,
        .clock = LL_AHB1_GRP1_PERIPH_DMA2,
        .dma = DMA2,
        .stream = LL_DMA_STREAM_6,
        .channel = LL_DMA_CHANNEL_6,
        .direction = LL_DMA_DIRECTION_MEMORY_TO_PERIPH,
        .priority = LL_DMA_PRIORITY_VERYHIGH,
        .mode = LL_DMA_MODE_CIRCULAR,
        .destination_incrementation_mode = LL_DMA_PERIPH_NOINCREMENT,
        .source_incrementation_mode = LL_DMA_MEMORY_INCREMENT,
        .destination_data_size = LL_DMA_MDATAALIGN_HALFWORD,
        .source_data_size = LL_DMA_PDATAALIGN_HALFWORD,
        .enable_fifo = false,
        .stream_interrupt = DMA2_Stream6_IRQn,
        .enable_stream_interrupt = false,
        .enable_ht_interrupt = false,
        .enable_tc_interrupt = false,
        .enable_te_interrupt = false,
    },
    [eDmaDriver_Stream_ADC] = {
        .clock_function = &LL_AHB1_GRP1_EnableClock,
        .clock = LL_AHB1_GRP1_PERIPH_DMA2,
        .dma = DMA2,
        .stream = LL_DMA_STREAM_0,
        .channel = LL_DMA_CHANNEL_0,
        .direction = LL_DMA_DIRECTION_PERIPH_TO_MEMORY,
        .priority = LL_DMA_PRIORITY_VERYHIGH,
        .mode = LL_DMA_MODE_CIRCULAR,
        .destination_incrementation_mode = LL_DMA_PERIPH_NOINCREMENT,
        .source_incrementation_mode = LL_DMA_MEMORY_INCREMENT,
        .destination_data_size = LL_DMA_MDATAALIGN_HALFWORD,
        .source_data_size = LL_DMA_PDATAALIGN_HALFWORD,
        .enable_fifo = false,
        .stream_interrupt = DMA2_Stream0_IRQn,
        .enable_stream_interrupt = false,
        .enable_ht_interrupt = false,
        .enable_tc_interrupt = false,
        .enable_te_interrupt = false,
    },
    [eDmaDriver_Stream_I2C2_TX] = {
        .clock_function = &LL_AHB1_GRP1_EnableClock,
        .clock = LL_AHB1_GRP1_PERIPH_DMA1,
        .dma = DMA1,
        .stream = LL_DMA_STREAM_7,
        .channel = LL_DMA_CHANNEL_7,
        .direction = LL_DMA_DIRECTION_MEMORY_TO_PERIPH,
        .priority = LL_DMA_PRIORITY_HIGH,
        .mode = LL_DMA_MODE_NORMAL,
        .destination_incrementation_mode = LL_DMA_PERIPH_NOINCREMENT,
        .source_incrementation_mode = LL_DMA_MEMORY_INCREMENT,
        .destination_data_size = LL_DMA_MDATAALIGN_BYTE,
        .source_data_size = LL_DMA_PDATAALIGN_BYTE,
        .enable_fifo = false,
        .stream_interrupt = DMA1_Stream7_IRQn,
        .enable_stream_interrupt = true,
        .enable_ht_interrupt = false,
        .enable_tc_interrupt = true,
        .enable_te_interrupt = false,
    },
    [eDmaDriver_Stream_I2C2_RX] = {
        .clock_function = &LL_AHB1_GRP1_EnableClock,
        .clock = LL_AHB1_GRP1_PERIPH_DMA1,
        .dma = DMA1,
        .stream = LL_DMA_STREAM_2,
        .channel = LL_DMA_CHANNEL_7,
        .direction = LL_DMA_DIRECTION_PERIPH_TO_MEMORY,
        .priority = LL_DMA_PRIORITY_HIGH,
        .mode = LL_DMA_MODE_NORMAL,
        .destination_incrementation_mode = LL_DMA_PERIPH_NOINCREMENT,
        .source_incrementation_mode = LL_DMA_MEMORY_INCREMENT,
        .destination_data_size = LL_DMA_MDATAALIGN_BYTE,
        .source_data_size = LL_DMA_PDATAALIGN_BYTE,
        .enable_fifo = false,
        .stream_interrupt = DMA1_Stream2_IRQn,
        .enable_stream_interrupt = true,
        .enable_ht_interrupt = false,
        .enable_tc_interrupt = true,
        .enable_te_interrupt = false,
    }
};
/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/
sDmaDriverDynamic_t g_dynamic_dma_driver_stream_lut[eDmaDriver_Stream_Last] = {
    [eDmaDriver_Stream_USART6_RX]   = {.IT_callback = NULL, .initialized = false},
    [eDmaDriver_Stream_USART6_TX]   = {.IT_callback = NULL, .initialized = false},
    [eDmaDriver_Stream_ARGB1]       = {.IT_callback = NULL, .initialized = false},
    [eDmaDriver_Stream_ARGB2]       = {.IT_callback = NULL, .initialized = false},
    [eDmaDriver_Stream_ARGB3]       = {.IT_callback = NULL, .initialized = false},
    [eDmaDriver_Stream_ADC]         = {.IT_callback = NULL, .initialized = false},
    [eDmaDriver_Stream_I2C2_TX]     = {.IT_callback = NULL, .initialized = false},
    [eDmaDriver_Stream_I2C2_RX]     = {.IT_callback = NULL, .initialized = false}
};
/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/
void DMA2_Stream1_IRQHandler (void);
void DMA2_Stream3_IRQHandler (void);
void DMA2_Stream7_IRQHandler (void);
void DMA1_Stream7_IRQHandler (void);
void DMA1_Stream2_IRQHandler (void);
static bool DMA_Driver_ClearFlagTC (uint32_t stream);
static bool DMA_Driver_ClearFlagHT (uint32_t stream);
static bool DMA_Driver_ClearFlagTE (uint32_t stream);
static bool DMA_Driver_IsActiveFlagTC (uint32_t stream, bool *return_value);
static bool DMA_Driver_IsActiveFlagHT (uint32_t stream, bool *return_value);
static bool DMA_Driver_IsActiveFlagTE (uint32_t stream, bool *return_value);
/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/
static bool DMA_Driver_ClearFlagTC (uint32_t stream) {
    if (stream > LL_DMA_STREAM_7) {
        return false;
    }
    switch (stream) {
        case LL_DMA_STREAM_0:
            LL_DMA_ClearFlag_TC0(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_1:
            LL_DMA_ClearFlag_TC1(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_2:
            LL_DMA_ClearFlag_TC2(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_3:
            LL_DMA_ClearFlag_TC3(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_4:
            LL_DMA_ClearFlag_TC4(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_5:
            LL_DMA_ClearFlag_TC5(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_6:
            LL_DMA_ClearFlag_TC6(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_7:
            LL_DMA_ClearFlag_TC7(g_static_dma_driver_stream_lut[stream].dma);
            break;
        default:
            return false;
    }
    return true;
}

static bool DMA_Driver_ClearFlagHT (uint32_t stream) {
    if (stream > LL_DMA_STREAM_7) {
        return false;
    }
    switch (stream) {
        case LL_DMA_STREAM_0:
            LL_DMA_ClearFlag_HT0(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_1:
            LL_DMA_ClearFlag_HT1(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_2:
            LL_DMA_ClearFlag_HT2(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_3:
            LL_DMA_ClearFlag_HT3(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_4:
            LL_DMA_ClearFlag_HT4(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_5:
            LL_DMA_ClearFlag_HT5(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_6:
            LL_DMA_ClearFlag_HT6(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_7:
            LL_DMA_ClearFlag_HT7(g_static_dma_driver_stream_lut[stream].dma);
            break;
        default:
            return false;
    }
    return true;
}

static bool DMA_Driver_ClearFlagTE (uint32_t stream) {
    if (stream > LL_DMA_STREAM_7) {
        return false;
    }
    switch (stream) {
        case LL_DMA_STREAM_0:
            LL_DMA_ClearFlag_TE0(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_1:
            LL_DMA_ClearFlag_TE1(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_2:
            LL_DMA_ClearFlag_TE2(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_3:
            LL_DMA_ClearFlag_TE3(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_4:
            LL_DMA_ClearFlag_TE4(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_5:
            LL_DMA_ClearFlag_TE5(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_6:
            LL_DMA_ClearFlag_TE6(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_7:
            LL_DMA_ClearFlag_TE7(g_static_dma_driver_stream_lut[stream].dma);
            break;
        default:
            return false;
    }
    return true;
}

static bool DMA_Driver_IsActiveFlagTC (uint32_t stream, bool *return_value) {
    if (stream > LL_DMA_STREAM_7) {
        return false;
    }
    if (return_value == NULL) {
        return false;
    }
    switch (stream) {
        case LL_DMA_STREAM_0:
            *return_value = LL_DMA_IsActiveFlag_TC0(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_1:
            *return_value = LL_DMA_IsActiveFlag_TC1(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_2:
            *return_value = LL_DMA_IsActiveFlag_TC2(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_3:
            *return_value = LL_DMA_IsActiveFlag_TC3(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_4:
            *return_value = LL_DMA_IsActiveFlag_TC4(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_5:
            *return_value = LL_DMA_IsActiveFlag_TC5(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_6:
            *return_value = LL_DMA_IsActiveFlag_TC6(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_7:
            *return_value = LL_DMA_IsActiveFlag_TC7(g_static_dma_driver_stream_lut[stream].dma);
            break;
        default:
            return false;
    }
    return true;
}

static bool DMA_Driver_IsActiveFlagHT (uint32_t stream, bool *return_value) {
    if (stream > LL_DMA_STREAM_7) {
        return false;
    }
    if (return_value == NULL) {
        return false;
    }
    switch (stream) {
        case LL_DMA_STREAM_0:
            *return_value = LL_DMA_IsActiveFlag_HT0(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_1:
            *return_value = LL_DMA_IsActiveFlag_HT1(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_2:
            *return_value = LL_DMA_IsActiveFlag_HT2(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_3:
            *return_value = LL_DMA_IsActiveFlag_HT3(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_4:
            *return_value = LL_DMA_IsActiveFlag_HT4(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_5:
            *return_value = LL_DMA_IsActiveFlag_HT5(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_6:
            *return_value = LL_DMA_IsActiveFlag_HT6(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_7:
            *return_value = LL_DMA_IsActiveFlag_HT7(g_static_dma_driver_stream_lut[stream].dma);
            break;
        default:
            return false;
    }
    return true;
}

static bool DMA_Driver_IsActiveFlagTE (uint32_t stream, bool *return_value) {
    if (stream > LL_DMA_STREAM_7) {
        return false;
    }
    if (return_value == NULL) {
        return false;
    }
    switch (stream) {
        case LL_DMA_STREAM_0:
            *return_value = LL_DMA_IsActiveFlag_TE0(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_1:
            *return_value = LL_DMA_IsActiveFlag_TE1(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_2:
            *return_value = LL_DMA_IsActiveFlag_TE2(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_3:
            *return_value = LL_DMA_IsActiveFlag_TE3(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_4:
            *return_value = LL_DMA_IsActiveFlag_TE4(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_5:
            *return_value = LL_DMA_IsActiveFlag_TE5(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_6:
            *return_value = LL_DMA_IsActiveFlag_TE6(g_static_dma_driver_stream_lut[stream].dma);
            break;
        case LL_DMA_STREAM_7:
            *return_value = LL_DMA_IsActiveFlag_TE7(g_static_dma_driver_stream_lut[stream].dma);
            break;
        default:
            return false;
    }
    return true;
}
/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool DMA_Driver_Init (DMA_Driver_Init_t *init_data) {
    if (init_data->stream >= eDmaDriver_Stream_Last) {
        return false;
    }
    if (init_data->IT_callback == NULL) {
        return false;
    }

    if (g_dynamic_dma_driver_stream_lut[init_data->stream].initialized == true) {
        return true;
    }

    g_dynamic_dma_driver_stream_lut[init_data->stream].IT_callback = init_data->IT_callback;

    g_dynamic_dma_driver_stream_lut[init_data->stream].data_buffer_size = init_data->data_amount;

    g_static_dma_driver_stream_lut[init_data->stream].clock_function(g_static_dma_driver_stream_lut[init_data->stream].clock);

    if (g_static_dma_driver_stream_lut[init_data->stream].enable_stream_interrupt == true) {
        NVIC_SetPriority(g_static_dma_driver_stream_lut[init_data->stream].stream_interrupt, 79);
        NVIC_EnableIRQ(g_static_dma_driver_stream_lut[init_data->stream].stream_interrupt);
    } else {
        NVIC_DisableIRQ(g_static_dma_driver_stream_lut[init_data->stream].stream_interrupt);
    }

    LL_DMA_InitTypeDef DMA_InitStruct = {
        .PeriphOrM2MSrcAddress = (uint32_t) init_data->peripheral_or_source_address,
        .MemoryOrM2MDstAddress = (uint32_t) init_data->destination_address,
        .Direction = g_static_dma_driver_stream_lut[init_data->stream].direction,
        .Mode = g_static_dma_driver_stream_lut[init_data->stream].mode,
        .PeriphOrM2MSrcIncMode = g_static_dma_driver_stream_lut[init_data->stream].source_incrementation_mode,
        .MemoryOrM2MDstIncMode = g_static_dma_driver_stream_lut[init_data->stream].destination_incrementation_mode,
        .PeriphOrM2MSrcDataSize = g_static_dma_driver_stream_lut[init_data->stream].source_data_size,
        .MemoryOrM2MDstDataSize = g_static_dma_driver_stream_lut[init_data->stream].destination_data_size,
        .NbData = g_dynamic_dma_driver_stream_lut[init_data->stream].data_buffer_size,
        .Priority = g_static_dma_driver_stream_lut[init_data->stream].priority,
        .Channel = g_static_dma_driver_stream_lut[init_data->stream].channel,
    };

    if (LL_DMA_Init(g_static_dma_driver_stream_lut[init_data->stream].dma, g_static_dma_driver_stream_lut[init_data->stream].stream, &DMA_InitStruct) != SUCCESS) {
        return false;
    }

    if (g_static_dma_driver_stream_lut[init_data->stream].enable_fifo == true) {
        LL_DMA_EnableFifoMode(g_static_dma_driver_stream_lut[init_data->stream].dma, g_static_dma_driver_stream_lut[init_data->stream].stream);
    } else {
        LL_DMA_DisableFifoMode(g_static_dma_driver_stream_lut[init_data->stream].dma, g_static_dma_driver_stream_lut[init_data->stream].stream);
    }

    if (g_static_dma_driver_stream_lut[init_data->stream].enable_ht_interrupt == true) {
        DMA_Driver_EnableEventInterrupt(init_data->stream, eDmaDriver_IT_HT);
    } else {
        DMA_Driver_DisableEventInterrupt(init_data->stream, eDmaDriver_IT_HT);
    }

    if (g_static_dma_driver_stream_lut[init_data->stream].enable_tc_interrupt == true) {
        DMA_Driver_EnableEventInterrupt(init_data->stream, eDmaDriver_IT_TC);
    } else {
        DMA_Driver_DisableEventInterrupt(init_data->stream, eDmaDriver_IT_TC);
    }

    if (g_static_dma_driver_stream_lut[init_data->stream].enable_te_interrupt == true) {
        DMA_Driver_EnableEventInterrupt(init_data->stream, eDmaDriver_IT_TE);
    } else {
        DMA_Driver_DisableEventInterrupt(init_data->stream, eDmaDriver_IT_TE);
    }

    return true;
}

bool DMA_Driver_EnableStream (eDmaDriver_Stream_Enum_t stream) {
    if (stream >= eDmaDriver_Stream_Last) {
        return false;
    }
    LL_DMA_EnableStream(g_static_dma_driver_stream_lut[stream].dma, g_static_dma_driver_stream_lut[stream].stream);
    return true;
}

bool DMA_Driver_DisableStream (eDmaDriver_Stream_Enum_t stream) {
    if (stream >= eDmaDriver_Stream_Last) {
        return false;
    }
    LL_DMA_DisableStream(g_static_dma_driver_stream_lut[stream].dma, g_static_dma_driver_stream_lut[stream].stream);
    return true;
}

bool DMA_Driver_ClearFlag (eDmaDriver_Stream_Enum_t stream, eDmaDriver_IT_Enum_t flag) {
    if (stream >= eDmaDriver_Stream_Last) {
        return false;
    }
    if (flag >= eDmaDriver_IT_Last) {
        return false;
    }
    switch (flag) {
        case eDmaDriver_IT_TC:
            DMA_Driver_ClearFlagTC(stream);
            break;
        case eDmaDriver_IT_HT:
            DMA_Driver_ClearFlagHT(stream);
            break;
        case eDmaDriver_IT_TE:
            DMA_Driver_ClearFlagTE(stream);
            break;
        default:
            return false;
    }
    return true;
}

bool DMA_Driver_IsActiveFlag (eDmaDriver_Stream_Enum_t stream, eDmaDriver_IT_Enum_t flag, bool *return_value) {
    if (stream >= eDmaDriver_Stream_Last) {
        return false;
    }
    if (flag >= eDmaDriver_IT_Last) {
        return false;
    }
    if (return_value == NULL) {
        return false;
    }
    switch (flag) {
        case eDmaDriver_IT_TC:
            DMA_Driver_IsActiveFlagTC(stream, return_value);
            break;
        case eDmaDriver_IT_HT:
            DMA_Driver_IsActiveFlagHT(stream, return_value);
            break;
        case eDmaDriver_IT_TE:
            DMA_Driver_IsActiveFlagTE(stream, return_value);
            break;
        default:
            return false;
    }
    return true;
}

bool DMA_Driver_EnableEventInterrupt (eDmaDriver_Stream_Enum_t stream, eDmaDriver_IT_Enum_t flag) {
    if (stream >= eDmaDriver_Stream_Last) {
        return false;
    }
    if (flag >= eDmaDriver_IT_Last) {
        return false;
    }
    switch (flag) {
        case eDmaDriver_IT_TC:
            LL_DMA_EnableIT_TC(g_static_dma_driver_stream_lut[stream].dma, g_static_dma_driver_stream_lut[stream].stream);
            break;
        case eDmaDriver_IT_HT:
            LL_DMA_EnableIT_HT(g_static_dma_driver_stream_lut[stream].dma, g_static_dma_driver_stream_lut[stream].stream);
            break;
        case eDmaDriver_IT_TE:
            LL_DMA_EnableIT_TE(g_static_dma_driver_stream_lut[stream].dma, g_static_dma_driver_stream_lut[stream].stream);
            break;
        default:
            return false;
    }
    return true;
}

bool DMA_Driver_DisableEventInterrupt (eDmaDriver_Stream_Enum_t stream, eDmaDriver_IT_Enum_t flag) {
    if (stream >= eDmaDriver_Stream_Last) {
        return false;
    }
    if (flag >= eDmaDriver_IT_Last) {
        return false;
    }
    switch (flag) {
        case eDmaDriver_IT_TC:
            LL_DMA_DisableIT_TC(g_static_dma_driver_stream_lut[stream].dma, g_static_dma_driver_stream_lut[stream].stream);
            break;
        case eDmaDriver_IT_HT:
            LL_DMA_DisableIT_HT(g_static_dma_driver_stream_lut[stream].dma, g_static_dma_driver_stream_lut[stream].stream);
            break;
        case eDmaDriver_IT_TE:
            LL_DMA_DisableIT_TE(g_static_dma_driver_stream_lut[stream].dma, g_static_dma_driver_stream_lut[stream].stream);
            break;
        default:
            return false;
    }
    return true;
}

bool DMA_Driver_EnableStreamInterrupt (eDmaDriver_Stream_Enum_t stream) {
    if (stream >= eDmaDriver_Stream_Last) {
        return false;
    }
    NVIC_SetPriority(g_static_dma_driver_stream_lut[stream].stream_interrupt, 79);
    NVIC_EnableIRQ(g_static_dma_driver_stream_lut[stream].stream_interrupt);
    return true;
}

bool DMA_Driver_DisableStreamInterrupt (eDmaDriver_Stream_Enum_t stream) {
    if (stream >= eDmaDriver_Stream_Last) {
        return false;
    }
    NVIC_DisableIRQ(g_static_dma_driver_stream_lut[stream].stream_interrupt);
    return true;
}

bool DMA_Driver_IsInterruptEnabled (eDmaDriver_Stream_Enum_t stream, eDmaDriver_IT_Enum_t flag, bool *return_value) {
    if (stream >= eDmaDriver_Stream_Last) {
        return false;
    }
    if (flag >= eDmaDriver_IT_Last) {
        return false;
    }
    if (return_value == NULL) {
        return false;
    }
    switch (flag) {
        case eDmaDriver_IT_TC:
            *return_value = LL_DMA_IsEnabledIT_TC(g_static_dma_driver_stream_lut[stream].dma, g_static_dma_driver_stream_lut[stream].stream);
            break;
        case eDmaDriver_IT_HT:
            *return_value = LL_DMA_IsEnabledIT_HT(g_static_dma_driver_stream_lut[stream].dma, g_static_dma_driver_stream_lut[stream].stream);
            break;
        case eDmaDriver_IT_TE:
            *return_value = LL_DMA_IsEnabledIT_TE(g_static_dma_driver_stream_lut[stream].dma, g_static_dma_driver_stream_lut[stream].stream);
            break;
        default:
            return false;
    }
    return true;
}

bool DMA_Driver_SetDataCount (eDmaDriver_Stream_Enum_t stream, uint16_t data_count) {
    if (stream >= eDmaDriver_Stream_Last) {
        return false;
    }
    if (data_count == 0) {
        return false;
    }
    LL_DMA_SetDataLength(g_static_dma_driver_stream_lut[stream].dma, g_static_dma_driver_stream_lut[stream].stream, data_count);
    return true;
}

bool DMA_Driver_SetDataPointer (eDmaDriver_Stream_Enum_t stream, uint8_t *data) {
    if (stream >= eDmaDriver_Stream_Last) {
        return false;
    }
    if (data == NULL) {
        return false;
    }
    LL_DMA_SetMemoryAddress(g_static_dma_driver_stream_lut[stream].dma, g_static_dma_driver_stream_lut[stream].stream, (uint32_t) data);
    return true;
}

bool DMA_Driver_GetRemainingCount (eDmaDriver_Stream_Enum_t stream, uint16_t *remaining_data) {
    if (stream >= eDmaDriver_Stream_Last) {
        return false;
    }
    if (remaining_data == NULL) {
        return false;
    }
    *remaining_data = LL_DMA_GetDataLength(g_static_dma_driver_stream_lut[stream].dma, g_static_dma_driver_stream_lut[stream].stream);
    return true;
}

bool DMA_Driver_GetCompletedCount (eDmaDriver_Stream_Enum_t stream, uint16_t *remaining_data) {
//    if (stream >= eDmaDriver_Stream_Last) {
//        return false;
//    }
//    if (remaining_data == NULL) {
//        return false;
//    }
    *remaining_data = g_dynamic_dma_driver_stream_lut[stream].data_buffer_size - LL_DMA_GetDataLength(g_static_dma_driver_stream_lut[stream].dma, g_static_dma_driver_stream_lut[stream].stream);
    return true;
}

void DMA2_Stream1_IRQHandler (void) {
    if (LL_DMA_IsActiveFlag_TC1(DMA2) == true) {
        LL_DMA_ClearFlag_TC1(DMA2);
        g_dynamic_dma_driver_stream_lut[eDmaDriver_Stream_USART6_RX].IT_callback(eDmaDriver_Stream_USART6_RX, eDmaDriver_IT_TC);
    }
    if (LL_DMA_IsActiveFlag_HT1(DMA2) == true) {
        LL_DMA_ClearFlag_HT1(DMA2);
        g_dynamic_dma_driver_stream_lut[eDmaDriver_Stream_USART6_RX].IT_callback(eDmaDriver_Stream_USART6_RX, eDmaDriver_IT_HT);
    }
    if (LL_DMA_IsActiveFlag_TE1(DMA2) == true) {
        LL_DMA_ClearFlag_TE1(DMA2);
        g_dynamic_dma_driver_stream_lut[eDmaDriver_Stream_USART6_RX].IT_callback(eDmaDriver_Stream_USART6_RX, eDmaDriver_IT_TE);
    }
}

void DMA2_Stream2_IRQHandler (void) {
    if (LL_DMA_IsActiveFlag_TC2(DMA2) == true) {
        LL_DMA_ClearFlag_TC2(DMA2);
        g_dynamic_dma_driver_stream_lut[eDmaDriver_Stream_ARGB2].IT_callback(eDmaDriver_Stream_ARGB2, eDmaDriver_IT_TC);
    }
    if (LL_DMA_IsActiveFlag_HT2(DMA2) == true) {
        LL_DMA_ClearFlag_HT2(DMA2);
        g_dynamic_dma_driver_stream_lut[eDmaDriver_Stream_ARGB2].IT_callback(eDmaDriver_Stream_ARGB2, eDmaDriver_IT_HT);
    }
    if (LL_DMA_IsActiveFlag_TE2(DMA2) == true) {
        LL_DMA_ClearFlag_TE2(DMA2);
        g_dynamic_dma_driver_stream_lut[eDmaDriver_Stream_ARGB2].IT_callback(eDmaDriver_Stream_ARGB2, eDmaDriver_IT_TE);
    }
}

void DMA2_Stream3_IRQHandler (void) {
    //GPIO_Driver_SetPin(eGpioDriver_Pin_I2C_SDA);
    if (LL_DMA_IsActiveFlag_TC3(DMA2) == true) {
        LL_DMA_ClearFlag_TC3(DMA2);
        g_dynamic_dma_driver_stream_lut[eDmaDriver_Stream_ARGB1].IT_callback(eDmaDriver_Stream_ARGB1, eDmaDriver_IT_TC);
    }
    if (LL_DMA_IsActiveFlag_HT3(DMA2) == true) {
        LL_DMA_ClearFlag_HT3(DMA2);
        g_dynamic_dma_driver_stream_lut[eDmaDriver_Stream_ARGB1].IT_callback(eDmaDriver_Stream_ARGB1, eDmaDriver_IT_HT);
    }
    if (LL_DMA_IsActiveFlag_TE3(DMA2) == true) {
        LL_DMA_ClearFlag_TE3(DMA2);
        g_dynamic_dma_driver_stream_lut[eDmaDriver_Stream_ARGB1].IT_callback(eDmaDriver_Stream_ARGB1, eDmaDriver_IT_TE);
    }
    //GPIO_Driver_ResetPin(eGpioDriver_Pin_I2C_SDA);
}

void DMA2_Stream6_IRQHandler (void) {
    if (LL_DMA_IsActiveFlag_TC6(DMA2) == true) {
        LL_DMA_ClearFlag_TC6(DMA2);
        g_dynamic_dma_driver_stream_lut[eDmaDriver_Stream_ARGB3].IT_callback(eDmaDriver_Stream_ARGB3, eDmaDriver_IT_TC);
    }
    if (LL_DMA_IsActiveFlag_HT6(DMA2) == true) {
        LL_DMA_ClearFlag_HT6(DMA2);
        g_dynamic_dma_driver_stream_lut[eDmaDriver_Stream_ARGB3].IT_callback(eDmaDriver_Stream_ARGB3, eDmaDriver_IT_HT);
    }
    if (LL_DMA_IsActiveFlag_TE6(DMA2) == true) {
        LL_DMA_ClearFlag_TE6(DMA2);
        g_dynamic_dma_driver_stream_lut[eDmaDriver_Stream_ARGB3].IT_callback(eDmaDriver_Stream_ARGB3, eDmaDriver_IT_TE);
    }
}

void DMA2_Stream7_IRQHandler (void) {
    if (LL_DMA_IsActiveFlag_TC7(DMA2) == true) {
        LL_DMA_ClearFlag_TC7(DMA2);
        g_dynamic_dma_driver_stream_lut[eDmaDriver_Stream_USART6_TX].IT_callback(eDmaDriver_Stream_USART6_TX, eDmaDriver_IT_TC);
    }
    if (LL_DMA_IsActiveFlag_HT7(DMA2) == true) {
        LL_DMA_ClearFlag_HT7(DMA2);
        g_dynamic_dma_driver_stream_lut[eDmaDriver_Stream_USART6_TX].IT_callback(eDmaDriver_Stream_USART6_TX, eDmaDriver_IT_HT);
    }
    if (LL_DMA_IsActiveFlag_TE7(DMA2) == true) {
        LL_DMA_ClearFlag_TE7(DMA2);
        g_dynamic_dma_driver_stream_lut[eDmaDriver_Stream_USART6_TX].IT_callback(eDmaDriver_Stream_USART6_TX, eDmaDriver_IT_TE);
    }
}

void DMA2_Stream0_IRQHandler (void) {
    if (LL_DMA_IsActiveFlag_TC0(DMA2) == true) {
        LL_DMA_ClearFlag_TC0(DMA2);
        g_dynamic_dma_driver_stream_lut[eDmaDriver_Stream_ADC].IT_callback(eDmaDriver_Stream_ADC, eDmaDriver_IT_TC);
    }
    if (LL_DMA_IsActiveFlag_HT0(DMA2) == true) {
        LL_DMA_ClearFlag_HT0(DMA2);
        g_dynamic_dma_driver_stream_lut[eDmaDriver_Stream_ADC].IT_callback(eDmaDriver_Stream_ADC, eDmaDriver_IT_HT);
    }
    if (LL_DMA_IsActiveFlag_TE0(DMA2) == true) {
        LL_DMA_ClearFlag_TE0(DMA2);
        g_dynamic_dma_driver_stream_lut[eDmaDriver_Stream_ADC].IT_callback(eDmaDriver_Stream_ADC, eDmaDriver_IT_TE);
    }
}

void DMA1_Stream7_IRQHandler (void) {
    if (LL_DMA_IsActiveFlag_TC7(DMA1) == true) {
        LL_DMA_ClearFlag_TC7(DMA1);
        g_dynamic_dma_driver_stream_lut[eDmaDriver_Stream_I2C2_TX].IT_callback(eDmaDriver_Stream_I2C2_TX, eDmaDriver_IT_TC);
    }
    if (LL_DMA_IsActiveFlag_HT7(DMA1) == true) {
        LL_DMA_ClearFlag_HT7(DMA1);
        g_dynamic_dma_driver_stream_lut[eDmaDriver_Stream_I2C2_TX].IT_callback(eDmaDriver_Stream_I2C2_TX, eDmaDriver_IT_HT);
    }
    if (LL_DMA_IsActiveFlag_TE7(DMA1) == true) {
        LL_DMA_ClearFlag_TE7(DMA1);
        g_dynamic_dma_driver_stream_lut[eDmaDriver_Stream_I2C2_TX].IT_callback(eDmaDriver_Stream_I2C2_TX, eDmaDriver_IT_TE);
    }
}

void DMA1_Stream2_IRQHandler (void) {
    if (LL_DMA_IsActiveFlag_TC2(DMA1) == true) {
        LL_DMA_ClearFlag_TC2(DMA1);
        g_dynamic_dma_driver_stream_lut[eDmaDriver_Stream_I2C2_RX].IT_callback(eDmaDriver_Stream_I2C2_RX, eDmaDriver_IT_TC);
    }
    if (LL_DMA_IsActiveFlag_HT2(DMA1) == true) {
        LL_DMA_ClearFlag_HT2(DMA1);
        g_dynamic_dma_driver_stream_lut[eDmaDriver_Stream_I2C2_RX].IT_callback(eDmaDriver_Stream_I2C2_RX, eDmaDriver_IT_HT);
    }
    if (LL_DMA_IsActiveFlag_TE2(DMA1) == true) {
        LL_DMA_ClearFlag_TE2(DMA1);
        g_dynamic_dma_driver_stream_lut[eDmaDriver_Stream_I2C2_RX].IT_callback(eDmaDriver_Stream_I2C2_RX, eDmaDriver_IT_TE);
    }
}
