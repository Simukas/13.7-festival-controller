/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "adc_driver.h"
#include "stm32f4xx_ll_adc.h"
#include "stdbool.h"
#include "stdint.h"
#include "stm32f4xx_ll_bus.h"
#include "dma_driver.h"
#include "gpio_driver.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
#define VDDA_APPLI                     ((uint32_t) 3300)    /* Value of analog voltage supply Vdda (unit: mV) */
#define RANGE_12BITS                   ((uint32_t) 4095)    /* Max digital value with a full range of 12 bits */
#define VAR_CONVERTED_DATA_INIT_VALUE    (__LL_ADC_DIGITAL_SCALE(LL_ADC_RESOLUTION_12B) + 1)
#define ADC_DELAY_CALIB_ENABLE_CPU_CYCLES  (LL_ADC_DELAY_CALIB_ENABLE_ADC_CYCLES * 32)
#define ADC_VALUE_TO_VOLTAGE_mV_CONSTANT 6.8
/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/
typedef struct sAdcDriver_Channel_Static_t {
    uint32_t rank;
    uint32_t channel;
    uint32_t sampling_time;
    eGpioDriver_Pin_Enum_t pin;
    bool is_internal;
    uint32_t internal_path;
} sAdcDriver_Channel_Static_t;

typedef struct sAdcDriver_Channel_Dynamic_t {
    uint16_t current_value;
} sAdcDriver_Channel_Dynamic_t;

typedef struct sAdcDriver_Static_t {
    void (*clock_function) (uint32_t);
    uint32_t clock;
    uint32_t clock_divisor;
    uint32_t resolution;
    uint32_t data_alignment;
    ADC_TypeDef *adc;
    uint32_t sequence;
    uint32_t trigger_source;
    uint32_t sequencer_length;
    uint32_t continuous_mode;
    uint32_t discontinuous_mode;
    uint32_t flag_trigger;
    uint32_t dma_transfer;
    bool enable_interrupts;
    bool enable_dma;
    IRQn_Type irqn;
    uint32_t irqn_priority;

    // Channel parameters
    const sAdcDriver_Channel_Static_t *channel_lut;
    const eAdcDriver_Channel_Enum_t channel_count;

} sAdcDriver_Static_t;
/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
static const sAdcDriver_Channel_Static_t g_static_adc_driver_channel_lut[eAdcDriver_Channel_Last] = {
    [eAdcDriver_Channel_1] = {.channel = LL_ADC_CHANNEL_13, .rank = LL_ADC_REG_RANK_1, .sampling_time = LL_ADC_SAMPLINGTIME_480CYCLES, .pin = eGpioDriver_Pin_AnalogIn_1, .is_internal = false, },
    [eAdcDriver_Channel_2] = {.channel = LL_ADC_CHANNEL_12, .rank = LL_ADC_REG_RANK_2, .sampling_time = LL_ADC_SAMPLINGTIME_480CYCLES, .pin = eGpioDriver_Pin_AnalogIn_2, .is_internal = false, },
    [eAdcDriver_Channel_3] = {.channel = LL_ADC_CHANNEL_11, .rank = LL_ADC_REG_RANK_3, .sampling_time = LL_ADC_SAMPLINGTIME_480CYCLES, .pin = eGpioDriver_Pin_AnalogIn_3, .is_internal = false, },
    [eAdcDriver_Channel_InputVoltage] = {.channel = LL_ADC_CHANNEL_10, .rank = LL_ADC_REG_RANK_4, .sampling_time = LL_ADC_SAMPLINGTIME_480CYCLES, .pin = eGpioDriver_Pin_InputVoltage, .is_internal = false, },
    [eAdcDriver_Channel_Temperature] = {.channel = LL_ADC_CHANNEL_TEMPSENSOR, .rank = LL_ADC_REG_RANK_5, .sampling_time = LL_ADC_SAMPLINGTIME_480CYCLES, .is_internal = true, .internal_path = LL_ADC_PATH_INTERNAL_TEMPSENSOR},
};

static uint16_t g_adc_driver_channel_value_lut[eAdcDriver_Channel_Last];

static const sAdcDriver_Static_t g_static_adc_driver_lut[eAdcDriver_Adc_Last] = {
    [eAdcDriver_Adc_1] = {
        .clock_function = &LL_APB2_GRP1_EnableClock,
        .clock = LL_APB2_GRP1_PERIPH_ADC1,
        .clock_divisor = LL_ADC_CLOCK_SYNC_PCLK_DIV2,
        .resolution = LL_ADC_RESOLUTION_12B,
        .data_alignment = LL_ADC_DATA_ALIGN_RIGHT,
        .adc = ADC1,
        .sequence = LL_ADC_SEQ_SCAN_ENABLE,
        .trigger_source = LL_ADC_REG_TRIG_SOFTWARE,
        .sequencer_length = LL_ADC_REG_SEQ_SCAN_ENABLE_5RANKS,
        .continuous_mode = LL_ADC_REG_CONV_SINGLE,
        .discontinuous_mode = LL_ADC_REG_SEQ_DISCONT_DISABLE,
        .flag_trigger = LL_ADC_REG_FLAG_EOC_SEQUENCE_CONV,
        .dma_transfer = LL_ADC_REG_DMA_TRANSFER_UNLIMITED,
        .enable_interrupts = true,
        .enable_dma = true,
        .irqn = ADC_IRQn,
        .irqn_priority = 0,

        // Channel parameters
        .channel_lut = g_static_adc_driver_channel_lut,
        .channel_count = eAdcDriver_Channel_Last,
    },
};
/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/
void ADC1_IRQHandler (void);
/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/
void ADC_Driver_DMA_ITCallback (eDmaDriver_Stream_Enum_t stream, eDmaDriver_IT_Enum_t IT) {

}

void ADC1_IRQHandler (void) {
    if (LL_ADC_IsActiveFlag_EOCS(ADC1) == 1) {
        LL_ADC_ClearFlag_EOCS(ADC1);
    }
    if (LL_ADC_IsActiveFlag_JEOS(ADC1) == 1) {
        LL_ADC_ClearFlag_JEOS(ADC1);
    }
    if (LL_ADC_IsActiveFlag_OVR(ADC1)) {
        LL_ADC_ClearFlag_OVR(ADC1);
        LL_ADC_DisableIT_OVR(ADC1);
    }
}
/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool ADC_Driver_Init (eAdcDriver_Adc_Enum_t adc) {
    if (adc >= eAdcDriver_Adc_Last) {
        return false;
    }

    NVIC_SetPriority(g_static_adc_driver_lut[adc].irqn, g_static_adc_driver_lut[adc].irqn_priority);
    NVIC_EnableIRQ(g_static_adc_driver_lut[adc].irqn);

    g_static_adc_driver_lut[adc].clock_function(g_static_adc_driver_lut[adc].clock);

    if (__LL_ADC_IS_ENABLED_ALL_COMMON_INSTANCE() == true) {
        return false;
    }

    LL_ADC_SetCommonClock(__LL_ADC_COMMON_INSTANCE(g_static_adc_driver_lut[adc].adc), g_static_adc_driver_lut[adc].clock_divisor);

    LL_ADC_InitTypeDef adc_init_struct = {0};
    adc_init_struct.Resolution = g_static_adc_driver_lut[adc].resolution;
    adc_init_struct.DataAlignment = g_static_adc_driver_lut[adc].data_alignment;
    adc_init_struct.SequencersScanMode = g_static_adc_driver_lut[adc].sequence;
    if (LL_ADC_Init(g_static_adc_driver_lut[adc].adc, &adc_init_struct) != SUCCESS) {
        return false;
    }

    LL_ADC_REG_InitTypeDef adc_reg_init_struct = {0};
    adc_reg_init_struct.TriggerSource = g_static_adc_driver_lut[adc].trigger_source;
    adc_reg_init_struct.SequencerLength = g_static_adc_driver_lut[adc].sequencer_length;
    adc_reg_init_struct.SequencerDiscont = g_static_adc_driver_lut[adc].discontinuous_mode;
    adc_reg_init_struct.ContinuousMode = g_static_adc_driver_lut[adc].continuous_mode;
    adc_reg_init_struct.DMATransfer = g_static_adc_driver_lut[adc].dma_transfer;
    if (LL_ADC_REG_Init(g_static_adc_driver_lut[adc].adc, &adc_reg_init_struct) != SUCCESS) {
        return false;
    }

    //LL_ADC_REG_SetFlagEndOfConversion(g_static_adc_driver_lut[adc].adc, g_static_adc_driver_lut[adc].flag_trigger);

    for (uint8_t channel = 0; channel < g_static_adc_driver_lut[adc].channel_count; channel++) {
        if (g_static_adc_driver_lut[adc].channel_lut[channel].is_internal == false) {
            GPIO_Driver_InitPin(g_static_adc_driver_lut[adc].channel_lut[channel].pin, NULL);
        }
        LL_ADC_REG_SetSequencerRanks(g_static_adc_driver_lut[adc].adc, g_static_adc_driver_lut[adc].channel_lut[channel].rank, g_static_adc_driver_lut[adc].channel_lut[channel].channel);
        LL_ADC_SetChannelSamplingTime(g_static_adc_driver_lut[adc].adc, g_static_adc_driver_lut[adc].channel_lut[channel].channel, g_static_adc_driver_lut[adc].channel_lut[channel].sampling_time);
        if (g_static_adc_driver_lut[adc].channel_lut[channel].is_internal == true) {
            LL_ADC_SetCommonPathInternalCh(__LL_ADC_COMMON_INSTANCE(g_static_adc_driver_lut[adc].adc), g_static_adc_driver_lut[adc].channel_lut[channel].internal_path);
        }
    }

    if (g_static_adc_driver_lut[adc].enable_dma == true) {
        DMA_Driver_Init_t dma_init_struct = {0};
        dma_init_struct.IT_callback = &ADC_Driver_DMA_ITCallback;
        dma_init_struct.data_amount = eAdcDriver_Channel_Last;
        dma_init_struct.destination_address = &g_adc_driver_channel_value_lut;
        dma_init_struct.peripheral_or_source_address = (void*) LL_ADC_DMA_GetRegAddr(ADC1, LL_ADC_DMA_REG_REGULAR_DATA);
        dma_init_struct.stream = eDmaDriver_Stream_ADC;
        DMA_Driver_Init(&dma_init_struct);
    }

    LL_ADC_Enable(g_static_adc_driver_lut[adc].adc);

    DMA_Driver_EnableStream(eDmaDriver_Stream_ADC);

    return true;
}

bool ADC_Driver_ReadChannels (eAdcDriver_Adc_Enum_t adc) {
    if (adc >= eAdcDriver_Adc_Last) {
        return false;
    }

    LL_ADC_REG_StartConversionSWStart(g_static_adc_driver_lut[adc].adc);

    return true;
}

bool ADC_Driver_GetChannelValue (eAdcDriver_Channel_Enum_t channel, uint16_t *value) {
    if (channel >= eAdcDriver_Channel_PinLast) {
        return false;
    }
    if (value == NULL) {
        return false;
    }

    *value = g_adc_driver_channel_value_lut[channel];

    return true;
}

bool ADC_Driver_GetTemperature (int8_t *temperature) {
    if (temperature == NULL) {
        return false;
    }

    *temperature = __LL_ADC_CALC_TEMPERATURE(VDDA_APPLI, g_adc_driver_channel_value_lut[eAdcDriver_Channel_Temperature], LL_ADC_RESOLUTION_12B);

    return true;
}

bool ADC_Driver_GetSupplyVoltage (uint16_t *voltage_mV) {
    if (voltage_mV == NULL) {
        return false;
    }

    *voltage_mV = g_adc_driver_channel_value_lut[eAdcDriver_Channel_InputVoltage] * ADC_VALUE_TO_VOLTAGE_mV_CONSTANT;

    return true;
}
