/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "i2c_driver.h"

#include "string.h"
#include "stdlib.h"
#include "stdbool.h"
#include "stddef.h"

#include "stm32f4xx_ll_i2c.h"
#include "stm32f4xx_ll_bus.h"
#include "gpio_driver.h"
#include "dma_driver.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
#define I2C_DRIVER_SPEED 100000
#define BTF_FLAG_TIMEOUT 100
#define I2C_DRIVER_TX_BUFFER_SIZE 200
/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/
typedef struct sI2CDriver_config_t {
    I2C_TypeDef *i2c;
    eGpioDriver_Pin_Enum_t sda_pin;
    eGpioDriver_Pin_Enum_t scl_pin;
    uint32_t peripheral_mode;
    uint32_t clock_speed;
    uint32_t duty_cycle;
    uint32_t own_address1;
    uint32_t own_address2;
    uint32_t type_acknowledge;
    uint32_t own_addr_size;
    void (*clock_function) (uint32_t);
    uint32_t clock;
    uint32_t IRQn;
    bool enable_DMA_RX;
    eDmaDriver_Stream_Enum_t DMA_RX_stream;
    bool enable_DMA_TX;
    eDmaDriver_Stream_Enum_t DMA_TX_stream;
    bool enable_IT_EVT;
    bool enable_IT_BUF;
} sI2CDriver_config_t;

typedef struct sI2CDriver_Dynamic_lut {
    bool initialized;
    uint8_t *rx_buffer;
    uint8_t tx_buffer[I2C_DRIVER_TX_BUFFER_SIZE];
    eDmaDriver_Stream_Enum_t stream;
    uint16_t transfer_count;
    uint16_t transfer_size;
    uint8_t address;
    uint16_t mem_address;
    bool is_rx_mem_address_sent;
    bool volatile comms_completed;
} sI2CDriver_Dynamic_lut;

typedef enum eI2CDriver_Flag_Enum_t {
    eI2cDriver_Flag_First,
        eI2cDriver_Flag_Busy = eI2cDriver_Flag_First,
        eI2cDriver_Flag_BTF,
        eI2cDriver_Flag_SB,
        eI2cDriver_Flag_ADDR,
        eI2cDriver_Flag_TXE,
        eI2cDriver_Flag_Last
} eI2CDriver_Flag_Enum_t;
/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
static const sI2CDriver_config_t i2c_driver_static_lut[eI2cDriver_Last] = {
    [eI2cDriver_2] = {
        .i2c = I2C2,
        .sda_pin = eGpioDriver_Pin_I2C_SDA,
        .scl_pin = eGpioDriver_Pin_I2C_SCL,
        .peripheral_mode = LL_I2C_MODE_I2C,
        .clock_speed = I2C_DRIVER_SPEED,
        .duty_cycle = LL_I2C_DUTYCYCLE_2,
        .own_address1 = 0,
        .own_address2 = 0,
        .type_acknowledge = LL_I2C_ACK,
        .own_addr_size = LL_I2C_OWNADDRESS1_7BIT,
        .clock_function = LL_APB1_GRP1_EnableClock,
        .clock = LL_APB1_GRP1_PERIPH_I2C2,
        .enable_DMA_RX = true,
        .DMA_RX_stream = eDmaDriver_Stream_I2C2_RX,
        .enable_DMA_TX = true,
        .DMA_TX_stream = eDmaDriver_Stream_I2C2_TX,
        .IRQn = I2C2_EV_IRQn,
        .enable_IT_EVT = false,
        .enable_IT_BUF = false
    }
};

static const eI2cDriver_Enum_t g_i2c_driver_dma_stream_lut[eDmaDriver_Stream_Last] = {
    [eDmaDriver_Stream_I2C2_RX] = eI2cDriver_2,
    [eDmaDriver_Stream_I2C2_TX] = eI2cDriver_2,
};
/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/
static sI2CDriver_Dynamic_lut g_dynamic_i2c_driver_lut[eI2cDriver_Last] = {
    [eI2cDriver_2] = {
        .comms_completed = true,
        .initialized = false,
    }
};
/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/
static void I2C_Driver_DMA_ITCallback_RX (eDmaDriver_Stream_Enum_t stream, eDmaDriver_IT_Enum_t IT);
static void I2C_Driver_DMA_ITCallback_TX (eDmaDriver_Stream_Enum_t stream, eDmaDriver_IT_Enum_t IT);
static bool I2C_Driver_RequestToRead (eI2cDriver_Enum_t i2c, uint32_t timeout);
static bool I2C_Driver_RequestToSend (eI2cDriver_Enum_t i2c, uint32_t timeout);
static bool I2C_Driver_WaitForFlag (eI2cDriver_Enum_t i2c, eI2CDriver_Flag_Enum_t flag,
bool status, uint32_t timeout, uint32_t tickstart);
/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/
static void I2C_Driver_DMA_ITCallback_RX (eDmaDriver_Stream_Enum_t stream, eDmaDriver_IT_Enum_t IT) {
    eI2cDriver_Enum_t i2c = g_i2c_driver_dma_stream_lut[stream];
    switch (IT) {
        case eDmaDriver_IT_TC: {
            LL_I2C_GenerateStopCondition(i2c_driver_static_lut[i2c].i2c);
            LL_I2C_DisableLastDMA(i2c_driver_static_lut[i2c].i2c);
            DMA_Driver_DisableStream(i2c_driver_static_lut[i2c].DMA_RX_stream);
            g_dynamic_i2c_driver_lut[i2c].rx_buffer = NULL;
            g_dynamic_i2c_driver_lut[i2c].address = 0;
            g_dynamic_i2c_driver_lut[i2c].mem_address = 0;
            g_dynamic_i2c_driver_lut[i2c].comms_completed = true;
            break;
        }

        case eDmaDriver_IT_HT: {
            break;
        }

        case eDmaDriver_IT_TE: {
            break;
        }

        default: {
            break;
        }
    }
}

static void I2C_Driver_DMA_ITCallback_TX (eDmaDriver_Stream_Enum_t stream, eDmaDriver_IT_Enum_t IT) {
    eI2cDriver_Enum_t i2c = g_i2c_driver_dma_stream_lut[stream];
    switch (IT) {
        case eDmaDriver_IT_TC: {
            uint32_t tickstart = HAL_GetTick();
            I2C_Driver_WaitForFlag(i2c, eI2cDriver_Flag_BTF, true, BTF_FLAG_TIMEOUT, tickstart);
            LL_I2C_GenerateStopCondition(i2c_driver_static_lut[i2c].i2c);
            DMA_Driver_DisableStream(i2c_driver_static_lut[i2c].DMA_TX_stream);
            g_dynamic_i2c_driver_lut[i2c].address = 0;
            g_dynamic_i2c_driver_lut[i2c].mem_address = 0;
            g_dynamic_i2c_driver_lut[i2c].comms_completed = true;
            break;
        }

        case eDmaDriver_IT_HT: {
            break;
        }

        case eDmaDriver_IT_TE: {
            break;
        }

        default: {
            break;
        }
    }
}

static bool I2C_Driver_WaitForFlag (eI2cDriver_Enum_t i2c, eI2CDriver_Flag_Enum_t flag, bool status, uint32_t timeout, uint32_t tickstart) {
    if (i2c >= eI2cDriver_Last || flag >= eI2cDriver_Flag_Last) {
        return false;
    }

    uint32_t (*ll_flag_function) (I2C_TypeDef*);

    switch (flag) {
        case eI2cDriver_Flag_Busy: {
            ll_flag_function = LL_I2C_IsActiveFlag_BUSY;
            break;
        }

        case eI2cDriver_Flag_BTF: {
            ll_flag_function = LL_I2C_IsActiveFlag_BTF;
            break;
        }

        case eI2cDriver_Flag_SB: {
            ll_flag_function = LL_I2C_IsActiveFlag_SB;
            break;
        }

        case eI2cDriver_Flag_ADDR: {
            ll_flag_function = LL_I2C_IsActiveFlag_ADDR;
            break;
        }

        case eI2cDriver_Flag_TXE: {
            ll_flag_function = LL_I2C_IsActiveFlag_TXE;
            break;
        }

        default: {
            return false;
        }
    }

    uint32_t ticks = 0;

    while (ll_flag_function(i2c_driver_static_lut[i2c].i2c) != status) { //TODO: make sure of timeout functionality
        ticks = HAL_GetTick();
        if (((ticks - tickstart) > timeout) || timeout == 0U) {
            return false;
        }
    }

    return true;
}

static bool I2C_Driver_RequestToRead (eI2cDriver_Enum_t i2c, uint32_t timeout) {
    if (i2c >= eI2cDriver_Last) {
        return false;
    }
    uint32_t tickstart = HAL_GetTick();

    if (!I2C_Driver_WaitForFlag(i2c, eI2cDriver_Flag_Busy, false, timeout, tickstart)) {
        return false;
    }

    LL_I2C_GenerateStartCondition(i2c_driver_static_lut[i2c].i2c);

    if (!I2C_Driver_WaitForFlag(i2c, eI2cDriver_Flag_SB, true, timeout, tickstart)) {
        return false;
    }

    LL_I2C_TransmitData8(i2c_driver_static_lut[i2c].i2c, g_dynamic_i2c_driver_lut[i2c].address);

    if (!I2C_Driver_WaitForFlag(i2c, eI2cDriver_Flag_ADDR, true, timeout, tickstart)) {
        return false;
    }

    LL_I2C_ClearFlag_ADDR(i2c_driver_static_lut[i2c].i2c);

    LL_I2C_TransmitData8(i2c_driver_static_lut[i2c].i2c, (uint8_t) (g_dynamic_i2c_driver_lut[i2c].mem_address >> 8));

    if (!I2C_Driver_WaitForFlag(i2c, eI2cDriver_Flag_TXE, true, timeout, tickstart)) {
        return false;
    }

    LL_I2C_TransmitData8(i2c_driver_static_lut[i2c].i2c, (uint8_t) (g_dynamic_i2c_driver_lut[i2c].mem_address & 0xFF));

    if (!I2C_Driver_WaitForFlag(i2c, eI2cDriver_Flag_TXE, true, timeout, tickstart)) {
        return false;
    }

    g_dynamic_i2c_driver_lut[i2c].address += 1;

    LL_I2C_GenerateStartCondition(i2c_driver_static_lut[i2c].i2c);

    if (!I2C_Driver_WaitForFlag(i2c, eI2cDriver_Flag_SB, true, timeout, tickstart)) {
        return false;
    }

    LL_I2C_TransmitData8(i2c_driver_static_lut[i2c].i2c, g_dynamic_i2c_driver_lut[i2c].address);

    if (!I2C_Driver_WaitForFlag(i2c, eI2cDriver_Flag_ADDR, true, timeout, tickstart)) {
        return false;
    }

    LL_I2C_ClearFlag_ADDR(i2c_driver_static_lut[i2c].i2c);

    if (g_dynamic_i2c_driver_lut[i2c].transfer_count >= 2) {
        LL_I2C_EnableLastDMA(i2c_driver_static_lut[i2c].i2c);
        LL_I2C_AcknowledgeNextData(i2c_driver_static_lut[i2c].i2c, LL_I2C_ACK);
        if (DMA_Driver_EnableStream(g_dynamic_i2c_driver_lut[i2c].stream) == false) {
            return false;
        }
    } else {
        LL_I2C_AcknowledgeNextData(i2c_driver_static_lut[i2c].i2c, LL_I2C_NACK);
        LL_I2C_ClearFlag_ADDR(i2c_driver_static_lut[i2c].i2c);
        if (DMA_Driver_EnableStream(g_dynamic_i2c_driver_lut[i2c].stream) == false) {
            return false;
        }
    }

    return true;
}

static bool I2C_Driver_RequestToSend (eI2cDriver_Enum_t i2c, uint32_t timeout) {
    if (i2c >= eI2cDriver_Last) {
        return false;
    }

    uint32_t tickstart = HAL_GetTick();

    if (!I2C_Driver_WaitForFlag(i2c, eI2cDriver_Flag_Busy, false, timeout, tickstart)) {
        return false;
    }

    LL_I2C_GenerateStartCondition(i2c_driver_static_lut[i2c].i2c);

    if (!I2C_Driver_WaitForFlag(i2c, eI2cDriver_Flag_SB, true, timeout, tickstart)) {
        return false;
    }

    LL_I2C_TransmitData8(i2c_driver_static_lut[i2c].i2c, g_dynamic_i2c_driver_lut[i2c].address);

    if (!I2C_Driver_WaitForFlag(i2c, eI2cDriver_Flag_ADDR, true, timeout, tickstart)) {
        return false;
    }

    LL_I2C_ClearFlag_ADDR(i2c_driver_static_lut[i2c].i2c);

    LL_I2C_TransmitData8(i2c_driver_static_lut[i2c].i2c, (uint8_t) (g_dynamic_i2c_driver_lut[i2c].mem_address >> 8));

    if (!I2C_Driver_WaitForFlag(i2c, eI2cDriver_Flag_TXE, true, timeout, tickstart)) {
        return false;
    }

    LL_I2C_TransmitData8(i2c_driver_static_lut[i2c].i2c, (uint8_t) (g_dynamic_i2c_driver_lut[i2c].mem_address & 0xFF));

    if (!I2C_Driver_WaitForFlag(i2c, eI2cDriver_Flag_TXE, true, timeout, tickstart)) {
        return false;
    }

    if (DMA_Driver_EnableStream(g_dynamic_i2c_driver_lut[eI2cDriver_2].stream) == false) {
        return false;
    }

    return true;
}
/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool I2C_Driver_Init (eI2cDriver_Enum_t i2c) {
    if (i2c >= eI2cDriver_Last) {
        return false;
    }

    if (g_dynamic_i2c_driver_lut[i2c].initialized == true) {
        return true;
    }

    if (!GPIO_Driver_InitPin(i2c_driver_static_lut[i2c].sda_pin, NULL)) {
        return false;
    }

    if (!GPIO_Driver_InitPin(i2c_driver_static_lut[i2c].scl_pin, NULL)) {
        return false;
    }

    if (i2c_driver_static_lut[i2c].enable_DMA_RX) {
        DMA_Driver_Init_t dma_init_struct = {
            .stream = (uint32_t) i2c_driver_static_lut[i2c].DMA_RX_stream,
            .peripheral_or_source_address = (void*) LL_I2C_DMA_GetRegAddr(i2c_driver_static_lut[i2c].i2c),
            .destination_address = NULL,
            .data_amount = 0,
            .IT_callback = &I2C_Driver_DMA_ITCallback_RX
        };
        if (DMA_Driver_Init(&dma_init_struct) == false) {
            return false;
        }
    }

    if (i2c_driver_static_lut[i2c].enable_DMA_TX) {
        DMA_Driver_Init_t dma_init_struct = {
            .stream = (uint32_t) i2c_driver_static_lut[i2c].DMA_TX_stream,
            .peripheral_or_source_address = (void*) LL_I2C_DMA_GetRegAddr(i2c_driver_static_lut[i2c].i2c),
            .destination_address = NULL,
            .data_amount = 0,
            .IT_callback = &I2C_Driver_DMA_ITCallback_TX
        };
        if (DMA_Driver_Init(&dma_init_struct) == false) {
            return false;
        }
    }

    i2c_driver_static_lut[i2c].clock_function(i2c_driver_static_lut[i2c].clock);

    LL_I2C_Disable(i2c_driver_static_lut[i2c].i2c);

    LL_I2C_DisableOwnAddress2(i2c_driver_static_lut[i2c].i2c);
    LL_I2C_DisableGeneralCall(i2c_driver_static_lut[i2c].i2c);
    LL_I2C_EnableClockStretching(i2c_driver_static_lut[i2c].i2c);

    LL_I2C_InitTypeDef I2C_InitStruct = {0};

    I2C_InitStruct.PeripheralMode = i2c_driver_static_lut[i2c].peripheral_mode;
    I2C_InitStruct.ClockSpeed = i2c_driver_static_lut[i2c].clock_speed;
    I2C_InitStruct.DutyCycle = i2c_driver_static_lut[i2c].duty_cycle;
    I2C_InitStruct.OwnAddress1 = i2c_driver_static_lut[i2c].own_address1;
    I2C_InitStruct.TypeAcknowledge = i2c_driver_static_lut[i2c].type_acknowledge;
    I2C_InitStruct.OwnAddrSize = i2c_driver_static_lut[i2c].own_addr_size;

    NVIC_SetPriority(i2c_driver_static_lut[i2c].IRQn, 0);
    NVIC_EnableIRQ(i2c_driver_static_lut[i2c].IRQn);

    if (LL_I2C_Init(i2c_driver_static_lut[i2c].i2c, &I2C_InitStruct) != SUCCESS) {
        return false;
    }

    if (i2c_driver_static_lut[i2c].enable_IT_EVT) {
        LL_I2C_EnableIT_EVT(i2c_driver_static_lut[i2c].i2c);
    } else {
        LL_I2C_DisableIT_EVT(i2c_driver_static_lut[i2c].i2c);
    }

    if (i2c_driver_static_lut[i2c].enable_IT_BUF) {
        LL_I2C_EnableIT_BUF(i2c_driver_static_lut[i2c].i2c);
    } else {
        LL_I2C_DisableIT_BUF(i2c_driver_static_lut[i2c].i2c);
    }

    if (i2c_driver_static_lut[i2c].enable_DMA_RX) {
        LL_I2C_EnableDMAReq_TX(i2c_driver_static_lut[i2c].i2c);
    }

    if (i2c_driver_static_lut[i2c].enable_DMA_TX) {
        LL_I2C_EnableDMAReq_RX(i2c_driver_static_lut[i2c].i2c);
    }

    LL_I2C_SetOwnAddress2(i2c_driver_static_lut[i2c].i2c, i2c_driver_static_lut[i2c].own_address2);

    LL_I2C_Enable(i2c_driver_static_lut[i2c].i2c);

    g_dynamic_i2c_driver_lut[i2c].initialized = true;

    return true;
}

bool I2C_Driver_Send (eI2cDriver_Enum_t i2c, uint8_t address, uint16_t mem_address, uint8_t *buffer, uint32_t byte_count, uint32_t timeout) {
    if (i2c >= eI2cDriver_Last || buffer == NULL || byte_count == 0) {
        return false;
    }

    while (g_dynamic_i2c_driver_lut[i2c].comms_completed != true) {
    }

    g_dynamic_i2c_driver_lut[i2c].stream = eDmaDriver_Stream_I2C2_TX;

    memcpy(g_dynamic_i2c_driver_lut[i2c].tx_buffer, buffer, byte_count);

    if (DMA_Driver_SetDataCount(i2c_driver_static_lut[i2c].DMA_TX_stream, byte_count) == false) {
        return false;
    }

    if (DMA_Driver_SetDataPointer(i2c_driver_static_lut[i2c].DMA_TX_stream, g_dynamic_i2c_driver_lut[i2c].tx_buffer) == false) {
        return false;
    }

    g_dynamic_i2c_driver_lut[i2c].address = address;
    g_dynamic_i2c_driver_lut[i2c].mem_address = mem_address;
    g_dynamic_i2c_driver_lut[i2c].comms_completed = false;

    if (!I2C_Driver_RequestToSend(i2c, timeout)) {
        g_dynamic_i2c_driver_lut[i2c].comms_completed = true;
        return false;
    }

    return true;
}

bool I2C_Driver_Read (eI2cDriver_Enum_t i2c, uint8_t address, uint16_t mem_address, uint8_t *buffer, uint32_t byte_count, uint32_t timeout) {
    if (i2c >= eI2cDriver_Last) {
        return false;
    }
    if (buffer == NULL) {
        return false;
    }
    if (byte_count == 0) {
        return false;
    }
    if (byte_count > I2C_DRIVER_TX_BUFFER_SIZE) {
        return false;
    }
    while (g_dynamic_i2c_driver_lut[i2c].comms_completed != true) {
    }

    g_dynamic_i2c_driver_lut[i2c].stream = eDmaDriver_Stream_I2C2_RX;

    g_dynamic_i2c_driver_lut[i2c].rx_buffer = buffer;

    if (DMA_Driver_SetDataCount(i2c_driver_static_lut[i2c].DMA_RX_stream, byte_count) == false) {
        return false;
    }

    if (DMA_Driver_SetDataPointer(i2c_driver_static_lut[i2c].DMA_RX_stream, g_dynamic_i2c_driver_lut[i2c].rx_buffer) == false) {
        return false;
    }

    g_dynamic_i2c_driver_lut[i2c].address = address;
    g_dynamic_i2c_driver_lut[i2c].mem_address = mem_address;
    g_dynamic_i2c_driver_lut[i2c].transfer_count = byte_count;
    g_dynamic_i2c_driver_lut[i2c].comms_completed = false;

    if (!I2C_Driver_RequestToRead(i2c, timeout)) {
        g_dynamic_i2c_driver_lut[i2c].comms_completed = true;
        return false;
    }

    while (!g_dynamic_i2c_driver_lut[i2c].comms_completed) {
    }

    return true;
}
