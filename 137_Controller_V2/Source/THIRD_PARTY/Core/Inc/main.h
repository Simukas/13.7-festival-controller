/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

#include "stm32f4xx_ll_adc.h"
#include "stm32f4xx_ll_crc.h"
#include "stm32f4xx_ll_dma.h"
#include "stm32f4xx_ll_i2c.h"
#include "stm32f4xx_ll_rcc.h"
#include "stm32f4xx_ll_bus.h"
#include "stm32f4xx_ll_system.h"
#include "stm32f4xx_ll_exti.h"
#include "stm32f4xx_ll_cortex.h"
#include "stm32f4xx_ll_utils.h"
#include "stm32f4xx_ll_pwr.h"
#include "stm32f4xx_ll_rtc.h"
#include "stm32f4xx_ll_spi.h"
#include "stm32f4xx_ll_tim.h"
#include "stm32f4xx_ll_usart.h"
#include "stm32f4xx_ll_gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define GPIO1_Pin LL_GPIO_PIN_13
#define GPIO1_GPIO_Port GPIOC
#define GPIO2_Pin LL_GPIO_PIN_14
#define GPIO2_GPIO_Port GPIOC
#define GPIO3_Pin LL_GPIO_PIN_15
#define GPIO3_GPIO_Port GPIOC
#define Input_Voltage_Pin LL_GPIO_PIN_0
#define Input_Voltage_GPIO_Port GPIOC
#define ADC_1_Pin LL_GPIO_PIN_1
#define ADC_1_GPIO_Port GPIOC
#define ADC_2_Pin LL_GPIO_PIN_2
#define ADC_2_GPIO_Port GPIOC
#define ADC_3_Pin LL_GPIO_PIN_3
#define ADC_3_GPIO_Port GPIOC
#define MOTOR1_A_Pin LL_GPIO_PIN_0
#define MOTOR1_A_GPIO_Port GPIOA
#define MOTOR1_B_Pin LL_GPIO_PIN_1
#define MOTOR1_B_GPIO_Port GPIOA
#define MOTOR2_A_Pin LL_GPIO_PIN_2
#define MOTOR2_A_GPIO_Port GPIOA
#define MOTOR2_B_Pin LL_GPIO_PIN_3
#define MOTOR2_B_GPIO_Port GPIOA
#define SPI_NSS_Pin LL_GPIO_PIN_4
#define SPI_NSS_GPIO_Port GPIOA
#define ADD0_Pin LL_GPIO_PIN_5
#define ADD0_GPIO_Port GPIOA
#define ADD1_Pin LL_GPIO_PIN_6
#define ADD1_GPIO_Port GPIOA
#define ADD2_Pin LL_GPIO_PIN_7
#define ADD2_GPIO_Port GPIOA
#define ADD3_Pin LL_GPIO_PIN_4
#define ADD3_GPIO_Port GPIOC
#define ADD4_Pin LL_GPIO_PIN_5
#define ADD4_GPIO_Port GPIOC
#define ADD5_Pin LL_GPIO_PIN_0
#define ADD5_GPIO_Port GPIOB
#define ADD6_Pin LL_GPIO_PIN_1
#define ADD6_GPIO_Port GPIOB
#define ADD7_Pin LL_GPIO_PIN_2
#define ADD7_GPIO_Port GPIOB
#define I2C_SCL_Pin LL_GPIO_PIN_10
#define I2C_SCL_GPIO_Port GPIOB
#define GPIO5_Pin LL_GPIO_PIN_12
#define GPIO5_GPIO_Port GPIOB
#define GPIO4_Pin LL_GPIO_PIN_13
#define GPIO4_GPIO_Port GPIOB
#define USART6_TX_DE_Pin LL_GPIO_PIN_14
#define USART6_TX_DE_GPIO_Port GPIOB
#define USART6_RX_DE_Pin LL_GPIO_PIN_15
#define USART6_RX_DE_GPIO_Port GPIOB
#define RGB1_W_Pin LL_GPIO_PIN_8
#define RGB1_W_GPIO_Port GPIOC
#define RGB1_B_Pin LL_GPIO_PIN_9
#define RGB1_B_GPIO_Port GPIOC
#define ARGB_CH1_Pin LL_GPIO_PIN_8
#define ARGB_CH1_GPIO_Port GPIOA
#define ARGB_CH2_Pin LL_GPIO_PIN_9
#define ARGB_CH2_GPIO_Port GPIOA
#define ARGB_CH3_Pin LL_GPIO_PIN_10
#define ARGB_CH3_GPIO_Port GPIOA
#define SPI_SCK_Pin LL_GPIO_PIN_10
#define SPI_SCK_GPIO_Port GPIOC
#define SPI_MISO_Pin LL_GPIO_PIN_11
#define SPI_MISO_GPIO_Port GPIOC
#define SPI_MOSI_Pin LL_GPIO_PIN_12
#define SPI_MOSI_GPIO_Port GPIOC
#define GPIO6_Pin LL_GPIO_PIN_2
#define GPIO6_GPIO_Port GPIOD
#define I2C_SDA_Pin LL_GPIO_PIN_3
#define I2C_SDA_GPIO_Port GPIOB
#define RGB1_R_Pin LL_GPIO_PIN_4
#define RGB1_R_GPIO_Port GPIOB
#define RGB1_G_Pin LL_GPIO_PIN_5
#define RGB1_G_GPIO_Port GPIOB
#define RGB2_G_Pin LL_GPIO_PIN_6
#define RGB2_G_GPIO_Port GPIOB
#define RGB2_R_Pin LL_GPIO_PIN_7
#define RGB2_R_GPIO_Port GPIOB
#define RGB2_B_Pin LL_GPIO_PIN_8
#define RGB2_B_GPIO_Port GPIOB
#define RGB2_W_Pin LL_GPIO_PIN_9
#define RGB2_W_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
