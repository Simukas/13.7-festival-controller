#ifndef __TIME_UTILS__H__
#define __TIME_UTILS__H__

/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "stdint.h"
#include "stdbool.h"
/**********************************************************************************************************************
 * Exported definitions and macros
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported types
 *********************************************************************************************************************/
typedef enum eWeekday_Enum_t {
    eWeekday_First = 1,
    eWeekday_Monday = eWeekday_First,
    eWeekday_Tuesday,
    eWeekday_Wednesday,
    eWeekday_Thursday,
    eWeekday_Friday,
    eWeekday_Saturday,
    eWeekday_Sunday,
    eWeekday_Last
} eWeekday_Enum_t;

typedef enum eMonth_Enum_t {
    eMonth_First = 1,
    eMonth_January = eMonth_First,
    eMonth_February,
    eMonth_March,
    eMonth_April,
    eMonth_May,
    eMonth_June,
    eMonth_July,
    eMonth_August,
    eMonth_September,
    eMonth_October,
    eMonth_November,
    eMonth_December,
    eMonth_Last
} eMonth_Enum_t;

typedef struct sDateTime_t {
    uint16_t milliseconds;
    uint16_t year;
    eMonth_Enum_t month;
    uint8_t day;
    uint8_t hours;
    uint8_t minutes;
    uint8_t seconds;
    eWeekday_Enum_t weekday;
} sDateTime_t;

typedef uint64_t timestamp_t;
/**********************************************************************************************************************
 * Exported variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of exported functions
 *********************************************************************************************************************/
void timestampToTimeStruct (timestamp_t *t, sDateTime_t *date);
void timeStructToTimestamp (timestamp_t *t, sDateTime_t *date);
char* formatDateTime (sDateTime_t *date);
bool delay_init (void);
void delay (uint16_t microseconds);
#endif /* __TIME_UTILS__H__ */
