/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "time_utils.h"
#include "stdint.h"
#include "stdio.h"
#include "stdbool.h"
#include "timer_driver.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/
static eWeekday_Enum_t computeDayOfWeek (uint16_t y, eMonth_Enum_t m, uint8_t d);
/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/
static eWeekday_Enum_t computeDayOfWeek (uint16_t y, eMonth_Enum_t m, uint8_t d) {
    uint16_t h;
    uint16_t j;
    uint16_t k;

    //January and February are counted as months 13 and 14 of the previous year
    if (m <= 2) {
        m += 12;
        y -= 1;
    }

    //J is the century
    j = y / 100;
    //K the year of the century
    k = y % 100;

    //Compute H using Zeller's congruence
    h = d + (26 * (m + 1) / 10) + k + (k / 4) + (5 * j) + (j / 4);

    //Return the day of the week
    return ((h + 5) % 7) + 1;
}
/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
void timestampToTimeStruct (timestamp_t *t, sDateTime_t *date) {
    //Retrieve milliseconds
    date->milliseconds = *t % 1000;
    *t /= 1000;
    //Retrieve hours, minutes and seconds
    date->seconds = *t % 60;
    *t /= 60;
    date->minutes = *t % 60;
    *t /= 60;
    date->hours = *t % 24;
    *t /= 24;

    //Convert Unix time to date
    uint32_t a = (uint32_t) ((4 * *t + 102032) / 146097 + 15);
    uint32_t b = (uint32_t) (*t + 2442113 + a - (a / 4));
    uint32_t c = (20 * b - 2442) / 7305;
    uint32_t d = b - 365 * c - (c / 4);
    uint32_t e = d * 1000 / 30601;
    uint32_t f = d - e * 30 - e * 601 / 1000;

    //January and February are counted as months 13 and 14 of the previous year
    if (e <= 13) {
        c -= 4716;
        e -= 1;
    } else {
        c -= 4715;
        e -= 13;
    }

    //Retrieve year, month and day
    date->year = c;
    date->month = e;
    date->day = f;

    //Calculate day of week
    date->weekday = computeDayOfWeek(c, e, f);
}

void timeStructToTimestamp (timestamp_t *t, sDateTime_t *date) {
    //Year
    uint16_t y = date->year;
    //Month of year
    eMonth_Enum_t m = date->month;
    //Day of month
    uint8_t d = date->day;

    //January and February are counted as months 13 and 14 of the previous year
    if (m <= 2)
        {
        m += 12;
        y -= 1;
    }

    //Convert years to days
    *t = (365 * y) + (y / 4) - (y / 100) + (y / 400);
    //Convert months to days
    *t += (30 * m) + (3 * (m + 1) / 5) + d;
    //Unix time starts on January 1st, 1970
    *t -= 719561;
    //Convert days to seconds
    *t *= 86400;
    //Add hours, minutes and seconds
    *t += (3600 * date->hours) + (60 * date->minutes) + date->seconds;
    //Convert to milliseconds
    *t *= 1000;
    //Add milliseconds
    *t += date->milliseconds;
}

char* formatDateTime (sDateTime_t *date) {
    static char buffer[24];

    //Format date and time
    snprintf(buffer, 40, "%02u-%02hu-%02hu %02hu:%02hu:%02hu.%03u", date->year, date->month, date->day, date->hours, date->minutes, date->seconds, date->milliseconds);

    //Return a pointer to the formatted string
    return buffer;
}

bool delay_init (void) {
    if (Timer_Driver_Init(eTimerDriver_Timer_10) == false) {
        return false;
    }
    if (Timer_Driver_Start(eTimerDriver_Timer_10) == false) {
        return false;
    }
    return true;
}

void delay (uint16_t microseconds) {
    Timer_Driver_SetCounter(eTimerDriver_Timer_10, 0);
    uint16_t timer_counter = 0;
    while (true) {
        Timer_Driver_GetCounter(eTimerDriver_Timer_10, &timer_counter);
        if (timer_counter >= microseconds) {
            return;
        }
    }
}
