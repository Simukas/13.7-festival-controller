/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include <math_utils.h>
#include "stdint.h"
#include "stdlib.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
uint32_t randomRange (uint32_t min, uint32_t max) {
    return (rand() % (max + 1 - min)) + min;
}

uint32_t map (uint32_t input, uint32_t input_min, uint32_t input_max, uint32_t output_min, uint32_t output_max) {
    return ((((input - input_min) * (output_max - output_min)) / (input_max - input_min)) + output_min);
}
