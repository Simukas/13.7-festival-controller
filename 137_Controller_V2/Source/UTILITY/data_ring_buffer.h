#ifndef __DATA_RING_BUFFER__H__
#define __DATA_RING_BUFFER__H__
/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
/**********************************************************************************************************************
 * Exported definitions and macros
 *********************************************************************************************************************/
// @formatter:off
/**********************************************************************************************************************
 * Exported types
 *********************************************************************************************************************/
typedef struct sData_RB_t {
    size_t head;
    size_t tail;
    size_t size;
    bool full;
    uint8_t *buffer;
} sData_RB_t;

typedef sData_RB_t *data_rb_handle_t;
/**********************************************************************************************************************
 * Exported variables
 *********************************************************************************************************************/
// @formatter:on
/**********************************************************************************************************************
 * Prototypes of exported functions
 *********************************************************************************************************************/
data_rb_handle_t Data_RB_Init (size_t amount);
void Data_RB_Free (data_rb_handle_t cbuf);
void Data_RB_Reset (data_rb_handle_t cbuf);
void Data_RB_Push (data_rb_handle_t cbuf, uint8_t byte);
bool Data_RB_Pop (data_rb_handle_t cbuf, uint8_t *byte);
bool Data_RB_IsEmpty (data_rb_handle_t cbuf);
bool Data_RB_IsFull (data_rb_handle_t cbuf);
uint32_t Data_RB_GetSize (data_rb_handle_t cbuf);
uint32_t Data_RB_GetFreeSpace (data_rb_handle_t cbuf);
#endif /* __DATA_RING_BUFFER__H__ */
