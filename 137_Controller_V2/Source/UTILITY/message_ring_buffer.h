#ifndef __MESSAGE_RING_BUFFER__H__
#define __MESSAGE_RING_BUFFER__H__
/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
/**********************************************************************************************************************
 * Exported definitions and macros
 *********************************************************************************************************************/
// @formatter:off
/**********************************************************************************************************************
 * Exported types
 *********************************************************************************************************************/
typedef struct sMessage_t {
    uint8_t *buffer;
    uint16_t buffer_size;
} sMessage_t;

typedef struct sMessage_RB_t {
    size_t head;
    size_t tail;
    size_t size;
    bool full;
    sMessage_t *buffer;
} sMessage_RB_t;

typedef sMessage_RB_t *message_rb_handle_t;
/**********************************************************************************************************************
 * Exported variables
 *********************************************************************************************************************/
// @formatter:on
/**********************************************************************************************************************
 * Prototypes of exported functions
 *********************************************************************************************************************/
message_rb_handle_t Message_RB_Init (size_t amount);
void Message_RB_Free (message_rb_handle_t cbuf);
void Message_RB_Reset (message_rb_handle_t cbuf);
void Message_RB_Push (message_rb_handle_t cbuf, sMessage_t *message);
bool Message_RB_Pop (message_rb_handle_t cbuf, sMessage_t *message);
bool Message_RB_IsEmpty (message_rb_handle_t cbuf);
bool Message_RB_IsFull (message_rb_handle_t cbuf);
uint32_t Message_RB_GetSize (message_rb_handle_t cbuf);
uint32_t Message_RB_GetFreeSpace (message_rb_handle_t cbuf);
#endif /* __MESSAGE_RING_BUFFER__H__ */
