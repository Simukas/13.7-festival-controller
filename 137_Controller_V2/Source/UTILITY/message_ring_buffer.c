/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include <message_ring_buffer.h>
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/
static void Message_RB_AdvancePointer (message_rb_handle_t rb) {
    if (rb->full) {
        if (++(rb->tail) == rb->size) {
            rb->tail = 0;
        }
    }
    if (++(rb->head) == rb->size) {
        rb->head = 0;
    }
    rb->full = (rb->head == rb->tail);
}

static void Message_RB_RetreatPointer (message_rb_handle_t rb) {
    rb->full = false;
    if (++(rb->tail) == rb->size) {
        rb->tail = 0;
    }
}
/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
message_rb_handle_t Message_RB_Init (size_t amount) {
    message_rb_handle_t rb = calloc(1, sizeof(sMessage_RB_t));
    if (rb == NULL) {
        return NULL;
    }
    rb->buffer = calloc(amount, sizeof(sMessage_t));
    if (rb->buffer == NULL) {
        free(rb);
        return NULL;
    }
    rb->size = amount;
    Message_RB_Reset(rb);
    return rb;
}

void Message_RB_Reset (message_rb_handle_t rb) {
    rb->head = 0;
    rb->tail = 0;
    rb->full = false;
}

void Message_RB_Free (message_rb_handle_t rb) {
    free(rb->buffer);
    free(rb);
}

bool Message_RB_IsFull (message_rb_handle_t rb) {
    return rb->full;
}

bool Message_RB_IsEmpty (message_rb_handle_t rb) {
    return (!rb->full && (rb->head == rb->tail));
}

uint32_t Message_RB_GetSize (message_rb_handle_t rb) {
    return rb->size;
}

uint32_t Message_RB_GetFreeSpace (message_rb_handle_t rb) {
    uint32_t size = rb->size;
    if (!rb->full) {
        if (rb->head >= rb->tail) {
            size = (rb->head - rb->tail);
        }
        else {
            size = (rb->size + rb->head - rb->tail);
        }
    }
    return size;
}

void Message_RB_Push (message_rb_handle_t rb, sMessage_t *message) {
    rb->buffer[rb->head] = *message;
    Message_RB_AdvancePointer(rb);
}

bool Message_RB_Pop (message_rb_handle_t rb, sMessage_t *message) {
    bool popped = false;
    if (!Message_RB_IsEmpty(rb)) {
        *message = rb->buffer[rb->tail];
        Message_RB_RetreatPointer(rb);
        popped = true;
    }
    return popped;
}
