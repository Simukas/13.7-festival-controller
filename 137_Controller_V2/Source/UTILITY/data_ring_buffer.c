/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include <data_ring_buffer.h>
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/
static void Data_RB_AdvancePointer (data_rb_handle_t rb) {
    if (rb->full) {
        if (++(rb->tail) == rb->size) {
            rb->tail = 0;
        }
    }
    if (++(rb->head) == rb->size) {
        rb->head = 0;
    }
    rb->full = (rb->head == rb->tail);
}

static void Data_RB_RetreatPointer (data_rb_handle_t rb) {
    rb->full = false;
    if (++(rb->tail) == rb->size) {
        rb->tail = 0;
    }
}
/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
data_rb_handle_t Data_RB_Init (size_t amount) {
    data_rb_handle_t rb = calloc(1, sizeof(sData_RB_t));
    if (rb == NULL) {
        return NULL;
    }
    rb->buffer = calloc(amount, sizeof(uint8_t));
    if (rb->buffer == NULL) {
        free(rb);
        return NULL;
    }
    rb->size = amount;
    Data_RB_Reset(rb);
    return rb;
}

void Data_RB_Reset (data_rb_handle_t rb) {
    rb->head = 0;
    rb->tail = 0;
    rb->full = false;
}

void Data_RB_Free (data_rb_handle_t rb) {
    free(rb->buffer);
    free(rb);
}

bool Data_RB_IsFull (data_rb_handle_t rb) {
    return rb->full;
}

bool Data_RB_IsEmpty (data_rb_handle_t rb) {
    return (!rb->full && (rb->head == rb->tail));
}

uint32_t Data_RB_GetSize (data_rb_handle_t rb) {
    return rb->size;
}

uint32_t Data_RB_GetFreeSpace (data_rb_handle_t rb) {
    uint32_t size = rb->size;
    if (!rb->full) {
        if (rb->head >= rb->tail) {
            size = (rb->head - rb->tail);
        }
        else {
            size = (rb->size + rb->head - rb->tail);
        }
    }
    return size;
}

void Data_RB_Push (data_rb_handle_t rb, uint8_t byte) {
    rb->buffer[rb->head] = byte;
    Data_RB_AdvancePointer(rb);
}

bool Data_RB_Pop (data_rb_handle_t rb, uint8_t *byte) {
    bool popped = false;
    if (!Data_RB_IsEmpty(rb)) {
        *byte = rb->buffer[rb->tail];
        Data_RB_RetreatPointer(rb);
        popped = true;
    }
    return popped;
}
