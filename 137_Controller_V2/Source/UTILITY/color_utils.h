#ifndef __COLOR_UTILS__H__
#define __COLOR_UTILS__H__
/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "stdint.h"
/**********************************************************************************************************************
 * Exported definitions and macros
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported types
 *********************************************************************************************************************/
typedef struct sRgbColor_t {
    uint8_t r;
    uint8_t g;
    uint8_t b;
} sRgbColor_t;

typedef struct sRgbwColor_t {
    uint8_t r;
    uint8_t g;
    uint8_t b;
    uint8_t w;
} sRgbwColor_t;

typedef struct sHsvColor_t {
    uint8_t h;
    uint8_t s;
    uint8_t v;
} sHsvColor_t;
/**********************************************************************************************************************
 * Exported variables
 *********************************************************************************************************************/
extern const sRgbColor_t rgb_black;
extern const sRgbColor_t rgb_white;
extern const sRgbColor_t rgb_red;
extern const sRgbColor_t rgb_green;
extern const sRgbColor_t rgb_blue;

extern const sHsvColor_t hsv_white;
extern const sHsvColor_t hsv_black;
extern const sHsvColor_t hsv_cyan;
extern const sHsvColor_t hsv_pink;
extern const sHsvColor_t hsv_red;
extern const sHsvColor_t hsv_green;
extern const sHsvColor_t hsv_blue;
/**********************************************************************************************************************
 * Prototypes of exported functions
 *********************************************************************************************************************/
sHsvColor_t RandomHsv (void);
sRgbColor_t RandomRgb (void);
sRgbwColor_t RandomRgbw (void);
sRgbColor_t HsvToRgb (sHsvColor_t hsv);
sRgbwColor_t HsvToRgbw (sHsvColor_t hsv);
sHsvColor_t RgbToHsv (sRgbColor_t rgb);
sRgbColor_t InvertRgb (sRgbColor_t rgb);
sRgbColor_t GetGradient (sRgbColor_t start_color, sRgbColor_t end_color, double ratio);
#endif /* __COLOR_UTILS__H__ */
