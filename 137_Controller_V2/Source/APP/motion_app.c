/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include <thread_info.h>
#include "motion_app.h"
#include "stdbool.h"
#include "cmsis_os.h"
#include "debug_api.h"
#include "motor_api.h"
#include "servo_api.h"
#include "adc_driver.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
//DEBUG_MODULE(MOTION_APP)
#define MOTOR_SPEED_MAX 255
#define MOTOR_SPEED_MIN 100

#define MOTION_APP_MESSAGE_QUEUE_COUNT 3
/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
//static osThreadId_t g_motion_app_task_handle = NULL;
//
//const static osThreadAttr_t g_motion_app_task_attributes = {
//    .name = "motion_app_task",
//    .stack_size = MOTION_APP_TASK_STACK_SIZE,
//    .priority = (osPriority_t) MOTION_APP_TASK_PRIORITY
//};
/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/
//static osMessageQueueId_t g_motion_app_msg_queue = NULL;
/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/
//static void Motion_APP_Task (void *argument);
/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/
//static void Motion_APP_Task (void *argument) {
//    sMotionApp_Task_t task = {0};
////    uint8_t value = 0;
////    bool dir = true;
//    //Servo_API_EnableServo(eServoApi_Pwm);
//    //Motor_API_EnableMotor(eMotorApi_Motor_2);
//    while (true) {
////        Motor_API_SetDirection(eMotorApi_Motor_2, eMotorApi_Direction_Left);
////        Motor_API_SetSpeed(eMotorApi_Motor_2, 0);
////        Motor_API_UpdateMotor(eMotorApi_Motor_2);
//////        debug("Value: %u\r", value);
//////        if (dir) {
//////            value++;
//////            if (value == 255) {
//////                dir = false;
//////            }
//////        } else {
//////            value--;
//////            if (value == 0) {
//////                dir = true;
//////            }
//////        }
////        osDelay(10);
//
//        if (osMessageQueueGet(g_motion_app_msg_queue, &task, NULL, osWaitForever) == osOK) {
//            switch (task.task) {
//                case eMotionApp_Task_EnableMotors: {
//
//                    break;
//                }
//                case eMotionApp_Task_DisableMotors: {
//
//                    break;
//                }
//                case eMotionApp_Task_MoveServo: {
//
//                    break;
//                }
//                case eMotionApp_Task_SpinMotor: {
//
//                    break;
//                }
//                default: {
//                    break;
//                }
//            }
//        }
//    }
//}
/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool Motion_APP_Init (void) {
//    if (Servo_API_Init(eServoApi_Pwm) == false) {
//        error("Failed to initialize Servo API!\r");
//        return false;
//    }
//    if (Servo_API_Init(eServoApi_Primary) == false) {
//        error("Failed to initialize Servo API!\r");
//        return false;
//    }
//    if (Servo_API_Init(eServoApi_Secondary) == false) {
//        error("Failed to initialize Servo API!\r");
//        return false;
//    }
//
//    g_motion_app_msg_queue = osMessageQueueNew(MOTION_APP_MESSAGE_QUEUE_COUNT, sizeof(sMotionApp_Task_t), NULL);
//    if (g_motion_app_msg_queue == NULL) {
//        error("Failed to create message queue\r");
//        return false;
//    }
//
//    g_motion_app_task_handle = osThreadNew(Motion_APP_Task, NULL, &g_motion_app_task_attributes);
//    if (g_motion_app_task_handle == NULL) {
//        error("Failed to create thread!\r");
//        return false;
//    }
    return true;
}
