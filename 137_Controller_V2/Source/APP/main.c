/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "main.h"
#include "system_app.h"
#include "cmsis_os2.h"
#include "debug_api.h"
#include "gpio_driver.h"
#include "stm32f4xx_ll_gpio.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/
static void SystemClock_Config (void);
/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/
void SystemClock_Config (void) {
    LL_FLASH_SetLatency(LL_FLASH_LATENCY_2);
    while (LL_FLASH_GetLatency() != LL_FLASH_LATENCY_2){}
    LL_PWR_SetRegulVoltageScaling(LL_PWR_REGU_VOLTAGE_SCALE2);
    LL_RCC_HSE_Enable();
    while (LL_RCC_HSE_IsReady() != 1){}
    LL_RCC_LSI_Enable();
    while (LL_RCC_LSI_IsReady() != 1){}
    LL_PWR_EnableBkUpAccess();
    LL_RCC_PLL_ConfigDomain_SYS(LL_RCC_PLLSOURCE_HSE, LL_RCC_PLLM_DIV_4, 168, LL_RCC_PLLP_DIV_4);
    LL_RCC_PLL_ConfigDomain_48M(LL_RCC_PLLSOURCE_HSE, LL_RCC_PLLM_DIV_4, 168, LL_RCC_PLLQ_DIV_7);
    LL_RCC_PLL_Enable();
    while (LL_RCC_PLL_IsReady() != 1){}
    LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);
    LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_2);
    LL_RCC_SetAPB2Prescaler(LL_RCC_APB2_DIV_1);
    LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_PLL);
    while (LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_PLL){}
    LL_SetSystemCoreClock(84000000);
    if (HAL_InitTick(TICK_INT_PRIORITY) != HAL_OK){
        Error_Handler();
    }
    LL_RCC_SetTIMPrescaler(LL_RCC_TIM_PRESCALER_TWICE);
}
/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
int main (void) {
    HAL_Init();

    SystemClock_Config();

    osKernelInitialize();

    //sGpioDriver_Static_t gpio_info = {0};

//    GPIO_Driver_GetPinInfo(eGpioDriver_Pin_I2C_SDA, &gpio_info);
//
//    gpio_info.mode = LL_GPIO_MODE_OUTPUT;
//    gpio_info.alternate = LL_GPIO_AF_0;
//    gpio_info.output = LL_GPIO_OUTPUT_PUSHPULL;
//
//    GPIO_Driver_InitPin(eGpioDriver_Pin_I2C_SDA, &gpio_info);

    if (System_APP_Init() == false) {
        Error_Handler();
    }

    if (Debug_API_Init() == false) {
        Error_Handler();
    }

    osKernelStart();

    while (true) {
    }
}

void Error_Handler (void) {
    __disable_irq();
    while (true) {
    }
}

void HAL_TIM_PeriodElapsedCallback (TIM_HandleTypeDef *htim) {
    if (htim->Instance == TIM11) {
        HAL_IncTick();
    }
}
