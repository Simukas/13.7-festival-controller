/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include <math_utils.h>
#include "stdbool.h"
#include "rs485_app.h"
#include "uart_api.h"
#include "cmsis_os2.h"
#include "thread_info.h"
#include "debug_api.h"
#include "address_api.h"
#include "binary_protocol.h"
#include "light_app.h"
#include "rtc_driver.h"
#include "heap_api.h"
#include "baudrate_utils.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
DEBUG_MODULE(RS485_APP)

#define RS485_APP_TEXT_RESPONSE_BUFFER_SIZE 150
//#define RS485_APP_BINARY_RESPONSE_BUFFER_SIZE 100
#define RS485_APP_BAUDRATE eBaudrate_460800

#define RS485_APP_TIMEOUT 100
/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/
// @formatter:off
typedef enum eRs485App_Role_Enum_t {
    eRs485App_Role_First = 0,
    eRs485App_Role_Unknown = eRs485App_Role_First,
    eRs485App_Role_Master,
    eRs485App_Role_Slave,
    eRs485App_Role_Last
} eRs485App_Role_Enum_t;

typedef struct sRs485App_Dynamic_t {
    osMutexId_t tx_mutex_id;
    eBaudrate_Enum_t baudrate;
} sRs485App_Dynamic_t;

typedef struct sUartApp_Static_t {
    uint8_t msg_queue_count;
} sRs485App_Static_t;

// @formatter:on
/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
static const osThreadAttr_t rs485_app_slave_task_attributes = {
    .name = "rs485_app_slave_task",
    .stack_size = RS485_APP_TASK_STACK_SIZE,
    .priority = (osPriority_t) RS485_APP_TASK_PRIORITY,
};

//const static osThreadAttr_t rs485_app_master_task_attributes = {
//    .name = "rs485_app_master_task",
//    .stack_size = RS485_APP_TASK_STACK_SIZE,
//    .priority = (osPriority_t) RS485_APP_TASK_PRIORITY,
//};

//const static osMutexAttr_t rs485_app_mutex_attr = {
//    "rs485_app_mutex",
//    osMutexRecursive | osMutexPrioInherit,
//    NULL,
//    0U
//};

//const static eUartDriver_Enum_t g_static_rs485_app_translation_lut[eUartDriver_Last] = {
//    [eUartDriver_Usart6] = eRs485App_Port_1,
//};
/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/
static osThreadId_t g_rs485_app_task_handle = NULL;

//static sRs485App_Dynamic_t g_dynamic_rs485_app_lut = {0};

static uint8_t g_rs485_app_address = 0;

static eRs485App_Role_Enum_t g_rs485_app_role = eRs485App_Role_Unknown;

static char g_rs485_text_response_buffer[RS485_APP_TEXT_RESPONSE_BUFFER_SIZE] = {0};
//static uint8_t g_rs485_binary_response_buffer[RS485_APP_BINARY_RESPONSE_BUFFER_SIZE] = {0};

static bool g_rs485_app_usb_debug_enabled = false;
/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/
static void RS485_APP_SlaveTask (void *argument);
//static void RS485_APP_MasterTask (void *argument);
/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/
static void RS485_APP_SlaveTask (void *argument) {
    sMessage_t message = {0};
    while (true) {
        if (UART_API_GetMessage(RS485_APP_UART, &message, osWaitForever) == true) {
            if (message.buffer == NULL) {
                continue;
            }
            if (message.buffer_size < 2) {
                Heap_API_Free(message.buffer, message.buffer_size);
                continue;
            }
            uint8_t address = message.buffer[0];
            if ((address == g_rs485_app_address) || (address == ADDRESS_API_GLOBAL_ADDRESS)) {
                sBinaryProtocol_LauncherArgs_t launcher_args = {
                    .cmd_buffer = &message.buffer[1],
                    .cmd_buffer_size = message.buffer_size - 1,
                    .text_response_buffer = g_rs485_text_response_buffer,
                    .text_response_buffer_size = RS485_APP_TEXT_RESPONSE_BUFFER_SIZE,
                    .binary_response_buffer = NULL,
                    .binary_response_buffer_size = 0,
                    .cmd_id = eBinaryProtocol_Cmd_Unknown,
                };
                if (Binary_Protocol_Launcher(&launcher_args) == true) {
                    if (address != ADDRESS_API_GLOBAL_ADDRESS) {
                        RS485_APP_SendCommand(ADDRESS_API_MASTER_ADDRESS, launcher_args.cmd_id, launcher_args.binary_response_buffer, launcher_args.binary_response_buffer_size);
                    }
                    if (g_rs485_app_usb_debug_enabled == true) {
                        debug(launcher_args.text_response_buffer);
                    }
                } else {
                    if (address != ADDRESS_API_GLOBAL_ADDRESS) {
                        RS485_APP_SendCommand(ADDRESS_API_MASTER_ADDRESS, eBinaryProtocol_Cmd_System_ERROR, NULL, 0);
                    }
                    if (g_rs485_app_usb_debug_enabled == true) {
                        error(launcher_args.text_response_buffer);
                    }
                }
            }
            Heap_API_Free(message.buffer, message.buffer_size);
        }
    }
}

//static void RS485_APP_MasterTask (void *argument) {
//    sDateTime_t date_time = {0};
//    uint8_t last_minutes = 0;
//    while (true) {
//        RTC_Driver_GetDateTime(&date_time);
//        if (last_minutes != date_time.seconds) {
//            if (date_time.seconds % ANIMATION_CHANGE_MINUTE_COUNT == 0) {
//                sLightApp_Task_t task = {
//                    .task = eLightApp_Task_RGBW_AddAnimation,
//                    .task_data = NULL
//                };
//                Light_APP_AddTask(&task);
//                last_minutes = date_time.seconds;
//            }
//        }
//
//        osDelay(100);
//    }
//}

/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool RS485_APP_Init (void) {
    if (UART_API_Init(RS485_APP_UART, RS485_APP_BAUDRATE) == false) {
        error("Failed to initialize UART API\r");
        return false;
    }
    if (Address_API_Init() == false) {
        error("Failed to initialize Address API\r");
        return false;
    }
    if (Address_API_GetAddress(&g_rs485_app_address) == false) {
        error("Failed to get address\r");
        return false;
    }
    if (g_rs485_app_address == ADDRESS_API_GLOBAL_ADDRESS) {
        error("Selected address can't be global (%d)\r", ADDRESS_API_GLOBAL_ADDRESS);
        return false;
    }
    if (g_rs485_app_address == ADDRESS_API_MASTER_ADDRESS) {
//        g_rs485_app_task_handle = osThreadNew(RS485_APP_MasterTask, NULL, &rs485_app_master_task_attributes);
//        if (g_rs485_app_task_handle == NULL) {
//            error("Failed to create thread\r");
//            return false;
//        }
//        g_rs485_app_role = eRs485App_Role_Master;
    } else {
        g_rs485_app_task_handle = osThreadNew(RS485_APP_SlaveTask, NULL, &rs485_app_slave_task_attributes);
        if (g_rs485_app_task_handle == NULL) {
            error("Failed to create thread\r");
            return false;
        }
        g_rs485_app_role = eRs485App_Role_Slave;
    }
    return true;
}

bool RS485_APP_SendCommand (uint8_t address, eBinaryProtocol_Cmd_Enum_t command, uint8_t *data, uint32_t data_length) {
    if (command >= eBinaryProtocol_Cmd_Last) {
        error("Unsupported command! [%u]\r", command);
        return false;
    }
    if (data_length > 0) {
        if (data == NULL) {
            error("NULL data pointer!\r");
            return false;
        }
    }
    uint32_t command_length = data_length + 2;
    uint8_t *command_buffer = Heap_API_CallocISR(command_length, sizeof(uint8_t));
    if (command_buffer == NULL) {
        error("Failed to allocate command buffer!\r");
        return false;
    }
    command_buffer[0] = address;
    command_buffer[1] = command;
    bool return_value = true;
    if (data_length > 0) {
        memcpy(command_buffer + 2, data, data_length);
        if (Heap_API_Free(data, data_length) == false) {
            error("Failed to free data buffer!\r");
            return_value = false;
        }
    }
    if (UART_API_SendData(RS485_APP_UART, command_buffer, command_length, RS485_APP_TIMEOUT) == false) {
        error("Failed to send command!\r");
        return_value = false;
    }
    return return_value;
}

bool RS485_APP_SetBaudrate (eBaudrate_Enum_t baudrate) {
    if (baudrate >= eBaudrate_Last) {
        error("Wrong baudrate provided!\r");
        return false;
    }
    if (UART_API_SetBaudrate(RS485_APP_UART, baudrate) == false) {
        return false;
    }
    return true;
}

bool RS485_CheckAddress(uint8_t address) {
    if (address == ADDRESS_API_GLOBAL_ADDRESS || address == g_rs485_app_address) {
        return true;
    } else {
        return false;
    }
}

bool RS485_APP_IsUSBDebugEnabled (void) {
    return g_rs485_app_usb_debug_enabled;
}

bool RS485_APP_EnableUSBDebug (void) {
    g_rs485_app_usb_debug_enabled = true;
    return true;
}
