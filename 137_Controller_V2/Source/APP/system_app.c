/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "system_app.h"
#include <usb_api.h>
#include <thread_info.h>
#include "cmsis_os2.h"
#include "stdbool.h"
#include "debug_api.h"
#include "thread_info.h"
#include "time_utils.h"
#include "heap_api.h"
#include "stdbool.h"
#include "cli_app.h"
#include "rs485_app.h"
#include "light_app.h"
#include "motion_app.h"
#include "adc_driver.h"
#include "rtc_driver.h"
#include "vl53l3_api.h"
#include "hx711_api.h"
#include "stdint.h"
#include "adc_driver.h"
#include "rs485_app.h"
#include "uart_api.h"
#include "timer_driver.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
//DEBUG_MODULE(SYSTEM_APP)

#define SYSTEM_APP_LOOP_PERIOD 50
/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
static const osThreadAttr_t g_system_app_task_attributes = {
    .name = "system_app_task",
    .stack_size = SYSTEM_APP_TASK_STACK_SIZE,
    .priority = (osPriority_t) SYSTEM_APP_TASK_PRIORITY
};
/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/
static osThreadId_t g_system_app_task_handle = NULL;
/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/
static void System_APP_Task (void *argument);
/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/
static void System_APP_Task (void *argument) {
    if (RS485_APP_Init() == false) {
        Error_Handler();
    }

    if (CLI_APP_Init() == false) {
        Error_Handler();
    }

//    if (Motion_APP_Init() == false) {
//        Error_Handler();
//    }
//
//    if (Light_APP_Init() == false) {
//        Error_Handler();
//    }
    while (true) {
        for (eVL53L3Api_Enum_t distance_sensor = eVL53L3Api_First; distance_sensor < eVL53L3Api_Last; distance_sensor++) {
            VL53L3_API_MeasureDistance(distance_sensor);
        }
        for (eHx711Api_Enum_t weight_sensor = eHx711Api_First; weight_sensor < eHx711Api_Last; weight_sensor++) {
            HX711_API_MeasureWeight(weight_sensor);
        }
        ADC_Driver_ReadChannels(eAdcDriver_Adc_1);

        Timer_Driver_ResetWatchdog();

        osDelay(SYSTEM_APP_LOOP_PERIOD);
    }
}
/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool System_APP_Init (void) {
    if (Heap_API_Init() == false) {
        return false;
    }

    if (delay_init() == false) {
        return false;
    }

    if (RTC_Driver_Init() == false) {
        return false;
    }

    if (ADC_Driver_Init(eAdcDriver_Adc_1) == false) {
        return false;
    }

    if (Timer_Driver_InitWatchdog() == false) {
        return false;
    }

    g_system_app_task_handle = osThreadNew(System_APP_Task, NULL, &g_system_app_task_attributes);
    if (g_system_app_task_handle == NULL) {
        return false;
    }
    return true;
}
