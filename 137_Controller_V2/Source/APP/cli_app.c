/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include <string_cmd_handlers.h>
#include <string_protocol.h>
#include "stdbool.h"
#include "cli_app.h"
#include "cmsis_os.h"
#include "thread_info.h"
#include "debug_api.h"
#include "string_utils.h"
#include "usb_api.h"
#include "heap_api.h"
#include "address_api.h"
#include "adc_driver.h"
#include "heap_api.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
DEBUG_MODULE(CLI_APP)

#define CLI_APP_FULL_CMD_SPACE 30
#define CLI_APP_DESCRIPTION_SPACE 70

#define CLI_APP_FULL_CMD_SPACE_STRING str(30)
#define CLI_APP_DESCRIPTION_SPACE_STRING str(70)
/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/
typedef enum eCliApp_Table_Enum_t {
    eCliApp_Table_First = 0,
        eCliApp_Table_Start = eCliApp_Table_First,
        eCliApp_Table_Middle,
        eCliApp_Table_End,
        eCliApp_Table_Last
} eCliApp_Table_Enum_t;

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/
static bool CLI_APP_PrintStatus (sStringProtocol_HandlerArgs_t args);
static void CLI_APP_Task (void *argument);
static bool CLI_APP_PrintBars (uint8_t amount);
static bool CLI_APP_PrintOutline (eCliApp_Table_Enum_t type);
static bool CLI_APP_PrintInfo (sStringProtocol_HandlerArgs_t args);
static bool CLI_APP_PrintHelp (sStringProtocol_HandlerArgs_t args);
/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
// @formatter:off
static const osThreadAttr_t cli_app_task_attributes = {
    .name = "cli_app_task",
    .stack_size = CLI_APP_TASK_STACK_SIZE,
    .priority = (osPriority_t) CLI_APP_TASK_PRIORITY
};

const sCmdApiCmd_t g_static_cli_app_cmd_lut[eCliApp_Cmd_Last] = {
    [eCliApp_Cmd_Info]                  = {.name = "info",      .fun_ptr = &CLI_APP_PrintInfo,          .parameters = "",           .description = "Provides information about the controller and it's functions"},
    [eCliApp_Cmd_Help]                  = {.name = "help",      .fun_ptr = &CLI_APP_PrintHelp,          .parameters = "",           .description = "Provides a list of available CLI commands"},
    [eCliApp_Cmd_Status]                = {.name = "status",    .fun_ptr = &CLI_APP_PrintStatus,        .parameters = "",           .description = "Provides certain status information"},
    [eCliApp_Cmd_EnableRS485UsbDebug]   = {.name = "rs485",     .fun_ptr = &CMD_RS485_EnableUsbDebug,   .parameters = "",           .description = "Enables RS485 USB debug messages"},
//    [eCliApp_Cmd_ARGB_SET_PIXEL]    = {.name = "argb_set_pixel",    .fun_ptr = &CMD_ARGB_SET_PIXEL,     .parameters = ":p,i,r,g,b", .description = "Sets selected pixel (i) of a port (p) to a provided color (r,g,b)"},
//    [eCliApp_Cmd_ARGB_FILL_STRIP]   = {.name = "argb_fill_strip",   .fun_ptr = &CMD_ARGB_FILL_STRIP,    .parameters = ":p,r,g,b",   .description = "Sets the whole strip of a port (p) to a provided color (r,g,b)"},
//    [eCliApp_Cmd_RGB_SET_COLOR]     = {.name = "rgb_set_color",     .fun_ptr = &CMD_RGBW_SET_COLOR,     .parameters = ":r,g,b",     .description = ""},
//    [eCliApp_Cmd_RS_SEND_CMD]       = {.name = "rs_send_cmd",       .fun_ptr = &CMD_RS_SEND_CMD,        .parameters = "",           .description = ""},
};
// @formatter:on
const char g_cli_app_logo[] = "\t  _ _____ _____   _     _       _     _     _____         _   _            _\r"
                              "\t / |___ /|___  | | |   (_) __ _| |__ | |_  |  ___|__  ___| |_(_)_   ____ _| |\r"
                              "\t | | |_ \\   / /  | |   | |/ _` | '_ \\| __| | |_ / _ \\/ __| __| \\ \\ / / _` | |\r"
                              "\t | |___) | / /   | |___| | (_| | | | | |_  |  _|  __/\\__ \\ |_| |\\ V / (_| | |\r"
                              "\t |_|____(_)_/    |_____|_|\\__, |_| |_|\\__| |_|  \\___||___/\\__|_| \\_/ \\__,_|_|\r"
                              "\t                          |___/\r";

const char g_cli_app_board[] = "N/A\r";

const char g_cli_app_info[] = "\t13.7 Controller is a kinematic lighting controller with ability to control motion and \r"
                              "\tlighting elements, receive input from common sensors, and provide information on supply \r"
                              "\tvoltage and board temperature.\r";
/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/
static osThreadId_t cli_app_task_handle = NULL;
static char response_buffer[CLI_APP_RESPONSE_BUFFER_SIZE] = {0};
/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/
static bool CLI_APP_PrintStatus (sStringProtocol_HandlerArgs_t args) {
    if (Debug_API_Lock() == false) {
        return false;
    }
    debug("System status message start:\r");

    uint32_t blocks = 0;
    if (Heap_API_GetUnfreedBlocks(&blocks) == false) {

    }
    printF("\tUnfreed memory blocks: %lu\r", blocks);

    uint32_t bytes = 0;
    if (Heap_API_GetUnfreedBytes(&bytes) == false) {

    }
    printF("\tUnfreed memory bytes: %lu\r", bytes);

    uint16_t voltage_mV = 0;
    if (ADC_Driver_GetSupplyVoltage(&voltage_mV) == false) {

    }
    printF("\tSupply voltage: %umV\r", voltage_mV);

    int8_t temperature = 0;
    if (ADC_Driver_GetTemperature(&temperature) == false) {

    }
    printF("\tController temperature: %d°C\r", temperature);

    uint8_t address = 0;
    if (Address_API_GetAddress(&address) == false) {

    }
    printF("\tController RS485 address: 0x%02x\r", address);

    debug("System status message end\r\r");

    Debug_API_Unlock();
    return true;
}

static bool CLI_APP_PrintInfo (sStringProtocol_HandlerArgs_t args) {
    if (Debug_API_Lock() == false) {
        return false;
    }
    print(g_cli_app_logo);
    print("\r");
    print(g_cli_app_info);
    print("\r");
    //print(g_cli_app_board);
    Debug_API_Unlock();
    return true;
}

static bool CLI_APP_PrintHelp (sStringProtocol_HandlerArgs_t args) {
    if (Debug_API_Lock() == false) {
        return false;
    }
    CLI_APP_PrintOutline(eCliApp_Table_Start);
    printF("\t│%-30s│%-70s│\r", "COMMAND NAME AND PARAMETERS", "COMMAND DESCRIPTION");
    CLI_APP_PrintOutline(eCliApp_Table_Middle);
    for (eCliApp_Cmd_t cmd = eCliApp_Cmd_First; cmd < eCliApp_Cmd_Last; cmd++) {
        uint8_t full_cmd_length = strlen(g_static_cli_app_cmd_lut[cmd].name) + strlen(g_static_cli_app_cmd_lut[cmd].parameters);
        char *full_cmd = calloc(full_cmd_length + 1, sizeof(char));
        strcat(full_cmd, g_static_cli_app_cmd_lut[cmd].name);
        strcat(full_cmd, g_static_cli_app_cmd_lut[cmd].parameters);
        printF("\t│%-30s│%-70s│\r", full_cmd, g_static_cli_app_cmd_lut[cmd].description);
        free(full_cmd);
        if (cmd < eCliApp_Cmd_Last - 1) {
            CLI_APP_PrintOutline(eCliApp_Table_Middle);
        }
    }
    CLI_APP_PrintOutline(eCliApp_Table_End);
    Debug_API_Unlock();
    return true;
}

static bool CLI_APP_PrintOutline (eCliApp_Table_Enum_t type) {
    if (type >= eCliApp_Table_Last) {
        return false;
    }
    switch (type) {
        case eCliApp_Table_Start: {
            print("\t┌");
            CLI_APP_PrintBars(CLI_APP_FULL_CMD_SPACE);
            print("┬");
            CLI_APP_PrintBars(CLI_APP_DESCRIPTION_SPACE);
            print("┐\r");
            break;
        }
        case eCliApp_Table_Middle: {
            print("\t├");
            CLI_APP_PrintBars(CLI_APP_FULL_CMD_SPACE);
            print("┼");
            CLI_APP_PrintBars(CLI_APP_DESCRIPTION_SPACE);
            print("┤\r");
            break;
        }
        case eCliApp_Table_End: {
            print("\t└");
            CLI_APP_PrintBars(CLI_APP_FULL_CMD_SPACE);
            print("┴");
            CLI_APP_PrintBars(CLI_APP_DESCRIPTION_SPACE);
            print("┘\r");
            break;
        }
        default: {
            break;
        }
    }
    return true;
}

static bool CLI_APP_PrintBars (uint8_t amount) {
    if (amount == 0) {
        return false;
    }
    while (amount > 0) {
        print("─");
        amount--;
    }
    return true;
}

static void CLI_APP_Task (void *argument) {
    sUsbApi_Message_t message = {0};
    while (true) {
        if (USB_API_GetMessage(&message) == true) {
            sStringProtocol_LauncherArgs_t cmd_launcher_args = {
                .cmd_lut = g_static_cli_app_cmd_lut,
                .cmd_lut_size = eCliApp_Cmd_Last,
                .response_buffer_size = CLI_APP_RESPONSE_BUFFER_SIZE,
                .response_buffer = response_buffer,
                .cmd_raw = (char*) message.buffer,
                .cmd_length = message.buffer_length,
            };
            if (String_Protocol_Launcher(&cmd_launcher_args) == true) {
                if (response_buffer[0] != 0) {
                    debug(response_buffer);
                }
            } else {
                debug(response_buffer);
            }
            if (Heap_API_Free(message.buffer, message.buffer_length) == false) {
                error("Failed to free message buffer!\r");
            }
        }
    }
}
/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool CLI_APP_Init (void) {
    cli_app_task_handle = osThreadNew(CLI_APP_Task, NULL, &cli_app_task_attributes);
    if (cli_app_task_handle == NULL) {
        error("Failed to create thread\r");
        return false;
    }
    return true;
}
