/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include <thread_info.h>
#include "light_app.h"
#include "stdbool.h"
#include "cmsis_os2.h"
#include "debug_api.h"
#include "color_utils.h"
#include "math_utils.h"
#include "led_api.h"
#include "ws2815_api.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
//DEBUG_MODULE(LIGHT_APP)

#define LIGHT_APP_ARGB_PIXEL_AMOUNT 1200
#define LIGHT_APP_MESSAGE_QUEUE_COUNT 3

#define LIGHT_APP_RGBW_UPDATE_PERIOD 33
#define LIGHT_APP_ARGB_UPDATE_PERIOD 70

#define UPDATE_COUNTS 5
/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
//static const osThreadAttr_t g_light_app_task_attributes = {
//    .name = "light_app_task",
//    .stack_size = LIGHT_APP_TASK_STACK_SIZE,
//    .priority = (osPriority_t) LIGHT_APP_TASK_PRIORITY
//};
/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/
//sRgbColor_t g_argb_strip_data[LIGHT_APP_ARGB_PIXEL_AMOUNT] = {0};

//static sLightApp_StrobeData_t g_dynamic_strobe_data[eLedApi_Port_Last] = {
//    [eLedApi_Port_1] = {
//        .state = eLightApp_StrobeState_Generate
//    },
//    [eLedApi_Port_2] = {
//        .state = eLightApp_StrobeState_Generate
//    },
//};
//
//static sLightApp_PulseData_t g_dynamic_pulse_data[eLedApi_Port_Last] = {
//    [eLedApi_Port_1] = {
//        .state = eLightApp_PulseState_Generate
//    },
//    [eLedApi_Port_2] = {
//        .state = eLightApp_PulseState_Generate
//    },
//};

//static sLightApp_Task_t task = {0};

//static sLightApp_StaticColorData_t g_dynamic_static_color_data[eLedApi_Port_Last] = {0};

//static osEventFlagsId_t g_light_app_event_flag = NULL;

static osTimerId_t g_light_app_argb_timer = NULL;
//static osTimerId_t g_light_app_rgbw_timer = NULL;
//static osThreadId_t g_light_app_task = NULL;
//static osMessageQueueId_t g_light_app_msg_queue = NULL;

static uint16_t led_index = 0;
//static uint8_t hue = 0;
//static uint8_t update_counter = 0;
/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/
//static void Light_APP_Task (void *argument);
/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/
//static void Light_APP_Task (void *argument) {
//    sLightApp_Task_t task = {0};
//    while (true) {
//        if (osMessageQueueGet(g_light_app_msg_queue, &task, NULL, osWaitForever) == osOK) {
//
//        }
//    }
//}

static sRgbColor_t rgb_color = {0};

static void Light_APP_WS2815_Timer_Callback (void *argument) {
    WS2815_API_FillSegmentRgb(eWs2815Api_Port_1, 0, led_index, rgb_color);
    WS2815_API_FillSegmentRgb(eWs2815Api_Port_2, 0, led_index, rgb_color);
    led_index++;
    if (led_index >= LIGHT_APP_ARGB_PIXEL_AMOUNT) {
        rgb_color = RandomRgb();
        led_index = 0;
    }

    WS2815_API_UpdatePixels();
}

//static void Light_APP_RGBW_Timer_Callback (void *argument) {
//    switch (current_animation) {
//        case eLedApi_Animation_Pulse: {
//            LED_API_Pulse(eLedApi_Port_1, &g_dynamic_pulse_data[eLedApi_Port_1]);
//            LED_API_Pulse(eLedApi_Port_2, &g_dynamic_pulse_data[eLedApi_Port_2]);
//            break;
//        }
//        case eLedApi_Animation_PulseSingleColor: {
//            LED_API_Pulse(eLedApi_Port_1, &g_dynamic_pulse_data[eLedApi_Port_1]);
//            LED_API_Pulse(eLedApi_Port_2, &g_dynamic_pulse_data[eLedApi_Port_2]);
//            break;
//        }
//        case eLedApi_Animation_Strobe: {
//            LED_API_Strobe(eLedApi_Port_1, &g_dynamic_strobe_data[eLedApi_Port_1]);
//            LED_API_Strobe(eLedApi_Port_2, &g_dynamic_strobe_data[eLedApi_Port_2]);
//            break;
//        }
//        case eLedApi_Animation_StrobeSingleColor: {
//            LED_API_Strobe(eLedApi_Port_1, &g_dynamic_strobe_data[eLedApi_Port_1]);
//            LED_API_Strobe(eLedApi_Port_2, &g_dynamic_strobe_data[eLedApi_Port_2]);
//            break;
//        }
//        default: {
//            break;
//        }
//    }
//}
/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool Light_APP_Init (void) {
    g_light_app_argb_timer = osTimerNew(Light_APP_WS2815_Timer_Callback, osTimerPeriodic, NULL, NULL);
    if (g_light_app_argb_timer == NULL) {
        //error("Failed to create WS2815 timer!\r");
        return false;
    }
//
//    g_light_app_rgbw_timer = osTimerNew(Light_APP_RGBW_Timer_Callback, osTimerPeriodic, NULL, NULL);
//    if (g_light_app_rgbw_timer == NULL) {
//        error("Failed to create RGBW timer!\r");
//        return false;
//    }

//    g_light_app_msg_queue = osMessageQueueNew(LIGHT_APP_MESSAGE_QUEUE_COUNT, sizeof(sLightApp_Task_t), NULL);
//    if (g_light_app_msg_queue == NULL) {
//        error("Failed to create message queue!\r");
//        return false;
//    }

    if (WS2815_API_Enable(eWs2815Api_Port_1, LIGHT_APP_ARGB_PIXEL_AMOUNT) == false) {
        return false;
    }
    if (WS2815_API_Enable(eWs2815Api_Port_2, LIGHT_APP_ARGB_PIXEL_AMOUNT) == false) {
        return false;
    }

    rgb_color = RandomRgb();

    rgb_color.r = 129;
    rgb_color.g = 129;
    rgb_color.b = 129;
//
//    g_light_app_task = osThreadNew(Light_APP_Task, NULL, &g_light_app_task_attributes);
//    if (g_light_app_task == NULL) {
//        debug("Failed to create thread!\r");
//        return false;
//    }

    //osTimerStart(g_light_app_rgbw_timer, LIGHT_APP_RGBW_UPDATE_PERIOD);
    osTimerStart(g_light_app_argb_timer, LIGHT_APP_ARGB_UPDATE_PERIOD);
    return true;
}

//bool Light_APP_AddTask (sLightApp_Task_t *task) {
//    if (task == NULL) {
//        error("NULL pointer provided!\r");
//        return false;
//    }
//    if (osMessageQueuePut(g_light_app_msg_queue, task, 0U, osWaitForever) != osOK) {
//        error("Failed to put message into queue!\r");
//        return false;
//    }
//    return true;
//}
