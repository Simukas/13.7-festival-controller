#ifndef __RS485_APP__H__
#define __RS485_APP__H__
/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "stdbool.h"
#include "uart_driver.h"
#include "binary_protocol.h"
/**********************************************************************************************************************
 * Exported definitions and macros
 *********************************************************************************************************************/
#define RS485_APP_UART eUartApi_RS485
/**********************************************************************************************************************
 * Exported types
 *********************************************************************************************************************/
typedef enum eRs485App_Port_Enum_t {
    eRs485App_Port_First,
    eRs485App_Port_1 = eRs485App_Port_First,
    eRs485App_Port_Last,
} eRs485App_Port_Enum_t;
/**********************************************************************************************************************
 * Exported variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of exported functions
 *********************************************************************************************************************/
bool RS485_APP_Init                 (void);
bool RS485_APP_SendBytes            (uint8_t *bytes, uint16_t buffer_size);
bool RS485_APP_SendByte             (uint8_t byte);
bool RS485_APP_SendCommand          (uint8_t address, eBinaryProtocol_Cmd_Enum_t command, uint8_t *data, uint32_t data_length);
bool RS485_APP_SetBaudrate          (eBaudrate_Enum_t baudrate);
bool RS485_APP_EnableUSBDebug       (void);
bool RS485_APP_IsUSBDebugEnabled    (void);
bool RS485_CheckAddress             (uint8_t address);
#endif /* __RS485_API__H__ */
