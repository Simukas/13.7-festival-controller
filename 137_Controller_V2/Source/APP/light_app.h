#ifndef __LIGHT_APP__H__
#define __LIGHT_APP__H__
/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "stdbool.h"
#include "led_api.h"
/**********************************************************************************************************************
 * Exported definitions and macros
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported types
 *********************************************************************************************************************/
typedef enum eLightApp_Task_Enum_t {
    eLightApp_Task_First = 0,
    eLightApp_Task_ARGB_AddAnimation = eLightApp_Task_First,
    eLightApp_Task_ARGB_StartAnimation,
    eLightApp_Task_ARGB_StopAnimation,
    eLightApp_Task_RGBW_AddAnimation,
    eLightApp_Task_RGBW_StartAnimation,
    eLightApp_Task_RGBW_StopAnimation,
    eLightApp_Task_Last,
} eLightApp_Task_Enum_t;

typedef struct sLightApp_Task_t {
    eLightApp_Task_Enum_t task;
    void *task_data;
} sLightApp_Task_t;
/**********************************************************************************************************************
 * Exported variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of exported functions
 *********************************************************************************************************************/
bool Light_APP_Init (void);
bool Light_APP_Pause (void);
bool Light_APP_Resume (void);
bool Light_APP_AddTask (sLightApp_Task_t *task);
#endif /* __LIGHT_APP__H__ */
