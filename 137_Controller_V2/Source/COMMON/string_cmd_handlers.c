/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include <rs485_app.h>
#include <string_cmd_handlers.h>
#include "stdbool.h"
#include "string.h"
#include "color_utils.h"
#include "stdlib.h"
#include "stdio.h"
#include "led_api.h"
#include "string_utils.h"
#include "light_app.h"
#include "rs485_app.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
#define CMD_HANDLER_ARGUMENT_SEPARATOR ","
/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool CMD_RS485_EnableUsbDebug (sStringProtocol_HandlerArgs_t args) {
    if (RS485_APP_EnableUSBDebug() == false) {
        return false;
    }
    return true;
}

bool CMD_ARGB_SET_PIXEL (sStringProtocol_HandlerArgs_t args) {

    return true;
}

bool CMD_ARGB_FILL_STRIP (sStringProtocol_HandlerArgs_t args) {

    return true;
}

bool CMD_RGBW_SET_COLOR (sStringProtocol_HandlerArgs_t args) {
//    sRgbwColor_t rgbw_color = {0};
//    eLedApi_Port_Enum_t port = 0;
//    char *token = NULL;
//    token = strtok(args.cmd_args, CMD_HANDLER_ARGUMENT_SEPARATOR);
//    if (token == NULL) {
//        return false;
//    }
//    port = atoi(token);
//    token = strtok(NULL, CMD_HANDLER_ARGUMENT_SEPARATOR);
//    if (token == NULL) {
//        return false;
//    }
//    rgbw_color.r = atoi(token);
//    token = strtok(NULL, CMD_HANDLER_ARGUMENT_SEPARATOR);
//    if (token == NULL) {
//        return false;
//    }
//    rgbw_color.g = atoi(token);
//    token = strtok(NULL, CMD_HANDLER_ARGUMENT_SEPARATOR);
//    if (token == NULL) {
//        return false;
//    }
//    rgbw_color.b = atoi(token);
//    token = strtok(NULL, CMD_HANDLER_ARGUMENT_SEPARATOR);
//    if (token == NULL) {
//        return false;
//    }
//    rgbw_color.w = atoi(token);
//    LED_API_SetRgbwColor(port, rgbw_color);
//    snprintf(args.response_buffer, args.response_buffer_size, "Set LED strip to RGB(%d,%d,%d)\r", rgbw_color.r, rgbw_color.g, rgbw_color.b);
    return true;
}

bool CMD_RS_SEND_CMD (sStringProtocol_HandlerArgs_t args) {
//    char *token = NULL;
//    token = strtok(args.cmd_args, CMD_HANDLER_ARGUMENT_SEPARATOR);
//    if (token == NULL) {
//        return false;
//    }
//    uint8_t address = atoi(token);
//    token = strtok(NULL, CMD_HANDLER_ARGUMENT_SEPARATOR);
//    uint16_t buffer_size = strlen(token) / 2;
//    uint8_t *buffer = calloc(buffer_size, sizeof(uint8_t));
//    parsehex(token, buffer);
//    RS485_API_SendCommand(address, buffer, buffer_size);
    return true;
}
