#ifndef __CMD_HANDLERS__H__
#define __CMD_HANDLERS__H__
/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include <string_protocol.h>
/**********************************************************************************************************************
 * Exported definitions and macros
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported types
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of exported functions
 *********************************************************************************************************************/
bool CMD_RS485_EnableUsbDebug (sStringProtocol_HandlerArgs_t args);
//bool CMD_ARGB_SET_PIXEL (sStringProtocol_HandlerArgs_t args);
//bool CMD_ARGB_FILL_STRIP (sStringProtocol_HandlerArgs_t args);
//bool CMD_RGBW_SET_COLOR (sStringProtocol_HandlerArgs_t args);
//bool CMD_RS_SEND_CMD (sStringProtocol_HandlerArgs_t args);
#endif /* __CMD_HANDLERS__H__ */
