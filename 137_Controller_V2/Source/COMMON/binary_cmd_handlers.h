#ifndef __BINARY_CMD_HANDLERS__H__
#define __BINARY_CMD_HANDLERS__H__
/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "stdbool.h"
#include <binary_protocol.h>
/**********************************************************************************************************************
 * Exported definitions and macros
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported types
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of exported functions
 *********************************************************************************************************************/
bool BIN_CMD_System_ERROR               (sBinaryProtocol_HandlerArgs_t *args);
bool BIN_CMD_System_OK                  (sBinaryProtocol_HandlerArgs_t *args);
bool BIN_CMD_System_ASCIICmd            (sBinaryProtocol_HandlerArgs_t *args);
bool BIN_CMD_System_SetTime             (sBinaryProtocol_HandlerArgs_t *args);
bool BIN_CMD_System_SetBaudrate         (sBinaryProtocol_HandlerArgs_t *args);
bool BIN_CMD_System_GetTemperature      (sBinaryProtocol_HandlerArgs_t *args);
bool BIN_CMD_System_GetSupplyVoltage    (sBinaryProtocol_HandlerArgs_t *args);
bool BIN_CMD_System_EnableRS485USBDebug (sBinaryProtocol_HandlerArgs_t *args);

bool BIN_CMD_Motor_Enable               (sBinaryProtocol_HandlerArgs_t *args);
bool BIN_CMD_Motor_Disable              (sBinaryProtocol_HandlerArgs_t *args);
bool BIN_CMD_Motor_SetSpeedAndDirection (sBinaryProtocol_HandlerArgs_t *args);
bool BIN_CMD_Motor_MoveServo            (sBinaryProtocol_HandlerArgs_t *args);

bool BIN_CMD_RGBW_Enable                (sBinaryProtocol_HandlerArgs_t *args);
bool BIN_CMD_RGBW_Disable               (sBinaryProtocol_HandlerArgs_t *args);
bool BIN_CMD_RGBW_Update                (sBinaryProtocol_HandlerArgs_t *args);
bool BIN_CMD_RGBW_SetColor              (sBinaryProtocol_HandlerArgs_t *args);

bool BIN_CMD_ARGB_Enable                (sBinaryProtocol_HandlerArgs_t *args);
bool BIN_CMD_ARGB_Disable               (sBinaryProtocol_HandlerArgs_t *args);
bool BIN_CMD_ARGB_Update                (sBinaryProtocol_HandlerArgs_t *args);
bool BIN_CMD_ARGB_SetSegmentColor       (sBinaryProtocol_HandlerArgs_t *args);
bool BIN_CMD_ARGB_DimSegmentColor       (sBinaryProtocol_HandlerArgs_t *args);
bool BIN_CMD_ARGB_GradientSegmentColor  (sBinaryProtocol_HandlerArgs_t *args);

bool BIN_CMD_IO_Enable                  (sBinaryProtocol_HandlerArgs_t *args);
bool BIN_CMD_IO_Disable                 (sBinaryProtocol_HandlerArgs_t *args);
bool BIN_CMD_IO_SetPort                 (sBinaryProtocol_HandlerArgs_t *args);
bool BIN_CMD_IO_ReadPort                (sBinaryProtocol_HandlerArgs_t *args);

bool BIN_CMD_HX711_Enable               (sBinaryProtocol_HandlerArgs_t *args);
bool BIN_CMD_HX711_Disable              (sBinaryProtocol_HandlerArgs_t *args);
bool BIN_CMD_HX711_Calibrate            (sBinaryProtocol_HandlerArgs_t *args);
bool BIN_CMD_HX711_GetWeight            (sBinaryProtocol_HandlerArgs_t *args);

bool BIN_CMD_Distance_Enable            (sBinaryProtocol_HandlerArgs_t *args);
bool BIN_CMD_Distance_Disable           (sBinaryProtocol_HandlerArgs_t *args);
bool BIN_CMD_Distance_Calibrate         (sBinaryProtocol_HandlerArgs_t *args);
bool BIN_CMD_Distance_Read              (sBinaryProtocol_HandlerArgs_t *args);
#endif /* __BINARY_CMD_HANDLERS__H__ */
