/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "string_protocol.h"
#include "stdio.h"
#include "string_utils.h"
#include "debug_api.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
//DEBUG_MODULE(CMD_API)
#define DEFAULT_STRING_PROTOCOL_DELIMITER ":"
/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
const char bad_chars[] = "\r\n ";
/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool String_Protocol_Launcher (sStringProtocol_LauncherArgs_t *launcher_args) {
    sStringProtocol_HandlerArgs_t handler_args = {
        .response_buffer = launcher_args->response_buffer,
        .response_buffer_size = launcher_args->response_buffer_size
    };
    uint16_t cmd_id = 0;
    bool cmd_recognized = false;
    char *token = NULL;
    remchar(launcher_args->cmd_raw, bad_chars, launcher_args->cmd_length);

    if (launcher_args->cmd_raw[0] == 0) {
        snprintf(launcher_args->response_buffer, launcher_args->response_buffer_size, "CMD is empty!\r");
        return false;
    }

    token = strtok(launcher_args->cmd_raw, DEFAULT_STRING_PROTOCOL_DELIMITER);
    for (uint8_t cmd = 0; cmd < launcher_args->cmd_lut_size; cmd++) {
        if (strequal(token, launcher_args->cmd_lut[cmd].name, launcher_args->cmd_length) == true) {
            cmd_id = cmd;
            handler_args.cmd_args = launcher_args->cmd_raw + strlen(launcher_args->cmd_lut[cmd].name) + 1;
            cmd_recognized = true;
            break;
        }
    }

    if (cmd_recognized == false) {
        snprintf(launcher_args->response_buffer, launcher_args->response_buffer_size, "CMD recognition failed! [%s]\r", token);
        return false;
    }
    if (launcher_args->cmd_lut[cmd_id].fun_ptr(handler_args) == false) {
        snprintf(launcher_args->response_buffer, launcher_args->response_buffer_size, "Wrong parameters entered!\r");
        return false;
    }
    return true;
}
