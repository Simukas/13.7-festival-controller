/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "binary_cmd_handlers.h"
#include "stdlib.h"
#include "stdio.h"
#include "uart_api.h"
#include "string.h"
#include "cli_app.h"
#include "rtc_driver.h"
#include "time_utils.h"
#include "heap_api.h"
#include "cmsis_os2.h"
#include "vl53l3_api.h"
#include "motor_api.h"
#include "servo_api.h"
#include "ws2815_api.h"
#include "hx711_api.h"
#include "io_api.h"
#include "rs485_app.h"
#include "led_api.h"
#include "ws2815_lut.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/
typedef enum eBinaryCmdHandlers_Compare_Enum {
    eBinaryCmdHandlers_Compare_First = 0,
        eBinaryCmdHandlers_Compare_Less = eBinaryCmdHandlers_Compare_First,
        eBinaryCmdHandlers_Compare_LessOrEqual,
        eBinaryCmdHandlers_Compare_Equal,
        eBinaryCmdHandlers_Compare_More,
        eBinaryCmdHandlers_Compare_MoreOrEqual,
        eBinaryCmdHandlers_Compare_NotEqual,
        eBinaryCmdHandlers_Compare_Last
} eBinaryCmdHandlers_Compare_Enum;
/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/
bool BIN_CMD_CheckBufferSize (sBinaryProtocol_HandlerArgs_t *args, uint16_t required_size);
/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/
bool BIN_CMD_CheckBufferSize (sBinaryProtocol_HandlerArgs_t *args, uint16_t required_size) {
    if (args->cmd_data_length != required_size) {
        snprintf(args->text_response_buffer, args->text_response_buffer_size, "Incorrect amount of bytes! [%u != %u]\r", args->cmd_data_length, required_size);
        return false;
    }
    return true;
}

bool BIN_CMD_CompareValues (sBinaryProtocol_HandlerArgs_t *args, uint16_t input_number, eBinaryCmdHandlers_Compare_Enum operation, uint16_t comparison_number) {
    switch (operation) {
        case eBinaryCmdHandlers_Compare_Less: {
            if (input_number < comparison_number) {
                return true;
            } else {
                if (RS485_APP_IsUSBDebugEnabled() == true) {
                    snprintf(args->text_response_buffer, args->text_response_buffer_size, "Wrong value! [%u < %u]\r", input_number, comparison_number);
                }
                return false;
            }
        }
        case eBinaryCmdHandlers_Compare_LessOrEqual: {
            if (input_number <= comparison_number) {
                return true;
            } else {
                if (RS485_APP_IsUSBDebugEnabled() == true) {
                    snprintf(args->text_response_buffer, args->text_response_buffer_size, "Wrong value! [%u <= %u]\r", input_number, comparison_number);
                }
                return false;
            }
        }
        case eBinaryCmdHandlers_Compare_Equal: {
            if (input_number == comparison_number) {
                return true;
            } else {
                if (RS485_APP_IsUSBDebugEnabled() == true) {
                    snprintf(args->text_response_buffer, args->text_response_buffer_size, "Wrong value! [%u == %u]\r", input_number, comparison_number);
                }
                return false;
            }
        }
        case eBinaryCmdHandlers_Compare_More: {
            if (input_number > comparison_number) {
                return true;
            } else {
                if (RS485_APP_IsUSBDebugEnabled() == true) {
                    snprintf(args->text_response_buffer, args->text_response_buffer_size, "Wrong value! [%u > %u]\r", input_number, comparison_number);
                }
                return false;
            }
        }
        case eBinaryCmdHandlers_Compare_MoreOrEqual: {
            if (input_number >= comparison_number) {
                return true;
            } else {
                if (RS485_APP_IsUSBDebugEnabled() == true) {
                    snprintf(args->text_response_buffer, args->text_response_buffer_size, "Wrong value! [%u >= %u]\r", input_number, comparison_number);
                }
                return false;
            }
        }
        case eBinaryCmdHandlers_Compare_NotEqual: {
            if (input_number != comparison_number) {
                return true;
            } else {
                if (RS485_APP_IsUSBDebugEnabled() == true) {
                    snprintf(args->text_response_buffer, args->text_response_buffer_size, "Wrong value! [%u != %u]\r", input_number, comparison_number);
                }
                return false;
            }
        }
        default: {
            break;
        }
    }
    return false;
}
/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool BIN_CMD_System_ERROR (sBinaryProtocol_HandlerArgs_t *args) {
    if (RS485_APP_IsUSBDebugEnabled() == true) {
        snprintf(args->text_response_buffer, args->text_response_buffer_size, "ERROR!\r");
    }
    return true;
}

bool BIN_CMD_System_OK (sBinaryProtocol_HandlerArgs_t *args) {
    if (RS485_APP_IsUSBDebugEnabled() == true) {
        snprintf(args->text_response_buffer, args->text_response_buffer_size, "OK!\r");
    }
    return true;
}

bool BIN_CMD_System_ASCIICmd (sBinaryProtocol_HandlerArgs_t *args) {
    char *temp_buffer = Heap_API_Calloc(args->cmd_data_length + 1, sizeof(char));
    if (temp_buffer == NULL) {
        return false;
    }
    memcpy(temp_buffer, args->cmd_data, args->cmd_data_length);
    sStringProtocol_LauncherArgs_t cmd_launcher_args = {
        .cmd_lut = g_static_cli_app_cmd_lut,
        .cmd_lut_size = eCliApp_Cmd_Last,
        .response_buffer_size = args->text_response_buffer_size,
        .response_buffer = args->text_response_buffer,
        .cmd_raw = temp_buffer,
        .cmd_length = args->cmd_data_length,
    };
    bool return_value = true;
    if (String_Protocol_Launcher(&cmd_launcher_args) == false) {
        return_value = false;
    }
    args->cmd_id = eBinaryProtocol_Cmd_System_OK;
    Heap_API_Free(temp_buffer, args->cmd_data_length + 1);
    return return_value;
}

bool BIN_CMD_System_SetTime (sBinaryProtocol_HandlerArgs_t *args) {
    if (BIN_CMD_CheckBufferSize(args, 8) == false) {
        return false;
    }
    timestamp_t timestamp = 0;
    for (uint8_t byte_index = 0; byte_index < 8; byte_index++) {
        timestamp <<= 8;
        timestamp |= (timestamp_t) args->cmd_data[byte_index];
    }
    bool result = true;
    if (RTC_Driver_SetTimestamp(&timestamp) == true) {
        args->cmd_id = eBinaryProtocol_Cmd_System_OK;
    } else {
        result = false;
    }
    if (RS485_APP_IsUSBDebugEnabled() == true) {
        if (result == true) {
            sDateTime_t date_time = {0};
            RTC_Driver_GetDateTime(&date_time);
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Set the time to %s successfully!\r", formatDateTime(&date_time));
        } else {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Failed to set time!\r");
        }
    }
    return result;
}

bool BIN_CMD_System_SetBaudrate (sBinaryProtocol_HandlerArgs_t *args) {
    if (BIN_CMD_CheckBufferSize(args, 1) == false) {
        return false;
    }
    eBaudrate_Enum_t baudrate = (eBaudrate_Enum_t) args->cmd_data[0];
    if (BIN_CMD_CompareValues(args, baudrate, eBinaryCmdHandlers_Compare_Less, eBaudrate_Last) == false) {
        return false;
    }
    bool result = true;
    if (RS485_APP_SetBaudrate(baudrate) == true) {
        args->cmd_id = eBinaryProtocol_Cmd_System_OK;
    } else {
        result = false;
    }
    if (RS485_APP_IsUSBDebugEnabled() == true) {
        if (result == true) {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Changed baudrate to [%u]!\r", baudrate);

        } else {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Failed to change baudrate to [%u]!\r", baudrate);
        }
    }
    return result;
}

bool BIN_CMD_System_GetTemperature (sBinaryProtocol_HandlerArgs_t *args) {
    if (BIN_CMD_CheckBufferSize(args, 0) == false) {
        return false;
    }
    uint8_t *temp_buffer = Heap_API_Calloc(1, sizeof(uint8_t));
    if (temp_buffer == NULL) {
        return false;
    }
    int8_t temperature = 0;
    bool result = true;
    if (ADC_Driver_GetTemperature(&temperature) == true) {
        temp_buffer[0] = (uint8_t) temperature;
        args->binary_response_buffer = temp_buffer;
        args->binary_response_buffer_size = 1;
        args->cmd_id = eBinaryProtocol_Cmd_System_GetTemperature;
    } else {
        result = false;
        Heap_API_Free(temp_buffer, 1);
    }

    if (RS485_APP_IsUSBDebugEnabled() == true) {
        if (result == true) {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Got controller temperature [%d]!\r", temperature);
        } else {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Failed to get controller temperature [%d]!\r", temperature);
        }
    }
    return result;
}

bool BIN_CMD_System_GetSupplyVoltage (sBinaryProtocol_HandlerArgs_t *args) {
    if (BIN_CMD_CheckBufferSize(args, 0) == false) {
        return false;
    }
    uint8_t *temp_buffer = Heap_API_Calloc(2, sizeof(uint8_t));
    if (temp_buffer == NULL) {
        return false;
    }
    bool result = true;
    uint16_t voltage = 0;
    if (ADC_Driver_GetSupplyVoltage(&voltage) == true) {
        temp_buffer[0] = (uint8_t) voltage;
        temp_buffer[1] = (uint8_t) voltage >> 8;
        args->binary_response_buffer = temp_buffer;
        args->binary_response_buffer_size = 2;
        args->cmd_id = eBinaryProtocol_Cmd_System_GetSupplyVoltage;
    } else {
        result = false;
        Heap_API_Free(temp_buffer, 2);
        return false;
    }
    if (RS485_APP_EnableUSBDebug() == true) {
        if (result == true) {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Got controller supply voltage [%u]!\r", voltage);
        } else {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Failed to get controller supply voltage [%u]!\r", voltage);
        }
    }
    return true;
}

bool BIN_CMD_System_EnableRS485USBDebug (sBinaryProtocol_HandlerArgs_t *args) {
    if (BIN_CMD_CheckBufferSize(args, 0) == false) {
        return false;
    }
    if (RS485_APP_EnableUSBDebug() == true) {
        snprintf(args->text_response_buffer, args->text_response_buffer_size, "Enabled RS485 USB debug messages!\r");
        args->cmd_id = eBinaryProtocol_Cmd_System_OK;
    } else {
        snprintf(args->text_response_buffer, args->text_response_buffer_size, "Failed enable RS485 USB debug messages!\r");
        return false;
    }
    return true;
}

bool BIN_CMD_Motor_Enable (sBinaryProtocol_HandlerArgs_t *args) {
    if (BIN_CMD_CheckBufferSize(args, 1) == false) {
        return false;
    }
    uint8_t motor = args->cmd_data[0];
    if (BIN_CMD_CompareValues(args, motor, eBinaryCmdHandlers_Compare_LessOrEqual, 4) == false) {
        return false;
    }
    bool result = true;
    switch (motor) {
        case 0: {
            if (Motor_API_Enable(eMotorApi_Motor_Primary) == false) {
                result = false;
            }
            break;
        }
        case 1: {
            if (Motor_API_Enable(eMotorApi_Motor_Secondary) == false) {
                result = false;
            }
            break;
        }
        case 2: {
            if (Servo_API_Enable(eServoApi_Primary) == false) {
                result = false;
            }
            break;
        }
        case 3: {
            if (Servo_API_Enable(eServoApi_Secondary) == false) {
                result = false;
            }
            break;
        }
        case 4: {
            if (Servo_API_Enable(eServoApi_Pwm) == false) {
                result = false;
            }
            break;
        }
        default: {
            result = false;
            break;
        }
    }

    if (result == true) {
        args->cmd_id = eBinaryProtocol_Cmd_System_OK;
    }
    if (RS485_APP_EnableUSBDebug() == true) {
        if (result == true) {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Enabled motor [%u]!\r", motor);
        } else {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Failed to enable motor [%u]!\r", motor);
            return false;
        }
    }
    return result;
}

bool BIN_CMD_Motor_Disable (sBinaryProtocol_HandlerArgs_t *args) {
    if (BIN_CMD_CheckBufferSize(args, 1) == false) {
        return false;
    }
    uint8_t motor = args->cmd_data[0];
    if (BIN_CMD_CompareValues(args, motor, eBinaryCmdHandlers_Compare_LessOrEqual, 4) == false) {
        return false;
    }
    bool result = true;
    switch (motor) {
        case 0: {
            if (Motor_API_Disable(eMotorApi_Motor_Primary) == false) {

                result = false;
            }
            break;
        }
        case 1: {
            if (Motor_API_Disable(eMotorApi_Motor_Secondary) == false) {
                result = false;
            }
            break;
        }
        case 2: {
            if (Servo_API_Disable(eServoApi_Primary) == false) {
                result = false;
            }
            break;
        }
        case 3: {
            if (Servo_API_Disable(eServoApi_Secondary) == false) {
                result = false;
            }
            break;
        }
        case 4: {
            if (Servo_API_Disable(eServoApi_Pwm) == false) {
                result = false;
            }
            break;
        }
        default: {
            result = false;
            break;
        }
    }
    if (result == true) {
        args->cmd_id = eBinaryProtocol_Cmd_System_OK;
    }
    if (RS485_APP_IsUSBDebugEnabled() == true) {
        if (result == true) {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Disabled motor [%u]!\r", motor);
        } else {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Failed to disable motor [%u]!\r", motor);
            return false;
        }
    }
    return true;
}

bool BIN_CMD_Motor_SetSpeedAndDirection (sBinaryProtocol_HandlerArgs_t *args) {
    if (BIN_CMD_CheckBufferSize(args, 3) == false) {
        return false;
    }
    eMotorApi_Motor_Enum_t motor = (eMotorApi_Motor_Enum_t) args->cmd_data[0];
    if (BIN_CMD_CompareValues(args, motor, eBinaryCmdHandlers_Compare_LessOrEqual, 1) == false) {
        return false;
    }
    eMotorApi_Direction_Enum_t direction = (eMotorApi_Direction_Enum_t) args->cmd_data[2];
    if (BIN_CMD_CompareValues(args, direction, eBinaryCmdHandlers_Compare_Less, eMotorApi_Direction_Last) == false) {
        return false;
    }
    uint8_t speed = args->cmd_data[1];
    bool result = true;
    if (Motor_API_SetSpeed(motor, speed) == false) {
        result = false;
    }
    if (Motor_API_SetDirection(motor, direction) == false) {
        result = false;
    }
    if (result == true) {
        args->cmd_id = eBinaryProtocol_Cmd_System_OK;
    }
    if (RS485_APP_IsUSBDebugEnabled() == true) {
        if (result == true) {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Set motor speed [%u] and direction [%u]!\r", speed, direction);
        } else {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Failed to set motor speed [%u] and direction [%u]!\r", speed, direction);
        }
    }
    return result;
}

bool BIN_CMD_Motor_MoveServo (sBinaryProtocol_HandlerArgs_t *args) {
    if (BIN_CMD_CheckBufferSize(args, 2) == false) {
        return false;
    }
    eServoApi_Enum_t servo = (eServoApi_Enum_t) args->cmd_data[0];
    if (BIN_CMD_CompareValues(args, servo, eBinaryCmdHandlers_Compare_LessOrEqual, 2) == false) {
        return false;
    }
    uint8_t position = args->cmd_data[1];
    bool result = true;
    if (Servo_API_SetPosition(servo, position) == true) {
        args->cmd_id = eBinaryProtocol_Cmd_System_OK;
    } else {
        result = false;
    }
    if (RS485_APP_IsUSBDebugEnabled() == true) {
        if (result == true) {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Set servo [%u] to position [%u]!\r", servo, position);
        } else {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Failed to set servo [%u] to position [%u]!\r", servo, position);
        }
    }
    return result;
}

bool BIN_CMD_RGBW_Enable (sBinaryProtocol_HandlerArgs_t *args) {
    if (BIN_CMD_CheckBufferSize(args, 1) == false) {
        return false;
    }
    eLedApi_Port_Enum_t port = (eLedApi_Port_Enum_t) args->cmd_data[0];
    if (BIN_CMD_CompareValues(args, port, eBinaryCmdHandlers_Compare_LessOrEqual, 1) == false) {
        return false;
    }
    bool result = true;
    if (LED_API_Enable(port) == true) {
        args->cmd_id = eBinaryProtocol_Cmd_System_OK;
    } else {
        result = false;
    }
    if (RS485_APP_IsUSBDebugEnabled() == true) {
        if (result == true) {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Enabled RGBW port [%u]!\r", port);
        } else {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Failed to enable RGBW port [%u]!\r", port);
        }
    }
    return result;
}

bool BIN_CMD_RGBW_Disable (sBinaryProtocol_HandlerArgs_t *args) {
    if (BIN_CMD_CheckBufferSize(args, 1) == false) {
        return false;
    }
    eLedApi_Port_Enum_t port = (eLedApi_Port_Enum_t) args->cmd_data[0];
    if (BIN_CMD_CompareValues(args, port, eBinaryCmdHandlers_Compare_LessOrEqual, 1) == false) {
        return false;
    }
    bool result = true;
    if (LED_API_Disable(port) == true) {
        args->cmd_id = eBinaryProtocol_Cmd_System_OK;
    } else {
        result = false;
    }
    if (RS485_APP_IsUSBDebugEnabled() == true) {
        if (result == true) {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Disabled RGBW port [%u]!\r", port);
        } else {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Failed to disable RGBW port [%u]!\r", port);
        }
    }
    return result;
}

bool BIN_CMD_RGBW_Update (sBinaryProtocol_HandlerArgs_t *args) {
    if (BIN_CMD_CheckBufferSize(args, 0) == false) {
        return false;
    }
    bool result = true;
    if (LED_API_Update() == true) {
        args->cmd_id = eBinaryProtocol_Cmd_System_OK;
    } else {
        result = false;
    }
    if (RS485_APP_IsUSBDebugEnabled() == true) {
        if (result == true) {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Updated RGBW ports!\r");
        } else {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Failed to update RGBW ports!\r");
        }
    }
    return result;
}

bool BIN_CMD_RGBW_SetColor (sBinaryProtocol_HandlerArgs_t *args) {
    if (BIN_CMD_CheckBufferSize(args, 5) == false) {
        return false;
    }
    eLedApi_Port_Enum_t port = (eLedApi_Port_Enum_t) args->cmd_data[0];
    if (BIN_CMD_CompareValues(args, port, eBinaryCmdHandlers_Compare_LessOrEqual, 1) == false) {
        return false;
    }
    sRgbwColor_t rgbw_color = {
        .r = args->cmd_data[1],
        .g = args->cmd_data[2],
        .b = args->cmd_data[3],
        .w = args->cmd_data[4],
    };
    bool result = true;
    if (LED_API_SetRgbwColor(port, rgbw_color) == true) {
        args->cmd_id = eBinaryProtocol_Cmd_System_OK;
    } else {
        result = false;
    }
    if (RS485_APP_IsUSBDebugEnabled() == true) {
        if (result == true) {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Set RGBW port [%u] to color (%u,%u,%u,%u)!\r", port, rgbw_color.r, rgbw_color.g, rgbw_color.b, rgbw_color.w);
        } else {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Failed to set RGBW port [%u] to color (%u,%u,%u,%u)!\r", port, rgbw_color.r, rgbw_color.g, rgbw_color.b, rgbw_color.w);
        }
    }
    return result;
}

bool BIN_CMD_ARGB_Enable (sBinaryProtocol_HandlerArgs_t *args) {
    if (BIN_CMD_CheckBufferSize(args, 3) == false) {
        return false;
    }
    eWs2815Api_Port_Enum_t port = (eWs2815Api_Port_Enum_t) args->cmd_data[0];
    if (BIN_CMD_CompareValues(args, port, eBinaryCmdHandlers_Compare_LessOrEqual, 2) == false) {
        return false;
    }
    uint16_t pixel_count = (uint16_t) args->cmd_data[2] | args->cmd_data[1] << 8;
    if (BIN_CMD_CompareValues(args, pixel_count, eBinaryCmdHandlers_Compare_LessOrEqual, 2000) == false) {
        return false;
    }
    bool result = true;
    if (WS2815_API_Enable(port, pixel_count) == true) {
        args->cmd_id = eBinaryProtocol_Cmd_System_OK;
    } else {
        result = false;
    }
    if (RS485_APP_IsUSBDebugEnabled() == true) {
        if (result == true) {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Enabled ARGB strip [%u] with pixel count [%u]!\r", port, pixel_count);
        } else {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Failed to enable ARGB strip [%u] with pixel count [%u]!\r", port, pixel_count);
        }
    }
    return result;
}

bool BIN_CMD_ARGB_Disable (sBinaryProtocol_HandlerArgs_t *args) {
    if (BIN_CMD_CheckBufferSize(args, 1) == false) {
        return false;
    }
    eWs2815Api_Port_Enum_t port = (eWs2815Api_Port_Enum_t) args->cmd_data[0];
    if (BIN_CMD_CompareValues(args, port, eBinaryCmdHandlers_Compare_LessOrEqual, 2) == false) {
        return false;
    }
    bool result = true;
    if (WS2815_API_Disable(port) == true) {
        args->cmd_id = eBinaryProtocol_Cmd_System_OK;
    } else {
        result = false;
    }
    if (RS485_APP_IsUSBDebugEnabled() == true) {
        if (result == true) {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Disabled ARGB strip [%u]!\r", port);
        } else {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Failed to disable ARGB strip [%u]!\r", port);
        }
    }
    return result;
}

bool BIN_CMD_ARGB_Update (sBinaryProtocol_HandlerArgs_t *args) {
    if (BIN_CMD_CheckBufferSize(args, 0) == false) {
        return false;
    }
    bool result = true;
    if (WS2815_API_UpdatePixels() == true) {
        args->cmd_id = eBinaryProtocol_Cmd_System_OK;
    } else {
        result = false;
    }
    if (RS485_APP_IsUSBDebugEnabled() == true) {
        if (result == true) {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Updated ARGB LEDs!\r");
        } else {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Failed to update ARGB LEDs!\r");
        }
    }
    return result;
}

bool BIN_CMD_ARGB_SetSegmentColor (sBinaryProtocol_HandlerArgs_t *args) {
    if (BIN_CMD_CheckBufferSize(args, 8) == false) {
        return false;
    }
    uint8_t port = args->cmd_data[0];
    if (BIN_CMD_CompareValues(args, port, eBinaryCmdHandlers_Compare_LessOrEqual, 2) == false) {
        return false;
    }
    uint16_t start_index = (uint16_t) args->cmd_data[2] | args->cmd_data[1] << 8;
    if (BIN_CMD_CompareValues(args, start_index, eBinaryCmdHandlers_Compare_LessOrEqual, 2000) == false) {
        return false;
    }
    uint16_t end_index = (uint16_t) args->cmd_data[4] | args->cmd_data[3] << 8;
    if (BIN_CMD_CompareValues(args, end_index, eBinaryCmdHandlers_Compare_LessOrEqual, 2000) == false) {
        return false;
    }
//    if (BIN_CMD_CompareValues(args, start_index, eBinaryCmdHandlers_Compare_LessOrEqual, end_index) == false) {
//        return false;
//    }
    bool result = true;
    sRgbColor_t rgb_color = {
        .r = args->cmd_data[5],
        .g = args->cmd_data[6],
        .b = args->cmd_data[7],
    };
    if (WS2815_API_FillSegmentRgb(port, start_index, end_index, rgb_color) == true) {
        args->cmd_id = eBinaryProtocol_Cmd_System_OK;
    } else {
        result = false;
    }
    if (RS485_APP_IsUSBDebugEnabled() == true) {
        if (result == true) {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Set ARGB strip [%u] segment [%u-%u] to color (%u,%u,%u)!\r", port, start_index, end_index, rgb_color.r, rgb_color.g, rgb_color.b);
        } else {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Failed to set ARGB strip [%u] segment [%u-%u] to color (%u,%u,%u)!\r", port, start_index, end_index, rgb_color.r, rgb_color.g, rgb_color.b);
        }
    }
    return result;
}

bool BIN_CMD_ARGB_DimSegmentColor (sBinaryProtocol_HandlerArgs_t *args) {
    if (BIN_CMD_CheckBufferSize(args, 8) == false) {
        return false;
    }
    uint8_t port = args->cmd_data[0];
    if (BIN_CMD_CompareValues(args, port, eBinaryCmdHandlers_Compare_LessOrEqual, 2) == false) {
        return false;
    }
    uint16_t start_index = (uint16_t) args->cmd_data[2] | args->cmd_data[1] << 8;
    if (BIN_CMD_CompareValues(args, start_index, eBinaryCmdHandlers_Compare_LessOrEqual, 2000) == false) {
        return false;
    }
    uint16_t end_index = (uint16_t) args->cmd_data[4] | args->cmd_data[3] << 8;
    if (BIN_CMD_CompareValues(args, end_index, eBinaryCmdHandlers_Compare_LessOrEqual, 2000) == false) {
        return false;
    }
//    if (BIN_CMD_CompareValues(args, start_index, eBinaryCmdHandlers_Compare_LessOrEqual, end_index) == false) {
//        return false;
//    }
    bool result = true;
    uint8_t dim_amount = args->cmd_data[5];
    if (WS2815_API_DimSegmentRgb(port, start_index, end_index, dim_amount) == true) {
        args->cmd_id = eBinaryProtocol_Cmd_System_OK;
    } else {
        result = false;
    }
    if (RS485_APP_IsUSBDebugEnabled() == true) {
        if (result == true) {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Dimmed ARGB strip [%u] segment [%u-%u] by amount [%u]!\r", port, start_index, end_index, dim_amount);
        } else {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Failed to dim ARGB strip [%u] segment [%u-%u] by amount [%u]!\r", port, start_index, end_index, dim_amount);
        }
    }
    return result;
}

bool BIN_CMD_ARGB_GradientSegmentColor (sBinaryProtocol_HandlerArgs_t *args) {
    if (BIN_CMD_CheckBufferSize(args, 12) == false) {
        return false;
    }
    uint8_t port = args->cmd_data[0];
    if (BIN_CMD_CompareValues(args, port, eBinaryCmdHandlers_Compare_LessOrEqual, 2) == false) {
        return false;
    }
    eWs2815Api_PowerLut_Enum_t power_lut_index = (eWs2815Api_PowerLut_Enum_t) args->cmd_data[1];
    if (BIN_CMD_CompareValues(args, power_lut_index, eBinaryCmdHandlers_Compare_LessOrEqual, 4) == false) {
        return false;
    }
    uint16_t start_index = (uint16_t) args->cmd_data[3] | args->cmd_data[2] << 8;
    if (BIN_CMD_CompareValues(args, start_index, eBinaryCmdHandlers_Compare_LessOrEqual, 2000) == false) {
        return false;
    }
    uint16_t end_index = (uint16_t) args->cmd_data[5] | args->cmd_data[4] << 8;
    if (BIN_CMD_CompareValues(args, end_index, eBinaryCmdHandlers_Compare_LessOrEqual, 2000) == false) {
        return false;
    }
//    if (BIN_CMD_CompareValues(args, start_index, eBinaryCmdHandlers_Compare_LessOrEqual, end_index) == false) {
//        return false;
//    }
    bool result = true;
    sRgbColor_t start_rgb_color = {
        .r = args->cmd_data[6],
        .g = args->cmd_data[7],
        .b = args->cmd_data[8],
    };
    sRgbColor_t end_rgb_color = {
        .r = args->cmd_data[9],
        .g = args->cmd_data[10],
        .b = args->cmd_data[11],
    };
    if (WS2815_API_GradientSegmentRgb(port, power_lut_index, start_index, end_index, start_rgb_color, end_rgb_color) == true) {
        args->cmd_id = eBinaryProtocol_Cmd_System_OK;
    } else {
        result = false;
    }
    if (RS485_APP_IsUSBDebugEnabled() == true) {
        if (result == true) {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Set gradient to ARGB strip [%u] with lut [%u] segment [%u-%u] from color (%u,%u,%u) to color (%u,%u,%u)!\r", port, power_lut_index, start_index, end_index, start_rgb_color.r, start_rgb_color.g, start_rgb_color.b, end_rgb_color.r, end_rgb_color.g, end_rgb_color.b);
        } else {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Failed to set gradient to ARGB strip [%u] with lut [%u] segment [%u-%u] from color (%u,%u,%u) to color (%u,%u,%u)!\r", port, power_lut_index, start_index, end_index, start_rgb_color.r, start_rgb_color.g, start_rgb_color.b, end_rgb_color.r, end_rgb_color.g, end_rgb_color.b);
        }
    }
    return result;
}

bool BIN_CMD_IO_Enable (sBinaryProtocol_HandlerArgs_t *args) {
    if (BIN_CMD_CheckBufferSize(args, 2) == false) {
        return false;
    }
    eIoApi_Port_Enum_t port = (eIoApi_Port_Enum_t) args->cmd_data[0];
    if (BIN_CMD_CompareValues(args, port, eBinaryCmdHandlers_Compare_LessOrEqual, 5) == false) {
        return false;
    }
    eIoApi_Function_Enum_t function = (eIoApi_Function_Enum_t) args->cmd_data[1];
    if (BIN_CMD_CompareValues(args, function, eBinaryCmdHandlers_Compare_LessOrEqual, 1) == false) {
        return false;
    }
    bool result = true;
    if (IO_API_Enable(port, function) == true) {
        args->cmd_id = eBinaryProtocol_Cmd_System_OK;
    } else {
        result = false;
    }
    if (RS485_APP_IsUSBDebugEnabled() == true) {
        if (result == true) {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Enabled IO port [%u] with function [%u]!\r", port, function);
        } else {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Failed to enable IO port [%u] with function [%u]!\r", port, function);
        }
    }
    return result;
}

bool BIN_CMD_IO_Disable (sBinaryProtocol_HandlerArgs_t *args) {
    if (BIN_CMD_CheckBufferSize(args, 1) == false) {
        return false;
    }
    eIoApi_Port_Enum_t port = (eIoApi_Port_Enum_t) args->cmd_data[0];
    if (BIN_CMD_CompareValues(args, port, eBinaryCmdHandlers_Compare_LessOrEqual, 5) == false) {
        return false;
    }
    bool result = true;
    if (IO_API_Disable(port) == true) {
        args->cmd_id = eBinaryProtocol_Cmd_System_OK;
    } else {
        result = false;
    }
    if (RS485_APP_IsUSBDebugEnabled() == true) {
        if (result == true) {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Disabled IO port [%u]!\r", port);
        } else {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Failed to disable IO port [%u]!\r", port);
        }
    }
    return result;
}

bool BIN_CMD_IO_SetPort (sBinaryProtocol_HandlerArgs_t *args) {
    if (BIN_CMD_CheckBufferSize(args, 2) == false) {
        return false;
    }
    eIoApi_Port_Enum_t port = (eIoApi_Port_Enum_t) args->cmd_data[0];
    if (BIN_CMD_CompareValues(args, port, eBinaryCmdHandlers_Compare_LessOrEqual, 5) == false) {
        return false;
    }
    bool state = (bool) args->cmd_data[1];
    if (BIN_CMD_CompareValues(args, state, eBinaryCmdHandlers_Compare_LessOrEqual, 1) == false) {
        return false;
    }
    bool result = true;
    if (IO_API_SetPort(port, state) == true) {
        args->cmd_id = eBinaryProtocol_Cmd_System_OK;
    } else {
        result = false;
    }
    if (RS485_APP_IsUSBDebugEnabled() == true) {
        if (result == true) {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Set IO port [%u] to [%u]!\r", port, state);
        } else {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Failed to set IO port [%u] to [%u]!\r", port, state);
        }
    }
    return result;
}

bool BIN_CMD_IO_ReadPort (sBinaryProtocol_HandlerArgs_t *args) {
    if (BIN_CMD_CheckBufferSize(args, 2) == false) {
        return false;
    }
    eIoApi_Port_Enum_t port = (eIoApi_Port_Enum_t) args->cmd_data[0];
    if (BIN_CMD_CompareValues(args, port, eBinaryCmdHandlers_Compare_LessOrEqual, 5) == false) {
        return false;
    }
    uint8_t *temp_buffer = Heap_API_Calloc(2, sizeof(uint8_t));
    if (temp_buffer == NULL) {
        return false;
    }
    bool result = true;
    bool state = 0;
    if (IO_API_ReadPort(port, &state) == true) {
        temp_buffer[0] = (uint8_t) port;
        temp_buffer[1] = (uint8_t) state;
        args->binary_response_buffer = temp_buffer;
        args->binary_response_buffer_size = 2;
        args->cmd_id = eBinaryProtocol_Cmd_IO_ReadState;
    } else {
        result = false;
        Heap_API_Free(temp_buffer, 2);
    }
    if (RS485_APP_IsUSBDebugEnabled() == true) {
        if (result == true) {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Got IO port [%u] state [%u]!\r", port, state);
        } else {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Failed to get IO port [%u] state [%u]!\r", port, state);
        }
    }
    return result;
}

bool BIN_CMD_HX711_Enable (sBinaryProtocol_HandlerArgs_t *args) {
    if (BIN_CMD_CheckBufferSize(args, 1) == false) {
        return false;
    }
    eHx711Api_Enum_t port = (eHx711Api_Enum_t) args->cmd_data[0];
    if (BIN_CMD_CompareValues(args, port, eBinaryCmdHandlers_Compare_LessOrEqual, 1) == false) {
        return false;
    }
    bool result = true;
    if (HX711_API_Enable(port) == true) {
        args->cmd_id = eBinaryProtocol_Cmd_System_OK;
    } else {
        result = false;
    }
    if (RS485_APP_IsUSBDebugEnabled() == true) {
        if (result == true) {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Enabled weight sensor [%u]!\r", port);
        } else {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Failed to enable weight sensor [%u]!\r", port);
        }
    }
    return result;
}

bool BIN_CMD_HX711_Disable (sBinaryProtocol_HandlerArgs_t *args) {
    if (BIN_CMD_CheckBufferSize(args, 1) == false) {
        return false;
    }
    eHx711Api_Enum_t port = (eHx711Api_Enum_t) args->cmd_data[0];
    if (BIN_CMD_CompareValues(args, port, eBinaryCmdHandlers_Compare_LessOrEqual, 1) == false) {
        return false;
    }
    bool result = true;
    if (HX711_API_Disable(port) == true) {
        args->cmd_id = eBinaryProtocol_Cmd_System_OK;
    } else {
        result = false;
    }
    if (RS485_APP_IsUSBDebugEnabled() == true) {
        if (result == true) {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Disabled weight sensor [%u]!\r", port);
        } else {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Failed to disable weight sensor [%u]!\r", port);
        }
    }
    return result;
}

bool BIN_CMD_HX711_Calibrate (sBinaryProtocol_HandlerArgs_t *args) {
    if (BIN_CMD_CheckBufferSize(args, 1) == false) {
        return false;
    }
    eHx711Api_Enum_t port = (eHx711Api_Enum_t) args->cmd_data[0];
    if (BIN_CMD_CompareValues(args, port, eBinaryCmdHandlers_Compare_LessOrEqual, 1) == false) {
        return false;
    }
    bool result = true;
    if (HX711_API_Callibrate(port) == true) {
        args->cmd_id = eBinaryProtocol_Cmd_System_OK;
    } else {
        result = false;
    }
    if (RS485_APP_IsUSBDebugEnabled() == true) {
        if (result == true) {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Calibrated weight sensor [%u]!\r", port);
        } else {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Failed to calibrate weight sensor [%u]!\r", port);
        }
    }
    return result;
}

bool BIN_CMD_HX711_GetWeight (sBinaryProtocol_HandlerArgs_t *args) {
    if (BIN_CMD_CheckBufferSize(args, 1) == false) {
        return false;
    }
    eHx711Api_Enum_t port = (eHx711Api_Enum_t) args->cmd_data[0];
    if (BIN_CMD_CompareValues(args, port, eBinaryCmdHandlers_Compare_LessOrEqual, 1) == false) {
        return false;
    }
    uint8_t *temp_buffer = Heap_API_Calloc(5, sizeof(uint8_t));
    if (temp_buffer == NULL) {
        return false;
    }
    bool result = true;
    uint32_t measured_weight = 0;
    if (HX711_API_GetWeight(port, &measured_weight) == true) {
        temp_buffer[0] = (uint8_t) port;
        temp_buffer[1] = measured_weight;
        temp_buffer[2] = measured_weight >> 8;
        temp_buffer[3] = measured_weight >> 16;
        temp_buffer[4] = measured_weight >> 24;
        args->binary_response_buffer = temp_buffer;
        args->binary_response_buffer_size = 5;
        args->cmd_id = eBinaryProtocol_Cmd_Weight_GetWeight;
    } else {
        result = false;
        Heap_API_Free(temp_buffer, 5);
    }
    if (RS485_APP_IsUSBDebugEnabled() == true) {
        if (result == true) {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Got weight sensor [%u] value [%lu]!\r", port, measured_weight);
        } else {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Failed to get weight sensor [%u] value [%lu]!\r", port, measured_weight);
        }
    }
    return true;
}

bool BIN_CMD_Distance_Enable (sBinaryProtocol_HandlerArgs_t *args) {
    if (BIN_CMD_CheckBufferSize(args, 1) == false) {
        return false;
    }
    eVL53L3Api_Enum_t port = (eVL53L3Api_Enum_t) args->cmd_data[0];
    if (BIN_CMD_CompareValues(args, port, eBinaryCmdHandlers_Compare_LessOrEqual, 1) == false) {
        return false;
    }
    bool result = true;
    if (VL53L3_API_Enable(port) == true) {
        args->cmd_id = eBinaryProtocol_Cmd_System_OK;
    } else {
        result = false;
    }
    if (RS485_APP_IsUSBDebugEnabled() == true) {
        if (result == true) {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Enabled distance sensor [%u]!\r", port);
        } else {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Failed to enable distance sensor [%u]!\r", port);
        }
    }
    return result;
}

bool BIN_CMD_Distance_Disable (sBinaryProtocol_HandlerArgs_t *args) {
    if (BIN_CMD_CheckBufferSize(args, 1) == false) {
        return false;
    }
    eVL53L3Api_Enum_t port = (eVL53L3Api_Enum_t) args->cmd_data[0];
    if (BIN_CMD_CompareValues(args, port, eBinaryCmdHandlers_Compare_LessOrEqual, 1) == false) {
        return false;
    }
    bool result = true;
    if (VL53L3_API_Disable(port) == true) {
        args->cmd_id = eBinaryProtocol_Cmd_System_OK;
    } else {
        result = false;
    }
    if (RS485_APP_IsUSBDebugEnabled() == true) {
        if (result == true) {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Disabled distance sensor [%u]!\r", port);
        } else {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Failed to disable distance sensor [%u]!\r", port);
        }
    }
    return result;
}

bool BIN_CMD_Distance_Calibrate (sBinaryProtocol_HandlerArgs_t *args) {
    if (BIN_CMD_CheckBufferSize(args, 1) == false) {
        return false;
    }
    eVL53L3Api_Enum_t port = (eVL53L3Api_Enum_t) args->cmd_data[0];
    if (BIN_CMD_CompareValues(args, port, eBinaryCmdHandlers_Compare_LessOrEqual, 1) == false) {
        return false;
    }
    bool result = true;
    if (VL53L3_API_Calibrate(port) == true) {
        args->cmd_id = eBinaryProtocol_Cmd_System_OK;
    } else {
        result = false;
    }
    if (RS485_APP_IsUSBDebugEnabled() == true) {
        if (result == true) {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Calibrated distance sensor [%u]!\r", port);
        } else {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Failed to calibrate distance sensor [%u]!\r", port);
        }
    }
    return result;
}

bool BIN_CMD_Distance_Read (sBinaryProtocol_HandlerArgs_t *args) {
    if (BIN_CMD_CheckBufferSize(args, 1) == false) {
        return false;
    }
    eVL53L3Api_Enum_t port = (eVL53L3Api_Enum_t) args->cmd_data[0];
    if (BIN_CMD_CompareValues(args, port, eBinaryCmdHandlers_Compare_LessOrEqual, 1) == false) {
        return false;
    }
    uint8_t *temp_buffer = Heap_API_Calloc(3, sizeof(uint8_t));
    if (temp_buffer == NULL) {
        return false;
    }
    bool result = true;
    uint16_t measured_distance = 0;
    if (VL53L3_API_GetDistance(port, &measured_distance) == true) {
        temp_buffer[0] = (uint8_t) port;
        temp_buffer[1] = measured_distance;
        temp_buffer[2] = measured_distance >> 8;
        args->binary_response_buffer = temp_buffer;
        args->binary_response_buffer_size = 3;
        args->cmd_id = eBinaryProtocol_Cmd_Distance_GetDistance;
    } else {
        result = false;
        Heap_API_Free(temp_buffer, 3);
    }
    if (RS485_APP_IsUSBDebugEnabled() == true) {
        if (result == true) {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Got distance sensor [%u] value [%u]!\r", port, measured_distance);
        } else {
            snprintf(args->text_response_buffer, args->text_response_buffer_size, "Failed to get distance sensor [%u] value [%u]!\r", port, measured_distance);
        }
    }
    return result;
}
