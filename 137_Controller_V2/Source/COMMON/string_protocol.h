#ifndef __CMD_API__H__
#define __CMD_API__H__
/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "stdint.h"
#include "stdbool.h"
/**********************************************************************************************************************
 * Exported definitions and macros
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported types
 *********************************************************************************************************************/
typedef struct sCmdHandlerArgs_t {
    char *cmd_args;
    char *response_buffer;
    uint16_t response_buffer_size;
} sStringProtocol_HandlerArgs_t;

typedef struct sCliAppCmd_t {
    const char *name;
    const char *parameters;
    const char *description;
    const bool (*fun_ptr) (sStringProtocol_HandlerArgs_t);
} sCmdApiCmd_t;

typedef struct sCmdLauncherArgs_t {
    const sCmdApiCmd_t *cmd_lut;
    uint8_t cmd_lut_size;
    char *cmd_raw;
    uint32_t cmd_length;
    char *response_buffer;
    uint16_t response_buffer_size;
} sStringProtocol_LauncherArgs_t;
/**********************************************************************************************************************
 * Exported variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of exported functions
 *********************************************************************************************************************/
bool String_Protocol_Launcher (sStringProtocol_LauncherArgs_t *launcher_args);
#endif /* __CMD_API__H__ */
