/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include <binary_protocol.h>
#include "stdint.h"
#include "stdbool.h"
#include "stdio.h"
#include "binary_cmd_handlers.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
#define CMD_ID_BYTE_INDEX 0
/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
// @formatter:off
static const sBinaryProtocol_Cmd_t g_static_binary_protocol_lut[eBinaryProtocol_Cmd_Last] = {
    [eBinaryProtocol_Cmd_System_ERROR]                = {.data_size = 0,    .name = "Error",                               .parameters = "",                                                             .description = "", .fun_ptr = &BIN_CMD_System_ERROR},
    [eBinaryProtocol_Cmd_System_OK]                   = {.data_size = 0,    .name = "OK",                                  .parameters = "",                                                             .description = "", .fun_ptr = &BIN_CMD_System_OK},
    [eBinaryProtocol_Cmd_System_ASCIICmd]             = {.data_size = 255,  .name = "Text cmd",                            .parameters = "",                                                             .description = "", .fun_ptr = &BIN_CMD_System_ASCIICmd},
    [eBinaryProtocol_Cmd_System_SetTime]              = {.data_size = 8,    .name = "Set time",                            .parameters = "[0-7] timestamp (ms)",                                         .description = "", .fun_ptr = &BIN_CMD_System_SetTime},
    [eBinaryProtocol_Cmd_System_SetBaudrate]          = {.data_size = 1,    .name = "",                                    .parameters = "[0-7] timestamp (ms)",                                         .description = "", .fun_ptr = &BIN_CMD_System_SetBaudrate},
    [eBinaryProtocol_Cmd_System_GetTemperature]       = {.data_size = 0,    .name = "",                                    .parameters = "[0-7] timestamp (ms)",                                         .description = "", .fun_ptr = &BIN_CMD_System_GetTemperature},
    [eBinaryProtocol_Cmd_System_GetSupplyVoltage]     = {.data_size = 0,    .name = "",                                    .parameters = "[0-7] timestamp (ms)",                                         .description = "", .fun_ptr = &BIN_CMD_System_GetSupplyVoltage},
    [eBinaryProtocol_Cmd_System_EnableRS485UsbDebug]  = {.data_size = 0,    .name = "",                                    .parameters = "[0-7] timestamp (ms)",                                         .description = "", .fun_ptr = &BIN_CMD_System_EnableRS485USBDebug},
    [eBinaryProtocol_Cmd_Motors_Enable]               = {.data_size = 1,    .name = "Enable motor",                        .parameters = "[0] port: <0-4>",                                              .description = "", .fun_ptr = &BIN_CMD_Motor_Enable},
    [eBinaryProtocol_Cmd_Motors_Disable]              = {.data_size = 1,    .name = "Disable motor",                       .parameters = "[0] port: <0-4>",                                              .description = "", .fun_ptr = &BIN_CMD_Motor_Disable},
    [eBinaryProtocol_Cmd_Motors_SetSpeedAndDirection] = {.data_size = 3,    .name = "Set motor speed and direction",       .parameters = "[0] port: <0-1>, [1] speed: <0-255>, [2] direction: <0-1>",    .description = "", .fun_ptr = &BIN_CMD_Motor_SetSpeedAndDirection},
    [eBinaryProtocol_Cmd_Motors_MoveServo]            = {.data_size = 2,    .name = "Move servo",                          .parameters = "",                                                             .description = "", .fun_ptr = &BIN_CMD_Motor_MoveServo},
    [eBinaryProtocol_Cmd_RGBW_Enable]                 = {.data_size = 1,    .name = "",                                    .parameters = "",                                                             .description = "", .fun_ptr = &BIN_CMD_RGBW_Enable},
    [eBinaryProtocol_Cmd_RGBW_Disable]                = {.data_size = 1,    .name = "",                                    .parameters = "",                                                             .description = "", .fun_ptr = &BIN_CMD_RGBW_Disable},
    [eBinaryProtocol_Cmd_RGBW_Update]                 = {.data_size = 0,    .name = "",                                    .parameters = "",                                                             .description = "", .fun_ptr = &BIN_CMD_RGBW_Update},
    [eBinaryProtocol_Cmd_RGBW_SetColor]               = {.data_size = 5,    .name = "",                                    .parameters = "",                                                             .description = "", .fun_ptr = &BIN_CMD_RGBW_SetColor},
    [eBinaryProtocol_Cmd_ARGB_Enable]                 = {.data_size = 3,    .name = "Enable ARGB LED strip",               .parameters = "",                                                             .description = "", .fun_ptr = &BIN_CMD_ARGB_Enable},
    [eBinaryProtocol_Cmd_ARGB_Disable]                = {.data_size = 1,    .name = "Disable ARGB LED strip",              .parameters = "",                                                             .description = "", .fun_ptr = &BIN_CMD_ARGB_Disable},
    [eBinaryProtocol_Cmd_ARGB_Update]                 = {.data_size = 0,    .name = "",                                    .parameters = "",                                                             .description = "", .fun_ptr = &BIN_CMD_ARGB_Update},
    [eBinaryProtocol_Cmd_ARGB_SetSegment]             = {.data_size = 8,    .name = "",                                    .parameters = "",                                                             .description = "", .fun_ptr = &BIN_CMD_ARGB_SetSegmentColor},
    [eBinaryProtocol_Cmd_ARGB_DimSegment]             = {.data_size = 8,    .name = "",                                    .parameters = "",                                                             .description = "", .fun_ptr = &BIN_CMD_ARGB_DimSegmentColor},
    [eBinaryProtocol_Cmd_ARGB_GradientSegment]        = {.data_size = 12,   .name = "",                                    .parameters = "",                                                             .description = "", .fun_ptr = &BIN_CMD_ARGB_GradientSegmentColor},
    [eBinaryProtocol_Cmd_IO_Enable]                   = {.data_size = 2,    .name = "",                                    .parameters = "",                                                             .description = "", .fun_ptr = &BIN_CMD_IO_Enable},
    [eBinaryProtocol_Cmd_IO_Disable]                  = {.data_size = 1,    .name = "",                                    .parameters = "",                                                             .description = "", .fun_ptr = &BIN_CMD_IO_Disable},
    [eBinaryProtocol_Cmd_IO_ReadState]                = {.data_size = 1,    .name = "",                                    .parameters = "",                                                             .description = "", .fun_ptr = &BIN_CMD_IO_ReadPort},
    [eBinaryProtocol_Cmd_IO_SetState]                 = {.data_size = 2,    .name = "",                                    .parameters = "",                                                             .description = "", .fun_ptr = &BIN_CMD_IO_SetPort},
    [eBinaryProtocol_Cmd_Weight_Enable]               = {.data_size = 1,    .name = "",                                    .parameters = "",                                                             .description = "", .fun_ptr = &BIN_CMD_HX711_Enable},
    [eBinaryProtocol_Cmd_Weight_Disable]              = {.data_size = 1,    .name = "",                                    .parameters = "",                                                             .description = "", .fun_ptr = &BIN_CMD_HX711_Disable},
    [eBinaryProtocol_Cmd_Weight_Calibrate]            = {.data_size = 1,    .name = "",                                    .parameters = "",                                                             .description = "", .fun_ptr = &BIN_CMD_HX711_Calibrate},
    [eBinaryProtocol_Cmd_Weight_GetWeight]            = {.data_size = 1,    .name = "",                                    .parameters = "",                                                             .description = "", .fun_ptr = &BIN_CMD_HX711_GetWeight},
    [eBinaryProtocol_Cmd_Distance_Enable]             = {.data_size = 1,    .name = "Enable distance sensor",              .parameters = "[0] port: <0-2>",                                              .description = "", .fun_ptr = &BIN_CMD_Distance_Enable},
    [eBinaryProtocol_Cmd_Distance_Disable]            = {.data_size = 1,    .name = "Disable distance sensor",             .parameters = "[0] port: <0-2>",                                              .description = "", .fun_ptr = &BIN_CMD_Distance_Disable},
    [eBinaryProtocol_Cmd_Distance_Calibrate]          = {.data_size = 1,    .name = "Calibrate distance sensor",           .parameters = "[0] port: <0-2>",                                              .description = "", .fun_ptr = &BIN_CMD_Distance_Calibrate},
    [eBinaryProtocol_Cmd_Distance_GetDistance]        = {.data_size = 1,    .name = "Read distance in mm",                 .parameters = "[0] port: <0-2>",                                              .description = "", .fun_ptr = &BIN_CMD_Distance_Read},
};
// @formatter:on
/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool Binary_Protocol_Launcher (sBinaryProtocol_LauncherArgs_t *launcher_args) {
    if (launcher_args->cmd_buffer == NULL || launcher_args->cmd_buffer_size == 0) {
        snprintf(launcher_args->text_response_buffer, launcher_args->text_response_buffer_size, "No CMD data!\r");
        return false;
    }

    eBinaryProtocol_Cmd_Enum_t cmd_id = launcher_args->cmd_buffer[CMD_ID_BYTE_INDEX];
    if (cmd_id >= eBinaryProtocol_Cmd_Last) {
        snprintf(launcher_args->text_response_buffer, launcher_args->text_response_buffer_size, "No CMD ID!\r");
        return false;
    }

    if (g_static_binary_protocol_lut[cmd_id].fun_ptr == NULL) {
        snprintf(launcher_args->text_response_buffer, launcher_args->text_response_buffer_size, "CMD not yet supported!\r");
        return false;
    }

    if (g_static_binary_protocol_lut[cmd_id].data_size != 255) {
        if (g_static_binary_protocol_lut[cmd_id].data_size != launcher_args->cmd_buffer_size - 1) {
            snprintf(launcher_args->text_response_buffer, launcher_args->text_response_buffer_size, "Wrong CMD data amount [%u != %u]!\r", launcher_args->cmd_buffer_size - 1, g_static_binary_protocol_lut[cmd_id].data_size);
            return false;
        }
    }

    sBinaryProtocol_HandlerArgs_t handler_args = {
        .text_response_buffer = launcher_args->text_response_buffer,
        .text_response_buffer_size = launcher_args->text_response_buffer_size,
        .cmd_data = &launcher_args->cmd_buffer[1],
        .cmd_data_length = launcher_args->cmd_buffer_size - 1,
        .binary_response_buffer = NULL,
        .binary_response_buffer_size = 0,
        .cmd_id = cmd_id,
    };

    if (g_static_binary_protocol_lut[cmd_id].fun_ptr(&handler_args) == false) {
        return false;
    }
    launcher_args->cmd_id = handler_args.cmd_id;
    launcher_args->binary_response_buffer = handler_args.binary_response_buffer;
    launcher_args->binary_response_buffer_size = handler_args.binary_response_buffer_size;
    return true;
}

bool Binary_Protocol_GetCommandSize (eBinaryProtocol_Cmd_Enum_t cmd_id, uint8_t *cmd_data_size) {
    if (cmd_id >= eBinaryProtocol_Cmd_Last) {
        return false;
    }
    *cmd_data_size = g_static_binary_protocol_lut[cmd_id].data_size;
    return true;
}
