/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported definitions and macros
 *********************************************************************************************************************/
#define UART_API_TASK_STACK_SIZE (256 * 4)
#define UART_API_TASK_PRIORITY osPriorityNormal

#define LIGHT_APP_TASK_STACK_SIZE (128 * 4)
#define LIGHT_APP_TASK_PRIORITY osPriorityNormal

#define SYSTEM_APP_TASK_STACK_SIZE (256 * 4)
#define SYSTEM_APP_TASK_PRIORITY osPriorityNormal

#define CLI_APP_TASK_STACK_SIZE (256 * 4)
#define CLI_APP_TASK_PRIORITY osPriorityNormal

#define MOTION_APP_TASK_STACK_SIZE (128 * 4)
#define MOTION_APP_TASK_PRIORITY osPriorityNormal

#define RS485_APP_TASK_STACK_SIZE (128 * 4)
#define RS485_APP_TASK_PRIORITY osPriorityNormal

#define USB_API_TASK_STACK_SIZE (128 * 4)
#define USB_API_TASK_PRIORITY osPriorityNormal
/**********************************************************************************************************************
 * Exported types
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of exported functions
 *********************************************************************************************************************/
