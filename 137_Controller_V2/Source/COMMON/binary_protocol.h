#ifndef __RS485_PROTOCOL__H__
#define __RS485_PROTOCOL__H__
/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "stdbool.h"
#include "stdint.h"
/**********************************************************************************************************************
 * Exported definitions and macros
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported types
 *********************************************************************************************************************/
// @formatter:off
typedef enum eBinaryProtocol_Cmd_Enum_t {
    eBinaryProtocol_Cmd_First = 0,
    eBinaryProtocol_Cmd_System_First = eBinaryProtocol_Cmd_First,
    eBinaryProtocol_Cmd_System_ERROR = eBinaryProtocol_Cmd_System_First,
    eBinaryProtocol_Cmd_System_OK,
    eBinaryProtocol_Cmd_System_ASCIICmd,
    eBinaryProtocol_Cmd_System_SetTime,
    eBinaryProtocol_Cmd_System_SetBaudrate,
    eBinaryProtocol_Cmd_System_GetTemperature,
    eBinaryProtocol_Cmd_System_GetSupplyVoltage,
    eBinaryProtocol_Cmd_System_EnableRS485UsbDebug,
    // Add system commands here
    eBinaryProtocol_Cmd_System_last = eBinaryProtocol_Cmd_System_EnableRS485UsbDebug,

    eBinaryProtocol_Cmd_Motors_First = 16,
    eBinaryProtocol_Cmd_Motors_Enable = eBinaryProtocol_Cmd_Motors_First,
    eBinaryProtocol_Cmd_Motors_Disable,
    eBinaryProtocol_Cmd_Motors_SetSpeedAndDirection,
    eBinaryProtocol_Cmd_Motors_MoveServo,
    // Add motor commands here
    eBinaryProtocol_Cmd_Motors_Last = eBinaryProtocol_Cmd_Motors_MoveServo,

    eBinaryProtocol_Cmd_RGBW_First = 32,
    eBinaryProtocol_Cmd_RGBW_Enable = eBinaryProtocol_Cmd_RGBW_First,
    eBinaryProtocol_Cmd_RGBW_Disable,
    eBinaryProtocol_Cmd_RGBW_Update,
    eBinaryProtocol_Cmd_RGBW_SetColor,
    // Add RGBW commands here
    eBinaryProtocol_Cmd_RGBW_Last = eBinaryProtocol_Cmd_RGBW_SetColor,

    eBinaryProtocol_Cmd_ARGB_First = 48,
    eBinaryProtocol_Cmd_ARGB_Enable = eBinaryProtocol_Cmd_ARGB_First,
    eBinaryProtocol_Cmd_ARGB_Disable,
    eBinaryProtocol_Cmd_ARGB_Update,
    eBinaryProtocol_Cmd_ARGB_SetSegment,
    eBinaryProtocol_Cmd_ARGB_DimSegment,
    eBinaryProtocol_Cmd_ARGB_GradientSegment,
    // Add ARGB commands here
    eBinaryProtocol_Cmd_ARGB_Last = eBinaryProtocol_Cmd_ARGB_GradientSegment,

    eBinaryProtocol_Cmd_IO_First = 64,
    eBinaryProtocol_Cmd_IO_Enable = eBinaryProtocol_Cmd_IO_First,
    eBinaryProtocol_Cmd_IO_Disable,
    eBinaryProtocol_Cmd_IO_ReadState,
    eBinaryProtocol_Cmd_IO_SetState,
    // Add IO commands here
    eBinaryProtocol_Cmd_IO_Last = eBinaryProtocol_Cmd_IO_SetState,

    eBinaryProtocol_Cmd_Weight_First = 80,
    eBinaryProtocol_Cmd_Weight_Enable = eBinaryProtocol_Cmd_Weight_First,
    eBinaryProtocol_Cmd_Weight_Disable,
    eBinaryProtocol_Cmd_Weight_Calibrate,
    eBinaryProtocol_Cmd_Weight_GetWeight,
    // Add weight sensor commands here
    eBinaryProtocol_Cmd_Weight_Last = eBinaryProtocol_Cmd_Weight_GetWeight,

    eBinaryProtocol_Cmd_Distance_First = 96,
    eBinaryProtocol_Cmd_Distance_Enable = eBinaryProtocol_Cmd_Distance_First,
    eBinaryProtocol_Cmd_Distance_Disable,
    eBinaryProtocol_Cmd_Distance_Calibrate,
    eBinaryProtocol_Cmd_Distance_GetDistance,
    // Add distance sensor commands here
    eBinaryProtocol_Cmd_Distance_Last = eBinaryProtocol_Cmd_Distance_GetDistance,

    eBinaryProtocol_Cmd_Analog_First = 112,
    eBinaryProtocol_Cmd_Analog_Enable = eBinaryProtocol_Cmd_Analog_First,
    eBinaryProtocol_Cmd_Analog_Disable,
    eBinaryProtocol_Cmd_Analog_Read,
    // Add analog input commands here
    eBinaryProtocol_Cmd_Analog_Last = eBinaryProtocol_Cmd_Analog_Read,

    eBinaryProtocol_Cmd_Last,
    eBinaryProtocol_Cmd_Unknown
} eBinaryProtocol_Cmd_Enum_t;
// @formatter:on
typedef struct sRs485ProtocolHandlerArgs_t {
    uint8_t *cmd_data;
    uint16_t cmd_data_length;
    char *text_response_buffer;
    uint16_t text_response_buffer_size;
    uint8_t *binary_response_buffer;
    uint16_t binary_response_buffer_size;
    eBinaryProtocol_Cmd_Enum_t cmd_id;
} sBinaryProtocol_HandlerArgs_t;

typedef struct sBinaryProtocol_Cmd_t {
    const uint8_t data_size;
    const char *name;
    const char *parameters;
    const char *description;
    const bool (*fun_ptr) (sBinaryProtocol_HandlerArgs_t*);
} sBinaryProtocol_Cmd_t;

typedef struct sRs485Protocol_LauncherArgs_t {
    uint8_t *cmd_buffer;
    uint16_t cmd_buffer_size;
    char *text_response_buffer;
    uint16_t text_response_buffer_size;
    uint8_t *binary_response_buffer;
    uint16_t binary_response_buffer_size;
    eBinaryProtocol_Cmd_Enum_t cmd_id;
} sBinaryProtocol_LauncherArgs_t;
/**********************************************************************************************************************
 * Exported variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of exported functions
 *********************************************************************************************************************/
bool Binary_Protocol_Launcher (sBinaryProtocol_LauncherArgs_t *launcher_args);
bool Binary_Protocol_GetCommandSize (eBinaryProtocol_Cmd_Enum_t cmd_id, uint8_t *cmd_data_size);
#endif /* __RS485_PROTOCOL__H__ */
