/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "led_api.h"
#include "debug_api.h"
#include "timer_driver.h"
#include "stdbool.h"
#include "color_utils.h"
#include "gpio_driver.h"
#include "cmsis_os2.h"
#include "number_utils.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
DEBUG_MODULE(LED_API)

#define MAX_DELAY_BETWEEN_BLINKS 100
#define MIN_DELAY_BETWEEN_BLINKS 50
#define MAX_BLINKS 5
#define MIN_BLINKS 2
#define MAX_ON_TIME 100
#define MIN_ON_TIME 20
/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/
typedef struct sLedApi_Static_t {
    eTimerDriver_Timer_Enum_t timer;
    eTimerDriver_OC_Enum_t r_channel;
    eTimerDriver_OC_Enum_t g_channel;
    eTimerDriver_OC_Enum_t b_channel;
    bool is_h_bridge;
    eGpioDriver_Pin_Enum_t high_pin;
    eGpioDriver_Pin_Enum_t r_pin;
    eGpioDriver_Pin_Enum_t g_pin;
    eGpioDriver_Pin_Enum_t b_pin;
    const sGpioDriverStatic_t *high_pin_struct;
    const sGpioDriverStatic_t *r_pin_struct;
    const sGpioDriverStatic_t *g_pin_struct;
    const sGpioDriverStatic_t *b_pin_struct;
    bool pwm_inverted;
} sLedApi_Static_t;
/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
static const sGpioDriverStatic_t g_h_bridge_high_pin_struct = {
    .port = GPIOA,
    .pin = LL_GPIO_PIN_10,
    .mode = LL_GPIO_MODE_OUTPUT,
    .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH,
    .output = LL_GPIO_OUTPUT_PUSHPULL,
    .pull = LL_GPIO_PULL_UP,
    .clock = LL_IOP_GRP1_PERIPH_GPIOA,
    .alternate = LL_GPIO_AF_0,
};

static const sGpioDriverStatic_t g_h_bridge_r_pin_struct = {
    .port = GPIOA,
    .pin = LL_GPIO_PIN_8,
    .mode = LL_GPIO_MODE_OUTPUT,
    .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH,
    .output = LL_GPIO_OUTPUT_PUSHPULL,
    .pull = LL_GPIO_PULL_NO,
    .clock = LL_IOP_GRP1_PERIPH_GPIOA,
    .alternate = LL_GPIO_AF_0,
};

static const sGpioDriverStatic_t g_h_bridge_g_pin_struct = {
    .port = GPIOA,
    .pin = LL_GPIO_PIN_11,
    .mode = LL_GPIO_MODE_OUTPUT,
    .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH,
    .output = LL_GPIO_OUTPUT_PUSHPULL,
    .pull = LL_GPIO_PULL_NO,
    .clock = LL_IOP_GRP1_PERIPH_GPIOA,
    .alternate = LL_GPIO_AF_0,
};

static const sGpioDriverStatic_t g_h_bridge_b_pin_struct = {
    .port = GPIOA,
    .pin = LL_GPIO_PIN_9,
    .mode = LL_GPIO_MODE_OUTPUT,
    .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH,
    .output = LL_GPIO_OUTPUT_PUSHPULL,
    .pull = LL_GPIO_PULL_NO,
    .clock = LL_IOP_GRP1_PERIPH_GPIOA,
    .alternate = LL_GPIO_AF_0,
};

static const sLedApi_Static_t g_static_led_api_port_lut[eLedApi_Port_Last] = {
    [eLedApi_Port_RGB] = {
        .timer = eTimerDriver_Timer_3,
        .r_channel = eTimerDriver_OC_1,
        .g_channel = eTimerDriver_OC_2,
        .b_channel = eTimerDriver_OC_4,
        .is_h_bridge = false,
        .pwm_inverted = false,
    },
    [eLedApi_Port_H_Bridge] = {
        .r_pin = eGpioDriver_Pin_Tim1_Ch1,
        .g_pin = eGpioDriver_Pin_Tim1_Ch4,
        .b_pin = eGpioDriver_Pin_Tim1_Ch2,
        .is_h_bridge = true,
        .high_pin = eGpioDriver_Pin_Tim1_Ch3,
        .high_pin_struct = &g_h_bridge_high_pin_struct,
        .r_pin_struct = &g_h_bridge_r_pin_struct,
        .g_pin_struct = &g_h_bridge_g_pin_struct,
        .b_pin_struct = &g_h_bridge_b_pin_struct,
        .pwm_inverted = true,
    }
};
/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool LED_API_Init (eLedApi_Port_Enum_t port) {
    if (port >= eLedApi_Port_Last) {
        error("Wrong port provided\n\r");
        return false;
    }

    if (g_static_led_api_port_lut[port].is_h_bridge == true) {
        GPIO_Driver_InitPin(g_static_led_api_port_lut[port].r_pin, g_static_led_api_port_lut[port].r_pin_struct);
        GPIO_Driver_InitPin(g_static_led_api_port_lut[port].g_pin, g_static_led_api_port_lut[port].g_pin_struct);
        GPIO_Driver_InitPin(g_static_led_api_port_lut[port].b_pin, g_static_led_api_port_lut[port].b_pin_struct);
        GPIO_Driver_InitPin(g_static_led_api_port_lut[port].high_pin, g_static_led_api_port_lut[port].high_pin_struct);
//        GPIO_Driver_SetPin(g_static_led_api_port_lut[port].high_pin);
//        GPIO_Driver_SetPin(g_static_led_api_port_lut[port].r_pin);
//        GPIO_Driver_SetPin(g_static_led_api_port_lut[port].g_pin);
//        GPIO_Driver_SetPin(g_static_led_api_port_lut[port].b_pin);
    } else {
        Timer_Driver_Init(g_static_led_api_port_lut[port].timer);
        Timer_Driver_Start(g_static_led_api_port_lut[port].timer);
    }
    return true;
}

bool LED_API_SetHsvColor (eLedApi_Port_Enum_t port, sHsvColor_t color) {
    if (port >= eLedApi_Port_Last) {
        error("Wrong port provided\n\r");
        return false;
    }
    sRgbColor_t rgb_color = HsvToRgb(color);
    LED_API_SetRgbColor(port, rgb_color);
    return true;
}

bool LED_API_SetRgbColor (eLedApi_Port_Enum_t port, sRgbColor_t color) {
    if (port >= eLedApi_Port_Last) {
        error("Wrong port provided\n\r");
        return false;
    }
    sRgbColor_t rgb_color = color;
    if (g_static_led_api_port_lut[port].pwm_inverted == true) {
        rgb_color = InvertRgb(color);
    }
    if (g_static_led_api_port_lut[port].is_h_bridge == true) {
        bool r_on = (rgb_color.r > 127);
        bool g_on = (rgb_color.g > 127);
        bool b_on = (rgb_color.b > 127);

        bool r_set = false;
        bool g_set = false;
        bool b_set = false;
        bool h_set = false;

        if (r_on && g_on && b_on) {
            r_set = false;
            g_set = false;
            b_set = false;
            h_set = false;
        }
        if (!r_on && !g_on && !b_on) {
            r_set = false;
            g_set = false;
            b_set = false;
            h_set = false;
        }
        if (r_on && !g_on && !b_on) {
            r_set = false;
            g_set = false;
            b_set = true;
            h_set = false;
        }
        if (!r_on && !g_on && b_on) {
            r_set = true;
            g_set = false;
            b_set = false;
            h_set = false;
        }
        if (!r_on && g_on && !b_on) {
            r_set = false;
            g_set = false;
            b_set = false;
            h_set = true;
        }
        if (!r_on && g_on && b_on) {
            r_set = true;
            g_set = false;
            b_set = false;
            h_set = true;
        }
        if (r_on && !g_on && b_on) {
            r_set = false;
            g_set = false;
            b_set = false;
            h_set = false;
        }
        if (r_on && g_on && !b_on) {
            r_set = false;
            g_set = false;
            b_set = true;
            h_set = true;
        }
        if (r_set) {
            GPIO_Driver_SetPin(g_static_led_api_port_lut[port].r_pin);
        } else {
            GPIO_Driver_ResetPin(g_static_led_api_port_lut[port].r_pin);
        }
        if (g_set) {
            GPIO_Driver_SetPin(g_static_led_api_port_lut[port].g_pin);
        } else {
            GPIO_Driver_ResetPin(g_static_led_api_port_lut[port].g_pin);
        }
        if (b_set) {
            GPIO_Driver_SetPin(g_static_led_api_port_lut[port].b_pin);
        } else {
            GPIO_Driver_ResetPin(g_static_led_api_port_lut[port].b_pin);
        }
        if (h_set) {
            GPIO_Driver_SetPin(g_static_led_api_port_lut[port].high_pin);
        } else {
            GPIO_Driver_ResetPin(g_static_led_api_port_lut[port].high_pin);
        }
    } else {
        eTimerDriver_Timer_Enum_t timer = g_static_led_api_port_lut[port].timer;
        Timer_Driver_SetDutyCycle(timer, g_static_led_api_port_lut[port].r_channel, rgb_color.r);
        Timer_Driver_SetDutyCycle(timer, g_static_led_api_port_lut[port].g_channel, rgb_color.g);
        Timer_Driver_SetDutyCycle(timer, g_static_led_api_port_lut[port].b_channel, rgb_color.b);
    }
    return true;
}

bool LED_API_AdvanceRgbColor (sRgbColor_t *color) {
    if (color == NULL) {
        return false;
    }
    sHsvColor_t hsv_color = RgbToHsv(*color);
    hsv_color.h++;
    *color = HsvToRgb(hsv_color);
    return true;
}

bool LED_API_AdvanceHsvColor (sHsvColor_t *color) {
    if (color == NULL) {
        return false;
    }
    color->h++;
    return true;
}

bool LED_API_Strobe (eLedApi_Port_Enum_t port, sLightApp_StrobeData_t *strobe_data) {
    if (port >= eLedApi_Port_Last) {
        error("Wrong port provided\n\r");
        return false;
    }
    if (strobe_data == NULL) {
        error("NULL pointer provided\n\r");
        return false;
    }
    uint32_t time = osKernelGetTickCount();
    if (time >= strobe_data->trigger_time) {
        switch (strobe_data->state) {
            case eLightApp_StrobeState_Generate: {
                strobe_data->hsv_color = RandomHsv();
                strobe_data->delay_between_blinks = RandomRange(MIN_DELAY_BETWEEN_BLINKS, MAX_DELAY_BETWEEN_BLINKS);
                strobe_data->blinks_counter = RandomRange(MIN_BLINKS, MAX_BLINKS);
                strobe_data->on_time = RandomRange(MIN_ON_TIME, MAX_ON_TIME);
            }
            case eLightApp_StrobeState_TurnOn: {
                LED_API_SetHsvColor(port, strobe_data->hsv_color);
                strobe_data->state = eLightApp_StrobeState_TurnOff;
                strobe_data->trigger_time += strobe_data->on_time;
                break;
            }
            case eLightApp_StrobeState_TurnOff: {
                LED_API_SetRgbColor(port, rgb_black);
                if (strobe_data->blinks_counter > 0) {
                    strobe_data->blinks_counter--;
                    strobe_data->state = eLightApp_StrobeState_TurnOn;
                    strobe_data->trigger_time += strobe_data->delay_between_blinks;
                } else {
                    strobe_data->state = eLightApp_StrobeState_Off;
                }
                break;
            }
            default: {
                break;
            }
        }
    }
    return true;
}

bool LED_API_SimpleColor (eLedApi_Port_Enum_t port, sLightApp_StaticColorData_t *simple_color_data) {
    if (port >= eLedApi_Port_Last) {
        error("Wrong port provided\n\r");
        return false;
    }
    if (simple_color_data == NULL) {
        error("NULL pointer provided\n\r");
        return false;
    }
    simple_color_data->hsv_color = RandomHsv();
    LED_API_SetHsvColor(port, simple_color_data->hsv_color);
    return true;
}
