/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "debug_api.h"
#include "uart_driver.h"
#include "cli_app.h"
#include "cmsis_os.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
#define DEBUG_MESSAGE_SIZE 150
/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
const osMutexAttr_t debug_api_mutex_attr = {
    "debug_api_mutex",
    osMutexRecursive | osMutexPrioInherit,
    NULL,
    0U
};
/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/
osMutexId_t debug_api_mutex_id = NULL;
/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool Debug_API_Init (void) {
    debug_api_mutex_id = osMutexNew(&debug_api_mutex_attr);
    if (debug_api_mutex_id == NULL) {
        return false;
    }
    if (UART_API_Init(CLI_UART, CLI_BAUDRATE, CLI_APP_COMMAND_BUFFER_SIZE, CLI_DELIMITER) == false) {
        return false;
    }
    return true;
}

void Debug_API_Log (const char *debug_module_name, const char *format, ...) {
    osMutexAcquire(debug_api_mutex_id, osWaitForever);
    va_list args;
    va_start(args, format);

    char buffer[DEBUG_MESSAGE_SIZE] = {0};
    uint16_t offset = 0;
    offset += snprintf(buffer, DEBUG_MESSAGE_SIZE, "[%s]\tDEBUG:\t", debug_module_name);
    if (format != NULL) {
        offset += vsnprintf(&buffer[offset], DEBUG_MESSAGE_SIZE - offset, format, args);
    }
    UART_API_Lock(CLI_UART, osWaitForever);
    UART_API_SendString(CLI_UART, buffer);
    UART_API_Unlock(CLI_UART);

    va_end(args);
    osMutexRelease(debug_api_mutex_id);
}

void Debug_API_Error (const char *debug_module_name, const char *file, const uint16_t line, const char *format, ...) {
    osMutexAcquire(debug_api_mutex_id, osWaitForever);
    va_list args;
    va_start(args, format);

    char buffer[DEBUG_MESSAGE_SIZE] = {0};
    uint16_t offset = 0;
    offset += snprintf(buffer, DEBUG_MESSAGE_SIZE, "[%s]\tERROR: (%s (%d))\t", debug_module_name, file, line);
    if (format != NULL) {
        offset += vsnprintf(&buffer[offset], DEBUG_MESSAGE_SIZE - offset, format, args);
    }
    UART_API_Lock(CLI_UART, osWaitForever);
    UART_API_SendString(CLI_UART, buffer);
    UART_API_Unlock(CLI_UART);

    va_end(args);
    osMutexRelease(debug_api_mutex_id);
}

void Debug_API_Print (const char *string) {
    UART_API_SendString(CLI_UART, string);
}

void Debug_API_PrintF (const char *format, ...) {
    va_list args;
    va_start(args, format);

    char buffer[DEBUG_MESSAGE_SIZE] = {0};
    if (format != NULL) {
        vsnprintf(buffer, DEBUG_MESSAGE_SIZE, format, args);
    }
    UART_API_SendString(CLI_UART, buffer);

    va_end(args);
}

bool Debug_API_Lock (uint32_t timeout) {
    if (osMutexAcquire(debug_api_mutex_id, timeout) != osOK) {
        return false;
    }
    if (UART_API_Lock(CLI_UART, timeout) == false) {
        osMutexRelease(debug_api_mutex_id);
        return false;
    }
    return true;
}

bool Debug_API_Unlock (void) {
    if (UART_API_Unlock(CLI_UART) == false) {
        osMutexRelease(debug_api_mutex_id);
        return false;
    }
    if (osMutexRelease(debug_api_mutex_id) != osOK) {
        return false;
    }
    return true;
}
