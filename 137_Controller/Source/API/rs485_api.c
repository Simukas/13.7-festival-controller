/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "rs485_api.h"
#include "uart_driver.h"
#include "cmsis_os.h"
#include "stack_info.h"
#include "debug_api.h"
#include "address_api.h"
#include "number_utils.h"
#include "light_app.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
DEBUG_MODULE(RS485_API)

#define MAX_DELAY_BETWEEN_STROBES 10000
#define MIN_DELAY_BETWEEN_STROBES 1000

#define RS485_API_UART eUartDriverUsart3
/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/
// @formatter:off
typedef enum eRs485Api_State_Enum_t {
    eRs485Api_State_First = 0,
    eRs485Api_State_Setup = eRs485Api_State_First,
    eRs485Api_State_ReceiveAddress,
    eRs485Api_State_ReceiveCommand,
    eRs485Api_State_ReceiveData,
    eRs485Api_State_Flush,
    eRs485Api_State_Last,
} eRs485ApiS_tate_Enum_t;

typedef enum eRs485Api_Role_Enum_t {
    eRs485Api_Role_First = 0,
    eRs485Api_Role_Unknown = eRs485Api_Role_First,
    eRs485Api_Role_Master,
    eRs485Api_Role_Slave,
    eRs485Api_Role_Last
} eRs485Api_Role_Enum_t;

typedef struct sRs485Api_Dynamic_t {
    osMutexId_t tx_mutex_id;
    eUartBaudrateEnum_t baudrate;
} sRs485Api_Dynamic_t;

typedef struct sUartApiStatic_t {
    uint8_t msg_queue_count;
} sRs485ApiStatic_t;
// @formatter:on
/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
const static osThreadAttr_t rs485_api_slave_task_attributes = {
    .name = "rs485_api_slave_task",
    .stack_size = RS485_API_TASK_STACK_SIZE,
    .priority = (osPriority_t) RS485_API_TASK_PRIORITY,
};

const static osThreadAttr_t rs485_api_master_task_attributes = {
    .name = "rs485_api_master_task",
    .stack_size = RS485_API_TASK_STACK_SIZE,
    .priority = (osPriority_t) RS485_API_TASK_PRIORITY,
};

const static osMutexAttr_t rs485_api_mutex_attr = {
    "rs485_api_mutex",
    osMutexRecursive | osMutexPrioInherit,
    NULL,
    0U
};
/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/
static osThreadId_t g_rs485_api_task_handle = NULL;

static sRs485Api_Dynamic_t g_dynamic_rs485_api_lut = {0};

static uint8_t g_rs485_api_address = 0;

static eRs485Api_Role_Enum_t g_rs485_api_role = eRs485Api_Role_Unknown;

//static uint8_t last_address = 0;
/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/
static void RS485_API_SlaveTask (void *argument);
static void RS485_API_MasterTask (void *argument);
/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/
static void RS485_API_SlaveTask (void *argument) {
    uint8_t received_byte = 0;
    while (true) {
        while (UART_Driver_GetByte(RS485_API_UART, &received_byte) == true) {
            if (received_byte == g_rs485_api_address) {
                sLightApp_Task_t task = {
                    .port = eLedApi_Port_RGB
                };
                Light_APP_AddTask(&task);
            }
            if (received_byte == (g_rs485_api_address + 1)) {
                sLightApp_Task_t task = {
                    .port = eLedApi_Port_H_Bridge
                };
                Light_APP_AddTask(&task);
            }
        }
        osThreadYield();
    }
}

void RS485_API_MasterTask (void *argument) {
    uint8_t address = 0;
    while (true) {
        //do {
        address = 1 << RandomRange(1, 8);
        address += RandomRange(0, 1);
        //} while (address == last_address);
        //last_address = address;

        //uint16_t delay = RandomRange(MIN_DELAY_BETWEEN_STROBES, MAX_DELAY_BETWEEN_STROBES);
        if (address == ADDRESS_API_MASTER_ADDRESS) {
            sLightApp_Task_t task = {
                .port = eLedApi_Port_RGB
            };
            Light_APP_AddTask(&task);
        } else if (address == (ADDRESS_API_MASTER_ADDRESS + 1)) {
            sLightApp_Task_t task = {
                .port = eLedApi_Port_H_Bridge
            };
            Light_APP_AddTask(&task);
        } else {
            RS485_API_SendByte(address);
        }
        osDelay(70);
    }
}

/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool RS485_API_Init (eUartBaudrateEnum_t baudrate) {
    if (baudrate >= eUartBaudrateLast) {
        error("Wrong baudrate provided\n\r");
        return false;
    }
    if (UART_Driver_Init(RS485_API_UART, baudrate) == false) {
        error("Failed to initialize UART driver\n\r");
        return false;
    }
    g_dynamic_rs485_api_lut.tx_mutex_id = osMutexNew(&rs485_api_mutex_attr);
    if (g_dynamic_rs485_api_lut.tx_mutex_id == NULL) {
        error("Failed to create mutex\n\r");
        return false;
    }
    if (Address_API_GetAddress(&g_rs485_api_address) == false) {
        error("Failed to get address\n\r");
        return false;
    }
    if (g_rs485_api_address == ADDRESS_API_MASTER_ADDRESS) {
        g_rs485_api_task_handle = osThreadNew(RS485_API_MasterTask, NULL, &rs485_api_master_task_attributes);
        if (g_rs485_api_task_handle == NULL) {
            error("Failed to create thread\n\r");
            return false;
        }
        g_rs485_api_role = eRs485Api_Role_Master;
    } else {
        g_rs485_api_task_handle = osThreadNew(RS485_API_SlaveTask, NULL, &rs485_api_slave_task_attributes);
        if (g_rs485_api_task_handle == NULL) {
            error("Failed to create thread\n\r");
            return false;
        }
        g_rs485_api_role = eRs485Api_Role_Slave;
    }
    return true;
}

bool RS485_API_SendBytes (uint8_t *bytes, uint16_t buffer_size) {
    if (bytes == NULL) {
        error("NULL pointer provided\n\r");
        return false;
    }
    UART_Driver_SendBytes(RS485_API_UART, bytes, buffer_size);
    return true;
}

bool RS485_API_SendByte (uint8_t byte) {
    UART_Driver_SendByte(RS485_API_UART, byte);
    return true;
}

bool RS485_API_SendCommand (uint8_t address, uint8_t *bytes, uint16_t buffer_size) {

    return true;
}
