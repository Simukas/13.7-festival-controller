/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "hx711_api.h"
#include "stdint.h"
#include "gpio_driver.h"
#include "cmsis_os.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
#define HX711_API_CLOCK_PIN eGpioDriver_Pin_I2C_SCL
#define HX711_API_DATA_PIN eGpioDriver_Pin_I2C_SDA

#define HX711_API_DATA_BITS 24

#define HX711_API_RAW_WEIGHT_OFFSET 100
#define HX711_API_WEIGHT_DIVISOR 10
/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/
static uint32_t HX711_API_ReadValue (void);
/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/
static uint32_t HX711_API_ReadValue (void) {
    uint32_t return_value = 0;

    GPIO_Driver_SetPin(HX711_API_DATA_PIN);
    osDelay(1);
    GPIO_Driver_ResetPin(HX711_API_CLOCK_PIN);
    osDelay(1);

    for (uint8_t i = 0; i < HX711_API_DATA_BITS; i++) {
        GPIO_Driver_SetPin(HX711_API_CLOCK_PIN);
        osDelay(1);
        return_value = return_value << 1;
        GPIO_Driver_ResetPin(HX711_API_CLOCK_PIN);
        osDelay(1);
        bool pin_state = false;
        GPIO_Driver_ReadOutputPin(HX711_API_DATA_PIN, &pin_state);
        if (pin_state == true) {
            return_value += 1;
        }

    }

    GPIO_Driver_SetPin(HX711_API_CLOCK_PIN);
    osDelay(1);
    return_value = return_value ^ 0x800000;
    GPIO_Driver_SetPin(HX711_API_CLOCK_PIN);
    osDelay(1);

    return return_value;
}
/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool HX711_API_Init (void) {
    // Using I2C port pins
    sGpioDriverStatic_t hx711_clock = {
        .port = GPIOB,
        .pin = LL_GPIO_PIN_10,
        .mode = LL_GPIO_MODE_OUTPUT,
        .speed = LL_GPIO_SPEED_FREQ_LOW,
        .output = LL_GPIO_OUTPUT_PUSHPULL,
        .pull = LL_GPIO_PULL_NO,
        .clock = LL_IOP_GRP1_PERIPH_GPIOB,
        .alternate = LL_GPIO_AF_0,
    };
    sGpioDriverStatic_t hx711_data = {
        .port = GPIOB,
        .pin = LL_GPIO_PIN_11,
        .mode = LL_GPIO_MODE_INPUT,
        .speed = LL_GPIO_SPEED_FREQ_LOW,
        .output = LL_GPIO_OUTPUT_PUSHPULL,
        .pull = LL_GPIO_PULL_NO,
        .clock = LL_IOP_GRP1_PERIPH_GPIOB,
        .alternate = LL_GPIO_AF_0,
    };

    if (GPIO_Driver_InitPin(HX711_API_CLOCK_PIN, &hx711_clock) == false) {
        return false;
    }
    if (GPIO_Driver_InitPin(HX711_API_DATA_PIN, &hx711_data) == false) {
        return false;
    }

    return true;
}

bool HX711_API_GetWeight (uint32_t *weight) {
    if (weight == NULL) {
        return false;
    }
    uint32_t raw_weight = HX711_API_ReadValue();

    *weight = (raw_weight - HX711_API_RAW_WEIGHT_OFFSET) / HX711_API_WEIGHT_DIVISOR;
    return true;
}
