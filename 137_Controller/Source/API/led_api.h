#ifndef __LED_API__H__
#define __LED_API__H__
/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "stdbool.h"
#include "timer_driver.h"
#include "color_utils.h"
/**********************************************************************************************************************
 * Exported definitions and macros
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported types
 *********************************************************************************************************************/
typedef enum eLedApi_Port_Enum_t {
    eLedApi_Port_First = 0,
    eLedApi_Port_RGB = eLedApi_Port_First,
    eLedApi_Port_H_Bridge,
    eLedApi_Port_Last
} eLedApi_Port_Enum_t;

typedef enum eLightApp_StrobeState_Enum_t {
    eLightApp_StrobeState_Off,
    eLightApp_StrobeState_Generate,
    eLightApp_StrobeState_TurnOn,
    eLightApp_StrobeState_TurnOff,
} eLightApp_StrobeState_Enum_t;

typedef struct sLightApp_StrobeData_t {
    eLightApp_StrobeState_Enum_t state;
    sHsvColor_t hsv_color;
    uint16_t delay_between_blinks;
    uint32_t trigger_time;
    uint16_t on_time;
    uint8_t blinks_counter;
} sLightApp_StrobeData_t;

typedef struct sLightApp_StaticColorData_t {
    sHsvColor_t hsv_color;
} sLightApp_StaticColorData_t;
/**********************************************************************************************************************
 * Exported variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of exported functions
 *********************************************************************************************************************/
bool LED_API_Init (eLedApi_Port_Enum_t port);
bool LED_API_SetHsvColor(eLedApi_Port_Enum_t port, sHsvColor_t color);
bool LED_API_SetRgbColor(eLedApi_Port_Enum_t port, sRgbColor_t color);
bool LED_API_AdvanceRgbColor(sRgbColor_t *color);
bool LED_API_AdvanceHsvColor(sHsvColor_t *color);
bool LED_API_Strobe (eLedApi_Port_Enum_t port, sLightApp_StrobeData_t *strobe_data);
bool LED_API_SimpleColor (eLedApi_Port_Enum_t port, sLightApp_StaticColorData_t *simple_color_data);
#endif /* __LED_API__H__ */
