/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "cmd_api.h"
#include "stdio.h"
#include "string_utils.h"
#include "debug_api.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
//DEBUG_MODULE(CMD_API)
#define DEFAULT_CMD_DELIMITER ":"
/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
const char bad_chars[] = "\n\r";
/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool CMD_API_Launcher (sCmdApiLauncherArgs_t *launcher_args) {
    sCmdApiHandlerArgs_t handler_args = {
        .response_buffer = launcher_args->response_buffer,
        .response_buffer_size = launcher_args->response_buffer_size
    };
    uint16_t cmd_id = 0;
    bool cmd_recognized = false;
    char *token = NULL;
    remchar(launcher_args->cmd_raw, bad_chars);

    token = strtok(launcher_args->cmd_raw, DEFAULT_CMD_DELIMITER);
    for (uint8_t cmd = 0; cmd < launcher_args->cmd_lut_size; cmd++) {
        if (strequal(token, launcher_args->cmd_lut[cmd].name) == true) {
            cmd_id = cmd;
            handler_args.cmd_args = launcher_args->cmd_raw + strlen(launcher_args->cmd_lut[cmd].name) + 1;
            cmd_recognized = true;
            break;
        }
    }

    if (cmd_recognized == false) {
        snprintf(launcher_args->response_buffer, launcher_args->response_buffer_size, "CMD recognition failed!\n");
        return false;
    }
    if (launcher_args->cmd_lut[cmd_id].fun_ptr(handler_args) == false) {
        snprintf(launcher_args->response_buffer, launcher_args->response_buffer_size, "Wrong parameters entered!\n");
        return false;
    }
    return true;
}
