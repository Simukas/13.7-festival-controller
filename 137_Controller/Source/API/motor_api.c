/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "motor_api.h"
#include "debug_api.h"
#include "timer_driver.h"
#include "gpio_driver.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
DEBUG_MODULE(MOTOR_API)

#define MOTOR_API_MAX_SPEED_VALUE 255
#define MOTOR_API_MIN_SPEED_VALUE 1
/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/
typedef struct sMotorApi_Static_t {
    eTimerDriver_OC_Enum_t a1_channel;
    eTimerDriver_OC_Enum_t a2_channel;
} sMotorApi_Static_t;

typedef struct sMotorApi_Dynamic_t {
    uint8_t speed;
    eMotorApi_Direction_Enum_t direction;
} sMotorApi_Dynamic_t;
/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
const static sMotorApi_Static_t g_static_motor_api_lut[] = {
    [eMotorApi_Motor_1] = {.a1_channel = eTimerDriver_OC_1, .a2_channel = eTimerDriver_OC_2},
    [eMotorApi_Motor_2] = {.a1_channel = eTimerDriver_OC_3, .a2_channel = eTimerDriver_OC_4},
};
/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/
static sMotorApi_Dynamic_t g_dynamic_motor_api_lut[] = {
    [eMotorApi_Motor_1] = {.direction = eMotorApi_Direction_Left, .speed = 0},
    [eMotorApi_Motor_2] = {.direction = eMotorApi_Direction_Left, .speed = 0},
};
/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/
static bool Motor_API_UpdateMotor (eMotorApi_Motor_Enum_t motor);
/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/
static bool Motor_API_UpdateMotor (eMotorApi_Motor_Enum_t motor) {
    if (g_dynamic_motor_api_lut[motor].direction == eMotorApi_Direction_Left) {
        Timer_Driver_SetDutyCycle(eTimerDriver_Timer_1, g_static_motor_api_lut[motor].a1_channel, g_dynamic_motor_api_lut[motor].speed);
        Timer_Driver_EnableChannel(eTimerDriver_Timer_1, g_static_motor_api_lut[motor].a1_channel);
        Timer_Driver_DisableChannel(eTimerDriver_Timer_1, g_static_motor_api_lut[motor].a2_channel);
    } else {
        Timer_Driver_SetDutyCycle(eTimerDriver_Timer_1, g_static_motor_api_lut[motor].a2_channel, g_dynamic_motor_api_lut[motor].speed);
        Timer_Driver_EnableChannel(eTimerDriver_Timer_1, g_static_motor_api_lut[motor].a2_channel);
        Timer_Driver_DisableChannel(eTimerDriver_Timer_1, g_static_motor_api_lut[motor].a1_channel);
    }
    return true;
}
/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool Motor_API_Init (void) {
    Timer_Driver_Init(eTimerDriver_Timer_1);
    Timer_Driver_Start(eTimerDriver_Timer_1);
    return true;
}

bool Motor_API_SetSpeed (eMotorApi_Motor_Enum_t motor, uint8_t speed) {
    if (motor >= eMotorApi_Motor_Last) {
        error("Invalid motor provided\n");
        return false;
    }
    if (speed > MOTOR_API_MAX_SPEED_VALUE) {
        error("Invalid motor speed provided\n");
        return false;
    }
    g_dynamic_motor_api_lut[motor].speed = speed;
    Motor_API_UpdateMotor(motor);
    return true;
}

bool Motor_API_SetDirection (eMotorApi_Motor_Enum_t motor, eMotorApi_Direction_Enum_t direction) {
    if (motor >= eMotorApi_Motor_Last) {
        error("Invalid motor provided\n");
        return false;
    }
    if (direction >= eMotorApi_Direction_Last) {
        error("Invalid motor direction provided\n");
    }
    g_dynamic_motor_api_lut[motor].direction = direction;
    Motor_API_UpdateMotor(motor);
    return true;
}
