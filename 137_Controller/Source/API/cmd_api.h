#ifndef __CMD_API__H__
#define __CMD_API__H__
/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "stdint.h"
#include "stdbool.h"
/**********************************************************************************************************************
 * Exported definitions and macros
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported types
 *********************************************************************************************************************/
typedef struct sCmdHandlerArgs_t {
    char *cmd_args;
    char *response_buffer;
    uint16_t response_buffer_size;
} sCmdApiHandlerArgs_t;

typedef struct sCliAppCmd_t {
    const char *name;
    const char *parameters;
    const char *description;
    const bool (*fun_ptr) (sCmdApiHandlerArgs_t);
} sCmdApiCmd_t;

typedef struct sCmdLauncherArgs_t {
    const sCmdApiCmd_t *cmd_lut;
    uint8_t cmd_lut_size;
    char *cmd_raw;
    char *response_buffer;
    uint16_t response_buffer_size;
} sCmdApiLauncherArgs_t;
/**********************************************************************************************************************
 * Exported variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of exported functions
 *********************************************************************************************************************/
bool CMD_API_Launcher (sCmdApiLauncherArgs_t *launcher_args);
#endif /* __CMD_API__H__ */
