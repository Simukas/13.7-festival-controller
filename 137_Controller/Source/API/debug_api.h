#ifndef __DEBUG_API__H__
#define __DEBUG_API__H__

/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "stdio.h"
#include "string.h"
#include <stdarg.h>
#include <stdint.h>
#include <stdbool.h>
#include "uart_api.h"
#include "uart_driver.h"
/**********************************************************************************************************************
 * Exported definitions and macros
 *********************************************************************************************************************/

#define DEBUG_MODULE(module_name)   static const char *debug_module_name = #module_name;

#define debug(...) Debug_API_Log(debug_module_name, __VA_ARGS__)
//#define debug(...) ((void)__VA_ARGS__)

#define error(...) Debug_API_Error(debug_module_name,__FILE__, __LINE__, __VA_ARGS__)

#define print(string) Debug_API_Print(string)

#define printF(...) Debug_API_PrintF(__VA_ARGS__)


//#define driver_debug(message) UART_Driver_SendString(message)
//#define driver_debug(message) ((void)message)


/**********************************************************************************************************************
 * Exported types
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of exported functions
 *********************************************************************************************************************/
bool Debug_API_Init (void);
void Debug_API_Log (const char *debug_module_name, const char *format, ...);
void Debug_API_Error (const char *debug_module_name, const char *file, const uint16_t line, const char *format, ...);
void Debug_API_Print (const char *string);
void Debug_API_PrintF (const char *format, ...);
bool Debug_API_Lock (uint32_t timeout);
bool Debug_API_Unlock (void);
#endif /* __DEBUG_API__H__ */
