#ifndef __WS2815_API__H__
#define __WS2815_API__H__
/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "stdbool.h"
#include "stdint.h"
#include "color_utils.h"
/**********************************************************************************************************************
 * Exported definitions and macros
 *********************************************************************************************************************/
#define WS2815_API_DATA_PER_PIXEL 24
#define WS2815_API_DATA_ELEMENTS 2
#define WS2815_API_PULSE_DATA_BUFFER_SIZE (WS2815_API_DATA_PER_PIXEL * WS2815_API_DATA_ELEMENTS)
/**********************************************************************************************************************
 * Exported types
 *********************************************************************************************************************/
typedef enum eWs2815Api_Port_Enum_t {
    eWs2815Api_Port_First = 0,
    eWs2815Api_Port_1 = eWs2815Api_Port_First,
    eWs2815Api_Port_2,
    eWs2815Api_Port_Last,
} eWs2815Api_Port_Enum_t;
/**********************************************************************************************************************
 * Exported variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of exported functions
 *********************************************************************************************************************/
bool WS2815_API_Init (eWs2815Api_Port_Enum_t port, uint16_t pixel_count, sRgbColor_t *color_data_buffer);
bool WS2815_API_UpdatePixels (eWs2815Api_Port_Enum_t port);
bool WS2815_API_SetPixelColor (eWs2815Api_Port_Enum_t port, uint16_t pixel_index, sRgbColor_t color);
bool WS2815_API_FillPixels (eWs2815Api_Port_Enum_t port, sRgbColor_t color);
void WS2815_API_InterruptHandler (bool offset_data);
#endif /* __WS2815_API__H__ */
