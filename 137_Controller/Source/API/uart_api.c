/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "uart_api.h"
#include "uart_driver.h"
#include "cmsis_os.h"
#include "debug_api.h"
#include "stack_info.h"
#include <stdbool.h>
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
#define UART_API_BUFFER_SIZE 100
#define UART_API_MESSAGE_QUEUE_COUNT 3

DEBUG_MODULE(UART_API)
/**********************************************************************************************************************
 * Private types
 *********************************************************************************************************************/
// @formatter:off
typedef enum eStatesEnum_t {
    eUartApiStateFirst = 0,
    eUartApiSetupState = eUartApiStateFirst,
    eUartApiReceiveState,
    eUartApiFlushState,
    eUartApiStateLast,
} eStatesEnum_t;
// @formatter:on
typedef struct sUartApiDynamic_t {
    osMessageQueueId_t msg_queue;
    osMutexId_t tx_mutex_id;
    eStatesEnum_t state;
    eUartBaudrateEnum_t baudrate;
    eUartApiEnum_t uart;
    bool initialised;
    char buffer[UART_API_BUFFER_SIZE];
    uint16_t buffer_size;
    uint16_t index;
    char delimiter;
} sUartApiDynamic_t;

//typedef struct sUartApiStatic_t {
//    uint8_t msg_queue_count;
//} sUartApiStatic_t;
/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
eUartDriverEnum_t g_uart_api_translation_lut[] = {
    [eUartApiDebug] = eUartDriverUsart2,
};

const static osThreadAttr_t uart_api_task_attributes = {
    .name = "uart_api_task",
    .stack_size = UART_API_TASK_STACK_SIZE,
    .priority = (osPriority_t) UART_API_TASK_PRIORITY, };

//const static sUartApiStatic_t static_uart_api_lut[eUartApiLast] = {
//    [eUartApiFirst ... eUartApiLast - 1] = {.msg_queue_count = UART_API_MESSAGE_QUEUE_COUNT}
//};

const static osMutexAttr_t uart_api_mutex_attr = {
    "uart_api_mutex",
    osMutexRecursive | osMutexPrioInherit,
    NULL,
    0U
};

/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/
static osThreadId_t uart_api_task_handle = NULL;

static bool uart_api_task_started = false;

static sUartApiDynamic_t dynamic_uart_api_lut[eUartApiLast] = {
    [eUartApiFirst ... eUartApiLast - 1] = {.state = eUartApiSetupState, .buffer_size = 0, .initialised = false, .index = 0}
};
/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/
static void UART_API_Task (void *argument);
/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/
static void UART_API_Task (void *argument) {
    while (true) {
        for (eUartApiEnum_t uart = eUartApiFirst; uart < eUartApiLast; uart++) {
            if (!dynamic_uart_api_lut[uart].initialised) {
                continue;
            }
            switch (dynamic_uart_api_lut[uart].state) {

                case eUartApiSetupState: {
                    memset(dynamic_uart_api_lut[uart].buffer, 0, UART_API_BUFFER_SIZE);
                    dynamic_uart_api_lut[uart].index = 0;
                    dynamic_uart_api_lut[uart].state = eUartApiReceiveState;
                }

                case eUartApiReceiveState: {
                    uint8_t received_byte = 0;
                    while (UART_Driver_GetByte(g_uart_api_translation_lut[dynamic_uart_api_lut[uart].uart], &received_byte) == true) {
                        if (dynamic_uart_api_lut[uart].index >= dynamic_uart_api_lut[uart].buffer_size - 1) {
                            dynamic_uart_api_lut[uart].state = eUartApiSetupState;
                            error("Buffer overflow\n\r");
                            break;
                        }
                        if (received_byte == dynamic_uart_api_lut[uart].delimiter) {
                            dynamic_uart_api_lut[uart].state = eUartApiFlushState;
                        }
                        else {
                            dynamic_uart_api_lut[uart].buffer[dynamic_uart_api_lut[uart].index++] = received_byte;
                            break;
                        }
                    }
                    if (dynamic_uart_api_lut[uart].state != eUartApiFlushState) {
                        break;
                    }
                }

                case eUartApiFlushState: {
                    if (osMessageQueuePut(dynamic_uart_api_lut[uart].msg_queue, dynamic_uart_api_lut[uart].buffer, 0U, osWaitForever) == osOK) {
                        dynamic_uart_api_lut[uart].state = eUartApiSetupState;
                    }
                    else {
                        error("Failed to put message into queue\n\r");
                    }
                }

                default: {
                    break;
                }
            }
        }
        osThreadYield();
    }
}
/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool UART_API_Init (eUartApiEnum_t uart, eUartBaudrateEnum_t baudrate, uint16_t buffer_size, char delimiter) {
    if (uart >= eUartApiLast) {
        error("Wrong UART provided\n");
        return false;
    }
    if (baudrate >= eUartBaudrateLast) {
        error("Wrong baudrate provided\n");
        return false;
    }
    if (UART_Driver_Init(g_uart_api_translation_lut[uart], baudrate) == false) {
        error("Failed to initialize UART driver\n");
        return false;
    }
    dynamic_uart_api_lut[uart].index = 0;
    dynamic_uart_api_lut[uart].uart = uart;
    dynamic_uart_api_lut[uart].baudrate = baudrate;
    dynamic_uart_api_lut[uart].delimiter = delimiter;
    dynamic_uart_api_lut[uart].buffer_size = buffer_size;
    dynamic_uart_api_lut[uart].tx_mutex_id = osMutexNew(&uart_api_mutex_attr);
    if (dynamic_uart_api_lut[uart].tx_mutex_id == NULL) {
        error("Failed to create mutex\n");
        return false;
    }
    dynamic_uart_api_lut[uart].msg_queue = osMessageQueueNew(UART_API_MESSAGE_QUEUE_COUNT, UART_API_BUFFER_SIZE, NULL);
    if (dynamic_uart_api_lut[uart].msg_queue == NULL) {
        error("Failed to create message queue\n");
        return false;
    }
    dynamic_uart_api_lut[uart].initialised = true;
    if (!uart_api_task_started) {
        uart_api_task_handle = osThreadNew(UART_API_Task, NULL, &uart_api_task_attributes);
        if (uart_api_task_handle == NULL) {
            error("Failed to create thread\n");
            return false;
        }
        uart_api_task_started = true;
    }
    return true;
}

bool UART_API_SendString (eUartApiEnum_t uart, const char *string) {
    if (string == NULL) {
        error("NULL pointer provided\n");
        return false;
    }
    if (uart >= eUartApiLast) {
        error("Wrong UART provided\n");
        return false;
    }
    UART_Driver_SendString(uart, string);
    return true;
}

bool UART_API_Receive (eUartApiEnum_t uart, char *data, uint32_t timeout) {
    if (data == NULL) {
        error("NULL pointer provided\n");
        return false;
    }
    if (uart >= eUartApiLast) {
        error("Wrong UART provided\n");
        return false;
    }
    osStatus_t receive_status = osMessageQueueGet(dynamic_uart_api_lut[uart].msg_queue, data, NULL, timeout);
    if (receive_status != osOK) {
        error("%d\n", receive_status);
        return false;
    }
    return true;
}

bool UART_API_Lock (eUartApiEnum_t uart, uint32_t timeout) {
    if (osMutexAcquire(dynamic_uart_api_lut[uart].tx_mutex_id, timeout) != osOK) {
        return false;
    }
    return true;
}

bool UART_API_Unlock (eUartApiEnum_t uart) {
    if (osMutexRelease(dynamic_uart_api_lut[uart].tx_mutex_id) != osOK) {
        return false;
    }
    return true;
}
