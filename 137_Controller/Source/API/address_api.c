/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "address_api.h"
#include "stdbool.h"
#include "gpio_driver.h"
#include "debug_api.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
DEBUG_MODULE(ADRESS_API)
/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/
// @formatter:off
typedef enum eAddressApi_Bit_Enum_t {
    eAddressApi_Bit_First,
    eAddressApi_Bit_0 = eAddressApi_Bit_First,
    eAddressApi_Bit_1,
    eAddressApi_Bit_2,
    eAddressApi_Bit_3,
    eAddressApi_Bit_4,
    eAddressApi_Bit_5,
    eAddressApi_Bit_6,
    eAddressApi_Bit_7,
    eAddressApi_Bit_Last
} eAddressApi_Bit_Enum_t;
/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
static const eGpioDriver_Pin_Enum_t g_address_api_pins[eAddressApi_Bit_Last] = {
    [eAddressApi_Bit_0] = eGpioDriver_Pin_AddressBit_0,
    [eAddressApi_Bit_1] = eGpioDriver_Pin_AddressBit_1,
    [eAddressApi_Bit_2] = eGpioDriver_Pin_AddressBit_2,
    [eAddressApi_Bit_3] = eGpioDriver_Pin_AddressBit_3,
    [eAddressApi_Bit_4] = eGpioDriver_Pin_AddressBit_4,
    [eAddressApi_Bit_5] = eGpioDriver_Pin_AddressBit_5,
    [eAddressApi_Bit_6] = eGpioDriver_Pin_AddressBit_6,
    [eAddressApi_Bit_7] = eGpioDriver_Pin_AddressBit_7,
};
// @formatter:on
/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/
uint8_t address_api_current_address = 0;
/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool Address_API_Init (void) {
    bool init_ok = true;
    for (eAddressApi_Bit_Enum_t bit = eAddressApi_Bit_First; bit < eAddressApi_Bit_Last; bit++) {
        if (GPIO_Driver_InitPin(g_address_api_pins[bit], NULL) == false) {
            init_ok = false;
        }
    }
    if (Address_API_ReadAddress() == false) {
        init_ok = false;
    }
    return init_ok;
}

bool Address_API_ReadAddress (void) {
    address_api_current_address = 0;
    bool bit_value = false;
    for (eAddressApi_Bit_Enum_t bit = eAddressApi_Bit_First; bit < eAddressApi_Bit_Last; bit++) {
        if (GPIO_Driver_ReadInputPin(g_address_api_pins[bit], &bit_value) == true) {
            address_api_current_address |= !bit_value << bit;
        }
    }
    if (address_api_current_address == ADDRESS_API_MASTER_ADDRESS) {
        debug("Controller will work as master\n\r");
    }
    return true;
}

bool Address_API_GetAddress (uint8_t *address) {
    if (address == NULL) {
        error("NULL pointer provided\n\r");
        return false;
    }
    *address = address_api_current_address;
    return true;
}
