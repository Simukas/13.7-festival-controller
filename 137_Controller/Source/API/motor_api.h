#ifndef __MOTOR_API__H__
#define __MOTOR_API__H__
/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "stdbool.h"
#include "stdint.h"
/**********************************************************************************************************************
 * Exported definitions and macros
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported types
 *********************************************************************************************************************/
typedef enum eMotorApi_Direction_Enum_t {
    eMotorApi_Direction_First,
    eMotorApi_Direction_Left = eMotorApi_Direction_First,
    eMotorApi_Direction_Right,
    eMotorApi_Direction_Last,
} eMotorApi_Direction_Enum_t;

typedef enum eMotorApi_Motor_Enum_t {
    eMotorApi_Motor_First,
    eMotorApi_Motor_1 = eMotorApi_Motor_First,
    eMotorApi_Motor_2,
    eMotorApi_Motor_Last,
} eMotorApi_Motor_Enum_t;
/**********************************************************************************************************************
 * Exported variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of exported functions
 *********************************************************************************************************************/
bool Motor_API_Init (void);
bool Motor_API_SetSpeed (eMotorApi_Motor_Enum_t motor, uint8_t speed);
bool Motor_API_SetDirection (eMotorApi_Motor_Enum_t motor, eMotorApi_Direction_Enum_t direction);
#endif /* __MOTOR_DRIVER__H__ */
