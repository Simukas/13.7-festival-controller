#ifndef __UART_API__H__
#define __UART_API__H__

/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include "uart_driver.h"
/**********************************************************************************************************************
 * Exported definitions and macros
 *********************************************************************************************************************/
// @formatter:off
typedef enum eUartApiEnum_t {
    eUartApiFirst,
    eUartApiDebug = eUartApiFirst,
    eUartApiLast,
} eUartApiEnum_t;
/**********************************************************************************************************************
 * Exported types
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported variables
 *********************************************************************************************************************/
// @formatter:on
/**********************************************************************************************************************
 * Prototypes of exported functions
 *********************************************************************************************************************/
bool UART_API_Init (eUartApiEnum_t uart, eUartBaudrateEnum_t baudrate, uint16_t buffer_size, char delimiter);
bool UART_API_SendString (eUartApiEnum_t uart, const char *string);
bool UART_API_Receive (eUartApiEnum_t uart, char *data, uint32_t timeout);
bool UART_API_Lock (eUartApiEnum_t uart, uint32_t timeout);
bool UART_API_Unlock (eUartApiEnum_t uart);
#endif /* __UART_API__H__ */
