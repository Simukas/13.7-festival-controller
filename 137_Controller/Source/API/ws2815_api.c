/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "ws2815_api.h"
#include "stdint.h"
#include "stdbool.h"
#include "dma_driver.h"
#include "timer_driver.h"
#include "gpio_driver.h"
#include "color_utils.h"
#include "ws2815_lut.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
DEBUG_MODULE(WS2815_API)

#define WS2815_API_LEDS_PER_INTERRUPT (WS2815_API_DATA_ELEMENTS / 2)

#define WS2815_API_HALF_PULSE_DATA_BUFFER_SIZE (WS2815_API_DATA_PER_PIXEL * WS2815_API_LEDS_PER_INTERRUPT)

#define WS2815_API_RESET_CYCLES 230 //224
#define WS2815_API_HIGH_PULSE_VALUE 48
#define WS2815_API_LOW_PULSE_VALUE 15

#define WS2815_API_DATA_OFFSET (WS2815_API_DATA_PER_PIXEL * WS2815_API_LEDS_PER_INTERRUPT)
#define WS2815_API_LED_OFFSET WS2815_API_LEDS_PER_INTERRUPT
#define WS2815_API_PULSE_DATA_OFFSET (WS2815_API_DATA_PER_PIXEL - 1)
#define WS2815_API_0_OFFSET 0
/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/
typedef struct sWs2815ApiStatic_t {
    eTimerDriver_Timer_Enum_t timer;
    eTimerDriver_OC_Enum_t timer_channel;
    eGpioDriver_Pin_Enum_t pin;
    eDmaDriver_Channel_Enum_t dma_channel;
    void (*interrupt_handler) (bool);
} sWs2815ApiStatic_t;

typedef struct sWs2815ApiDynamic_t {
    uint16_t pixel_count;
    uint16_t pulse_data[WS2815_API_PULSE_DATA_BUFFER_SIZE];
    sRgbColor_t *color_data;
    uint16_t data_pulse_high_value;
    uint16_t data_pulse_low_value;
    bool is_running;
    uint16_t cycles_count;
} sWs2815ApiDynamic_t;
/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/
static void WS2815_API_LoadPulseData (bool offset_data);
//static void WS2815_API_InterruptHandler (bool offset_data);
//static bool WS2815_API_SetPulseData (eWs2815Api_Port_Enum_t port, bool offset_data, uint16_t value);
//static void WS2815_API_ClearPulseData (eWs2815Api_Port_Enum_t port, bool offset_data);
//static void WS2815_API_Port1InterruptHandler (bool offset_data);
//static void WS2815_API_Port2InterruptHandler (bool offset_data);
/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
static const sWs2815ApiStatic_t g_static_ws2815_api_lut[eWs2815Api_Port_Last] = {
    [eWs2815Api_Port_1] = {.timer = eTimerDriver_Timer_15, .timer_channel = eTimerDriver_OC_1, .pin = eGpioDriver_Pin_Tim15_Ch1, .dma_channel = eDmaDriver_Channel_2, .interrupt_handler = &WS2815_API_InterruptHandler},
    [eWs2815Api_Port_2] = {.timer = eTimerDriver_Timer_15, .timer_channel = eTimerDriver_OC_2, .pin = eGpioDriver_Pin_Tim15_Ch2, .dma_channel = eDmaDriver_Channel_2, .interrupt_handler = &WS2815_API_InterruptHandler},
};
/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/
static sWs2815ApiDynamic_t g_dynamic_ws2815_api_lut[] = {
    [eWs2815Api_Port_1] = {.pixel_count = 0, .data_pulse_high_value = 0, .data_pulse_low_value = 0, .is_running = false, .cycles_count = 0},
//[eWs2815Api_Port_2] = {.pixel_count = 0, .data_pulse_high_value = 0, .data_pulse_low_value = 0, .is_running = false, .cycles_count = 0},
};

static uint16_t leds_loaded = 0;
static uint16_t pixels = 0;
static sRgbColor_t *data = NULL;
static uint8_t pulse_data[WS2815_API_PULSE_DATA_BUFFER_SIZE] = {0};
/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/
__STATIC_INLINE void WS2815_API_LoadPulseData (bool offset_data) {
    uint8_t half_buffer_offset = (offset_data ? WS2815_API_DATA_OFFSET : WS2815_API_0_OFFSET);
    sRgbColor_t color = {0};
    uint8_t led_offset = 0;
    for (uint8_t led = 0; led < WS2815_API_LEDS_PER_INTERRUPT; led++) {
        color = data[leds_loaded + led];
        led_offset = led * WS2815_API_DATA_PER_PIXEL;
        memcpy(pulse_data + 0 + half_buffer_offset + led_offset, &g_static_ws2815_value_lut[color.r], 8);
        memcpy(pulse_data + 8 + half_buffer_offset + led_offset, &g_static_ws2815_value_lut[color.g], 8);
        memcpy(pulse_data + 16 + half_buffer_offset + led_offset, &g_static_ws2815_value_lut[color.b], 8);
    }
    leds_loaded += WS2815_API_LEDS_PER_INTERRUPT;
}

void WS2815_API_InterruptHandler (bool offset_data) {
    if (g_dynamic_ws2815_api_lut[eWs2815Api_Port_1].is_running == false) {
        return;
    }
    if (leds_loaded < pixels) {
        WS2815_API_LoadPulseData(offset_data);
        return;
    }
    if (leds_loaded < pixels + WS2815_API_DATA_ELEMENTS) {
        uint8_t offset = (offset_data ? WS2815_API_HALF_PULSE_DATA_BUFFER_SIZE : WS2815_API_0_OFFSET);
        memset(pulse_data + offset, 0, WS2815_API_HALF_PULSE_DATA_BUFFER_SIZE);
        leds_loaded += WS2815_API_LEDS_PER_INTERRUPT;
        return;
    }
    if (leds_loaded >= pixels + WS2815_API_DATA_ELEMENTS) {

        Timer_Driver_Stop(g_static_ws2815_api_lut[eWs2815Api_Port_1].timer);
        //Timer_Driver_DisableChannel(g_static_ws2815_api_lut[eWs2815Api_Port_1].timer, g_static_ws2815_api_lut[eWs2815Api_Port_1].timer_channel);
        DMA_Driver_DisableInterrupt(g_static_ws2815_api_lut[eWs2815Api_Port_1].dma_channel, eDmaDriver_Flag_TC);
        DMA_Driver_DisableInterrupt(g_static_ws2815_api_lut[eWs2815Api_Port_1].dma_channel, eDmaDriver_Flag_HT);
        DMA_Driver_DisableChannel(g_static_ws2815_api_lut[eWs2815Api_Port_1].dma_channel);
        //Timer_Driver_SetCounter(g_static_ws2815_api_lut[eWs2815Api_Port_1].timer, 0);
        //Timer_Driver_SetDutyCycle(g_static_ws2815_api_lut[eWs2815Api_Port_1].timer, g_static_ws2815_api_lut[eWs2815Api_Port_1].timer_channel, 0);

        g_dynamic_ws2815_api_lut[eWs2815Api_Port_1].is_running = false;
        return;
    }
}
/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool WS2815_API_Init (eWs2815Api_Port_Enum_t port, uint16_t pixel_count, sRgbColor_t *color_data_buffer) {
    data = color_data_buffer;
    if (Timer_Driver_Init(g_static_ws2815_api_lut[port].timer) == false) {
        return false;
    }

    sTimerDriverStatic_t timer_info = {0};
    if (Timer_Driver_GetTimerInfo(g_static_ws2815_api_lut[port].timer, &timer_info) == false) {
        return false;
    }

    volatile uint32_t *ccr_address = NULL;
    switch (g_static_ws2815_api_lut[port].timer_channel) {
        case eTimerDriver_OC_1:
            ccr_address = &timer_info.timer->CCR1;
            break;
        case eTimerDriver_OC_2:
            ccr_address = &timer_info.timer->CCR2;
            break;
        default:
            error("Wrong value\n");
            break;
    }

    if (DMA_Driver_Init(g_static_ws2815_api_lut[port].dma_channel, pulse_data, (void*) ccr_address, g_static_ws2815_api_lut[port].interrupt_handler) == false) {
        return false;
    }

    pixels = pixel_count;
    g_dynamic_ws2815_api_lut[port].pixel_count = pixel_count;

    return true;
}

bool WS2815_API_UpdatePixels (eWs2815Api_Port_Enum_t port) {
    if (g_dynamic_ws2815_api_lut[port].is_running == true) {
        error("LED update is still running\n");
        return false;
    }
    g_dynamic_ws2815_api_lut[port].is_running = true;
    leds_loaded = 0;

    WS2815_API_LoadPulseData(false);
    WS2815_API_LoadPulseData(true);

    DMA_Driver_EnableChannel(g_static_ws2815_api_lut[port].dma_channel);
    Timer_Driver_EnableChannel(g_static_ws2815_api_lut[port].timer, g_static_ws2815_api_lut[port].timer_channel);

    //DMA_Driver_ClearFlag(g_static_ws2815_api_lut[port].dma_channel, eDmaDriver_Flag_HT);
    //DMA_Driver_ClearFlag(g_static_ws2815_api_lut[port].dma_channel, eDmaDriver_Flag_TC);
    //DMA_Driver_ClearFlag(g_static_ws2815_api_lut[port].dma_channel, eDmaDriver_Flag_TE);

    DMA_Driver_EnableInterrupt(g_static_ws2815_api_lut[port].dma_channel, eDmaDriver_Flag_HT);
    DMA_Driver_EnableInterrupt(g_static_ws2815_api_lut[port].dma_channel, eDmaDriver_Flag_TC);

    Timer_Driver_Start(g_static_ws2815_api_lut[port].timer);
    return true;
}

bool WS2815_API_SetPixelColor (eWs2815Api_Port_Enum_t port, uint16_t pixel_index, sRgbColor_t color) {
    if (port >= eWs2815Api_Port_Last) {
        error("Wrong port\n");
        return false;
    }
    if (pixel_index >= g_dynamic_ws2815_api_lut[port].pixel_count) {
        error("Wrong index\n");
        return false;
    }
    data[pixel_index] = color;
    return true;
}

bool WS2815_API_FillPixels (eWs2815Api_Port_Enum_t port, sRgbColor_t color) {
    if (port >= eWs2815Api_Port_Last) {
        error("Wrong port\n");
        return false;
    }
    for (uint16_t pixel_index = 0; pixel_index < g_dynamic_ws2815_api_lut[port].pixel_count; pixel_index++) {
        WS2815_API_SetPixelColor(port, pixel_index, color);
    }
    return true;
}
