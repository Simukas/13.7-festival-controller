/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "color_utils.h"
#include "stdint.h"
#include "stdlib.h"
#include "number_utils.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/
const sRgbColor_t rgb_black = {
    .r = 0,
    .g = 0,
    .b = 0,
};

const sHsvColor_t hsv_black = {
    .h = 0,
    .s = 255,
    .v = 0,
};

const sRgbColor_t rgb_white = {
    .r = 255,
    .g = 255,
    .b = 255,
};

const sHsvColor_t hsv_white = {
    .h = 0,
    .s = 0,
    .v = 255,
};
/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
sRgbColor_t InvertRgb (sRgbColor_t rgb) {
    sRgbColor_t rgb_color = {
        .r = 255 - rgb.r,
        .g = 255 - rgb.g,
        .b = 255 - rgb.b,
    };
    return rgb_color;
}

sHsvColor_t RandomHsv (void) {
    sHsvColor_t hsv_color = {
        .h = RandomRange(0, 255),
        .s = 255,
        .v = 255
    };
    return hsv_color;
}

sRgbColor_t RandomRgb (void) {
    sRgbColor_t rgb_color = {
        .r = RandomRange(0, 255),
        .g = RandomRange(0, 255),
        .b = RandomRange(0, 255)
    };
    return rgb_color;
}

sRgbColor_t HsvToRgb (sHsvColor_t hsv) {
    sRgbColor_t rgb = {0};

    if (hsv.s == 0) {
        rgb.r = hsv.v;
        rgb.g = hsv.v;
        rgb.b = hsv.v;
        return rgb;
    }

    uint8_t region = hsv.h / 43;
    uint8_t remainder = (hsv.h - (region * 43)) * 6;
    uint8_t p = (hsv.v * (255 - hsv.s)) >> 8;
    uint8_t q = (hsv.v * (255 - ((hsv.s * remainder) >> 8))) >> 8;
    uint8_t t = (hsv.v * (255 - ((hsv.s * (255 - remainder)) >> 8))) >> 8;

    switch (region) {
        case 0:
            rgb.r = hsv.v;
            rgb.g = t;
            rgb.b = p;
            break;
        case 1:
            rgb.r = q;
            rgb.g = hsv.v;
            rgb.b = p;
            break;
        case 2:
            rgb.r = p;
            rgb.g = hsv.v;
            rgb.b = t;
            break;
        case 3:
            rgb.r = p;
            rgb.g = q;
            rgb.b = hsv.v;
            break;
        case 4:
            rgb.r = t;
            rgb.g = p;
            rgb.b = hsv.v;
            break;
        default:
            rgb.r = hsv.v;
            rgb.g = p;
            rgb.b = q;
            break;
    }

    return rgb;
}

sHsvColor_t RgbToHsv (sRgbColor_t rgb) {
    sHsvColor_t hsv = {0};

    uint8_t rgbMin = rgb.r < rgb.g ? (rgb.r < rgb.b ? rgb.r : rgb.b) : (rgb.g < rgb.b ? rgb.g : rgb.b);
    uint8_t rgbMax = rgb.r > rgb.g ? (rgb.r > rgb.b ? rgb.r : rgb.b) : (rgb.g > rgb.b ? rgb.g : rgb.b);

    hsv.v = rgbMax;
    if (hsv.v == 0) {
        hsv.h = 0;
        hsv.s = 0;
        return hsv;
    }

    hsv.s = 255 * (rgbMax - rgbMin) / hsv.v;

    if (hsv.s == 0) {
        hsv.h = 0;
        return hsv;
    }

    if (rgbMax == rgb.r) {
        hsv.h = 0 + 43 * (rgb.g - rgb.b) / (rgbMax - rgbMin);
    } else if (rgbMax == rgb.g) {
        hsv.h = 85 + 43 * (rgb.b - rgb.r) / (rgbMax - rgbMin);
    } else {
        hsv.h = 171 + 43 * (rgb.r - rgb.g) / (rgbMax - rgbMin);
    }
    return hsv;
}
