/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "light_app.h"
#include "stdbool.h"
#include "cmsis_os.h"
#include "debug_api.h"
#include "stack_info.h"
#include "color_utils.h"
#include "led_api.h"
#include "ws2815_api.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
DEBUG_MODULE(LIGHT_APP)

//#define LIGHT_APP_ARGB_PIXEL_AMOUNT 10
#define LIGHT_APP_MESSAGE_QUEUE_COUNT 3
/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/
typedef struct sLightApp_Dynamic_t {
    osMessageQueueId_t msg_queue;
} sLightApp_Dynamic_t;
/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
static osThreadId_t g_light_app_task_handle = NULL;

const static osThreadAttr_t g_light_app_task_attributes = {
    .name = "led_app_task",
    .stack_size = LIGHT_APP_TASK_STACK_SIZE,
    .priority = (osPriority_t) LIGHT_APP_TASK_PRIORITY
};
/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/
//sRgbColor_t g_argb_strip_data[LIGHT_APP_ARGB_PIXEL_AMOUNT] = {0};
sRgbColor_t rgb_yellow = {
    .r = 255,
    .g = 255,
    .b = 255,
};
sRgbColor_t rgb_red = {
    .r = 0,
    .g = 255,
    .b = 255,
};
static sLightApp_StrobeData_t g_dynamic_strobe_data = {
    .state = eLightApp_StrobeState_Off
};

static sLightApp_StaticColorData_t g_dynamic_simple_color_data = {0};

static sLightApp_Dynamic_t g_dynamic_light_app_lut = {0};
/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/
static void Light_APP_Task (void *argument);
/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/
static void Light_APP_Task (void *argument) {
    LED_API_SetRgbColor(eLedApi_Port_RGB, rgb_yellow);
    LED_API_SetRgbColor(eLedApi_Port_H_Bridge, rgb_red);
    while (true) {
        if (osMessageQueueGetCount(g_dynamic_light_app_lut.msg_queue) > 0) {
            sLightApp_Task_t task = {0};
            if (osMessageQueueGet(g_dynamic_light_app_lut.msg_queue, &task, 0U, osWaitForever) == osOK) {
                switch (task.port) {
                    case eLedApi_Port_RGB: {
                        if (g_dynamic_strobe_data.state == eLightApp_StrobeState_Off) {
                            g_dynamic_strobe_data.state = eLightApp_StrobeState_Generate;
                        }
                        break;
                    }
                    case eLedApi_Port_H_Bridge: {
                        LED_API_SimpleColor(eLedApi_Port_H_Bridge, &g_dynamic_simple_color_data);
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }
        }
        if (g_dynamic_strobe_data.state != eLightApp_StrobeState_Off) {
            LED_API_Strobe(eLedApi_Port_RGB, &g_dynamic_strobe_data);
        }
        osThreadYield();
    }
}

/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool Light_APP_Init (void) {
    LED_API_Init(eLedApi_Port_RGB);
    LED_API_Init(eLedApi_Port_H_Bridge);
//  WS2815_API_Init(eWs2815Api_Port_1, LIGHT_APP_ARGB_PIXEL_AMOUNT, g_argb_strip_data);
    g_dynamic_light_app_lut.msg_queue = osMessageQueueNew(LIGHT_APP_MESSAGE_QUEUE_COUNT, sizeof(sLightApp_Task_t), NULL);
    if (g_dynamic_light_app_lut.msg_queue == NULL) {
        error("Failed to create message queue\n\r");
        return false;
    }

    g_light_app_task_handle = osThreadNew(Light_APP_Task, NULL, &g_light_app_task_attributes);
    if (g_light_app_task_handle == NULL) {
        debug("Failed to create thread!\n\r");
        return false;
    }
    return true;
}

bool Light_APP_AddTask (sLightApp_Task_t *task) {
    if (task == NULL) {
        error("NULL pointer provided\n");
        return false;
    }
    if (osMessageQueuePut(g_dynamic_light_app_lut.msg_queue, task, 0U, osWaitForever) != osOK) {
        error("Failed to put message into queue\n\r");
        return false;
    }
    return true;
}

bool Light_APP_Pause (void) {
    if (osThreadSuspend(g_light_app_task_handle) != osOK) {
        return false;
    }
    return true;
}

bool Light_APP_Resume (void) {
    if (osThreadResume(g_light_app_task_handle) != osOK) {
        return false;
    }
    return true;
}
