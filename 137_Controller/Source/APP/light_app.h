#ifndef __LIGHT_APP__H__
#define __LIGHT_APP__H__
/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "stdbool.h"
#include "led_api.h"
/**********************************************************************************************************************
 * Exported definitions and macros
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported types
 *********************************************************************************************************************/
typedef struct sLightApp_Task_t {
    eLedApi_Port_Enum_t port;
} sLightApp_Task_t;
/**********************************************************************************************************************
 * Exported variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of exported functions
 *********************************************************************************************************************/
bool Light_APP_Init (void);
bool Light_APP_Pause (void);
bool Light_APP_Resume (void);
bool Light_APP_AddTask (sLightApp_Task_t *task);
#endif /* __LIGHT_APP__H__ */
