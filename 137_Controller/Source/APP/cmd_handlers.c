/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "cmd_handlers.h"
#include "stdbool.h"
#include "string.h"
#include "color_utils.h"
#include "stdlib.h"
#include "stdio.h"
#include "led_api.h"
#include "string_utils.h"
#include "rs485_api.h"
#include "light_app.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
#define CMD_HANDLER_ARGUMENT_SEPARATOR ","
/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool CMD_ARGB_SET_PIXEL (sCmdApiHandlerArgs_t args) {

    return true;
}

bool CMD_ARGB_FILL_STRIP (sCmdApiHandlerArgs_t args) {

    return true;
}

bool CMD_RGB_SET_COLOR (sCmdApiHandlerArgs_t args) {
    sRgbColor_t rgb_color = {0};
    char *token = NULL;
    token = strtok(args.cmd_args, CMD_HANDLER_ARGUMENT_SEPARATOR);
    if (token == NULL) {
        return false;
    }
    rgb_color.r = atoi(token);
    token = strtok(NULL, CMD_HANDLER_ARGUMENT_SEPARATOR);
    if (token == NULL) {
        return false;
    }
    rgb_color.g = atoi(token);
    token = strtok(NULL, CMD_HANDLER_ARGUMENT_SEPARATOR);
    if (token == NULL) {
        return false;
    }
    rgb_color.b = atoi(token);
    LED_API_SetRgbColor(eLedApi_Port_RGB, rgb_color);
    snprintf(args.response_buffer, args.response_buffer_size, "Set LED strip to RGB(%d,%d,%d)\n\r", rgb_color.r, rgb_color.g, rgb_color.b);
    return true;
}

bool CMD_RS_SEND_CMD (sCmdApiHandlerArgs_t args) {
    char *token = NULL;
    token = strtok(args.cmd_args, CMD_HANDLER_ARGUMENT_SEPARATOR);
    if (token == NULL) {
        return false;
    }
    uint8_t address = atoi(token);
    token = strtok(NULL, CMD_HANDLER_ARGUMENT_SEPARATOR);
    uint16_t buffer_size = strlen(token) / 2;
    uint8_t *buffer = calloc(buffer_size, sizeof(uint8_t));
    parsehex(token, buffer);
    RS485_API_SendCommand(address, buffer, buffer_size);
    return true;
}

bool CMD_LIGHT_APP_CTRL (sCmdApiHandlerArgs_t args) {
    char *token = NULL;
    token = strtok(args.cmd_args, CMD_HANDLER_ARGUMENT_SEPARATOR);
    if (token == NULL) {
        return false;
    }
    uint8_t setting = atoi(token);
    if (setting > true) {
        return false;
    }
    if (setting == true) {
        if (Light_APP_Resume() == true) {
            snprintf(args.response_buffer, args.response_buffer_size, "Resumed Light task\n\r");
        } else {
            snprintf(args.response_buffer, args.response_buffer_size, "Failed to resume Light task\n\r");
        }
    } else {
        if (Light_APP_Pause() == true) {
            snprintf(args.response_buffer, args.response_buffer_size, "Paused Light task\n\r");
        } else {
            snprintf(args.response_buffer, args.response_buffer_size, "Failed to pause Light task\n\r");
        }
    }
    return true;
}
