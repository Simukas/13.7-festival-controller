/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "motion_app.h"
#include "stdbool.h"
#include "cmsis_os.h"
#include "debug_api.h"
#include "stack_info.h"
#include "motor_api.h"
#include "servo_api.h"
#include "adc_driver.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
DEBUG_MODULE(MOTION_APP)
#define MOTOR_SPEED_MAX 255
#define MOTOR_SPEED_MIN 100
/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
static osThreadId_t g_motion_app_task_handle = NULL;

const static osThreadAttr_t g_motion_app_task_attributes = {
    .name = "motion_app_task",
    .stack_size = MOTION_APP_TASK_STACK_SIZE,
    .priority = (osPriority_t) MOTION_APP_TASK_PRIORITY
};
/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/
static void Motion_APP_Task (void *argument);
/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/
static void Motion_APP_Task (void *argument) {
//    uint8_t speed = MOTOR_SPEED_MIN;
//    bool speed_direction = true;
//    bool motor_direction = true;
//    Motor_API_SetDirection(eMotorApi_Motor_1, motor_direction);
    while (true) {
//        Motor_API_SetSpeed(eMotorApi_Motor_1, speed);
//        if (speed_direction) {
//            speed++;
//        } else {
//            speed--;
//        }
//        if (speed == MOTOR_SPEED_MIN) {
//            speed_direction = true;
//            motor_direction = !motor_direction;
//            Motor_API_SetDirection(eMotorApi_Motor_1, motor_direction);
//        }
//        if (speed == MOTOR_SPEED_MAX) {
//            speed_direction = false;
//        }
//        uint16_t temperature = 0;
//        ADC_Driver_GetInternalTemperature(&temperature);
//        debug("Temperature: %d *C\n", temperature);
//        uint16_t voltage = 0;
//        ADC_Driver_GetSupplyVoltage(&voltage);
//        debug("Input voltage: %d mV\n", voltage);
//        osDelay(100);
        osThreadYield();
    }
}
/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool Motion_APP_Init (void) {
    g_motion_app_task_handle = osThreadNew(Motion_APP_Task, NULL, &g_motion_app_task_attributes);
    if (g_motion_app_task_handle == NULL) {
        debug("Failed to create thread!\n");
        return false;
    }
    //Motor_API_Init();
    //Servo_API_Init();
    //ADC_Driver_Init();
    return true;
}
