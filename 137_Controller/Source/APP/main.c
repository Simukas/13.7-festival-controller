/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "light_app.h"
#include "motion_app.h"
#include "main.h"
#include "cmsis_os.h"
#include "stdbool.h"
#include "debug_api.h"
#include "cli_app.h"
#include "rs485_api.h"
//#include "gpio_driver.h"
//#include "timer_driver.h"
//#include "rtc_driver.h"
#include "adc_driver.h"
#include "address_api.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
DEBUG_MODULE(SYSTEM)
/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/
static void SystemClock_Config (void);
/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/
static void SystemClock_Config (void) {
    LL_FLASH_SetLatency(LL_FLASH_LATENCY_2);
    while (LL_FLASH_GetLatency() != LL_FLASH_LATENCY_2) {
    }
    LL_RCC_HSI_Enable();
    while (LL_RCC_HSI_IsReady() != 1) {
    }
    LL_RCC_PLL_ConfigDomain_SYS(LL_RCC_PLLSOURCE_HSI, LL_RCC_PLLM_DIV_1, 8, LL_RCC_PLLR_DIV_2);
    LL_RCC_PLL_Enable();
    LL_RCC_PLL_EnableDomain_SYS();
    while (LL_RCC_PLL_IsReady() != 1) {
    }
    LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);
    LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_PLL);
    while (LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_PLL) {
    }
    LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_1);
    LL_SetSystemCoreClock(64000000);
    if (HAL_InitTick(TICK_INT_PRIORITY) != HAL_OK) {
        Error_Handler();
    }
}
/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
int main (void) {
    HAL_Init();

    SystemClock_Config();

    //GPIO_Driver_InitPin(eGpioDriver_Pin_I2C_SDA, NULL);

    if (Debug_API_Init() == false) {
        error("Failed to Initialize Debug API\n\r");
        Error_Handler();
    }

    if (Address_API_Init() == false) {
        error("Failed to Initialize Address API\n\r");
        Error_Handler();
    }

    osKernelInitialize();

    if (RS485_API_Init(eUartBaudrate115200) == false) {
        error("Failed to Initialize RS485 API\n\r");
        Error_Handler();
    }

//    if (Motion_APP_Init() == false) {
//        error("Failed to Initialize Motion APP\n\r");
//        Error_Handler();
//    }

//    if (Light_APP_Init() == false) {
//        error("Failed to Initialize Light APP\n\r");
//        Error_Handler();
//    }

    if (CLI_APP_Init() == false) {
        error("Failed to Initialize CLI APP\n\r");
        Error_Handler();
    }

    debug("System started\n\r");

    osKernelStart();

    while (true) {
    }
}

void Error_Handler (void) {
    __disable_irq();
    while (true) {
    }
}

void HAL_TIM_PeriodElapsedCallback (TIM_HandleTypeDef *htim) {
    if (htim->Instance == TIM7) {
        HAL_IncTick();
    }
}
