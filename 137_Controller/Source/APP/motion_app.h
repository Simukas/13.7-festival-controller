#ifndef __MOTION_APP__H__
#define __MOTION_APP__H__
/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "stdbool.h"
/**********************************************************************************************************************
 * Exported definitions and macros
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported types
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of exported functions
 *********************************************************************************************************************/
bool Motion_APP_Init (void);
#endif /* __MOTION_APP__H__ */
