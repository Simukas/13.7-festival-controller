/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "stdbool.h"
#include "cli_app.h"
#include "uart_api.h"
#include "cmsis_os.h"
#include "stack_info.h"
#include "cmd_api.h"
#include "debug_api.h"
#include "cmd_handlers.h"
#include "string_utils.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
DEBUG_MODULE(CLI_APP)

#define CLI_APP_FULL_CMD_SPACE 30
#define CLI_APP_DESCRIPTION_SPACE 70

#define CLI_APP_FULL_CMD_SPACE_STRING str(30)
#define CLI_APP_DESCRIPTION_SPACE_STRING str(70)
/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
// @formatter:off
const static osThreadAttr_t cli_app_task_attributes = {
    .name = "cli_app_task",
    .stack_size = CLI_APP_TASK_STACK_SIZE,
    .priority = (osPriority_t) CLI_APP_TASK_PRIORITY
};

typedef enum eCliApp_Table_Enum_t {
    eCliApp_Table_First = 0,
    eCliApp_Table_Start = eCliApp_Table_First,
    eCliApp_Table_Middle,
    eCliApp_Table_End,
    eCliApp_Table_Last
} eeCliApp_Table_Enum_t;

typedef enum eCliApp_Cmd_t {
    eCliApp_Cmd_First = 0,
    eCliApp_Cmd_INFO = eCliApp_Cmd_First,
    eCliApp_Cmd_HELP,
    eCliApp_Cmd_ARGB_SET_PIXEL,
    eCliApp_Cmd_ARGB_FILL_STRIP,
    eCliApp_Cmd_RGB_SET_COLOR,
    eCliApp_Cmd_RS_SEND_CMD,
    eCliApp_Cmd_LIGHT_APP_CTRL,
    eCliApp_Cmd_Last,
} eCliApp_Cmd_t;

const static sCmdApiCmd_t g_static_cli_app_cmd_lut[eCliApp_Cmd_Last] = {
    [eCliApp_Cmd_INFO]              = {.name = "info",              .fun_ptr = &CLI_APP_PrintInfo,      .parameters = "",           .description = "Provides information about the controller and it's functions"},
    [eCliApp_Cmd_HELP]              = {.name = "help",              .fun_ptr = &CLI_APP_PrintHelp,      .parameters = "",           .description = "Provides a list of available CLI commands"},
    [eCliApp_Cmd_ARGB_SET_PIXEL]    = {.name = "argb_set_pixel",    .fun_ptr = &CMD_ARGB_SET_PIXEL,     .parameters = ":p,i,r,g,b", .description = "Sets selected pixel (i) of a port (p) to a provided color (r,g,b)"},
    [eCliApp_Cmd_ARGB_FILL_STRIP]   = {.name = "argb_fill_strip",   .fun_ptr = &CMD_ARGB_FILL_STRIP,    .parameters = ":p,r,g,b",   .description = "Sets the whole strip of a port (p) to a provided color (r,g,b)"},
    [eCliApp_Cmd_RGB_SET_COLOR]     = {.name = "rgb_set_color",     .fun_ptr = &CMD_RGB_SET_COLOR,      .parameters = ":r,g,b",     .description = ""},
    [eCliApp_Cmd_RS_SEND_CMD]       = {.name = "rs_send_cmd",       .fun_ptr = &CMD_RS_SEND_CMD,        .parameters = "",           .description = ""},
    [eCliApp_Cmd_LIGHT_APP_CTRL]    = {.name = "light_app_ctrl",    .fun_ptr = &CMD_LIGHT_APP_CTRL,     .parameters = "",           .description = ""},
};
// @formatter:on
const char *g_cli_app_logo =        "\t  _ _____ _____   _     _       _     _     _____         _   _            _\n\r"
                                    "\t / |___ /|___  | | |   (_) __ _| |__ | |_  |  ___|__  ___| |_(_)_   ____ _| |\n\r"
                                    "\t | | |_ \\   / /  | |   | |/ _` | '_ \\| __| | |_ / _ \\/ __| __| \\ \\ / / _` | |\n\r"
                                    "\t | |___) | / /   | |___| | (_| | | | | |_  |  _|  __/\\__ \\ |_| |\\ V / (_| | |\n\r"
                                    "\t |_|____(_)_/    |_____|_|\\__, |_| |_|\\__| |_|  \\___||___/\\__|_| \\_/ \\__,_|_|\n\r"
                                    "\t                          |___/\n\r";

const char *g_cli_app_board =       "\t ┌────────────┬──┬──┬─────────────────────────────┐ \n\r"
                                    "\t┌┴─────┐☼ΘΘΘ☼ │V │ V│        ARGB│V│V│VΘΘΘ│ ┌─────┴┐\n\r"
                                    "\t│      ├      │G │ G│            │Θ│Θ│ RGB  ┤      │\n\r"
                                    "\t│RS485 ├      └──┴──┘            │G│G│      ┤RS485 │\n\r"
                                    "\t│+ VCC ├    SPI│3ΘΘG│       │GG│GG│         ┤+ VCC │\n\r"
                                    "\t│      ├       │5ΘΘG│ DEBUG │ΘΘ│ΘΘ│M2 M1    ┤      │\n\r"
                                    "\t└┬─────┘☼  I2C│5ΘΘG│ΘΘΘΘΘGΘ3│33│33│ΘΘ│ΘΘ│ΘΘ│└─────┬┘\n\r"
                                    "\t └────────────────────────────────────────────────┘ \n\r";

const char *g_cli_app_info =        "\t13.7 Controller is a kinematic lighting controller with ability to control motion and \n\r"
                                    "\tlighting elements, receive input from common sensors, and provide information on supply \n\r"
                                    "\tvoltage and board temperature.\n\r";
/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/
static osThreadId_t cli_app_task_handle = NULL;
static char response_buffer[CLI_APP_RESPONSE_BUFFER_SIZE] = {0};
static char command_buffer[CLI_APP_COMMAND_BUFFER_SIZE] = {0};
/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/
static void CLI_APP_Task (void *argument);
bool CLI_APP_PrintBars (uint8_t amount);
bool CLI_APP_PrintOutline (eeCliApp_Table_Enum_t type);
/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/
static void CLI_APP_Task (void *argument) {
    while (true) {
        if (UART_API_Receive(CLI_UART, command_buffer, osWaitForever) == true) {
            sCmdApiLauncherArgs_t cmd_launcher_args = {
                .cmd_lut = g_static_cli_app_cmd_lut,
                .cmd_lut_size = eCliApp_Cmd_Last,
                .response_buffer_size = CLI_APP_RESPONSE_BUFFER_SIZE,
                .response_buffer = response_buffer,
                .cmd_raw = command_buffer,
            };
            if (CMD_API_Launcher(&cmd_launcher_args) == true) {
                if (response_buffer[0] != 0) {
                    debug(response_buffer);
                }
            } else {
                debug(response_buffer);
            }
        }
        osThreadYield();
    }
}
/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool CLI_APP_Init (void) {
    cli_app_task_handle = osThreadNew(CLI_APP_Task, NULL, &cli_app_task_attributes);
    if (cli_app_task_handle == NULL) {
        debug("Failed to create thread!\n\r");
        return false;
    }
    return true;
}

bool CLI_APP_PrintInfo (sCmdApiHandlerArgs_t args) {
    Debug_API_Lock(osWaitForever);
    print(g_cli_app_logo);
    print("\n\r");
    print(g_cli_app_info);
    print("\n\r");
    print(g_cli_app_board);
    Debug_API_Unlock();
    return true;
}

bool CLI_APP_PrintHelp (sCmdApiHandlerArgs_t args) {
    Debug_API_Lock(osWaitForever);
    CLI_APP_PrintOutline(eCliApp_Table_Start);
    printF("\t│%-" CLI_APP_FULL_CMD_SPACE_STRING "s│%-" CLI_APP_DESCRIPTION_SPACE_STRING "s│\n\r", "COMMAND NAME AND PARAMETERS", "COMMAND DESCRIPTION");
    CLI_APP_PrintOutline(eCliApp_Table_Middle);
    for (eCliApp_Cmd_t cmd = eCliApp_Cmd_First; cmd < eCliApp_Cmd_Last; cmd++) {
        uint8_t full_cmd_length = strlen(g_static_cli_app_cmd_lut[cmd].name) + strlen(g_static_cli_app_cmd_lut[cmd].parameters);
        char *full_cmd = calloc(full_cmd_length + 1, sizeof(char));
        strcat(full_cmd, g_static_cli_app_cmd_lut[cmd].name);
        strcat(full_cmd, g_static_cli_app_cmd_lut[cmd].parameters);
        printF("\t│%-" CLI_APP_FULL_CMD_SPACE_STRING "s│%-" CLI_APP_DESCRIPTION_SPACE_STRING "s│\n\r", full_cmd, g_static_cli_app_cmd_lut[cmd].description);
        free(full_cmd);
        if (cmd < eCliApp_Cmd_Last - 1) {
            CLI_APP_PrintOutline(eCliApp_Table_Middle);
        }
    }
    CLI_APP_PrintOutline(eCliApp_Table_End);
    Debug_API_Unlock();
    return true;
}

bool CLI_APP_PrintOutline (eeCliApp_Table_Enum_t type) {
    if (type >= eCliApp_Table_Last) {
        return false;
    }
    switch (type) {
        case eCliApp_Table_Start: {
            print("\t┌");
            CLI_APP_PrintBars(CLI_APP_FULL_CMD_SPACE);
            print("┬");
            CLI_APP_PrintBars(CLI_APP_DESCRIPTION_SPACE);
            print("┐\n\r");
            break;
        }
        case eCliApp_Table_Middle: {
            print("\t├");
            CLI_APP_PrintBars(CLI_APP_FULL_CMD_SPACE);
            print("┼");
            CLI_APP_PrintBars(CLI_APP_DESCRIPTION_SPACE);
            print("┤\n\r");
            break;
        }
        case eCliApp_Table_End: {
            print("\t└");
            CLI_APP_PrintBars(CLI_APP_FULL_CMD_SPACE);
            print("┴");
            CLI_APP_PrintBars(CLI_APP_DESCRIPTION_SPACE);
            print("┘\n\r");
            break;
        }
        default: {
            break;
        }
    }
    return true;
}

bool CLI_APP_PrintBars (uint8_t amount) {
    if (amount == 0) {
        return false;
    }
    while (amount > 0) {
        print("─");
        amount--;
    }
    return true;
}
