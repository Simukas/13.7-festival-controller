#ifndef __UART_DRIVER__H__
#define __UART_DRIVER__H__

/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "stdbool.h"
#include "stdint.h"
/**********************************************************************************************************************
 * Exported definitions and macros
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported types
 *********************************************************************************************************************/
// @formatter:off
typedef enum eUartDriverEnum_t {
	eUartDriverFirst = 0,
    eUartDriverUsart2 = eUartDriverFirst,
    eUartDriverUsart3,
    eUartDriverLast,
} eUartDriverEnum_t;

typedef enum eUartBaudrateEnum_t {
    eUartBaudrateFirst = 0,
    eUartBaudrate115200 = eUartBaudrateFirst,
    eUartBaudrate230400,
    eUartBaudrate460800,
    eUartBaudrate576000,
    eUartBaudrate921600,
    eUartBaudrate960000,
    eUartBaudrate1000000,
    eUartBaudrate1200000,
    eUartBaudrate1500000,
    eUartBaudrate2000000,
    eUartBaudrate3000000,
    eUartBaudrate6000000,
    eUartBaudrateLast,
} eUartBaudrateEnum_t;

static const uint32_t uart_baudrate_lut[] = {
    [eUartBaudrate115200]   = 115200,
    [eUartBaudrate230400]   = 230400,
    [eUartBaudrate460800]   = 460800,
    [eUartBaudrate576000]   = 576000,
    [eUartBaudrate921600]   = 921600,
    [eUartBaudrate960000]   = 960000,
    [eUartBaudrate1000000]  = 1000000,
    [eUartBaudrate1200000]  = 1200000,
    [eUartBaudrate1500000]  = 1500000,
    [eUartBaudrate2000000]  = 2000000,
    [eUartBaudrate3000000]  = 3000000,
    [eUartBaudrate6000000]  = 6000000,
};
// @formatter:on
/**********************************************************************************************************************
 * Exported variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of exported functions
 *********************************************************************************************************************/
bool UART_Driver_Init (eUartDriverEnum_t uart, eUartBaudrateEnum_t baudrate);
bool UART_Driver_SendByte (eUartDriverEnum_t uart, uint8_t byte);
bool UART_Driver_SendBytes (eUartDriverEnum_t uart, uint8_t *bytes, uint16_t len);
bool UART_Driver_SendString (eUartDriverEnum_t uart, const char *string);
bool UART_Driver_GetByte (eUartDriverEnum_t uart, uint8_t *byte);
//uint16_t UART_Driver_ReadBytes (eUartDriverEnum_t uart, uint8_t *bytes, uint16_t number_of_bytes);

#endif /*__UART_DRIVER__H__*/
