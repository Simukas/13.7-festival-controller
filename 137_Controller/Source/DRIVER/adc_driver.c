/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "adc_driver.h"
#include "stm32g0xx_ll_adc.h"
#include "stdbool.h"
#include "stdint.h"
#include "stm32g0xx_ll_bus.h"
#include "debug_api.h"
#include "dma_driver.h"
#include "gpio_driver.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
DEBUG_MODULE(ADC_DRIVER)
#define VDDA_APPLI                     ((uint32_t) 3300)    /* Value of analog voltage supply Vdda (unit: mV) */
#define RANGE_12BITS                   ((uint32_t) 4095)    /* Max digital value with a full range of 12 bits */
#define VAR_CONVERTED_DATA_INIT_VALUE    (__LL_ADC_DIGITAL_SCALE(LL_ADC_RESOLUTION_12B) + 1)
#define ADC_DELAY_CALIB_ENABLE_CPU_CYCLES  (LL_ADC_DELAY_CALIB_ENABLE_ADC_CYCLES * 32)
#define ADC_VALUE_TO_VOLTAGE_mV_CONSTANT 9200
/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/
typedef struct sAdcDriverChannelStatic_t {
    uint32_t rank;
    uint32_t channel;
    uint32_t sampling_time;
    eGpioDriver_Pin_Enum_t pin;

} sAdcDriverChannelStatic_t;

typedef struct sAdcDriverStatic_t {
    void (*clock_function) (uint32_t);
    uint32_t clock;
    uint32_t adc_clock;
    uint32_t clock_divisor;
    uint32_t resolution;
    uint32_t data_alignment;
    uint32_t low_power_mode;
    ADC_TypeDef *adc;
    uint32_t sequence;
    uint32_t trigger_source;
    uint32_t sequencer_length;
    uint32_t continuous_mode;
    uint32_t dma_transfer;
    uint32_t overrun;
    uint32_t oversampling_scope;
    uint32_t frequency_mode;
    uint32_t sampling_time_ch1;
    uint32_t sampling_time_ch2;
    uint32_t sampling_time;

    // Channel parameters
    const sAdcDriverChannelStatic_t *channel_lut;
    uint8_t channel_count;

} sAdcDriverStatic_t;
/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
const sAdcDriverChannelStatic_t g_static_adc_driver_adc1_channel_lut[] = {
    [eAdcDriver_Adc1_Channel_0] = {.channel = LL_ADC_CHANNEL_0, .rank = LL_ADC_REG_RANK_1, .sampling_time = LL_ADC_SAMPLINGTIME_COMMON_1, .pin = eGpioDriver_Pin_AnalogIn_0},
    [eAdcDriver_Adc1_Channel_1] = {.channel = LL_ADC_CHANNEL_1, .rank = LL_ADC_REG_RANK_1, .sampling_time = LL_ADC_SAMPLINGTIME_COMMON_1, .pin = eGpioDriver_Pin_AnalogIn_1},
    [eAdcDriver_Adc1_Channel_17] = {.channel = LL_ADC_CHANNEL_17, .rank = LL_ADC_REG_RANK_1, .sampling_time = LL_ADC_SAMPLINGTIME_COMMON_2, .pin = eGpioDriver_Pin_AnalogIn_17},
    [eAdcDriver_Adc1_Channel_Temperature] = {.channel = LL_ADC_CHANNEL_TEMPSENSOR, .rank = LL_ADC_REG_RANK_1, .sampling_time = LL_ADC_SAMPLINGTIME_COMMON_1, .pin = eGpioDriver_Pin_Last},
};

const sAdcDriverStatic_t g_static_adc_driver_lut[] = {
    [eAdcDriver_Adc_1] = {
        .clock_function = &LL_APB2_GRP1_EnableClock,
        .clock = LL_APB2_GRP1_PERIPH_ADC,
        .adc_clock = LL_RCC_ADC_CLKSOURCE_SYSCLK,
        .clock_divisor = LL_ADC_CLOCK_SYNC_PCLK_DIV4,
        .resolution = LL_ADC_RESOLUTION_12B,
        .data_alignment = LL_ADC_DATA_ALIGN_RIGHT,
        .low_power_mode = LL_ADC_LP_MODE_NONE,
        .adc = ADC1,
        .sequence = LL_ADC_REG_SEQ_CONFIGURABLE,
        .trigger_source = LL_ADC_REG_TRIG_SOFTWARE,
        .sequencer_length = LL_ADC_REG_SEQ_SCAN_DISABLE,
        .continuous_mode = LL_ADC_REG_CONV_SINGLE,
        .dma_transfer = LL_ADC_REG_DMA_TRANSFER_NONE,
        .overrun = LL_ADC_REG_OVR_DATA_OVERWRITTEN,
        .oversampling_scope = LL_ADC_OVS_DISABLE,
        .frequency_mode = LL_ADC_CLOCK_FREQ_MODE_HIGH,
        .sampling_time_ch1 = LL_ADC_SAMPLINGTIME_COMMON_1,
        .sampling_time_ch2 = LL_ADC_SAMPLINGTIME_COMMON_2,
        .sampling_time = LL_ADC_SAMPLINGTIME_160CYCLES_5,

        // Channel parameters
        .channel_lut = g_static_adc_driver_adc1_channel_lut,
        .channel_count = eAdcDriver_Adc1_Channel_Last,
    },

};
/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/
//bool ADC_Driver_Activate (void);
void ADC1_IRQHandler (void);
/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
void ADC1_IRQHandler (void) {
    if (LL_ADC_IsActiveFlag_EOC(ADC1) == 1) {
        LL_ADC_ClearFlag_EOC(ADC1);
    }
    if (LL_ADC_IsActiveFlag_EOS(ADC1) == 1) {
        LL_ADC_ClearFlag_EOS(ADC1);
    }
    if (LL_ADC_IsActiveFlag_OVR(ADC1)) {
        LL_ADC_ClearFlag_OVR(ADC1);
        LL_ADC_DisableIT_OVR(ADC1);
    }
}

bool ADC_Driver_Init (void) {
    for (eAdcDriver_Adc_Enum_t adc = eAdcDriver_Adc_First; adc < eAdcDriver_Adc_Last; adc++) {
        for (uint8_t channel = 0; channel < g_static_adc_driver_lut[adc].channel_count; channel++) {
            if (g_static_adc_driver_lut[adc].channel_lut[channel].channel != LL_ADC_CHANNEL_TEMPSENSOR) {
                GPIO_Driver_InitPin(g_static_adc_driver_lut[adc].channel_lut[channel].pin, NULL);
            }
            LL_ADC_SetChannelSamplingTime(g_static_adc_driver_lut[adc].adc, g_static_adc_driver_lut[adc].channel_lut[channel].channel, g_static_adc_driver_lut[adc].channel_lut[channel].sampling_time);
        }

        //LL_RCC_SetADCClockSource(g_static_adc_driver_lut[adc].adc_clock);
        g_static_adc_driver_lut[adc].clock_function(g_static_adc_driver_lut[adc].clock);
        LL_ADC_InitTypeDef ADC_InitStruct = {0};
        ADC_InitStruct.Clock = g_static_adc_driver_lut[adc].clock_divisor;
        ADC_InitStruct.Resolution = g_static_adc_driver_lut[adc].resolution;
        ADC_InitStruct.DataAlignment = g_static_adc_driver_lut[adc].data_alignment;
        ADC_InitStruct.LowPowerMode = g_static_adc_driver_lut[adc].low_power_mode;
        if (LL_ADC_Init(g_static_adc_driver_lut[adc].adc, &ADC_InitStruct) != SUCCESS) {
            error("Failed to initialize ADC\n");
            return false;
        }

        LL_ADC_REG_SetSequencerConfigurable(g_static_adc_driver_lut[adc].adc, g_static_adc_driver_lut[adc].sequence);
        while (LL_ADC_IsActiveFlag_CCRDY(g_static_adc_driver_lut[adc].adc) == false);
        LL_ADC_ClearFlag_CCRDY(g_static_adc_driver_lut[adc].adc);

        LL_ADC_SetCommonPathInternalCh(__LL_ADC_COMMON_INSTANCE(g_static_adc_driver_lut[adc].adc), LL_ADC_PATH_INTERNAL_TEMPSENSOR);

        LL_ADC_REG_InitTypeDef ADC_REG_InitStruct = {0};
        ADC_REG_InitStruct.TriggerSource = g_static_adc_driver_lut[adc].trigger_source;
        ADC_REG_InitStruct.SequencerLength = g_static_adc_driver_lut[adc].sequencer_length;
        ADC_REG_InitStruct.ContinuousMode = g_static_adc_driver_lut[adc].continuous_mode;
        ADC_REG_InitStruct.DMATransfer = g_static_adc_driver_lut[adc].dma_transfer;
        ADC_REG_InitStruct.Overrun = g_static_adc_driver_lut[adc].overrun;
        if (LL_ADC_REG_Init(g_static_adc_driver_lut[adc].adc, &ADC_REG_InitStruct) != SUCCESS) {
            error("Failed to initialize regular sequencer\n");
            return false;
        }

        LL_ADC_SetOverSamplingScope(g_static_adc_driver_lut[adc].adc, g_static_adc_driver_lut[adc].oversampling_scope);
        LL_ADC_SetTriggerFrequencyMode(g_static_adc_driver_lut[adc].adc, g_static_adc_driver_lut[adc].frequency_mode);
        LL_ADC_SetSamplingTimeCommonChannels(g_static_adc_driver_lut[adc].adc, g_static_adc_driver_lut[adc].sampling_time_ch1, g_static_adc_driver_lut[adc].sampling_time);
        LL_ADC_SetSamplingTimeCommonChannels(g_static_adc_driver_lut[adc].adc, g_static_adc_driver_lut[adc].sampling_time_ch2, g_static_adc_driver_lut[adc].sampling_time);
        LL_ADC_DisableIT_EOC(g_static_adc_driver_lut[adc].adc);
        LL_ADC_DisableIT_EOS(g_static_adc_driver_lut[adc].adc);
        LL_ADC_DisableIT_OVR(ADC1);

        LL_ADC_EnableInternalRegulator(g_static_adc_driver_lut[adc].adc);

        uint32_t wait_loop_index;
        wait_loop_index = ((LL_ADC_DELAY_INTERNAL_REGUL_STAB_US * (SystemCoreClock / (100000 * 2))) / 10);
        while (wait_loop_index != 0) {
            wait_loop_index--;
        }

        LL_ADC_StartCalibration(g_static_adc_driver_lut[adc].adc);
        while (LL_ADC_IsCalibrationOnGoing(g_static_adc_driver_lut[adc].adc) == true);

    }
    return true;
}

bool ADC_Driver_ReadChannel (eAdcDriver_Adc_Enum_t adc, eAdcDriver_Channel_Enum_t channel, uint16_t *measured_value) {
    if (adc >= eAdcDriver_Adc_Last) {
        error("Wrong adc\n");
        return false;
    }
    if (channel >= g_static_adc_driver_lut[adc].channel_count) {
        error("Wrong adc channel\n");
        return false;
    }
    if (measured_value == NULL) {
        error("Null pointer\n");
        return false;
    }

    LL_ADC_REG_SetSequencerRanks(g_static_adc_driver_lut[adc].adc, g_static_adc_driver_lut[adc].channel_lut[channel].rank, g_static_adc_driver_lut[adc].channel_lut[channel].channel);
    while (LL_ADC_IsActiveFlag_CCRDY(g_static_adc_driver_lut[adc].adc) == false);
    LL_ADC_ClearFlag_CCRDY(g_static_adc_driver_lut[adc].adc);

    LL_ADC_Enable(g_static_adc_driver_lut[adc].adc);
    while (LL_ADC_IsActiveFlag_ADRDY(g_static_adc_driver_lut[adc].adc) == false);

    if ((LL_ADC_IsEnabled(ADC1) == true) && (LL_ADC_IsDisableOngoing(ADC1) == false) && (LL_ADC_REG_IsConversionOngoing(ADC1) == false)) {
        LL_ADC_REG_StartConversion(g_static_adc_driver_lut[adc].adc);
    }

    while (LL_ADC_IsActiveFlag_EOC(g_static_adc_driver_lut[adc].adc) == false);
    LL_ADC_ClearFlag_EOC(g_static_adc_driver_lut[adc].adc);

    *measured_value = LL_ADC_REG_ReadConversionData12(g_static_adc_driver_lut[adc].adc);

    //LL_ADC_REG_StopConversion(g_static_adc_driver_lut[adc].adc);
    LL_ADC_Disable(g_static_adc_driver_lut[adc].adc);

    return true;
}

bool ADC_Driver_GetInternalTemperature (uint16_t *temperature) {
    if (temperature == NULL) {
        error("Null pointer\n");
        return false;
    }
    uint16_t raw_temperature = 0;
    ADC_Driver_ReadChannel(eAdcDriver_Adc_1, eAdcDriver_Adc1_Channel_Temperature, &raw_temperature);
    *temperature = __LL_ADC_CALC_TEMPERATURE(VDDA_APPLI, raw_temperature, LL_ADC_RESOLUTION_12B);

    return true;
}

bool ADC_Driver_GetSupplyVoltage (uint16_t *voltage_mV) {
    if (voltage_mV == NULL) {
        error("Null pointer\n");
        return false;
    }
    uint16_t raw_voltage = 0;
    ADC_Driver_ReadChannel(eAdcDriver_Adc_1, eAdcDriver_Adc1_Channel_17, &raw_voltage);
    *voltage_mV = raw_voltage * ADC_VALUE_TO_VOLTAGE_mV_CONSTANT;
    return true;
}
