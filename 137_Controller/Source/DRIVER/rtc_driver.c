/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "rtc_driver.h"
#include "stdbool.h"
#include "stm32g0xx_ll_rtc.h"
#include "stm32g0xx_ll_bus.h"
#include "time.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/
typedef struct sRtcDriverStatic_t {
    void (*clock_function) (uint32_t);
    uint32_t clock;
    RTC_TypeDef *rtc;
    uint32_t hour_format;
    uint32_t rtc_format;
    uint32_t async_prescaler;
    uint32_t sync_prescaler;
    uint32_t clock_source;
} sRtcDriverStatic_t;
/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
sRtcDriverStatic_t g_static_rtc_driver_lut = {
    .clock_source = LL_RCC_RTC_CLKSOURCE_LSI,
    .clock_function = &LL_APB1_GRP1_EnableClock,
    .clock = LL_APB1_GRP1_PERIPH_RTC,
    .hour_format = LL_RTC_HOURFORMAT_24HOUR,
    .rtc_format = LL_RTC_FORMAT_BIN,
    .async_prescaler = 127,
    .sync_prescaler = 255,
    .rtc = RTC,
};
/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool RTC_Driver_Init (void) {
    LL_RCC_ForceBackupDomainReset();
    LL_RCC_ReleaseBackupDomainReset();
    LL_RCC_SetRTCClockSource(g_static_rtc_driver_lut.clock_source);
    LL_RCC_EnableRTC();
    g_static_rtc_driver_lut.clock_function(g_static_rtc_driver_lut.clock);
    LL_RTC_InitTypeDef RTC_InitStruct = {0};
    RTC_InitStruct.HourFormat = g_static_rtc_driver_lut.hour_format;
    RTC_InitStruct.AsynchPrescaler = g_static_rtc_driver_lut.async_prescaler;
    RTC_InitStruct.SynchPrescaler = g_static_rtc_driver_lut.sync_prescaler;
    if (LL_RTC_Init(g_static_rtc_driver_lut.rtc, &RTC_InitStruct) != SUCCESS) {
        return false;
    }
    return true;
}

bool RTC_Driver_SetTime (time_t time) {
    struct tm *time_struct = localtime(&time);
    LL_RTC_TimeTypeDef rtc_time_struct = {0};
    rtc_time_struct.TimeFormat = LL_RTC_TIME_FORMAT_AM_OR_24;
    rtc_time_struct.Seconds = time_struct->tm_sec;
    rtc_time_struct.Minutes = time_struct->tm_min;
    rtc_time_struct.Hours = time_struct->tm_hour;
    if (LL_RTC_TIME_Init(g_static_rtc_driver_lut.rtc, g_static_rtc_driver_lut.rtc_format, &rtc_time_struct) != SUCCESS) {
        return false;
    }
    return true;
}

bool RTC_Driver_GetTime (void) {

    return true;
}

bool RTC_Driver_SetDate (void) {

    return true;
}

bool RTC_Driver_GetDate (void) {

    return true;
}
