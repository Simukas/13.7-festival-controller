#ifndef __DMA_DRIVER__H__
#define __DMA_DRIVER__H__
/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "stdint.h"
#include "stdbool.h"
/**********************************************************************************************************************
 * Exported definitions and macros
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported types
 *********************************************************************************************************************/
// @formatter:off
typedef enum eDmaDriver_Channel_Enum_t {
    eDmaDriver_Channel_First = 0,
    eDmaDriver_Channel_1 = eDmaDriver_Channel_First,
    eDmaDriver_Channel_2,
    eDmaDriver_Channel_3,
    eDmaDriver_Channel_4,
    eDmaDriver_Channel_5,
    eDmaDriver_Channel_6,
    eDmaDriver_Channel_7,
    eDmaDriver_Channel_Last,
} eDmaDriver_Channel_Enum_t;

typedef enum eDmaDriver_Flag_Enum_t {
    eDmaDriver_Flag_First = 0,
    eDmaDriver_Flag_GI = eDmaDriver_Flag_First,
    eDmaDriver_Flag_TC,
    eDmaDriver_Flag_HT,
    eDmaDriver_Flag_TE,
    eDmaDriver_Flag_Last,
} eDmaDriver_Flag_Enum_t;
// @formatter:on
/**********************************************************************************************************************
 * Exported variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of exported functions
 *********************************************************************************************************************/
bool DMA_Driver_Init (eDmaDriver_Channel_Enum_t channel, void *source_address, void *destination_address, void (*interrupt_handler) (bool));
bool DMA_Driver_EnableChannel (eDmaDriver_Channel_Enum_t channel);
bool DMA_Driver_DisableChannel (eDmaDriver_Channel_Enum_t channel);
bool DMA_Driver_ClearFlag (eDmaDriver_Channel_Enum_t channel, eDmaDriver_Flag_Enum_t flag);
bool DMA_Driver_EnableInterrupt (eDmaDriver_Channel_Enum_t channel, eDmaDriver_Flag_Enum_t flag);
bool DMA_Driver_DisableInterrupt (eDmaDriver_Channel_Enum_t channel, eDmaDriver_Flag_Enum_t flag);
bool DMA_Driver_IsInterruptEnabled (eDmaDriver_Channel_Enum_t channel, eDmaDriver_Flag_Enum_t flag, bool *return_value);
#endif /* __DMA_DRIVER__H__ */
