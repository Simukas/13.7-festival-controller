#ifndef __ADC_DRIVER__H__
#define __ADC_DRIVER__H__
/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "stdbool.h"
#include "stdint.h"
/**********************************************************************************************************************
 * Exported definitions and macros
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported types
 *********************************************************************************************************************/
// @formatter:off
typedef enum eAdcDriver_Adc_Enum_t {
    eAdcDriver_Adc_First = 0,
    eAdcDriver_Adc_1 = eAdcDriver_Adc_First,
    eAdcDriver_Adc_Last,
} eAdcDriver_Adc_Enum_t;

typedef enum eAdcDriver_Channel_Enum_t {
    eAdcDriver_Channel_First = 0,
    eAdcDriver_Channel_0 = eAdcDriver_Channel_First,
    eAdcDriver_Channel_1,
    eAdcDriver_Channel_2,
    eAdcDriver_Channel_3,
    eAdcDriver_Channel_4,
    eAdcDriver_Channel_5,
    eAdcDriver_Channel_6,
    eAdcDriver_Channel_7,
    eAdcDriver_Channel_8,
    eAdcDriver_Channel_9,
    eAdcDriver_Channel_10,
    eAdcDriver_Channel_11,
    eAdcDriver_Channel_12,
    eAdcDriver_Channel_13,
    eAdcDriver_Channel_14,
    eAdcDriver_Channel_15,
    eAdcDriver_Channel_16,
    eAdcDriver_Channel_17,
    eAdcDriver_Channel_Temperature,
    eAdcDriver_Channel_Last,
} eAdcDriver_Channel_Enum_t;

typedef enum eAdcDriver_Adc1_Channel_Enum_t {
    eAdcDriver_Adc1_Channel_First = 0,
    eAdcDriver_Adc1_Channel_0 = eAdcDriver_Adc1_Channel_First,
    eAdcDriver_Adc1_Channel_1,
    eAdcDriver_Adc1_Channel_17,
    eAdcDriver_Adc1_Channel_Temperature,
    eAdcDriver_Adc1_Channel_Last,
} eAdcDriver_Adc1_Channel_Enum_t;
// @formatter:on
/**********************************************************************************************************************
 * Exported variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of exported functions
 *********************************************************************************************************************/
bool ADC_Driver_Init (void);
bool ADC_Driver_ReadChannel (eAdcDriver_Adc_Enum_t adc, eAdcDriver_Channel_Enum_t channel, uint16_t *measured_value);
bool ADC_Driver_GetInternalTemperature(uint16_t *temperature);
bool ADC_Driver_GetSupplyVoltage(uint16_t *voltage_mV);
#endif /* __ADC_DRIVER__H__ */
