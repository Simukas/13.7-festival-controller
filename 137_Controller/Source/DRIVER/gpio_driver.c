/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "gpio_driver.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
//@formatterOff
static const sGpioDriverStatic_t static_gpio_driver_lut[eGpioDriver_Pin_Last] = {
    [eGpioDriver_Pin_USART2_TX] = {.port = GPIOA, .pin = LL_GPIO_PIN_2, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_IOP_GRP1_PERIPH_GPIOA, .alternate = LL_GPIO_AF_1, },
    [eGpioDriver_Pin_USART2_RX] = {.port = GPIOA, .pin = LL_GPIO_PIN_3, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_IOP_GRP1_PERIPH_GPIOA, .alternate = LL_GPIO_AF_1, },

    [eGpioDriver_Pin_USART3_TX] = {.port = GPIOB, .pin = LL_GPIO_PIN_2, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_IOP_GRP1_PERIPH_GPIOB, .alternate = LL_GPIO_AF_4, },
    [eGpioDriver_Pin_USART3_DE] = {.port = GPIOB, .pin = LL_GPIO_PIN_1, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_IOP_GRP1_PERIPH_GPIOB, .alternate = LL_GPIO_AF_4, },
    [eGpioDriver_Pin_USART3_RX] = {.port = GPIOB, .pin = LL_GPIO_PIN_0, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_IOP_GRP1_PERIPH_GPIOB, .alternate = LL_GPIO_AF_4, },

    [eGpioDriver_Pin_AddressBit_0] = {.port = GPIOC, .pin = LL_GPIO_PIN_13, .mode = LL_GPIO_MODE_INPUT, .speed = LL_GPIO_SPEED_FREQ_LOW, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_UP, .clock = LL_IOP_GRP1_PERIPH_GPIOC, .alternate = LL_GPIO_AF_0, },
    [eGpioDriver_Pin_AddressBit_1] = {.port = GPIOC, .pin = LL_GPIO_PIN_12, .mode = LL_GPIO_MODE_INPUT, .speed = LL_GPIO_SPEED_FREQ_LOW, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_UP, .clock = LL_IOP_GRP1_PERIPH_GPIOC, .alternate = LL_GPIO_AF_0, },
    [eGpioDriver_Pin_AddressBit_2] = {.port = GPIOC, .pin = LL_GPIO_PIN_11, .mode = LL_GPIO_MODE_INPUT, .speed = LL_GPIO_SPEED_FREQ_LOW, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_UP, .clock = LL_IOP_GRP1_PERIPH_GPIOC, .alternate = LL_GPIO_AF_0, },
    [eGpioDriver_Pin_AddressBit_3] = {.port = GPIOC, .pin = LL_GPIO_PIN_10, .mode = LL_GPIO_MODE_INPUT, .speed = LL_GPIO_SPEED_FREQ_LOW, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_UP, .clock = LL_IOP_GRP1_PERIPH_GPIOC, .alternate = LL_GPIO_AF_0, },
    [eGpioDriver_Pin_AddressBit_4] = {.port = GPIOB, .pin = LL_GPIO_PIN_9,  .mode = LL_GPIO_MODE_INPUT, .speed = LL_GPIO_SPEED_FREQ_LOW, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_UP, .clock = LL_IOP_GRP1_PERIPH_GPIOB, .alternate = LL_GPIO_AF_0, },
    [eGpioDriver_Pin_AddressBit_5] = {.port = GPIOB, .pin = LL_GPIO_PIN_8,  .mode = LL_GPIO_MODE_INPUT, .speed = LL_GPIO_SPEED_FREQ_LOW, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_UP, .clock = LL_IOP_GRP1_PERIPH_GPIOB, .alternate = LL_GPIO_AF_0, },
    [eGpioDriver_Pin_AddressBit_6] = {.port = GPIOB, .pin = LL_GPIO_PIN_7,  .mode = LL_GPIO_MODE_INPUT, .speed = LL_GPIO_SPEED_FREQ_LOW, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_UP, .clock = LL_IOP_GRP1_PERIPH_GPIOB, .alternate = LL_GPIO_AF_0, },
    [eGpioDriver_Pin_AddressBit_7] = {.port = GPIOB, .pin = LL_GPIO_PIN_6,  .mode = LL_GPIO_MODE_INPUT, .speed = LL_GPIO_SPEED_FREQ_LOW, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_UP, .clock = LL_IOP_GRP1_PERIPH_GPIOB, .alternate = LL_GPIO_AF_0, },

    [eGpioDriver_Pin_Tim1_Ch1] = {.port = GPIOA, .pin = LL_GPIO_PIN_8,  .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_LOW, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_DOWN, .clock = LL_IOP_GRP1_PERIPH_GPIOA, .alternate = LL_GPIO_AF_2, },
    [eGpioDriver_Pin_Tim1_Ch2] = {.port = GPIOA, .pin = LL_GPIO_PIN_9,  .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_LOW, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_DOWN, .clock = LL_IOP_GRP1_PERIPH_GPIOA, .alternate = LL_GPIO_AF_2, },
    [eGpioDriver_Pin_Tim1_Ch3] = {.port = GPIOA, .pin = LL_GPIO_PIN_10, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_LOW, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_DOWN, .clock = LL_IOP_GRP1_PERIPH_GPIOA, .alternate = LL_GPIO_AF_2, },
    [eGpioDriver_Pin_Tim1_Ch4] = {.port = GPIOA, .pin = LL_GPIO_PIN_11, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_LOW, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_DOWN, .clock = LL_IOP_GRP1_PERIPH_GPIOA, .alternate = LL_GPIO_AF_2, },

    [eGpioDriver_Pin_Tim3_Ch1] = {.port = GPIOB, .pin = LL_GPIO_PIN_4, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_IOP_GRP1_PERIPH_GPIOB, .alternate = LL_GPIO_AF_1, },
    [eGpioDriver_Pin_Tim3_Ch2] = {.port = GPIOB, .pin = LL_GPIO_PIN_5, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_IOP_GRP1_PERIPH_GPIOB, .alternate = LL_GPIO_AF_1, },
    [eGpioDriver_Pin_Tim3_Ch3] = {.port = GPIOC, .pin = LL_GPIO_PIN_8, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_IOP_GRP1_PERIPH_GPIOC, .alternate = LL_GPIO_AF_1, },
    [eGpioDriver_Pin_Tim3_Ch4] = {.port = GPIOC, .pin = LL_GPIO_PIN_9, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_IOP_GRP1_PERIPH_GPIOC, .alternate = LL_GPIO_AF_1, },

    [eGpioDriver_Pin_Tim15_Ch1] = {.port = GPIOC, .pin = LL_GPIO_PIN_1, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_IOP_GRP1_PERIPH_GPIOC, .alternate = LL_GPIO_AF_2, },
    [eGpioDriver_Pin_Tim15_Ch2] = {.port = GPIOC, .pin = LL_GPIO_PIN_2, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_IOP_GRP1_PERIPH_GPIOC, .alternate = LL_GPIO_AF_2, },

    [eGpioDriver_Pin_Tim16_Ch1] = {.port = GPIOD, .pin = LL_GPIO_PIN_0, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_IOP_GRP1_PERIPH_GPIOD, .alternate = LL_GPIO_AF_2, },

    [eGpioDriver_Pin_Tim17_Ch1] = {.port = GPIOD, .pin = LL_GPIO_PIN_1, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_VERY_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_IOP_GRP1_PERIPH_GPIOD, .alternate = LL_GPIO_AF_2, },

    [eGpioDriver_Pin_AnalogIn_0] = {.port = GPIOA, .pin = LL_GPIO_PIN_0, .mode = LL_GPIO_MODE_ANALOG, .speed = LL_GPIO_SPEED_FREQ_LOW, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_IOP_GRP1_PERIPH_GPIOA, .alternate = LL_GPIO_AF_0, },
    [eGpioDriver_Pin_AnalogIn_1] = {.port = GPIOA, .pin = LL_GPIO_PIN_1, .mode = LL_GPIO_MODE_ANALOG, .speed = LL_GPIO_SPEED_FREQ_LOW, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_IOP_GRP1_PERIPH_GPIOA, .alternate = LL_GPIO_AF_0, },

    [eGpioDriver_Pin_AnalogIn_17] = {.port = GPIOC, .pin = LL_GPIO_PIN_4, .mode = LL_GPIO_MODE_ANALOG, .speed = LL_GPIO_SPEED_FREQ_LOW, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_IOP_GRP1_PERIPH_GPIOC, .alternate = LL_GPIO_AF_0, },

    [eGpioDriver_Pin_I2C_SCL] = {.port = GPIOB, .pin = LL_GPIO_PIN_10, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_LOW, .output = LL_GPIO_OUTPUT_OPENDRAIN, .pull = LL_GPIO_PULL_NO, .clock = LL_IOP_GRP1_PERIPH_GPIOB, .alternate = LL_GPIO_AF_6, },
    [eGpioDriver_Pin_I2C_SDA] = {.port = GPIOB, .pin = LL_GPIO_PIN_11, .mode = LL_GPIO_MODE_OUTPUT, .speed = LL_GPIO_SPEED_FREQ_LOW, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_IOP_GRP1_PERIPH_GPIOB, .alternate = LL_GPIO_AF_0, },

    [eGpioDriver_Pin_SPI_NSS] = {.port = GPIOA, .pin = LL_GPIO_PIN_4, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_IOP_GRP1_PERIPH_GPIOA, .alternate = LL_GPIO_AF_0, },
    [eGpioDriver_Pin_SPI_SCK] = {.port = GPIOA, .pin = LL_GPIO_PIN_5, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_IOP_GRP1_PERIPH_GPIOA, .alternate = LL_GPIO_AF_0, },
    [eGpioDriver_Pin_SPI_MISO] = {.port = GPIOA, .pin = LL_GPIO_PIN_6, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_IOP_GRP1_PERIPH_GPIOA, .alternate = LL_GPIO_AF_0, },
    [eGpioDriver_Pin_SPI_MOSI] = {.port = GPIOA, .pin = LL_GPIO_PIN_7, .mode = LL_GPIO_MODE_ALTERNATE, .speed = LL_GPIO_SPEED_FREQ_HIGH, .output = LL_GPIO_OUTPUT_PUSHPULL, .pull = LL_GPIO_PULL_NO, .clock = LL_IOP_GRP1_PERIPH_GPIOA, .alternate = LL_GPIO_AF_0, },
};
//@formatterOn
/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool GPIO_Driver_InitPin (eGpioDriver_Pin_Enum_t pin, const sGpioDriverStatic_t *pin_parameters) {
    if (pin >= eGpioDriver_Pin_Last) {
        return false;
    }
    sGpioDriverStatic_t pin_init_parameters = {0};
    if (pin_parameters == NULL) {
        pin_init_parameters = static_gpio_driver_lut[pin];
    } else {
        pin_init_parameters = *pin_parameters;
    }
    LL_IOP_GRP1_EnableClock(pin_init_parameters.clock);
    LL_GPIO_ResetOutputPin(pin_init_parameters.port, pin_init_parameters.pin);

    LL_GPIO_InitTypeDef GPIO_InitStruct = {0};
    GPIO_InitStruct.Mode = pin_init_parameters.mode;
    GPIO_InitStruct.Speed = pin_init_parameters.speed;
    GPIO_InitStruct.OutputType = pin_init_parameters.output;
    GPIO_InitStruct.Pull = pin_init_parameters.pull;
    GPIO_InitStruct.Alternate = pin_init_parameters.alternate;
    GPIO_InitStruct.Pin = pin_init_parameters.pin;

    if (LL_GPIO_Init(pin_init_parameters.port, &GPIO_InitStruct) != SUCCESS) {
        return false;
    }
    return true;
}

bool GPIO_Driver_SetPin (eGpioDriver_Pin_Enum_t gpio) {
    if (gpio >= eGpioDriver_Pin_Last) {
        return false;
    }
    LL_GPIO_SetOutputPin(static_gpio_driver_lut[gpio].port, static_gpio_driver_lut[gpio].pin);
    return true;
}

bool GPIO_Driver_ResetPin (eGpioDriver_Pin_Enum_t gpio) {
    if (gpio >= eGpioDriver_Pin_Last) {
        return false;
    }
    LL_GPIO_ResetOutputPin(static_gpio_driver_lut[gpio].port, static_gpio_driver_lut[gpio].pin);
    return true;
}

bool GPIO_Driver_TogglePin (eGpioDriver_Pin_Enum_t gpio) {
    if (gpio >= eGpioDriver_Pin_Last) {
        return false;
    }
    LL_GPIO_TogglePin(static_gpio_driver_lut[gpio].port, static_gpio_driver_lut[gpio].pin);
    return true;
}

bool GPIO_Driver_ReadInputPin (eGpioDriver_Pin_Enum_t gpio, bool *pin_state) {
    if (gpio >= eGpioDriver_Pin_Last || pin_state == NULL) {
        return false;
    }
    if (LL_GPIO_IsInputPinSet(static_gpio_driver_lut[gpio].port, static_gpio_driver_lut[gpio].pin) == 1UL) {
        *pin_state = true;
    } else {
        *pin_state = false;
    }
    return true;
}

bool GPIO_Driver_ReadOutputPin (eGpioDriver_Pin_Enum_t gpio, bool *pin_state) {
    if (gpio >= eGpioDriver_Pin_Last || pin_state == NULL) {
        return false;
    }
    if (LL_GPIO_IsOutputPinSet(static_gpio_driver_lut[gpio].port, static_gpio_driver_lut[gpio].pin) == 1UL) {
        *pin_state = true;
    } else {
        *pin_state = false;
    }
    return true;
}

bool GPIO_Driver_GetPinInfo (eGpioDriver_Pin_Enum_t gpio, sGpioDriverStatic_t *info) {
    if (gpio >= eGpioDriver_Pin_Last || info == NULL) {
        return false;
    }
    *info = static_gpio_driver_lut[gpio];
    return true;
}
