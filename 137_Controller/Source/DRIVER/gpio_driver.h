#ifndef __GPIO_DRIVER__H__
#define __GPIO_DRIVER__H__
/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "stm32g0xx_ll_bus.h"
#include "stm32g0xx_ll_gpio.h"
#include "stm32g0xx_ll_exti.h"
#include "stm32g0xx_ll_system.h"
#include "stdbool.h"
/**********************************************************************************************************************
 * Exported definitions and macros
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Exported types
 *********************************************************************************************************************/
//@formatterOff
typedef struct sGpioDriverStatic_t {
    GPIO_TypeDef *port;
    uint32_t pin;
    uint32_t mode;
    uint32_t speed;
    uint32_t output;
    uint32_t pull;
    uint32_t clock;
    uint32_t alternate;
} sGpioDriverStatic_t;

typedef enum eGpioDriver_Pin_Enum_t {
    eGpioDriver_Pin_First = 0,

    // For debugging and testing
    eGpioDriver_Pin_USART2_TX = eGpioDriver_Pin_First,
    eGpioDriver_Pin_USART2_RX,

    // For RS485 communication
    eGpioDriver_Pin_USART3_TX,
    eGpioDriver_Pin_USART3_DE,
    eGpioDriver_Pin_USART3_RX,

    // For setting address of the controller
    eGpioDriver_Pin_AddressBit_0,
    eGpioDriver_Pin_AddressBit_1,
    eGpioDriver_Pin_AddressBit_2,
    eGpioDriver_Pin_AddressBit_3,
    eGpioDriver_Pin_AddressBit_4,
    eGpioDriver_Pin_AddressBit_5,
    eGpioDriver_Pin_AddressBit_6,
    eGpioDriver_Pin_AddressBit_7,

    // For controlling motors with H-bridges
    eGpioDriver_Pin_Tim1_Ch1,
    eGpioDriver_Pin_Tim1_Ch2,
    eGpioDriver_Pin_Tim1_Ch3,
    eGpioDriver_Pin_Tim1_Ch4,

    // For controlling the RGB led strip with N-Channel mosfets
    eGpioDriver_Pin_Tim3_Ch1, /* R */
    eGpioDriver_Pin_Tim3_Ch2, // G
    eGpioDriver_Pin_Tim3_Ch3, //Not connected
    eGpioDriver_Pin_Tim3_Ch4, // B

    // For controlling addressable LED's
    eGpioDriver_Pin_Tim15_Ch1,
    eGpioDriver_Pin_Tim15_Ch2,

    // For optional PWM generation
    eGpioDriver_Pin_Tim16_Ch1,
    eGpioDriver_Pin_Tim17_Ch1,

    // For analog input
    eGpioDriver_Pin_AnalogIn_0,
    eGpioDriver_Pin_AnalogIn_1,

    // For input voltage measurement
    eGpioDriver_Pin_AnalogIn_17,

    // For connecting I2C devices
    eGpioDriver_Pin_I2C_SCL,
    eGpioDriver_Pin_I2C_SDA,

    // For connecting SPI devices
    eGpioDriver_Pin_SPI_NSS,
    eGpioDriver_Pin_SPI_SCK,
    eGpioDriver_Pin_SPI_MISO,
    eGpioDriver_Pin_SPI_MOSI,

    eGpioDriver_Pin_Last,
} eGpioDriver_Pin_Enum_t;
//@formatterOn
/**********************************************************************************************************************
 * Exported variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of exported functions
 *********************************************************************************************************************/
bool GPIO_Driver_InitPin (eGpioDriver_Pin_Enum_t pin, const sGpioDriverStatic_t *pin_parameters);
bool GPIO_Driver_SetPin (eGpioDriver_Pin_Enum_t gpio);
bool GPIO_Driver_ResetPin (eGpioDriver_Pin_Enum_t gpio);
bool GPIO_Driver_TogglePin (eGpioDriver_Pin_Enum_t gpio);
bool GPIO_Driver_ReadInputPin (eGpioDriver_Pin_Enum_t gpio, bool *pin_state);
bool GPIO_Driver_ReadOutputPin (eGpioDriver_Pin_Enum_t gpio, bool *pin_state);
bool GPIO_Driver_GetPinInfo (eGpioDriver_Pin_Enum_t gpio, sGpioDriverStatic_t *info);
#endif /* __GPIO_DRIVER__H__ */
