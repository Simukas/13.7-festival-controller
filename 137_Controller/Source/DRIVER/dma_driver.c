/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "dma_driver.h"
#include "stm32g0xx_ll_dma.h"
#include "stm32g0xx_ll_bus.h"
#include "stdint.h"
#include "stdbool.h"
#include "debug_api.h"
#include "gpio_driver.h"
#include "ws2815_api.h"
#include "adc_driver.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
DEBUG_MODULE(DMA_DRIVER)
/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/
typedef struct sDmaDriverStatic_t {
    void (*clock_function) (uint32_t);
    uint32_t clock;
    DMA_TypeDef *dma;
    uint32_t channel;
    uint32_t direction;
    uint32_t mode;
    uint32_t source_incrementation_mode;
    uint32_t destination_incrementation_mode;
    uint32_t source_data_size;
    uint32_t destination_data_size;
    uint32_t data_units_to_transfer;
    uint32_t peripheral_request;
    uint32_t priority;
    uint32_t channel_interrupt;
    uint32_t channel_interrupt_priority;
    bool enable_channel_interrupt;
    bool enable_tc_interrupt;
    bool enable_ht_interrupt;
    bool enable_te_interrupt;
} sDmaDriverStatic_t;

typedef struct sDmaDriverDynamic_t {
    void (*interrupt_handler) (bool);
} sDmaDriverDynamic_t;
/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
static const sDmaDriverStatic_t g_static_dma_driver_lut[eDmaDriver_Channel_Last] = {
    [eDmaDriver_Channel_2] = {
        .clock_function = &LL_AHB1_GRP1_EnableClock,
        .clock = LL_AHB1_GRP1_PERIPH_DMA1,
        .dma = DMA1,
        .channel = LL_DMA_CHANNEL_2,
        .peripheral_request = LL_DMAMUX_REQ_TIM15_CH1,
        .direction = LL_DMA_DIRECTION_MEMORY_TO_PERIPH,
        .priority = LL_DMA_PRIORITY_VERYHIGH,
        .mode = LL_DMA_MODE_CIRCULAR,
        .destination_incrementation_mode = LL_DMA_PERIPH_NOINCREMENT,
        .source_incrementation_mode = LL_DMA_MEMORY_INCREMENT,
        .destination_data_size = LL_DMA_PDATAALIGN_HALFWORD,
        .source_data_size = LL_DMA_MDATAALIGN_BYTE,
        .data_units_to_transfer = WS2815_API_PULSE_DATA_BUFFER_SIZE,
        .channel_interrupt = DMA1_Channel2_3_IRQn,
        .channel_interrupt_priority = 0,
        .enable_channel_interrupt = true,
        .enable_ht_interrupt = false,
        .enable_tc_interrupt = false,
        .enable_te_interrupt = false,
    },
//    [eDmaDriver_Channel_1] = {
//        .clock_function = &LL_AHB1_GRP1_EnableClock,
//        .clock = LL_AHB1_GRP1_PERIPH_DMA1,
//        .dma = DMA1,
//        .channel = LL_DMA_CHANNEL_1,
//        .peripheral_request = LL_DMAMUX_REQ_ADC1,
//        .direction = LL_DMA_DIRECTION_PERIPH_TO_MEMORY,
//        .priority = LL_DMA_PRIORITY_HIGH,
//        .mode = LL_DMA_MODE_CIRCULAR,
//        .destination_incrementation_mode = LL_DMA_PERIPH_NOINCREMENT,
//        .source_incrementation_mode = LL_DMA_MEMORY_INCREMENT,
//        .destination_data_size = LL_DMA_PDATAALIGN_HALFWORD,
//        .source_data_size = LL_DMA_MDATAALIGN_HALFWORD,
//        .data_units_to_transfer = ADC_DRIVER_DATA_BUFFER_SIZE,
//        .channel_interrupt = DMA1_Channel1_IRQn,
//        .channel_interrupt_priority = 1,
//        .enable_channel_interrupt = true,
//        .enable_ht_interrupt = true,
//        .enable_tc_interrupt = true,
//        .enable_te_interrupt = true,
//    }
};
/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/
sDmaDriverDynamic_t g_dynamic_dma_driver_lut[] = {
    [eDmaDriver_Channel_2] = {.interrupt_handler = NULL},
    [eDmaDriver_Channel_3] = {.interrupt_handler = NULL},
};
/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/
uint32_t interrupt = 0;
/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/
void DMA1_Channel1_IRQHandler (void);
static bool DMA_Driver_ClearFlagGI (eDmaDriver_Channel_Enum_t channel);
static bool DMA_Driver_ClearFlagTC (eDmaDriver_Channel_Enum_t channel);
static bool DMA_Driver_ClearFlagHT (eDmaDriver_Channel_Enum_t channel);
static bool DMA_Driver_ClearFlagTE (eDmaDriver_Channel_Enum_t channel);
static bool DMA_Driver_IsActiveFlagGI (eDmaDriver_Channel_Enum_t channel, bool *return_value);
static bool DMA_Driver_IsActiveFlagTC (eDmaDriver_Channel_Enum_t channel, bool *return_value);
static bool DMA_Driver_IsActiveFlagHT (eDmaDriver_Channel_Enum_t channel, bool *return_value);
static bool DMA_Driver_IsActiveFlagTE (eDmaDriver_Channel_Enum_t channel, bool *return_value);
/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/
static bool DMA_Driver_ClearFlagGI (eDmaDriver_Channel_Enum_t channel) {
    if (channel >= eDmaDriver_Channel_Last) {
        error("Wrong channel\n");
        return false;
    }
    switch (channel) {
        case eDmaDriver_Channel_1:
            LL_DMA_ClearFlag_GI1(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_2:
            LL_DMA_ClearFlag_GI2(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_3:
            LL_DMA_ClearFlag_GI3(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_4:
            LL_DMA_ClearFlag_GI4(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_5:
            LL_DMA_ClearFlag_GI5(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_6:
            LL_DMA_ClearFlag_GI6(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_7:
            LL_DMA_ClearFlag_GI7(g_static_dma_driver_lut[channel].dma);
            break;
        default:
            error("Wrong value\n");
            return false;
    }
    return true;
}

static bool DMA_Driver_ClearFlagTC (eDmaDriver_Channel_Enum_t channel) {
    if (channel >= eDmaDriver_Channel_Last) {
        error("Wrong channel\n");
        return false;
    }
    switch (channel) {
        case eDmaDriver_Channel_1:
            LL_DMA_ClearFlag_TC1(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_2:
            LL_DMA_ClearFlag_TC2(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_3:
            LL_DMA_ClearFlag_TC3(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_4:
            LL_DMA_ClearFlag_TC4(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_5:
            LL_DMA_ClearFlag_TC5(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_6:
            LL_DMA_ClearFlag_TC6(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_7:
            LL_DMA_ClearFlag_TC7(g_static_dma_driver_lut[channel].dma);
            break;
        default:
            error("Wrong value\n");
            return false;
    }
    return true;
}

static bool DMA_Driver_ClearFlagHT (eDmaDriver_Channel_Enum_t channel) {
    if (channel >= eDmaDriver_Channel_Last) {
        error("Wrong channel\n");
        return false;
    }
    switch (channel) {
        case eDmaDriver_Channel_1:
            LL_DMA_ClearFlag_HT1(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_2:
            LL_DMA_ClearFlag_HT2(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_3:
            LL_DMA_ClearFlag_HT3(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_4:
            LL_DMA_ClearFlag_HT4(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_5:
            LL_DMA_ClearFlag_HT5(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_6:
            LL_DMA_ClearFlag_HT6(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_7:
            LL_DMA_ClearFlag_HT7(g_static_dma_driver_lut[channel].dma);
            break;
        default:
            error("Wrong value\n");
            return false;
    }
    return true;
}

static bool DMA_Driver_ClearFlagTE (eDmaDriver_Channel_Enum_t channel) {
    if (channel >= eDmaDriver_Channel_Last) {
        error("Wrong channel\n");
        return false;
    }
    switch (channel) {
        case eDmaDriver_Channel_1:
            LL_DMA_ClearFlag_TE1(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_2:
            LL_DMA_ClearFlag_TE2(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_3:
            LL_DMA_ClearFlag_TE3(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_4:
            LL_DMA_ClearFlag_TE4(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_5:
            LL_DMA_ClearFlag_TE5(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_6:
            LL_DMA_ClearFlag_TE6(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_7:
            LL_DMA_ClearFlag_TE7(g_static_dma_driver_lut[channel].dma);
            break;
        default:
            error("Wrong value\n");
            return false;
    }
    return true;
}

static bool DMA_Driver_IsActiveFlagGI (eDmaDriver_Channel_Enum_t channel, bool *return_value) {
    if (channel >= eDmaDriver_Channel_Last) {
        error("Wrong channel\n");
        return false;
    }
    if (return_value == NULL) {
        error("Null pointer\n");
        return false;
    }
    switch (channel) {
        case eDmaDriver_Channel_1:
            *return_value = LL_DMA_IsActiveFlag_GI1(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_2:
            *return_value = LL_DMA_IsActiveFlag_GI2(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_3:
            *return_value = LL_DMA_IsActiveFlag_GI3(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_4:
            *return_value = LL_DMA_IsActiveFlag_GI4(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_5:
            *return_value = LL_DMA_IsActiveFlag_GI5(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_6:
            *return_value = LL_DMA_IsActiveFlag_GI6(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_7:
            *return_value = LL_DMA_IsActiveFlag_GI7(g_static_dma_driver_lut[channel].dma);
            break;
        default:
            error("Wrong value\n");
            return false;
    }
    return true;
}

static bool DMA_Driver_IsActiveFlagTC (eDmaDriver_Channel_Enum_t channel, bool *return_value) {
    if (channel >= eDmaDriver_Channel_Last) {
        error("Wrong channel\n");
        return false;
    }
    if (return_value == NULL) {
        error("Null pointer\n");
        return false;
    }
    switch (channel) {
        case eDmaDriver_Channel_1:
            *return_value = LL_DMA_IsActiveFlag_TC1(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_2:
            *return_value = LL_DMA_IsActiveFlag_TC2(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_3:
            *return_value = LL_DMA_IsActiveFlag_TC3(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_4:
            *return_value = LL_DMA_IsActiveFlag_TC4(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_5:
            *return_value = LL_DMA_IsActiveFlag_TC5(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_6:
            *return_value = LL_DMA_IsActiveFlag_TC6(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_7:
            *return_value = LL_DMA_IsActiveFlag_TC7(g_static_dma_driver_lut[channel].dma);
            break;
        default:
            error("Wrong value\n");
            return false;
    }
    return true;
}

static bool DMA_Driver_IsActiveFlagHT (eDmaDriver_Channel_Enum_t channel, bool *return_value) {
    if (channel >= eDmaDriver_Channel_Last) {
        error("Wrong channel\n");
        return false;
    }
    if (return_value == NULL) {
        error("Null pointer\n");
        return false;
    }
    switch (channel) {
        case eDmaDriver_Channel_1:
            *return_value = LL_DMA_IsActiveFlag_HT1(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_2:
            *return_value = LL_DMA_IsActiveFlag_HT2(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_3:
            *return_value = LL_DMA_IsActiveFlag_HT3(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_4:
            *return_value = LL_DMA_IsActiveFlag_HT4(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_5:
            *return_value = LL_DMA_IsActiveFlag_HT5(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_6:
            *return_value = LL_DMA_IsActiveFlag_HT6(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_7:
            *return_value = LL_DMA_IsActiveFlag_HT7(g_static_dma_driver_lut[channel].dma);
            break;
        default:
            error("Wrong value\n");
            return false;
    }
    return true;
}

static bool DMA_Driver_IsActiveFlagTE (eDmaDriver_Channel_Enum_t channel, bool *return_value) {
    if (channel >= eDmaDriver_Channel_Last) {
        error("Wrong channel\n");
        return false;
    }
    if (return_value == NULL) {
        error("Null pointer\n");
        return false;
    }
    switch (channel) {
        case eDmaDriver_Channel_1:
            *return_value = LL_DMA_IsActiveFlag_TE1(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_2:
            *return_value = LL_DMA_IsActiveFlag_TE2(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_3:
            *return_value = LL_DMA_IsActiveFlag_TE3(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_4:
            *return_value = LL_DMA_IsActiveFlag_TE4(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_5:
            *return_value = LL_DMA_IsActiveFlag_TE5(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_6:
            *return_value = LL_DMA_IsActiveFlag_TE6(g_static_dma_driver_lut[channel].dma);
            break;
        case eDmaDriver_Channel_7:
            *return_value = LL_DMA_IsActiveFlag_TE7(g_static_dma_driver_lut[channel].dma);
            break;
        default:
            error("Wrong value\n");
            return false;
    }
    return true;
}
/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool DMA_Driver_Init (eDmaDriver_Channel_Enum_t channel, void *source_address, void *destination_address, void (*interrupt_handler) (bool)) {
    g_static_dma_driver_lut[channel].clock_function(g_static_dma_driver_lut[channel].clock);

    if (g_static_dma_driver_lut[channel].enable_channel_interrupt == true) {
        NVIC_SetPriority(g_static_dma_driver_lut[channel].channel_interrupt, g_static_dma_driver_lut[channel].channel_interrupt_priority);
        NVIC_EnableIRQ(g_static_dma_driver_lut[channel].channel_interrupt);
    } else {
        NVIC_DisableIRQ(g_static_dma_driver_lut[channel].channel_interrupt);
    }

    LL_DMA_InitTypeDef DMA_InitStruct = {
        .PeriphOrM2MSrcAddress = (uint32_t) destination_address,
        .MemoryOrM2MDstAddress = (uint32_t) source_address,
        .Direction = g_static_dma_driver_lut[channel].direction,
        .Mode = g_static_dma_driver_lut[channel].mode,
        .PeriphOrM2MSrcIncMode = g_static_dma_driver_lut[channel].source_incrementation_mode,
        .MemoryOrM2MDstIncMode = g_static_dma_driver_lut[channel].destination_incrementation_mode,
        .PeriphOrM2MSrcDataSize = g_static_dma_driver_lut[channel].source_data_size,
        .MemoryOrM2MDstDataSize = g_static_dma_driver_lut[channel].destination_data_size,
        .NbData = g_static_dma_driver_lut[channel].data_units_to_transfer,
        .PeriphRequest = g_static_dma_driver_lut[channel].peripheral_request,
        .Priority = g_static_dma_driver_lut[channel].priority,
    };

    if (LL_DMA_Init(g_static_dma_driver_lut[channel].dma, g_static_dma_driver_lut[channel].channel, &DMA_InitStruct) != SUCCESS) {
        error("Failed to initialize DMA\n");
        return false;
    }

    if (g_static_dma_driver_lut[channel].enable_ht_interrupt == true) {
        DMA_Driver_EnableInterrupt(channel, eDmaDriver_Flag_HT);
    } else {
        DMA_Driver_DisableInterrupt(channel, eDmaDriver_Flag_HT);
    }

    if (g_static_dma_driver_lut[channel].enable_tc_interrupt == true) {
        DMA_Driver_EnableInterrupt(channel, eDmaDriver_Flag_TC);
    } else {
        DMA_Driver_DisableInterrupt(channel, eDmaDriver_Flag_TC);
    }

    if (g_static_dma_driver_lut[channel].enable_te_interrupt == true) {
        DMA_Driver_EnableInterrupt(channel, eDmaDriver_Flag_TE);
    } else {
        DMA_Driver_DisableInterrupt(channel, eDmaDriver_Flag_TE);
    }

    DMA_Driver_ClearFlag(channel, eDmaDriver_Flag_TE);
    DMA_Driver_ClearFlag(channel, eDmaDriver_Flag_TC);
    DMA_Driver_ClearFlag(channel, eDmaDriver_Flag_HT);

    g_dynamic_dma_driver_lut[channel].interrupt_handler = interrupt_handler;

    return true;
}

bool DMA_Driver_EnableChannel (eDmaDriver_Channel_Enum_t channel) {
    if (channel >= eDmaDriver_Channel_Last) {
        error("Wrong channel\n");
        return false;
    }
    LL_DMA_EnableChannel(g_static_dma_driver_lut[channel].dma, g_static_dma_driver_lut[channel].channel);
    return true;
}

bool DMA_Driver_DisableChannel (eDmaDriver_Channel_Enum_t channel) {
    if (channel >= eDmaDriver_Channel_Last) {
        error("Wrong channel\n");
        return false;
    }
    LL_DMA_DisableChannel(g_static_dma_driver_lut[channel].dma, g_static_dma_driver_lut[channel].channel);
    return true;
}

bool DMA_Driver_ClearFlag (eDmaDriver_Channel_Enum_t channel, eDmaDriver_Flag_Enum_t flag) {
    if (channel >= eDmaDriver_Channel_Last) {
        error("Wrong channel\n");
        return false;
    }
    if (flag >= eDmaDriver_Flag_Last) {
        error("Wrong flag\n");
        return false;
    }
    switch (flag) {
        case eDmaDriver_Flag_GI:
            DMA_Driver_ClearFlagGI(channel);
            break;
        case eDmaDriver_Flag_TC:
            DMA_Driver_ClearFlagTC(channel);
            break;
        case eDmaDriver_Flag_HT:
            DMA_Driver_ClearFlagHT(channel);
            break;
        case eDmaDriver_Flag_TE:
            DMA_Driver_ClearFlagTE(channel);
            break;
        default:
            error("Wrong value\n");
            return false;
    }
    return true;
}

bool DMA_Driver_IsActiveFlag (eDmaDriver_Channel_Enum_t channel, eDmaDriver_Flag_Enum_t flag, bool *return_value) {
    if (channel >= eDmaDriver_Channel_Last) {
        error("Wrong channel\n");
        return false;
    }
    if (flag >= eDmaDriver_Flag_Last) {
        error("Wrong flag\n");
        return false;
    }
    if (return_value == NULL) {
        error("Null pointer\n");
        return false;
    }
    switch (flag) {
        case eDmaDriver_Flag_GI:
            DMA_Driver_IsActiveFlagGI(channel, return_value);
            break;
        case eDmaDriver_Flag_TC:
            DMA_Driver_IsActiveFlagTC(channel, return_value);
            break;
        case eDmaDriver_Flag_HT:
            DMA_Driver_IsActiveFlagHT(channel, return_value);
            break;
        case eDmaDriver_Flag_TE:
            DMA_Driver_IsActiveFlagTE(channel, return_value);
            break;
        default:
            error("Wrong value\n");
            return false;
    }
    return true;
}

bool DMA_Driver_EnableInterrupt (eDmaDriver_Channel_Enum_t channel, eDmaDriver_Flag_Enum_t flag) {
    if (channel >= eDmaDriver_Channel_Last) {
        error("Wrong channel\n");
        return false;
    }
    if (flag >= eDmaDriver_Flag_Last) {
        error("Wrong flag\n");
        return false;
    }
    switch (flag) {
        case eDmaDriver_Flag_TC:
            LL_DMA_EnableIT_TC(g_static_dma_driver_lut[channel].dma, g_static_dma_driver_lut[channel].channel);
            break;
        case eDmaDriver_Flag_HT:
            LL_DMA_EnableIT_HT(g_static_dma_driver_lut[channel].dma, g_static_dma_driver_lut[channel].channel);
            break;
        case eDmaDriver_Flag_TE:
            LL_DMA_EnableIT_TE(g_static_dma_driver_lut[channel].dma, g_static_dma_driver_lut[channel].channel);
            break;
        default:
            error("Wrong value\n");
            return false;
    }
    return true;
}

bool DMA_Driver_DisableInterrupt (eDmaDriver_Channel_Enum_t channel, eDmaDriver_Flag_Enum_t flag) {
    if (channel >= eDmaDriver_Channel_Last) {
        error("Wrong channel\n");
        return false;
    }
    if (flag >= eDmaDriver_Flag_Last) {
        error("Wrong flag\n");
        return false;
    }
    switch (flag) {
        case eDmaDriver_Flag_TC:
            LL_DMA_DisableIT_TC(g_static_dma_driver_lut[channel].dma, g_static_dma_driver_lut[channel].channel);
            break;
        case eDmaDriver_Flag_HT:
            LL_DMA_DisableIT_HT(g_static_dma_driver_lut[channel].dma, g_static_dma_driver_lut[channel].channel);
            break;
        case eDmaDriver_Flag_TE:
            LL_DMA_DisableIT_TE(g_static_dma_driver_lut[channel].dma, g_static_dma_driver_lut[channel].channel);
            break;
        default:
            error("Wrong value\n");
            return false;
    }
    return true;
}

bool DMA_Driver_IsInterruptEnabled (eDmaDriver_Channel_Enum_t channel, eDmaDriver_Flag_Enum_t flag, bool *return_value) {
    if (channel >= eDmaDriver_Channel_Last) {
        error("Wrong channel\n");
        return false;
    }
    if (flag >= eDmaDriver_Flag_Last) {
        error("Wrong flag\n");
        return false;
    }
    if (return_value == NULL) {
        error("Null pointer\n");
        return false;
    }
    switch (flag) {
        case eDmaDriver_Flag_TC:
            *return_value = LL_DMA_IsEnabledIT_TC(g_static_dma_driver_lut[channel].dma, g_static_dma_driver_lut[channel].channel);
            break;
        case eDmaDriver_Flag_HT:
            *return_value = LL_DMA_IsEnabledIT_HT(g_static_dma_driver_lut[channel].dma, g_static_dma_driver_lut[channel].channel);
            break;
        case eDmaDriver_Flag_TE:
            *return_value = LL_DMA_IsEnabledIT_TE(g_static_dma_driver_lut[channel].dma, g_static_dma_driver_lut[channel].channel);
            break;
        default:
            error("Wrong value\n");
            return false;
    }
    return true;
}

void DMA1_Channel1_IRQHandler (void) {
    if (LL_DMA_IsActiveFlag_TC1(DMA1) == 1) {
        /* Clear flag DMA transfer complete */
        LL_DMA_ClearFlag_TC1(DMA1);
        /* Call interruption treatment function */
    }
    /* Check whether DMA half transfer caused the DMA interruption */
    if (LL_DMA_IsActiveFlag_HT1(DMA1) == 1) {
        /* Clear flag DMA half transfer */
        LL_DMA_ClearFlag_HT1(DMA1);
        /* Call interruption treatment function */
    }
    if (LL_DMA_IsActiveFlag_TE1(DMA1) == 1) {
        LL_DMA_ClearFlag_TE1(DMA1);
    }
}

void DMA1_Channel2_3_IRQHandler (void) {
    LL_GPIO_SetOutputPin(GPIOB, LL_GPIO_PIN_11);
    if (LL_DMA_IsActiveFlag_HT2(DMA1)) {
        LL_DMA_ClearFlag_HT2(DMA1);
        WS2815_API_InterruptHandler(false);
    } else if (LL_DMA_IsActiveFlag_TC2(DMA1)) {
        LL_DMA_ClearFlag_TC2(DMA1);
        WS2815_API_InterruptHandler(true);
    }
    LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_11);
}
