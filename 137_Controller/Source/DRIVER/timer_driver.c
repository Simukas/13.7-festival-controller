/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "stm32g0xx_ll_tim.h"
#include "stm32g0xx_ll_bus.h"
#include "timer_driver.h"
#include "stdbool.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
DEBUG_MODULE(TIMER_DRIVER)
/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/
// @formatter:off
typedef enum eTimerDriver_Timer1_OC_Enum_t {
    eTimerDriver_Timer1_OC_First = 0,
    eTimerDriver_Timer1_OC_1 = eTimerDriver_Timer1_OC_First,
    eTimerDriver_Timer1_OC_2,
    eTimerDriver_Timer1_OC_3,
    eTimerDriver_Timer1_OC_4,
    eTimerDriver_Timer1_OC_Last,
} eTimerDriver_Timer1_OC_Enum_t;

typedef enum eTimerDriver_Timer3_OC_Enum_t {
    eTimerDriver_Timer3_OC_First = 0,
    eTimerDriver_Timer3_OC_1 = eTimerDriver_Timer3_OC_First,
    eTimerDriver_Timer3_OC_2,
    eTimerDriver_Timer3_OC_3, //not connected
    eTimerDriver_Timer3_OC_4,
    eTimerDriver_Timer3_OC_Last,
} eTimerDriver_Timer3_OC_Enum_t;

typedef enum eTimerDriver_Timer15_OC_Enum_t {
    eTimerDriver_Timer15_OC_First = 0,
    eTimerDriver_Timer15_OC_1 = eTimerDriver_Timer15_OC_First,
    eTimerDriver_Timer15_OC_2,
    eTimerDriver_Timer15_OC_Last,
} eTimerDriver_Timer15_OC_Enum_t;

typedef enum eTimerDriver_Timer16_OC_Enum_t {
    eTimerDriver_Timer16_OC_First = 0,
    eTimerDriver_Timer16_OC_1 = eTimerDriver_Timer16_OC_First,
    eTimerDriver_Timer16_OC_Last,
} eTimerDriver_Timer16_OC_Enum_t;

typedef enum eTimerDriverTimer17OCEnum_t {
    eTimerDriver_Timer17_OC_First = 0,
    eTimerDriver_Timer17_OC_1 = eTimerDriver_Timer17_OC_First,
    eTimerDriver_Timer17_OC_Last,
} eTimerDriver_Timer17_OC_Enum_t;
// @formatter:on
/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
// @formatter:off
const static sTimerDriverOCStatic_t g_static_timer_driver_timer1_oc_lut[] = {
    [eTimerDriver_Timer1_OC_1] = {.channel = LL_TIM_CHANNEL_CH1, .enable_preload = true, .oc_mode = LL_TIM_OCMODE_PWM1, .oc_state = LL_TIM_OCSTATE_ENABLE, .ocn_state = LL_TIM_OCSTATE_DISABLE, .compare_value = 0, .oc_polarity = LL_TIM_OCPOLARITY_HIGH, .oc_idle_state = LL_TIM_OCIDLESTATE_LOW, .ocn_idle_state = LL_TIM_OCIDLESTATE_LOW, .enable_fast = false, .enable_dma_request = false, .pin = eGpioDriver_Pin_Tim1_Ch1},
    [eTimerDriver_Timer1_OC_2] = {.channel = LL_TIM_CHANNEL_CH2, .enable_preload = true, .oc_mode = LL_TIM_OCMODE_PWM1, .oc_state = LL_TIM_OCSTATE_ENABLE, .ocn_state = LL_TIM_OCSTATE_DISABLE, .compare_value = 0, .oc_polarity = LL_TIM_OCPOLARITY_HIGH, .oc_idle_state = LL_TIM_OCIDLESTATE_LOW, .ocn_idle_state = LL_TIM_OCIDLESTATE_LOW, .enable_fast = false, .enable_dma_request = false, .pin = eGpioDriver_Pin_Tim1_Ch2},
    [eTimerDriver_Timer1_OC_3] = {.channel = LL_TIM_CHANNEL_CH3, .enable_preload = true, .oc_mode = LL_TIM_OCMODE_PWM1, .oc_state = LL_TIM_OCSTATE_ENABLE, .ocn_state = LL_TIM_OCSTATE_DISABLE, .compare_value = 0, .oc_polarity = LL_TIM_OCPOLARITY_HIGH, .oc_idle_state = LL_TIM_OCIDLESTATE_LOW, .ocn_idle_state = LL_TIM_OCIDLESTATE_LOW, .enable_fast = false, .enable_dma_request = false, .pin = eGpioDriver_Pin_Tim1_Ch3},
    [eTimerDriver_Timer1_OC_4] = {.channel = LL_TIM_CHANNEL_CH4, .enable_preload = true, .oc_mode = LL_TIM_OCMODE_PWM1, .oc_state = LL_TIM_OCSTATE_ENABLE, .ocn_state = LL_TIM_OCSTATE_DISABLE, .compare_value = 0, .oc_polarity = LL_TIM_OCPOLARITY_HIGH, .oc_idle_state = LL_TIM_OCIDLESTATE_LOW, .ocn_idle_state = LL_TIM_OCIDLESTATE_LOW, .enable_fast = false, .enable_dma_request = false, .pin = eGpioDriver_Pin_Tim1_Ch4},
};

const static sTimerDriverOCStatic_t g_static_timer_driver_timer3_oc_lut[] = {
    [eTimerDriver_Timer3_OC_1] = {.channel = LL_TIM_CHANNEL_CH1, .enable_preload = true, .oc_mode = LL_TIM_OCMODE_PWM1, .oc_state = LL_TIM_OCSTATE_ENABLE, .ocn_state = LL_TIM_OCSTATE_DISABLE, .compare_value = 0, .oc_polarity = LL_TIM_OCPOLARITY_HIGH, .oc_idle_state = LL_TIM_OCIDLESTATE_LOW, .ocn_idle_state = LL_TIM_OCIDLESTATE_LOW, .enable_fast = true, .enable_dma_request = false, .pin = eGpioDriver_Pin_Tim3_Ch1},
    [eTimerDriver_Timer3_OC_2] = {.channel = LL_TIM_CHANNEL_CH2, .enable_preload = true, .oc_mode = LL_TIM_OCMODE_PWM1, .oc_state = LL_TIM_OCSTATE_ENABLE, .ocn_state = LL_TIM_OCSTATE_DISABLE, .compare_value = 0, .oc_polarity = LL_TIM_OCPOLARITY_HIGH, .oc_idle_state = LL_TIM_OCIDLESTATE_LOW, .ocn_idle_state = LL_TIM_OCIDLESTATE_LOW, .enable_fast = true, .enable_dma_request = false, .pin = eGpioDriver_Pin_Tim3_Ch2},
    [eTimerDriver_Timer3_OC_3] = {.channel = LL_TIM_CHANNEL_CH3, .enable_preload = true, .oc_mode = LL_TIM_OCMODE_PWM1, .oc_state = LL_TIM_OCSTATE_ENABLE, .ocn_state = LL_TIM_OCSTATE_DISABLE, .compare_value = 0, .oc_polarity = LL_TIM_OCPOLARITY_HIGH, .oc_idle_state = LL_TIM_OCIDLESTATE_LOW, .ocn_idle_state = LL_TIM_OCIDLESTATE_LOW, .enable_fast = true, .enable_dma_request = false, .pin = eGpioDriver_Pin_Tim3_Ch3},
    [eTimerDriver_Timer3_OC_4] = {.channel = LL_TIM_CHANNEL_CH4, .enable_preload = true, .oc_mode = LL_TIM_OCMODE_PWM1, .oc_state = LL_TIM_OCSTATE_ENABLE, .ocn_state = LL_TIM_OCSTATE_DISABLE, .compare_value = 0, .oc_polarity = LL_TIM_OCPOLARITY_HIGH, .oc_idle_state = LL_TIM_OCIDLESTATE_LOW, .ocn_idle_state = LL_TIM_OCIDLESTATE_LOW, .enable_fast = true, .enable_dma_request = false, .pin = eGpioDriver_Pin_Tim3_Ch4},
};

const static sTimerDriverOCStatic_t g_static_timer_driver_timer15_oc_lut[] = {
    [eTimerDriver_Timer15_OC_1] = {.channel = LL_TIM_CHANNEL_CH1, .enable_preload = true, .oc_mode = LL_TIM_OCMODE_PWM1, .oc_state = LL_TIM_OCSTATE_ENABLE, .ocn_state = LL_TIM_OCSTATE_DISABLE, .compare_value = 0, .oc_polarity = LL_TIM_OCPOLARITY_HIGH, .oc_idle_state = LL_TIM_OCIDLESTATE_LOW, .ocn_idle_state = LL_TIM_OCIDLESTATE_LOW, .enable_fast = true, .enable_dma_request = true, .pin = eGpioDriver_Pin_Tim15_Ch1},
    [eTimerDriver_Timer15_OC_2] = {.channel = LL_TIM_CHANNEL_CH2, .enable_preload = true, .oc_mode = LL_TIM_OCMODE_PWM1, .oc_state = LL_TIM_OCSTATE_ENABLE, .ocn_state = LL_TIM_OCSTATE_DISABLE, .compare_value = 0, .oc_polarity = LL_TIM_OCPOLARITY_HIGH, .oc_idle_state = LL_TIM_OCIDLESTATE_LOW, .ocn_idle_state = LL_TIM_OCIDLESTATE_LOW, .enable_fast = true, .enable_dma_request = true, .pin = eGpioDriver_Pin_Tim15_Ch2},
};

const static sTimerDriverOCStatic_t g_static_timer_driver_timer16_oc_lut[] = {
    [eTimerDriver_Timer16_OC_1] = {.channel = LL_TIM_CHANNEL_CH1, .enable_preload = true, .oc_mode = LL_TIM_OCMODE_PWM1, .oc_state = LL_TIM_OCSTATE_ENABLE, .ocn_state = LL_TIM_OCSTATE_DISABLE, .compare_value = 0, .oc_polarity = LL_TIM_OCPOLARITY_HIGH, .oc_idle_state = LL_TIM_OCIDLESTATE_LOW, .ocn_idle_state = LL_TIM_OCIDLESTATE_LOW, .enable_fast = true, .enable_dma_request = false, .pin = eGpioDriver_Pin_Tim16_Ch1},
};

const static sTimerDriverOCStatic_t g_static_timer_driver_timer17_oc_lut[] = {
    [eTimerDriver_Timer17_OC_1] = {.channel = LL_TIM_CHANNEL_CH1, .enable_preload = true, .oc_mode = LL_TIM_OCMODE_PWM1, .oc_state = LL_TIM_OCSTATE_ENABLE, .ocn_state = LL_TIM_OCSTATE_DISABLE, .compare_value = 0, .oc_polarity = LL_TIM_OCPOLARITY_HIGH, .oc_idle_state = LL_TIM_OCIDLESTATE_LOW, .ocn_idle_state = LL_TIM_OCIDLESTATE_LOW, .enable_fast = true, .enable_dma_request = false, .pin = eGpioDriver_Pin_Tim17_Ch1},
};

const static sTimerDriverStatic_t g_static_timer_driver_lut[] = {
    [eTimerDriver_Timer_1] = {
        //Timer parameters
        .clock_function = &LL_APB2_GRP1_EnableClock,
        .clock = LL_APB2_GRP1_PERIPH_TIM1,
        .prescaler = 10,
        .counter_mode = LL_TIM_COUNTERMODE_UP,
        .autoreload = 255,
        .clock_division = LL_TIM_CLOCKDIVISION_DIV1,
        .repetition_counter = 0,
        .timer = TIM1,
        .enable_arr_preload = false,
        .clock_source = LL_TIM_CLOCKSOURCE_INTERNAL,
        .trigger_output_1 = LL_TIM_TRGO_RESET,
        .trigger_output_2 = LL_TIM_TRGO2_RESET,
        .enable_master_slave_mode = false,

        //OC parameters
        .oc_count = eTimerDriver_Timer1_OC_Last,
        .oc_table = g_static_timer_driver_timer1_oc_lut,

        //BDTR parameters
        .ossr_state = LL_TIM_OSSR_DISABLE,
        .ossi_state = LL_TIM_OSSI_DISABLE,
        .lock_level = LL_TIM_LOCKLEVEL_OFF,
        .dead_time = 0,
        .break_state = LL_TIM_BREAK_DISABLE,
        .break_polarity = LL_TIM_BREAK_POLARITY_LOW,
        .break_filter = LL_TIM_BREAK_FILTER_FDIV1,
        .break_af_mode = LL_TIM_BREAK_AFMODE_INPUT,
        .break_2_state = LL_TIM_BREAK2_DISABLE,
        .break_2_polarity = LL_TIM_BREAK_POLARITY_LOW,
        .break_2_filter = LL_TIM_BREAK2_FILTER_FDIV1,
        .break_2_af_mode = LL_TIM_BREAK_AFMODE_INPUT,
        .automatic_output = LL_TIM_AUTOMATICOUTPUT_DISABLE,
    },
    [eTimerDriver_Timer_3] = {
        //Timer parameters
        .clock_function = &LL_APB1_GRP1_EnableClock,
        .clock = LL_APB1_GRP1_PERIPH_TIM3,
        .prescaler = 625,
        .counter_mode = LL_TIM_COUNTERMODE_UP,
        .autoreload = 255,
        .clock_division = LL_TIM_CLOCKDIVISION_DIV1,
        .repetition_counter = 0,
        .timer = TIM3,
        .enable_arr_preload = false,
        .clock_source = LL_TIM_CLOCKSOURCE_INTERNAL,
        .trigger_output_1 = LL_TIM_TRGO_RESET,
        .enable_master_slave_mode = false,

        //OC parameters
        .oc_count = eTimerDriver_Timer3_OC_Last,
        .oc_table = g_static_timer_driver_timer3_oc_lut,
    },
    [eTimerDriver_Timer_6] = {
        //Timer parameters
        .clock_function = &LL_APB1_GRP1_EnableClock,
        .clock = LL_APB1_GRP1_PERIPH_TIM6,
        .prescaler = 640,
        .counter_mode = LL_TIM_COUNTERMODE_UP,
        .autoreload = 65535,
        .timer = TIM6,
        .enable_arr_preload = false,
        .trigger_output_1 = LL_TIM_TRGO_RESET,
        .enable_master_slave_mode = false,

        //OC parameters
        .oc_count = 0,
        .oc_table = NULL,
    },
    [eTimerDriver_Timer_14] = {
        //Timer parameters
        .clock_function = &LL_APB2_GRP1_EnableClock,
        .clock = LL_APB2_GRP1_PERIPH_TIM14,
        .prescaler = 0,
        .counter_mode = LL_TIM_COUNTERMODE_UP,
        .autoreload = 65535,
        .clock_division = LL_TIM_CLOCKDIVISION_DIV1,
        .timer = TIM14,
        .enable_arr_preload = false,
    },
    [eTimerDriver_Timer_15] = {
        //Timer parameters
        .clock_function = &LL_APB2_GRP1_EnableClock,
        .clock = LL_APB2_GRP1_PERIPH_TIM15,
        .prescaler = 0,
        .counter_mode = LL_TIM_COUNTERMODE_UP,
        .autoreload = 69, // 64MHz/909KHz = 70
        .clock_division = LL_TIM_CLOCKDIVISION_DIV1,
        .repetition_counter = 0,
        .timer = TIM15,
        .enable_arr_preload = true,
        .clock_source = LL_TIM_CLOCKSOURCE_INTERNAL,
        .trigger_output_1 = LL_TIM_TRGO_RESET,
        .enable_master_slave_mode = false,

        //OC parameters
        .oc_count = eTimerDriver_Timer15_OC_Last,
        .oc_table = g_static_timer_driver_timer15_oc_lut,

        //BDTR parameters
        .ossr_state = LL_TIM_OSSR_DISABLE,
        .ossi_state = LL_TIM_OSSI_DISABLE,
        .lock_level = LL_TIM_LOCKLEVEL_OFF,
        .dead_time = 0,
        .break_state = LL_TIM_BREAK_DISABLE,
        .break_polarity = LL_TIM_BREAK_POLARITY_HIGH,
        .break_filter = LL_TIM_BREAK_FILTER_FDIV1,
        .automatic_output = LL_TIM_AUTOMATICOUTPUT_DISABLE,
    },
    [eTimerDriver_Timer_16] = {
        //Timer parameters
        .clock_function = &LL_APB2_GRP1_EnableClock,
        .clock = LL_APB2_GRP1_PERIPH_TIM16,
        .prescaler = 0,
        .counter_mode = LL_TIM_COUNTERMODE_UP,
        .autoreload = 65535,
        .clock_division = LL_TIM_CLOCKDIVISION_DIV1,
        .repetition_counter = 0,
        .timer = TIM16,
        .enable_arr_preload = false,
        .clock_source = LL_TIM_CLOCKSOURCE_INTERNAL,
        .trigger_output_1 = LL_TIM_TRGO_RESET,
        .enable_master_slave_mode = false,

        //OC parameters
        .oc_count = eTimerDriver_Timer16_OC_Last,
        .oc_table = g_static_timer_driver_timer16_oc_lut,

        //BDTR parameters
        .ossr_state = LL_TIM_OSSR_DISABLE,
        .ossi_state = LL_TIM_OSSI_DISABLE,
        .lock_level = LL_TIM_LOCKLEVEL_OFF,
        .dead_time = 0,
        .break_state = LL_TIM_BREAK_DISABLE,
        .break_polarity = LL_TIM_BREAK_POLARITY_HIGH,
        .break_filter = LL_TIM_BREAK_FILTER_FDIV1,
        .automatic_output = LL_TIM_AUTOMATICOUTPUT_DISABLE,
    },
    [eTimerDriver_Timer_17] = {
        //Timer parameters
        .clock_function = &LL_APB2_GRP1_EnableClock,
        .clock = LL_APB2_GRP1_PERIPH_TIM17,
        .prescaler = 0,
        .counter_mode = LL_TIM_COUNTERMODE_UP,
        .autoreload = 65535,
        .clock_division = LL_TIM_CLOCKDIVISION_DIV1,
        .repetition_counter = 0,
        .timer = TIM17,
        .enable_arr_preload = false,
        .clock_source = LL_TIM_CLOCKSOURCE_INTERNAL,
        .trigger_output_1 = LL_TIM_TRGO_RESET,
        .enable_master_slave_mode = false,

        //OC parameters
        .oc_count = eTimerDriver_Timer17_OC_Last,
        .oc_table = g_static_timer_driver_timer17_oc_lut,

        //BDTR parameters
        .ossr_state = LL_TIM_OSSR_DISABLE,
        .ossi_state = LL_TIM_OSSI_DISABLE,
        .lock_level = LL_TIM_LOCKLEVEL_OFF,
        .dead_time = 0,
        .break_state = LL_TIM_BREAK_DISABLE,
        .break_polarity = LL_TIM_BREAK_POLARITY_HIGH,
        .break_filter = LL_TIM_BREAK_FILTER_FDIV1,
        .automatic_output = LL_TIM_AUTOMATICOUTPUT_DISABLE,
    },
};

// @formatter:on
/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/
//volatile unsigned long debug_timer_ticks = 0;
/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/
bool Timer_Driver_EnableDmaRequest (eTimerDriver_Timer_Enum_t timer, eTimerDriver_OC_Enum_t channel);
/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/
bool Timer_Driver_EnableDmaRequest (eTimerDriver_Timer_Enum_t timer, eTimerDriver_OC_Enum_t channel) {
    if (timer >= eTimerDriver_Timer_Last) {
        error("Wrong timer\n");
        return false;
    }
    if (channel >= g_static_timer_driver_lut[timer].oc_count) {
        error("Wrong output channel\n");
        return false;
    }
    switch (channel) {
        case eTimerDriver_OC_1:
            LL_TIM_EnableDMAReq_CC1(g_static_timer_driver_lut[timer].timer);
            break;
        case eTimerDriver_OC_2:
            LL_TIM_EnableDMAReq_CC2(g_static_timer_driver_lut[timer].timer);
            break;
        case eTimerDriver_OC_3:
            LL_TIM_EnableDMAReq_CC3(g_static_timer_driver_lut[timer].timer);
            break;
        case eTimerDriver_OC_4:
            LL_TIM_EnableDMAReq_CC4(g_static_timer_driver_lut[timer].timer);
            break;
        default:
            error("Wrong value\n");
            return false;
    }

    return true;
}
/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/

bool Timer_Driver_Init (eTimerDriver_Timer_Enum_t timer) {
    //Timer init
    g_static_timer_driver_lut[timer].clock_function(g_static_timer_driver_lut[timer].clock);
    LL_TIM_SetClockSource(g_static_timer_driver_lut[timer].timer, g_static_timer_driver_lut[timer].clock_source);
    LL_TIM_InitTypeDef TIM_InitStruct = {
        .Prescaler = g_static_timer_driver_lut[timer].prescaler,
        .CounterMode = g_static_timer_driver_lut[timer].counter_mode,
        .Autoreload = g_static_timer_driver_lut[timer].autoreload,
        .ClockDivision = g_static_timer_driver_lut[timer].clock_division,
        .RepetitionCounter = g_static_timer_driver_lut[timer].repetition_counter,
    };
    if (LL_TIM_Init(g_static_timer_driver_lut[timer].timer, &TIM_InitStruct) != SUCCESS) {
        error("Failed to initialize timer\n");
        return false;
    }
    if (g_static_timer_driver_lut[timer].enable_arr_preload) {
        LL_TIM_EnableARRPreload(g_static_timer_driver_lut[timer].timer);
    } else {
        LL_TIM_DisableARRPreload(g_static_timer_driver_lut[timer].timer);
    }
    if (IS_TIM_MASTER_INSTANCE(g_static_timer_driver_lut[timer].timer)) {
        LL_TIM_SetTriggerOutput(g_static_timer_driver_lut[timer].timer, g_static_timer_driver_lut[timer].trigger_output_1);
    }
    if (IS_TIM_TRGO2_INSTANCE(g_static_timer_driver_lut[timer].timer)) {
        LL_TIM_SetTriggerOutput2(g_static_timer_driver_lut[timer].timer, g_static_timer_driver_lut[timer].trigger_output_2);
    }

    if (IS_TIM_SLAVE_INSTANCE(g_static_timer_driver_lut[timer].timer)) {
        if (g_static_timer_driver_lut[timer].enable_master_slave_mode) {
            LL_TIM_EnableMasterSlaveMode(g_static_timer_driver_lut[timer].timer);
        } else {
            LL_TIM_DisableMasterSlaveMode(g_static_timer_driver_lut[timer].timer);
        }
    }

    //OC init
    for (eTimerDriver_OC_Enum_t channel = eTimerDriver_OC_First; channel < g_static_timer_driver_lut[timer].oc_count; channel++) {
        LL_TIM_OC_InitTypeDef TIM_OC_InitStruct = {0};
        TIM_OC_InitStruct.OCMode = g_static_timer_driver_lut[timer].oc_table[channel].oc_mode;
        TIM_OC_InitStruct.OCState = g_static_timer_driver_lut[timer].oc_table[channel].oc_state;
        TIM_OC_InitStruct.OCNState = g_static_timer_driver_lut[timer].oc_table[channel].ocn_state;
        TIM_OC_InitStruct.CompareValue = g_static_timer_driver_lut[timer].oc_table[channel].compare_value;
        TIM_OC_InitStruct.OCPolarity = g_static_timer_driver_lut[timer].oc_table[channel].oc_polarity;
        TIM_OC_InitStruct.OCNPolarity = g_static_timer_driver_lut[timer].oc_table[channel].ocn_polarity;
        TIM_OC_InitStruct.OCIdleState = g_static_timer_driver_lut[timer].oc_table[channel].oc_idle_state;
        TIM_OC_InitStruct.OCNIdleState = g_static_timer_driver_lut[timer].oc_table[channel].ocn_idle_state;
        if (LL_TIM_OC_Init(g_static_timer_driver_lut[timer].timer, g_static_timer_driver_lut[timer].oc_table[channel].channel, &TIM_OC_InitStruct) != SUCCESS) {
            error("Failed to output channel\n");
            return false;
        }
        if (GPIO_Driver_InitPin(g_static_timer_driver_lut[timer].oc_table[channel].pin, NULL) == false) {
            error("Failed to output channel\n");
            return false;
        }
        if (g_static_timer_driver_lut[timer].oc_table[channel].enable_preload) {
            LL_TIM_OC_EnablePreload(g_static_timer_driver_lut[timer].timer, g_static_timer_driver_lut[timer].oc_table[channel].channel);
        } else {
            LL_TIM_OC_DisablePreload(g_static_timer_driver_lut[timer].timer, g_static_timer_driver_lut[timer].oc_table[channel].channel);
        }
        if (g_static_timer_driver_lut[timer].oc_table[channel].enable_fast) {
            LL_TIM_OC_DisableFast(g_static_timer_driver_lut[timer].timer, g_static_timer_driver_lut[timer].oc_table[channel].channel);
        } else {
            LL_TIM_OC_DisableFast(g_static_timer_driver_lut[timer].timer, g_static_timer_driver_lut[timer].oc_table[channel].channel);
        }

        if (g_static_timer_driver_lut[timer].oc_table[channel].enable_dma_request) {
            Timer_Driver_EnableDmaRequest(timer, channel);
        }
    }

    //BDTR init
    if (IS_TIM_BREAK_INSTANCE(g_static_timer_driver_lut[timer].timer)) {
        LL_TIM_BDTR_InitTypeDef TIM_BDTRInitStruct = {0};
        TIM_BDTRInitStruct.OSSRState = g_static_timer_driver_lut[timer].ossr_state;
        TIM_BDTRInitStruct.OSSIState = g_static_timer_driver_lut[timer].ossi_state;
        TIM_BDTRInitStruct.LockLevel = g_static_timer_driver_lut[timer].lock_level;
        TIM_BDTRInitStruct.DeadTime = g_static_timer_driver_lut[timer].dead_time;
        TIM_BDTRInitStruct.BreakState = g_static_timer_driver_lut[timer].break_state;
        TIM_BDTRInitStruct.BreakPolarity = g_static_timer_driver_lut[timer].break_polarity;
        TIM_BDTRInitStruct.BreakFilter = g_static_timer_driver_lut[timer].break_filter;
        TIM_BDTRInitStruct.BreakAFMode = g_static_timer_driver_lut[timer].break_af_mode;
        TIM_BDTRInitStruct.Break2State = g_static_timer_driver_lut[timer].break_2_state;
        TIM_BDTRInitStruct.Break2Polarity = g_static_timer_driver_lut[timer].break_2_polarity;
        TIM_BDTRInitStruct.Break2Filter = g_static_timer_driver_lut[timer].break_2_filter;
        TIM_BDTRInitStruct.Break2AFMode = g_static_timer_driver_lut[timer].break_2_af_mode;
        TIM_BDTRInitStruct.AutomaticOutput = g_static_timer_driver_lut[timer].automatic_output;
        if (LL_TIM_BDTR_Init(g_static_timer_driver_lut[timer].timer, &TIM_BDTRInitStruct) != SUCCESS) {
            error("Failed to initialize BDTR\n");
            return false;
        }
        LL_TIM_EnableAllOutputs(g_static_timer_driver_lut[timer].timer);
    }

    return true;
}

bool Timer_Driver_SetDutyCycle (eTimerDriver_Timer_Enum_t timer, eTimerDriver_OC_Enum_t channel, uint8_t duty) {
    if (timer >= eTimerDriver_Timer_Last) {
        error("Wrong timer\n");
        return false;
    }
    if (channel >= g_static_timer_driver_lut[timer].oc_count) {
        error("Wrong output channel\n");
        return false;
    }
    switch (channel) {
        case eTimerDriver_OC_1:
            LL_TIM_OC_SetCompareCH1(g_static_timer_driver_lut[timer].timer, duty);
            break;
        case eTimerDriver_OC_2:
            LL_TIM_OC_SetCompareCH2(g_static_timer_driver_lut[timer].timer, duty);
            break;
        case eTimerDriver_OC_3:
            LL_TIM_OC_SetCompareCH3(g_static_timer_driver_lut[timer].timer, duty);
            break;
        case eTimerDriver_OC_4:
            LL_TIM_OC_SetCompareCH4(g_static_timer_driver_lut[timer].timer, duty);
            break;
        case eTimerDriver_OC_5:
            LL_TIM_OC_SetCompareCH5(g_static_timer_driver_lut[timer].timer, duty);
            break;
        case eTimerDriver_OC_6:
            LL_TIM_OC_SetCompareCH6(g_static_timer_driver_lut[timer].timer, duty);
            break;
        default:
            error("Wrong value\n");
            return false;
    }

    return true;
}

bool Timer_Driver_Start (eTimerDriver_Timer_Enum_t timer) {
    if (timer >= eTimerDriver_Timer_Last) {
        error("Wrong timer\n");
        return false;
    }
    LL_TIM_EnableCounter(g_static_timer_driver_lut[timer].timer);
    return true;
}

bool Timer_Driver_Stop (eTimerDriver_Timer_Enum_t timer) {
    if (timer >= eTimerDriver_Timer_Last) {
        error("Wrong timer\n");
        return false;
    }
    LL_TIM_DisableCounter(g_static_timer_driver_lut[timer].timer);
    return true;
}

bool Timer_Driver_EnableChannel (eTimerDriver_Timer_Enum_t timer, eTimerDriver_OC_Enum_t channel) {
    if (timer >= eTimerDriver_Timer_Last) {
        error("Wrong timer\n");
        return false;
    }
    if (channel >= g_static_timer_driver_lut[timer].oc_count) {
        error("Wrong output channel\n");
        return false;
    }
    LL_TIM_CC_EnableChannel(g_static_timer_driver_lut[timer].timer, g_static_timer_driver_lut[timer].oc_table[channel].channel);
    return true;
}

bool Timer_Driver_DisableChannel (eTimerDriver_Timer_Enum_t timer, eTimerDriver_OC_Enum_t channel) {
    if (timer >= eTimerDriver_Timer_Last) {
        error("Wrong timer\n");
        return false;
    }
    if (channel >= g_static_timer_driver_lut[timer].oc_count) {
        error("Wrong output channel\n");
        return false;
    }
    LL_TIM_CC_DisableChannel(g_static_timer_driver_lut[timer].timer, g_static_timer_driver_lut[timer].oc_table[channel].channel);
    return true;
}

bool Timer_Driver_GetTimerInfo (eTimerDriver_Timer_Enum_t timer, sTimerDriverStatic_t *info) {
    if (timer >= eTimerDriver_Timer_Last) {
        error("Wrong timer\n");
        return false;
    }
    if (info == NULL) {
        error("Null pointer\n");
        return false;
    }
    *info = g_static_timer_driver_lut[timer];
    return true;
}

bool Timer_Driver_SetCounter (eTimerDriver_Timer_Enum_t timer, uint16_t value) {
    if (timer >= eTimerDriver_Timer_Last) {
        error("Wrong timer\n");
        return false;
    }
    LL_TIM_SetCounter(g_static_timer_driver_lut[timer].timer, value);
    return true;
}


extern TIM_HandleTypeDef htim7;

void TIM7_IRQHandler(void) {
  HAL_TIM_IRQHandler(&htim7);
}

//For debugging stack sizes

volatile unsigned long debug_timer_ticks = 0;

void configureTimerForRunTimeStats (void) {
    debug_timer_ticks = 0;
    LL_TIM_EnableIT_UPDATE(TIM6);
    LL_TIM_EnableCounter(TIM6);
}

unsigned long getRunTimeCounterValue (void) {
    return debug_timer_ticks;
}

void TIM6_IRQHandler () {
    debug_timer_ticks++;
    LL_TIM_ClearFlag_UPDATE(TIM6);
}
