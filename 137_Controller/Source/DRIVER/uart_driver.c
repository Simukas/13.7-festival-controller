/**********************************************************************************************************************
 * Includes
 *********************************************************************************************************************/
#include "uart_driver.h"
#include "debug_api.h"
#include "stdbool.h"
#include "ring_buffer.h"
#include "stm32g0xx_ll_usart.h"
#include "stm32g0xx_ll_bus.h"
#include "stm32g0xx_it.h"
#include "gpio_driver.h"
/**********************************************************************************************************************
 * Private definitions and macros
 *********************************************************************************************************************/
//DEBUG_MODULE(UART_DRIVER)
/**********************************************************************************************************************
 * Private typedef
 *********************************************************************************************************************/
typedef struct sUartDriverStatic_t {
    USART_TypeDef *uart;
    uint32_t clock;
    void (*clock_function) (uint32_t);
    uint32_t baud;
    uint32_t width;
    uint32_t bits;
    uint32_t parity;
    uint32_t direction;
    uint32_t flow;
    uint32_t oversampling;
    uint32_t IRQn;
    uint16_t buffer_size;
    bool de_enabled;
    uint32_t de_polarity;
    bool fifo_enabled;
    uint32_t fifo_threshold;
    uint32_t prescaler;
    eGpioDriver_Pin_Enum_t de_pin;
    eGpioDriver_Pin_Enum_t rx_pin;
    eGpioDriver_Pin_Enum_t tx_pin;
} sUartDriverStatic_t;

typedef struct sUartDriverDynamic_t {
    rb_handle_t rb_handle;
} sUartDriverDynamic_t;
/**********************************************************************************************************************
 * Private constants
 *********************************************************************************************************************/
// @formatter:off
static const sUartDriverStatic_t static_uart_driver_lut[] = {
    [eUartDriverUsart2] = {
        .buffer_size = 64,
        .uart = USART2,
        .clock = LL_APB1_GRP1_PERIPH_USART2,
        .clock_function = &LL_APB1_GRP1_EnableClock,
        .width = LL_USART_DATAWIDTH_8B,
        .bits = LL_USART_STOPBITS_1,
        .parity = LL_USART_PARITY_NONE,
        .direction = LL_USART_DIRECTION_TX_RX,
        .flow = LL_USART_HWCONTROL_NONE,
        .oversampling = LL_USART_OVERSAMPLING_16,
        .IRQn = USART2_IRQn,
        .de_enabled = false,
        .de_polarity = LL_USART_DE_POLARITY_HIGH,
        .rx_pin = eGpioDriver_Pin_USART2_RX,
        .tx_pin = eGpioDriver_Pin_USART2_TX,
        .de_pin = eGpioDriver_Pin_Last,
        .fifo_enabled = false,
        .fifo_threshold = LL_USART_FIFOTHRESHOLD_1_8,
        .prescaler = LL_USART_PRESCALER_DIV1,
    },
	[eUartDriverUsart3] = {
	    .buffer_size = 64,
	    .uart = USART3,
	    .clock = LL_APB1_GRP1_PERIPH_USART3,
	    .clock_function = &LL_APB1_GRP1_EnableClock,
	    .width = LL_USART_DATAWIDTH_8B,
	    .bits = LL_USART_STOPBITS_1,
	    .parity = LL_USART_PARITY_NONE,
	    .direction = LL_USART_DIRECTION_TX_RX,
	    .flow = LL_USART_HWCONTROL_NONE,
	    .oversampling = LL_USART_OVERSAMPLING_16,
	    .IRQn = USART3_4_IRQn,
	    .de_enabled = true,
	    .de_polarity = LL_USART_DE_POLARITY_HIGH,
	    .de_pin = eGpioDriver_Pin_USART3_DE,
	    .rx_pin = eGpioDriver_Pin_USART3_RX,
	    .tx_pin = eGpioDriver_Pin_USART3_TX,
	    .fifo_enabled = false,
	    .fifo_threshold = LL_USART_FIFOTHRESHOLD_1_8,
	    .prescaler = LL_USART_PRESCALER_DIV1,
    },
};
// @formatter:on
/**********************************************************************************************************************
 * Private variables
 *********************************************************************************************************************/
static sUartDriverDynamic_t dynamic_uart_lut[] = {
    [eUartDriverFirst ... eUartDriverLast - 1] = {.rb_handle = NULL}
};
/**********************************************************************************************************************
 * Exported variables and references
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Prototypes of private functions
 *********************************************************************************************************************/
//static void USART2_IRQHandler (void);
//void USART3_4_IRQHandler (void);
/**********************************************************************************************************************
 * Definitions of private functions
 *********************************************************************************************************************/
static void UART_Driver_UARTx_IRQ_Handler (eUartDriverEnum_t uart) {
    if (uart >= eUartDriverLast) {
        return;
    }
    if (LL_USART_IsActiveFlag_RXNE_RXFNE(static_uart_driver_lut[uart].uart)) {
        uint8_t byte_received = LL_USART_ReceiveData8(static_uart_driver_lut[uart].uart);
        RB_Push(dynamic_uart_lut[uart].rb_handle, byte_received);
    }
}
/**********************************************************************************************************************
 * Definitions of exported functions
 *********************************************************************************************************************/
bool UART_Driver_Init (eUartDriverEnum_t uart, eUartBaudrateEnum_t baudrate) {
    if (uart >= eUartDriverLast) {
        return false;
    }
    if (baudrate >= eUartBaudrateLast) {
        return false;
    }
    GPIO_Driver_InitPin(static_uart_driver_lut[uart].rx_pin, NULL);
    GPIO_Driver_InitPin(static_uart_driver_lut[uart].tx_pin, NULL);
    LL_USART_InitTypeDef USART_InitStruct = {0};
    static_uart_driver_lut[uart].clock_function(static_uart_driver_lut[uart].clock);
    USART_InitStruct.BaudRate = uart_baudrate_lut[baudrate];
    USART_InitStruct.DataWidth = static_uart_driver_lut[uart].width;
    USART_InitStruct.StopBits = static_uart_driver_lut[uart].bits;
    USART_InitStruct.Parity = static_uart_driver_lut[uart].parity;
    USART_InitStruct.TransferDirection = static_uart_driver_lut[uart].direction;
    USART_InitStruct.HardwareFlowControl = static_uart_driver_lut[uart].flow;
    USART_InitStruct.OverSampling = static_uart_driver_lut[uart].oversampling;
    USART_InitStruct.PrescalerValue = static_uart_driver_lut[uart].prescaler;
    NVIC_SetPriority(static_uart_driver_lut[uart].IRQn, 3);
    NVIC_EnableIRQ(static_uart_driver_lut[uart].IRQn);
    if (LL_USART_Init(static_uart_driver_lut[uart].uart, &USART_InitStruct) != SUCCESS) {
        return false;
    }
    if (static_uart_driver_lut[uart].de_enabled) {
        GPIO_Driver_InitPin(static_uart_driver_lut[uart].de_pin, NULL);
        LL_USART_EnableDEMode(static_uart_driver_lut[uart].uart);
        LL_USART_SetDESignalPolarity(static_uart_driver_lut[uart].uart, static_uart_driver_lut[uart].de_polarity);
        LL_USART_SetDEAssertionTime(static_uart_driver_lut[uart].uart, 0);
        LL_USART_SetDEDeassertionTime(static_uart_driver_lut[uart].uart, 0);
    }
    if (IS_UART_FIFO_INSTANCE(static_uart_driver_lut[uart].uart)) {
        LL_USART_SetTXFIFOThreshold(static_uart_driver_lut[uart].uart, static_uart_driver_lut[uart].fifo_threshold);
        LL_USART_SetRXFIFOThreshold(static_uart_driver_lut[uart].uart, static_uart_driver_lut[uart].fifo_threshold);
        if (static_uart_driver_lut[uart].fifo_enabled) {
            LL_USART_EnableFIFO(static_uart_driver_lut[uart].uart);
        } else {
            LL_USART_DisableFIFO(static_uart_driver_lut[uart].uart);
        }
    }
    LL_USART_ConfigAsyncMode(static_uart_driver_lut[uart].uart);
    LL_USART_Enable(static_uart_driver_lut[uart].uart);
    while ((!(LL_USART_IsActiveFlag_TEACK(static_uart_driver_lut[uart].uart))) || (!(LL_USART_IsActiveFlag_REACK(static_uart_driver_lut[uart].uart))));
    LL_USART_EnableIT_RXNE_RXFNE(static_uart_driver_lut[uart].uart);
//LL_USART_DisableIT_TXE_TXFNF(static_uart_driver_lut[uart].uart);
//LL_USART_EnableIT_RXNE_RXFNE(static_uart_driver_lut[uart].uart);
//LL_USART_EnableIT_ERROR(static_uart_driver_lut[uart].uart);

    dynamic_uart_lut[uart].rb_handle = RB_Init(static_uart_driver_lut[uart].buffer_size);
    if (dynamic_uart_lut[uart].rb_handle == NULL) {
        return false;
    }
    return true;
}

bool UART_Driver_SendByte (eUartDriverEnum_t uart, uint8_t byte) {
    if (uart >= eUartDriverLast) {
        return false;
    }
    while (!LL_USART_IsActiveFlag_TXE_TXFNF(static_uart_driver_lut[uart].uart));
    LL_USART_TransmitData8(static_uart_driver_lut[uart].uart, byte);
    return true;
}

bool UART_Driver_SendBytes (eUartDriverEnum_t uart, uint8_t *bytes, uint16_t amount) {
    if (bytes == NULL) {
        return false;
    }
    if (uart >= eUartDriverLast) {
        return false;
    }
    if (amount == 0) {
        return false;
    }
    uint16_t index = 0;
//    if (static_uart_driver_lut[uart].de_enabled) {
//        GPIO_Driver_SetPin(static_uart_driver_lut[uart].de_pin);
//    }
    while (index < amount) {
        UART_Driver_SendByte(uart, bytes[index++]);
    }
//    if (static_uart_driver_lut[uart].de_enabled) {
//        GPIO_Driver_ResetPin(static_uart_driver_lut[uart].de_pin);
//    }
    return true;
}

bool UART_Driver_SendString (eUartDriverEnum_t uart, const char *string) {
    if (string == NULL) {
        return false;
    }
    if (uart >= eUartDriverLast) {
        return false;
    }
    uint16_t char_count = strlen(string);
    UART_Driver_SendBytes(uart, (uint8_t*) string, char_count);
    return true;
}

bool UART_Driver_GetByte (eUartDriverEnum_t uart, uint8_t *byte) {
    bool read_status = false;
    NVIC_DisableIRQ(static_uart_driver_lut[uart].IRQn);
    if (RB_Pop(dynamic_uart_lut[uart].rb_handle, byte)) {
        read_status = true;
    }
    NVIC_EnableIRQ(static_uart_driver_lut[uart].IRQn);
    return read_status;
}

void USART2_IRQHandler (void) {
    UART_Driver_UARTx_IRQ_Handler(eUartDriverUsart2);
}

void USART3_4_IRQHandler (void) {
    UART_Driver_UARTx_IRQ_Handler(eUartDriverUsart3);
}
