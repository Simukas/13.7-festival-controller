# 13.7 Festival Controller

## Controller (OLD)
[MCU](https://www.st.com/resource/en/datasheet/stm32g070rb.pdf)

[LL library](https://www.st.com/resource/en/user_manual/um2319-description-of-stm32g0-hal-and-lowlayer-drivers-stmicroelectronics.pdf)

[Documentation](https://docs.google.com/document/d/19Ujp0ypsuKvDu6Nuhj0l7jjQaXaKPI5ENlsuDQ8yoCs/edit?usp=sharing)

## Controller V2
[MCU](https://www.st.com/resource/en/datasheet/DM00086815.pdf)

[LL library](https://www.st.com/resource/en/user_manual/um1725-description-of-stm32f4-hal-and-lowlayer-drivers-stmicroelectronics.pdf)

[Documentation](https://docs.google.com/document/d/19Ujp0ypsuKvDu6Nuhj0l7jjQaXaKPI5ENlsuDQ8yoCs/edit?usp=sharing)
